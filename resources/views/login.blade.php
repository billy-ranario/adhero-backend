@extends('tpl.tpl-home')

@section('title', 'Customer Login | PrecisionBit')

@section( 'stylesheets' )
<link href="{{ asset('/css/bootstrap-social.css') }}">
@endsection

@section('login-customer')

<div class="col-md-12 col-sm-12 col-lg-12 login-container">
	<div class="col-md-6 col-md-offset-3 text-center" id="login" style="margin-top: 100px; margin-bottom: 100px;">
		<div class="panel panel-default">
			<div class="panel-header" style="background: #f5f5f5 url(../assets/img/pattern.png) repeat;">
				<p style="font-size: 30px;border: 1px solid #FFFEFE;color: #A9A6A6;">Login</p>
			</div>
			<div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
					{!! csrf_field() !!}

					{!! Session::get('msg') !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-sign-in"></i> Login</button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        </div>
                    </div>

                    <div class="form-group" hidden>
                    	<div class="col-md-12 text-right">
                    		<a href="{{url('register')}}">Not yet registered?</a>
                    	</div>
                    </div>
                </form>

			</div>
		</div>
	</div>
</div>

@endsection