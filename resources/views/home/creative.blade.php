@extends('tpl.tpl-home')
@section('title', 'Creative Experts | PrecisionBit')
<style type="text/css">
  body
  {
    background: #ffffff url(assets/img/bg-integrity-1.png) center top repeat;
  }
</style>
@section('creative')
  <div class="col-md-12 col-sm-12 col-lg-12 creative-container">
    <p class="creative-text">The Only Platform Where Content Creators Can Make Social Media Ads</p>
    <a href="/login-creative" class="btn btn-large custom-button-creative">REGISTER AS A CONTENT EXPERT</a>
  </div>
  <div class="col-md-12 col-sm-12 col-lg-12 creative-money-container">
    <div>
      <section class="creative-money-container-cash">
        <p class="creative-money-container-cash-text-first">Make ads, make money</p>
        <p class="creative-money-container-cash-text-second">AdHero creates the best-performing campaign for your business by using the most relevant images and messages through our technology. Launch campaigns with AdHero and get better results.</p>
      </section>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-lg-12 creative-showcase-container">
    <p class="creative-showcase-container-text-first">Showcase Your Social Media Copywriting Skills</p>
    <p class="creative-showcase-container-text-second">Build your portfolio of social media campaigns and showcase your skills through actual results benchmarked against copywriters from all over the world.</p>
    <img src="{{ asset('img/fb.png') }}" alt="" class="img-thumbnail fb-image-creative">
  </div>
  <div class="col-md-12 col-sm-12 col-lg-12 creative-no-reg-container">
    <p class="creative-no-reg-container-text-first">No Registration Fee</p>
    <p class="creative-no-reg-container-text-second">Making social media ads through AdHero gives you insightful tips and tricks about creating the perfect ad for a specific audience. We share best practices with our Content Experts to make the creation process quick and easy.</p>
  </div>
  <div class="col-md-12 col-sm-12 col-lg-12 creative-dashboard-container">
    <p class="creative-dashboard-container-text-first">Easy-to-Use Dashboard</p>
    <p class="creative-dashboard-container-text-second">AdHero automates the ad creation process for Content Experts. With the intuitive dashboard, creators can easily pick projects they want to work on.</p>
    <img src="{{ asset('img/dashboard.png') }}" alt="" class="img-thumbnail fb-image-creative">
  </div>
  <div class="col-md-12 col-lg-12 main-section-5" style="background-color: white!important;">
    <div class="col-md-12 col-lg-12 custom-how-it-works-title">
      <p class="main-section-5-text-p">HOW IT WORKS</p>
    </div>
    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-1.jpg') }}" alt="note">
      <p style="padding: 10px;">Register as Creative</p>
    </div>
    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-2.jpg') }}" alt="note">
      <p style="padding: 10px;">Choose a project you fancy</p>
    </div>
    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-3.jpg') }}" alt="note">
      <p style="padding: 10px;">Read the brief of your desired project then conceptualize your approach</p>
    </div>

    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-4.jpg') }}" alt="note">
      <p style="padding: 10px;">Create the ad by writing words and choosing a compelling image</p>
    </div>
    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-5.jpg') }}" alt="note">
      <p style="padding: 10px;">Wait for Evaluation of your ad. AdHero will test your ad during this step</p>
    </div>
    <div class="col-md-4 custom-center-how-it-works">
      <img src="{{ asset('img/step-6.jpg') }}" alt="note">
      <p style="padding: 10px;">Get paid. If your ad performs well, AdHero rewards you with up to $25 per ad</p>
    </div>
  </div>
  <div class="col-md-12 col-lg-12 main-section-7 text-center" style="height: 170px;">
    <p class="main-section-7-text-p" style="margin-top: 30px; margin-bottom: 30px;">Become A Creative Now</p>
    <a href="/login-creative" class="btn-lg bottom-button">REGISTER AS A CONTENT EXPERT</a>
  </div>
  <div class="col-lg-12 main-questions wow fadeInDown text-center" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <section class="question-container">
        <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
    </section>
  </div>
@endsection