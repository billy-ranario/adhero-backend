@extends('tpl.tpl-home')
@section('title', 'Creative Registration | PrecisionBit')
@section( 'stylesheets' )
<link href="{{ asset('/css/bootstrap-social.css') }}">
@endsection
@section('login-creative')
  <div class="col-md-12 col-sm-12 col-lg-12 login-container">
    <div class="col-md-12 col-sm-12 col-lg-12" style="text-align: center;">
       <section>
        <p style="font-weight: 700; color: black; font-size: 25px; padding: 20px;">Get access to our creative contests:</p>
      </section>
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12" id="login">
      <div class="login-box">
        <div class="login-title">
          <section style="50%;"><p style="float: left; color: #676666;">Register as a Creative Expert</p></section>
          <section style="100%;"><p style="float: right; color: #444; line-height: 20px; font-size: 12px;">Already Registered</p></section>
        </div>
        <div class="login-input">
          <section class="login-input-box">
            <label> <span class="icon-container"><i class="glyphicon glyphicon-user"></i></span> Username or E-mail <span>*</span></label>
            <input type="text" >
          </section>
          <section class="login-input-box">
            <label style="margin-right: 73px;"><span class="icon-container"><i class="glyphicon glyphicon-lock"in></i></span> Password <span>*</span></label>
            <input type="text" >
          </section>
          <section style="text-align: center;">
            <label style="margin-right: 55px; display: inline-block; font-weight: normal!important; font-size: 11px; text-transform: uppercase; color: #888 !important;"><input type="checkbox"> <span style="margin-left: 10px;">REMEMBER ME</span></label>
          </section>
        </div>
        <div class="submit-box">
          <button class="btn btn-default" type="submit" style="background-color: #3B3B3B; color: white;">Login</button>
          <a href="{{ url( 'auth/fbloginurl/creative' ) }}" class="btn btn-social btn-facebook">
            <i class="fa fa-facebook"></i>
            Sign in with Facebook as Creative Expert
          </a>
          <img src="http://www.youradhero.com/wp-content/plugins/userpro/skins/elegant/img/loading.gif" alt="" class="userpro-loading">
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 main-questions wow fadeInDown text-center" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <section class="question-container">
        <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
    </section>
  </div>
@endsection