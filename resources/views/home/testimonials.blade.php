@extends('tpl.tpl-home')
@section('title', 'Testimonials | PrecisionBit')

@section('testimonials')
  <div class="col-md-12 testi-container">
    <section class="testi-text-1">
      <p>Customers Say About Us</p>
    </section>
    <section class="testi-text-2">
      <p>Don't take our word for it</p>
    </section>
    <section class="testi-text-3">
      <p>Hopefully, you would speak the same lines too.</p>
    </section>
  </div>
  <div class="col-md-12">
    <img src="{{ asset('img/testimonial-cropped.jpg') }}" class="img-responsive">
  </div>
  <div class="col-lg-12 main-questions wow fadeInDown text-center" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <section class="question-container">
        <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
    </section>
  </div>
@endsection