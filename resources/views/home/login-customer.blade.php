@extends('tpl.tpl-home')
@section('title', 'Customer Login | PrecisionBit')
@section( 'stylesheets' )
<link href="{{ asset('/css/bootstrap-social.css') }}">
@endsection

@section('login-customer')
  <div class="col-md-12 col-sm-12 col-lg-12 login-container">
    <div class="col-md-6 col-md-offset-3 text-center" id="login" style="margin-top: 100px; margin-bottom: 100px;">
      <div class="panel panel-default">
        <div class="panel-header" style="background: #f5f5f5 url(../assets/img/pattern.png) repeat;">
          <p style="font-size: 30px;border: 1px solid #FFFEFE;color: #A9A6A6;">Customer Login</p>
        </div>
        <div class="panel-body">
          <a href="{{ url( 'auth/fbloginurl/customer' ) }}" class="btn btn-social btn-facebook" style="background: #405D9B;color: white;">
            <i class="fa fa-facebook"></i>
            Sign in with Facebook as Customer
          </a>
          <img src="http://www.youradhero.com/wp-content/plugins/userpro/skins/elegant/img/loading.gif" alt="" class="userpro-loading">
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 main-questions wow fadeInDown text-center" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <section class="question-container">
        <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
    </section>
  </div>
@endsection
