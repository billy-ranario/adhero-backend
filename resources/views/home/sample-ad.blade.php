@extends('tpl.tpl-home')
@section('title', 'Ads Gallery | AdHero')
@section('sample-ad')
  <div class="col-md-12 col-sm-12 col-lg-12 main-section-1 sample-ads-bg  ">
    <div class="bg-overlay"></div>
    <h1 class="bold-text-price">The Facebook Ads Gallery</h1>    
  </div>
    <div class="col-md-12 col-sm-12 col-lg-12" style="margin: 0px 0px 0px 0px; padding: 0px 00px 25px 0px; background-color: #ffffff;">
      <section class="main-content" style="margin-top: 50px;">
        <div class="container">
          <div class="heading-text-content" style="overflow:hidden">
            <p class="pull-left">
              Found <strong>2611 Ads Example</strong>
            </p>
            <p class="pull-right">
              Page <strong>1</strong> of 218
            </p>
            <br>
          </div>
          <div class="sample-ads-contents">
            <img src="{{ asset('img/SAMPLE_AD_PAGE_02.jpg') }}" class="img-responsive">
            <div class="col-md-4" hidden>
                <div class="sample-ads-wrapper">                    
                    <div class="ads-title text-center">
                      <h3>Liberty Mutual Insurance</h3>
                    </div>
                    
                    <div class="ads-content">
                      <div class="ads-heading">
                        
                        <div class="advertiser-content">
                          <div class="ads-logo">
                            <img src="{{ asset('img/ads-icons/1.png') }}">
                          </div>
                          <div class="ads-desc">
                            <p class="advertiser-name">
                              <a href="">Liberty Mutual Insurance</a>
                            </p>
                            <p class="advertiser-info">
                              Insurance Company &#8226; 1,955 Likes &#8226; October 10, 2014 
                            </p>
                          </div>
                          <button class="btn-btn-default pull-right"><i class="fa fa-facebook"></i> Profile</button>
                        </div>                       
                      </div>  
                    </div>
                </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12 risk-free-container">
      <p>Try Adhero risk free today</p>
      <button>Risk Free try out</button>
    </div>
    <div class="col-lg-12 main-questions wow fadeInDown text-center" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <section class="question-container">
        <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
    </section>
  </div>
@endsection