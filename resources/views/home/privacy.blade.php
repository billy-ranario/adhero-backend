@extends('tpl.tpl-home')
@section('title', 'Privacy | PrecisionBit')

@section( 'stylesheets' )
<link href="{{ asset('/css/privacy.css') }}">
@endsection
@section('privacy')
  <style type="text/css">
  .custom-navbar-privacy {
    border-radius: 0px;
    background-color: #29A9A1;
    border-color: transparent;
    padding: 20px;
  }
</style>
  <div class="col-lg-12" style="margin-top: 5%;padding: 80px;">
    <section class="policy-header">
      <p>PRIVACY POLICY</p>
    </section>
    <section class="policy-text">
      <p>AdHero (“AdHero” or “We”) is very sensitive to the privacy needs of its website visitors and members (collectively, “You” or “User”) and recognizes the importance of protecting personally identifiable information that You may choose to share with AdHero. This Online Privacy Policy (“Privacy Policy”) provides You with important information regarding AdHero’s collection, storage and use of personally identifiable information.</p>
      <p>This Privacy Policy applies only to personally identifiable information collected on the website(s) operated by AdHero on which this Privacy Policy is posted (collectively, “Website”), and does not apply to any other information collected by AdHero offline or through any other means.</p>
      <p>Capitalized terms not defined in this Privacy Policy are defined in the AdHero Terms of Service.</p>
      <br />
    </section>
    <section class="registration-info">
      <p style="text-decoration: underline;">Registration Information</p>
      <br />
      <p>AdHero never automatically collects any information that specifically identifies Users, such as their name, address, or e-mail address. AdHero collects personally identifiable information from Users only when Users voluntarily provide it as part of the registration process (“Registration Information”) to open an Account. We always ask Users whenever we need Registration Information that identifies Users or allows us to contact Users.</p>
      <p>AdHero may collect and use personally identifiable information from Users for purposes other than to open an Account, e.g., such as subscribing to a newsletter, participating in polls, surveys, and questionnaires, using tell-a-friend features, participating in contests, sweepstakes, or other promotions, or receiving technical support.</p>
    </section>
    <section class="consent-security">
      <p style="text-decoration: underline;">Consent and Security</p>
      <p>It is Your choice whether or not You provide Your Registration Information to AdHero, but Registration Information is required in order for AdHero to provide You with services – namely, access to and/or participation in Jobs, to post Jobs and receive Creative Services, and certain other features of the Website.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Use of Personally Identifiable Information</p>
      <p>Your personally identifiable information may be used internally within AdHero and its subsidiaries, affiliated companies, or other businesses or persons for the purpose of processing such information in conformance with our Privacy Policy. We require that such other businesses and persons agree to comply with our Privacy Policy. For example, AdHero may use Your personally identifiable information internally to provide access to You and process Your contributions to campaigns, to understand who uses the Website, to improve the Website, to provide updates on special events, news, and announcements of interest to our Users, and to contact You for customer service, payment, and billing purposes.</p>
      <p>In addition, AdHero reserves the right to disclose Your personally identifiable information under limited circumstances in the event AdHero believes that the Website is being used to commit unlawful acts; if disclosure of Your personally identifiable information is required to comply with applicable laws or regulations, or with a court or administrative order; or will help to enforce our Terms of Service, to protect Your safety or security) including the safety and security of property that belongs to You) or to protect the safety and security of the Website or third parties.</p>
      <p>From time to time, we may purchase a business or sell one or more of our businesses and Your personally identifiable information may be transferred as a part of the purchase or sale. The personally identifiable information transferred as a part of the purchase or sale would be treated in accordance with this Privacy Policy, if it is practicable and permissible to do so.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Usage Information</p>
      <p>Non-personally identifying information (“Usage Information”) comprises information that does not identify any particular User. Examples include: information about which of AdHero’s pages are visited on an aggregate level, browser type, operating system type, or IP address of a User.</p>
      <p>AdHero may collect other types of non-personally identifying information, e.g., age, date of birth, gender, hobbies, or other demographic information.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Use of Usage Information</p>
      <p>Your Usage Information may be disclosed internally within AdHero and to third parties. For example, AdHero may disclose Usage Information in order to better understand which parts of the Website Users are visiting, to improve the content of the Website, and for marketing, advertising, or research purposes.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Cookies and Log Files</p>
      <p>The Website may place a text file called a “cookie” in the browser files of Your computer. Cookies can be used to provide You with tailored information from the Website. The cookie itself does not contain Registration Information. You can set Your browser to notify You when You receive a cookie, giving You the chance to decide whether or not to accept it. AdHero also records information that Your web browser sends whenever You visit any website, such as Your Internet Protocol address.</p>
      <p>The Website uses a variety of technical methods for tracking purposes, including web beacons. Web beacons are small pieces of data that are embedded in images on the pages of websites. We also use these technical methods to analyze the traffic patterns on the Website, such as the frequency with which our Users visit various parts of the Website. These technical methods may involve the transmission of information either directly to us or to another party authorized by us to collect information on our behalf. We may collect information from use of these technical methods in a form that is personally identifiable.</p>
      <p>You may, of course, decline to submit personally identifiable information through the Website, in which case AdHero may not be able to provide certain services or features of the Website to You.</p>
      <p>To protect Your privacy and security, we take reasonable steps (such as requesting a unique password) to verify Your identity before granting You access to your profile or making corrections. You are responsible for maintaining the secrecy of Your unique password and account information at all times.</p>
      <p>If at any time You decide to remove Your personally identifiable information from our database, You may do so by emailing us at <a href="mailto:support@youradhero.com" style="color: red;">support@youradhero.com</a>.</p>
      <p>Upon such removal, AdHero may not be able to provide certain services or features of the Website to You.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Third-party Advertisers, Links to Other Sites</p>
      <p>AdHero may allow advertising services companies to serve advertisements within the Website. Advertising may be based on usage, text, demographic, or other information gathered by or on behalf of AdHero. AdHero’s Privacy Policy does not apply to, and we cannot control, the activities of advertisers.</p>
      <p>This Privacy Policy applies only to the Website operated by AdHero, and not to any other websites to which we may link.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Our Commitment To Data Security</p>
      <p>AdHero uses commercially reasonable physical, managerial, and technical safeguards to preserve the integrity and security of Your personally identifiable information. We cannot, however, ensure or warrant the security of any information You provide to AdHero and You do so at Your own risk. Once We receive Your transmission of information, AdHero makes commercially reasonable efforts to ensure the security of our systems. However, please note that this is not a guarantee that such information may not be accessed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Our Commitment To Children’s’ Privacy</p>
      <p>Protecting the privacy of young children is especially important. For that reason, AdHero does not knowingly collect or maintain personally identifiable information or non-personally-identifiable information on the Website from persons under 13 years of age, and no part of our website is directed to persons under 13. If You are under 13 years of age, then please do not use or access the Website at any time or in any manner. If AdHero learns that personally identifiable information of persons under 13 years of age has been collected on the Website without verified parental consent, then AdHero will take appropriate steps to delete this information.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Our Commitment To Children’s’ Privacy</p>
      <p>This Privacy Policy is effective as of August 1, 2015 and will remain in effect unless changed. AdHero may modify this Privacy Policy at any time, and such modifications shall be effective immediately upon posting of the modified Privacy Policy on the Website.</p>
    </section>
    <section class="indentifiable-info">
      <p style="text-decoration: underline;">Contacting the Website</p>
      <p>If You have any questions about this Privacy Policy, the practices of this Website, or Your dealings with this Website, please contact: support@youradhero.com</p>
    </section>
  </div>
@endsection