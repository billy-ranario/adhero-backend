<!-- 

<nav class="navbar navbar-default navbar-fixed-top custom-navbar custom-navbar-privacy wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">
    <div class="container-fluid" id="top-content">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed custom-collapse-button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand custom-navbar-brand" href="/">
        </a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="custom-navbar-text"><a href="/pricing" class="navbar-text">PRICING</a></li>
          <li class="custom-navbar-text"><a href="/testimonials" class="navbar-text">TESTIMONIALS</a></li>
          <li class="custom-navbar-text"><a href="/sample-ad" class="navbar-text">SAMPLE ADS</a></li>
          <li class="custom-navbar-text"><a href="/login-customer" class="navbar-text">JOIN AS ADVERTISERS</a></li>
          <li class="custom-navbar-text"><a href="/creative" class="navbar-text">FOR COPY WRITERS</a></li>
        </ul>
      </div>
    </div>
  </nav>
 -->
<script type="text/javascript">
  // $(document).ready(function(){
  //   var shrinkHeader = 50;
  //   var measuredHeader = 400;

  //   $(window).scroll(function() {
  //     var scroll = getCurrentScroll();

  //     if ( scroll >= shrinkHeader ) {
  //       $('.custom-navbar-default').addClass('replace-navbar');
  //       $('.dropdown-toggle').addClass('replace-dropdown-toggle-li-a');

  //       $('.bottom-content').fadeIn(500);
  //       $('.bottom-top-call').addClass('bottom-top-call-animate');
  //     } else {
  //       $('.custom-navbar-default').removeClass('replace-navbar');
  //       $('.dropdown-toggle').removeClass('replace-dropdown-toggle-li-a');

  //       $('.bottom-content').fadeOut(1000);
  //       $('.bottom-top-call').removeClass('bottom-top-call-animate');
  //     }

  //   });

  //   function getCurrentScroll() {
  //     return window.pageYOffset || document.documentElement.scrollTop;
  //   }
  // });
  </script>
 <nav class="navbar custom-navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand custom-navbar-brand" href="/">
        <img src="../img/logo/PBit logo white transparent.png">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right custom-navbar-right">
        <!-- <li class="menus-li dropdown li-solutions">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SOLUTIONS <span class="fa fa-chevron-down"></span></a>
          <ul class="dropdown-menu custom-dropdown-menu solutions-dropdown">
            <li class="large-padding"><a href="#">MANAGE SERVICES</a></li>
            <li role="separator" class="divider custom-divider"></li>
            <li class="large-padding"><a href="#">FACEBOOK</a></li>
            <li role="separator" class="divider custom-divider"></li>
            <li class="large-padding"><a href="#">YOUTUBE</a></li>
            <li role="separator" class="divider custom-divider"></li>
            <li class="large-padding"><a href="#">TWITTER</a></li>
            <li role="separator" class="divider custom-divider"></li>
            <li class="large-padding"><a href="#">INSTAGRAM</a></li>
          </ul>
        </li> -->
        <li class="menus-li li-platform">
          <a href="/home" class="dropdown-toggle">Home</span></a>
        </li>
        <li class="menus-li li-about">
          <a onclick="$('.content_5').animatescroll();" class="dropdown-toggle cursor-pointer">Solution</span></a>
        </li>
        <li class="menus-li li-about">
          <a href="/about" class="dropdown-toggle">About Us</span></a>
        </li>
        <!-- <li class="menus-li li-contact-us">
          <a href="#" class="btn btn-contact-us-home">About Us</a>
        </li>
        <li class="menus-li li-login">
          <a href="/login" class="btn btn-login">Login</a>
        </li> -->
        <!-- <li class="menus-li" class="dropdown-toggle"><a href="#" class="dropdown-toggle">BLOG </a></li> -->

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>