@extends('tpl.tpl-home')
@section('title', 'Pricing | PrecisionBit')

@section('pricing')
  <div class="main-section-1 pricing-bg">
    <p class="text-pricing">Pricing</p>
    <h1 class="bold-text-price">There is one price for <br /> every AdHero </br /> Facebook Ad</h1>
    <p class="bottom-text-price">It does not matter if you are a big company or a small one, <br /> we offer a very affordable price per campaign.</p>
    <div class="col-md-4"></div>
    <div class="col-md-12">
      <div style="width:33.33%;margin: auto;">
        <div class="header-price"></div>
        <div class="content-price">
          <p>$ 250</p>
        </div>
        <div class="footer-price">
          <span>Per Facebook Ad Campaign</span>
        </div>   
      </div>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-12 col-sm-12 col-lg-12 main-section-pricing" style="margin-top: 20px;">
      <span>Have more than 100 ? or prefer to pay annually? <br /> Contact us for special pricing. </span>
      <p class="exclusive-text">Inclusive of:</p>
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12" style="margin: 0px 0px 0px 0px; padding: 0px 00px 25px 0px; background-color: #ffffff;">
      <section class="exclusive-list">
        <p>1. Tailored ad creation for industry, country and language</p>
        <p>2. Split-testing to determine best-performing ad</p>
        <p>3. Personal Customer Support</p>
      </section>
    </div>

    <div class="col-md-12 col-sm-12 col-lg-12 risk-free-container">
      <p>Try Adhero risk free today</p>
      <button>Risk Free try out</button>
    </div>
  </div>
@endsection