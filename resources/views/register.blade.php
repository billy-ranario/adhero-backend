@extends('tpl.tpl-home')

@section('title', 'Registration | PrecisionBit')

@section( 'stylesheets' )
<link href="{{ asset('/css/bootstrap-social.css') }}">
@endsection

@section('login-customer')

<div class="col-md-12 col-sm-12 col-lg-12 login-container">
	<div class="col-md-6 col-md-offset-3 text-center" id="login" style="margin-top: 100px;">
		<div class="panel panel-default">
			<div class="panel-header" style="background: #f5f5f5 url(../assets/img/pattern.png) repeat;">
				<p style="font-size: 30px;border: 1px solid #FFFEFE;color: #A9A6A6;">Register</p>
			</div>
			<div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}">
					{!! csrf_field() !!}

                    @if (count($errors)>0)
                        <div class="alert alert-danger">
                            <ul class="list-unstyled">
                                @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">First Name</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Last Name</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                        </div>
                    </div>

                    <!-- <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Company</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="company" value="{{ old('company') }}">
                        </div>
                    </div> -->

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Re-Type</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-sign-in"></i> Register</button>
                        </div>
                    </div>

                    <div class="form-group">
                    	<div class="col-md-12 text-right">
                    		<a href="{{url('login')}}">Already have an account?</a>
                    	</div>
                    </div>

                </form>

			</div>
		</div>
	</div>
</div>

@endsection