@extends('tpl.tpl-dashboard-admin')

@section('stylesheets')
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<style>
		#wrapper-admin {
			margin-top: 50px;
		}
	</style>
@endsection

@section('admin')

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			{!! Session::get('msg') !!}
			<div class="panel panel-primary" id="wrapper-admin">
				<div class="panel-heading">
					<h3 class="panel-title">Admin Login</h3>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{ url('auth-admin') }}">
						<div class="form-group">
							<input type="email" class="form-control" name="email" id="email" placeholder="Email" autofocus>
						</div>
						<div class="form-group">
							<input type="password" name="password" class="form-control" id="password" placeholder="Password">
						</div>
						<button type="submit" class="btn btn-primary btn-block">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
