<p style="background:#28A6A0;width:100%;height:75px;padding:10px 15px;text-align:center;margin:0">
    <img src="http://52.74.62.3/img/adhero-mainlogo.png" alt="AdHero Logo">
</p>
<section>
    <article>
        <p>
            Hi <strong>{{ session('user.name')[0] }}</strong>,
        </p>
        <p>
           Congratulations! Your ads has been published on Facebook. You will be able to check the performance of your Ads through your company Ads Manager. If you have any questions, click here  <a href="http://52.74.62.3/dashboard#/contact">AdHero</a>
        </p>
        <p>
            Regards,<br>
            Ria Lao
        </p>
    </article>
</section>
<p style="background:#28A6A0;width:100%;min-height:20px;padding:10px 15px;text-align:center;margin-top:250px;">
    <span style="color:#fff">Copyrights &copy; 2015, AdHero</span>
</p>