<p style="background:#28A6A0;width:100%;height:75px;padding:10px 15px;text-align:center;margin:0">
    <img src="http://52.74.62.3/img/adhero-mainlogo.png" alt="AdHero Logo">
</p>
    
<section>
    
    <article>
        <p>
            Hi <strong>{{ session('user.name')[0] }}</strong>,
        </p>
        <p>
            Our team has received your Campaign Brief. Kindly make your deposit in order for us to commence in the creation of your Ads.
        </p>
        <p>
            Regards,<br>
            Ria Lao
        </p>
    </article>
</section>
    
<p style="background:#28A6A0;width:100%;min-height:20px;padding:10px 15px;text-align:center;margin-top:250px;">
    <span style="color:#fff">Copyrights &copy; 2015, AdHero</span>
</p>