<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<link rel="shortcut icon" href="{{ asset('/img/favicon.jpg') }}" />
	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
	<style>
	.page-break {
	    page-break-after: always;
	}
	</style>
 </head>
 <body>
	<div class="page-break"></div>
	<pre>
	</pre>

 </body>
 </html>