<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('/img/favicon.jpg') }}" />
  @yield( 'stylesheets' )
</head>
<body class="hold-transition skin-blue sidebar-mini">

@yield('admin')
@yield('scripts')

</body>
</html>