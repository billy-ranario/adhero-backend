<!DOCTYPE html>
<html ng-app="client">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>@yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('/img/favicon.jpg') }}" />
  @yield( 'stylesheets' )
</head>
<body class="hold-transition skin-blue sidebar-mini" page-load>

@yield('client-dashboard')
@yield('scripts')

</body>
</html>