@extends('tpl.tpl-dashboard-client')
@section('title', 'PrecisionBit | Dashboard')


@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('/css/angular-material.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/fileinput.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/loading-bar.css') }}">
  <link rel="stylesheet" href="{{ asset('/fontcustom/fontcustom.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/flot.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.css') }}">
  <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,400' rel='stylesheet' type='text/css'> -->
  <link rel="stylesheet" href="{{ asset('/css/fontello.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/precision.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/client-side.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.carousel.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.theme.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/bootstrap/bootstrap-switch.min.css' ) }}">

  <script src="{{ asset('/library/bootstrap/jquery.min.js') }}"></script>
  <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('/plugins/chartjs/Chart.js') }}"></script>
@endsection

@section('client-dashboard')
  <div class="loader-wrapper">
      <div class="loader-wrapper-info text-center">
        <h4>PrecisionBit is loading the Page...</h4>
      </div>
  </div>
	<div class="wrapper custom-wapper">
		<header class="main-header">
        <!-- Logo -->
        <a href="/dashboard" style="background-color: #FEFEFE!important;" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">PB</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg log-text">PrecisionBit</span>
        </a>
        <a href="javascript:void(0)" class="sidebar-toggle custom-side-bar-toggle visible-sm visible-xs hidden-md hidden-lg" data-toggle="offcanvas" role="button" style="color: #28A6A0;" ng-click="menuOpen()">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top navigation-top-menu" style="background-color: #FEFEFE;!important;" role="navigation">
          <!-- Sidebar toggle button-->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a ui-sref="upload" class="menu-header-global picture-menu dropdown-toggle" data-toggle="dropdown">
                	<span class="icon-photograph15" style="color: #28A6A0"></span>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a ui-sref="saved-results" class="menu-header-global suit-menu dropdown-toggle" data-toggle="dropdown">
                	<span class="icon-suitcase54" style="color: #28A6A0"></span>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a href="javascript:void(0)" class="menu-header-global settings-menu dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear custom-glyphicon-cog" style="color: #28A6A0"></i>
                </a>
                <ul class="dropdown-menu">
		            <li><a ui-sref="contact-us">Contact us</a></li>
		            <li><a ui-sref="faq">FAQ</a></li>
		            <!-- <li role="separator" class="divider"></li> -->
		          </ul>
              </li>
            </ul>
          </div>
        </nav>
        <section class="company-name-container">
        	<p class="pull-left text-first"></p>
        	<p class="pull-right text-second">Signed in as <span>{{Auth::user()->firstname}}&nbsp;{{Auth::user()->lastname}}</span> | <a href="{{url('logout')}}">logout</a></p>
        	<section class="clear-both"></section>
        </section>
	 </header>
	    <div class="content-wrapper">
      	<section class="content">
      		<div class="section-content row">
            <div ui-view="side-menu" class="ui-view-container"></div>
      			<div ui-view="main" class="ui-view-container"></div>
      		</div>
  	        <section class="network-status" hidden>
  	          <section class="content-header">
  	            <h1>
  	              500 Error Page
  	            </h1>
  	          </section>
  	          <section class="content">
  	            <div class="error-page">
  	              <h2 class="headline text-red">500</h2>
  	              <div class="error-content">
  	                <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
  	                <p>
  	                  Please make sure that you are connected to the internet.
  	                </p>
  	              </div>
  	            </div><!-- /.error-page -->
  	          </section>
  	        </section>
  	      </section>
        </div>

      <!-- pop menu -->
        <md-fab-speed-dial md-open="menu.isOpen" md-direction="up" ng-class="md-fling" class="md-fab-bottom-right md-scale hidden-md hidden-lg visible-xs visible-sm lock-size pop-up-menu" ng-cloak>
          <md-fab-trigger>
            <md-button aria-label="menu" class="md-fab md-warn">
              <md-icon md-font-icon="glyphicon glyphicon-menu-hamburger"></md-icon>
            </md-button>
          </md-fab-trigger>
          <md-fab-actions>
            <md-button aria-label="Home" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-home" aria-label="Home"></md-icon>
            </md-button>
            <md-button aria-label="Predict" class="md-fab md-raised md-mini" ui-sref="saved-results">
              <md-icon md-font-icon="fa fa-flask" aria-label="Predict"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="summary">
              <md-icon md-font-icon="fa fa-bar-chart" aria-label="Report"></md-icon>
            </md-button>
          </md-fab-actions>
        </md-fab-speed-dial>
      <!-- footer -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.2.0
        </div>
        <strong>Copyright &copy; 2015-2016 <a href="https://www.facebook.com/youradhero">PrecisionBit</a>.</strong> All rights reserved.
      </footer>
   </div>
@endsection

@section( 'scripts' )
  <script src="{{ asset( 'js/jquery-ui.min.js' ) }}"></script>
	<script src="{{ asset( 'library/angular/loadash.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.toaster.js' ) }}"></script>
	<script src="{{ asset( 'library/bootstrap/bootstrap.min.js' ) }}"></script>
	<script src="{{ asset('plugins/canvas-to-blob.min.js') }}"></script>
	<script src="{{ asset('library/bootstrap/fileinput.min.js') }}"></script>
	<script src="{{ asset( 'js/moment.min.js' ) }}"></script>
	<script src="{{ asset( 'plugins/fastclick/fastclick.min.js' ) }}"></script>
  
  <script type="text/javascript" src="{{ asset( 'library/charts/plotly-latest.min.js' ) }}"></script>
  <script src="{{ asset('library/bootstrap/animatescroll.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/sweetalert.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/bootstrap-switch.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/owl-carousel/owl.carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-animate.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-aria.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-material.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/owl-carousel/angular-owl-carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-ui-router.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular-filter.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ngStorage.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular.country-select.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/loading-bar.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ng-file-upload.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ordinal-browser.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/user-info.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/brief.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/image-request.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/report.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/directive/graph.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/upload.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/predict.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/dashboard-client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/main/client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/factory/factory.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/service/service.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/controller/client.js' ) }}"></script>
@endsection