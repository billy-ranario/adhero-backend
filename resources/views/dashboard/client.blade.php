@extends('tpl.tpl-dashboard-client')
@section('title', 'PrecisionBit | Dashboard')


@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('/css/angular-material.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/fileinput.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/loading-bar.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/jquery.mCustomScrollbar.css') }}">
  
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/flot.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.css') }}">
  <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,400' rel='stylesheet' type='text/css'> -->
  <link rel="stylesheet" href="{{ asset('/css/fontello.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('/fontcustom/fontcustom.css') }}"> -->
  <link rel="stylesheet" href="{{ asset('/css/precision.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/angular-chart.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/client-side.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/new-dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.carousel.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.theme.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/bootstrap/bootstrap-switch.min.css' ) }}">

  <script src="{{ asset('/library/bootstrap/jquery.min.js') }}"></script>
  <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('/plugins/chartjs/Chart.js') }}"></script>
  <script src="{{ asset('/library/charts/Chart.Spie.js') }}"></script>
  <script src="{{ asset('/library/charts/d3.v3.min.js') }}"></script>


  <script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCUEHlCS0ge0Urb_WjZW8xRzunI3q2iAIE"></script>
@endsection

@section('client-dashboard')
  <div class="loader-wrapper">
      <div class="loader-wrapper-info text-center">
        <h4 style="font-family: 'Helvetica'">PrecisionBit is loading the Page...</h4>
      </div>
  </div>
	<div class="wrapper custom-wapper">
		<header class="main-header">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" style="background-color: #FFAD30;!important;" role="navigation">
          <!-- Sidebar toggle button-->
          <!-- <a href="#" class="sidebar-toggle custom-side-bar-toggle" data-toggle="offcanvas" role="button" style="color: #28A6A0;">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <div class="navbar-logo">
            <ul class="nav navbar-nav">
              <li>
                <!-- Logo -->
                <a href="/dashboard" style="padding: 0px 15px !important;" class="logo">
                  <!-- mini logo for sidebar mini 50x50 pixels -->
                  <span class="logo-mini">PB</span>
                  <!-- logo for regular state and mobile devices -->
                  <span class="logo-lg log-text logo-container"><img src="../img/logo/PBit logo white transparent.png"></span>
                </a>
              </li>
            </ul>
          </div>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav ul-borders">
              <li class="dropdown">
                <a ui-sref="home" class="menu-header-global home-a dropdown-toggle active-menu" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-home fa-icon" ></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="predict" class="menu-header-global predict-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-file fa-icon" style="color: #fff"></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="influencers" class="menu-header-global user-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-user fa-icon" style="color: #fff"></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="location" class="menu-header-global pin-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-map-marker fa-icon" style="color: #fff"></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="gallery" class="menu-header-global gallery-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-picture fa-icon" style="color: #fff"></span>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a href="javascript:void(0)" class="menu-header-global settings-a dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear custom-glyphicon-cog" style="color: #fff"></i>
                </a>
                <ul class="dropdown-menu">
                  <li><a ui-sref="contact-us">Contact us</a></li>
                  <li><a ui-sref="faq">FAQ</a></li>
                  <!-- <li role="separator" class="divider"></li> -->
                </ul>
              </li>
              <li class="dropdown messages-menu" style="padding-right:40px;padding-top: 5px;">
                <p style="margin:0;color:#fff;font-family: 'Helvetica';" class="pull-right text-second"><span>{{Auth::user()->firstname}}&nbsp;{{Auth::user()->lastname}}</span></p>
                <p style="margin:0;"><a href="{{url('logout')}}" style="color: #181A27;font-size: 15px;font-family: 'Helvetica';">Logout</a></p>
              </li>
              <!-- <li class="dropdown">
                <a ui-sref="" class="menu-header-global dropdown-toggle" data-toggle="dropdown">
                  <span class="fa fa-bar-chart-o" style="color: #181A27"></span>
                </a>
              </li> -->
            </ul>
            <ul class="nav navbar-nav navi-right" hidden>
              <!-- Messages: style can be found in dropdown.less-->
             
              <li class="dropdown  messages-menu">
                <a ui-sref="upload" class="menu-header-global upload-a picture-menu dropdown-toggle" data-toggle="dropdown">
                	<span class="icon-photograph15 fa-icon" style="color: #181A27"></span>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a ui-sref="home" class="menu-header-global case-a suit-menu dropdown-toggle" data-toggle="dropdown">
                	<span class="icon-suitcase54 fa-icon" style="color: #181A27"></span>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a href="javascript:void(0)" class="menu-header-global settings-a dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear custom-glyphicon-cog" style="color: #181A27"></i>
                </a>
                <ul class="dropdown-menu">
		            <li><a ui-sref="contact-us">Contact us</a></li>
		            <li><a ui-sref="faq">FAQ</a></li>
		            <!-- <li role="separator" class="divider"></li> -->
		          </ul>
              </li>
            </ul>

          </div>
        </nav>
        <section class="company-name-container" hidden>
	        	<p class="pull-left text-first"></p>
	        	<p class="pull-right text-second">Signed in as <span>{{Auth::user()->firstname}}&nbsp;{{Auth::user()->lastname}}</span>  <a href="{{url('logout')}}" style="color: #181A27;">Logout</a></p>
	        	<section class="clear-both"></section>
	        </section>
	      </header>
	    <div class="content-wrapper">
      	<section class="content">

      		<div class="section-content row">
            <!-- <div ui-view="side-menu" class="ui-view-container"></div> -->
      			<div ui-view="main" class="ui-view-container"></div>
      		</div>

  	        <section class="network-status" hidden>
  	          <section class="content-header">
  	            <h1>
  	              500 Error Page
  	            </h1>
  	          </section>
  	          <section class="content">
  	            <div class="error-page">
  	              <h2 class="headline text-red">500</h2>
  	              <div class="error-content">
  	                <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
  	                <p>
  	                  Please make sure that you are connected to the internet.
  	                </p>
  	              </div>
  	            </div><!-- /.error-page -->
  	          </section>
  	        </section>
  	      </section>
        </div>



      <!-- pop menu -->
        <md-fab-speed-dial md-open="menu.isOpen" md-direction="up" ng-class="md-fling" class="md-fab-bottom-right md-scale hidden-md hidden-lg visible-xs visible-sm lock-size pop-up-menu" ng-cloak>
          <md-fab-trigger>
            <md-button aria-label="menu" class="md-fab md-warn">
              <md-icon md-font-icon="glyphicon glyphicon-menu-hamburger"></md-icon>
            </md-button>
          </md-fab-trigger>
          <md-fab-actions>
            <md-button aria-label="Home" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-home" aria-label="Home"></md-icon>
            </md-button>
            <md-button aria-label="Predict" class="md-fab md-raised md-mini" ui-sref="predict">
              <md-icon md-font-icon="fa fa-flask" aria-label="Predict"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-map-marker" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="User" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-user" aria-label="User"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="upload">
              <md-icon md-font-icon="icon-photograph15" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="summary">
              <md-icon md-font-icon="icon-suitcase54" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="Settings" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="icon-gear" aria-label="Settings"></md-icon>
            </md-button>
          </md-fab-actions>
        </md-fab-speed-dial>
      <!-- footer -->
      <footer class="main-footer">
        <span>Copyright <a href="https://www.facebook.com/youradhero" style="color:#5f6b64;font-weight:bold;">PrecisionBit</a> 2016. All rights reserved.</span>
      </footer>
   </div>
@endsection

@section( 'scripts' )
  <script src="{{ asset( 'js/jquery-ui.min.js' ) }}"></script>
  <script src="{{ asset( 'library/angular/loadash.js' ) }}"></script>
	<script src="{{ asset( 'library/bootstrap/randomColor.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.toaster.js' ) }}"></script>
	<script src="{{ asset( 'library/bootstrap/bootstrap.min.js' ) }}"></script>
	<script src="{{ asset('plugins/canvas-to-blob.min.js') }}"></script>
	<script src="{{ asset('library/bootstrap/fileinput.min.js') }}"></script>
	<script src="{{ asset( 'js/moment.min.js' ) }}"></script>
	<script src="{{ asset( 'plugins/fastclick/fastclick.min.js' ) }}"></script>
  <script src="{{ asset('/library/charts/d3pie.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/charts/plotly-latest.min.js' ) }}"></script>
  <script src="{{ asset('library/bootstrap/animatescroll.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/sweetalert.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/bootstrap-switch.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.mCustomScrollbar.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/owl-carousel/owl.carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-animate.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-aria.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-material.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/owl-carousel/angular-owl-carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/charts/angular-chart.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ng-map.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-ui-router.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular-filter.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ngStorage.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular.country-select.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/loading-bar.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ng-file-upload.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/ordinal-browser.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular-d3.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/user-info.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/brief.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/image-request.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/report.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/graph.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/directive/influencers.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/upload.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/predict.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/dashboard-client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/main/client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/factory/factory.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/service/service.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/controller/client.js' ) }}"></script>
@endsection