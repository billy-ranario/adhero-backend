<div class="row" style="margin-top: 10px;">
  <div class="col-md-6 col-md-offset-4">
    <div class="panel panel-default">
      <div class="panel-header">
        <p>Upload a file</p>
      </div>
      <div class="panel-body">
        <div class="form-group text-center">
          <form name="form">
            Single Image with validations
            <input type="file" class="form-control" ngf-select ng-model="file" name="file" ngf-pattern="'image/*'"
              accept="image/*" ngf-max-size="20MB" ngf-min-height="100" 
              ngf-resize="{width: 100, height: 100}">
              <br />
            <input type="text" ng-model="description.name" placeholder="description..." class="form-control">
            <br />
            <button type="submit" class="btn custom-btn" ng-click="submit()">submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>