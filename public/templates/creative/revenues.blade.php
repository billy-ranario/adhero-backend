<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px;margin: 10px 0 0 10px;">
        Revenue
        </h1>
    </section>
</div>
<div class="row" style="margin-top: 10px;">
  <div class="col-md-12" style="padding: 10px;width: 95%;border-bottom: 1px solid #d0d0d0;margin-left: 2.5%;">
    <p class="pull-left" style="padding: 10px;color: #777777;font-size: 15px;">Revenue Earned to Date (in USD)</p>
    <p class="pull-right" style="padding: 10px;color: #777777;font-size: 17px;font-weight: bold;">$0</p>
  </div>
</div>

<div class="row ng-scope">
  <p style="font-size: 22px;font-weight: bold;">{{CurrentDate | date:'MMMM yyyy'}}</p>
  <select class="form-control pull-right" style="width: 15%;margin-right: 20px;background: transparent;">
    <option value="November 2015">November 2015</option>
  </select>
  <p style="font-size: 17px;margin: 0;margin-left: 10px;padding: 10px;margin-bottom: 20px;color: #999;">*Notice: In line with Facebook's Ad Report Stats Guidelines, this data will only be available 4 days later.</p>
</div>
<div class="row" style="margin-top: 10px;">
   <div class="col-md-12" style="margin-bottom: 10px;">
    <table style="border-color: #ddd!important;" border="1">
      <tbody>
      <tr style="border-color: #ddd;">
        <th style="font-size: 20px;padding: 15px;background-color: #34ACA6;color: white;padding: 35px;" class="text-center">
          <p style="font-size: 14px;padding: 23px;margin-top: 0;margin-bottom: 0;">This Month's Revenue</p>
          <p style="padding: 10px;margin-top: -30px;font-size: 30px;font-weight: bold;">$0</p>
          <span class="revenue-active"></span>
        </th>
        <td rowspan="2" style="width: 100%;background: white;">
          <section class="text-center" style="height: 266px;border-top: 2px solid #d0d0d0;padding: 20px;width: 95%;margin-left: 2.5%;margin-top: 10px;">
            <p style="font-weight: normal;font-size: 30px;color: #777777;">There is currently no data available.</p>
          </section>
        </td>
      </tr>
      <tr style="border-color: #ddd;">
        <th class="text-center" style="background: white;font-weight: normal;">
          <section class="okay-container" style="margin-top: 10px;">
              <span class="glyphicon glyphicon-ok" style="color: #34ACA6;padding: 10px;border-radius: 50%;background: #E7EAEF;"></span>
            </section>
            <p style="margin: 0;padding: 0;border: 0;font: inherit;font-size: 16px;letter-spacing: 0.07em;vertical-align: baseline;margin-top: 10px;">Payment Due</p>
            <p style="margin: 0;padding: 0;border: 0;font: inherit;font-size: 16px;letter-spacing: 0.07em;vertical-align: baseline;margin-bottom: 20px;">December 15, 2015</p>
          </th>
        </tr>
      </tbody>
    </table>
   </div>
</div>