<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
  <section style="font-size: 20px!important;">
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 10px;">
      Dashboard
      <small class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s">Control panel.</small>
      </h1>
  </section>
</div>

<div class="row" style="margin-top: 10px;">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-header">
        <h2>Dashboard for creative</h2>
      </div>
      <div class="panel-body">
        <p>Content dashboad for creative dashboard</p>
      </div>
  </div>
</div>
