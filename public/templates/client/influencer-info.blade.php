<div influencers>
<div class="col-md-12 influencer-info" style="padding: 15px 55px;">
	<div class="col-md-3 col-sm-12 col-xs-12 left-wrapper influencers-left-menu" style="box-shadow: 1px 2px 1px #BBB;">
		<ul class="nav">
			<li ng-repeat="infu in influence" ng-click="selectInfluencer(infu)">
				<a href="javascript:void(0)" class="global_influence influence_user_{{infu.id}}">
					<img ng-src="{{ infu.image }}" >
					<span class="pull-right" ng-bind="infu.name"></span>
				</a>
			</li>
			<!-- <li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="" >
					<span class="pull-right"></span>
				</a>
			</li> -->
		</ul>
	</div>

	<div class="col-md-9 col-sm-12 col-xs-12 right-wrapper no-padding influencers-info">
		<div class="col-md-12 col-sm-12 col-xs-12" style="background:#FFF;box-shadow: 1px 2px 1px #BBB;">
			<div class="col-md-3 col-sm-12 col-xs-12 no-padding" style="">
				<img ng-src="{{ influence_user.image }}" class="img-responsive">
				<br>
				<div class="info">
					<p><span ng-bind="influence_user.username">@</span></p>
					<p><span ng-bind="influence_user.posts"></span> Posts</p>
					<p><span ng-bind="influence_user.followers">2,001,293</span> Followers</p>
					<p><span ng-bind="influence_user.following">8,192</span> Following</p>
				</div>		
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12 no-padding" style="background:#FFF;">
				<div class="col-md-3" style="">
					<div class="date-wrapper" style="background:#ABABAB;min-height: 360px;border-radius: 6px;">
						<div class="up-arrow">
							<a href="" ng-click="customDatePicker( 0 )" >
								<i class="glyphicon glyphicon-chevron-up"></i>
							</a>
						</div>
						<p ng-bind="datePicks.one" >01/12/16</p>
						<p ng-bind="datePicks.two" >01/23/16</p>
						<p class="selected" ng-bind="datePicks.three">03/14/16</p>
						<p ng-bind="datePicks.four" >03/26/16</p>
						<p ng-bind="datePicks.five" >04/01/16</p>
						<div class="down-arrow">
							<a href="" ng-click="customDatePicker( 1 )">
								<i class="glyphicon glyphicon-chevron-down"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div id="graph" style=""></div>
					</div>
				</div>
			</div>
		</div>	

		<div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="margin-top:15px;box-shadow: 1px 2px 1px #BBB;">
			<div class="col-md-12" style="background:#FFF;">
				<div class="col-md-12">
					<ul class="pie-graph-box" style="margin-top: 0;padding: 0; margin-left: -5px!important;">
						<li><span class="first-pie"></span> Male</li>
						<li><span class="second-pie"></span> Female</li>
					</ul>
				</div>
				<div class="col-md-6" style="padding: 0;">
					<div id="bar-graph" style=""></div>
					<!-- <span style="color: #A9ADB0;">Age</span> -->
				</div>
				<div class="col-md-6" style="padding: 0;">
					<div id="pie-graph" style="margin-top: 0px;"></div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="background:#FFF;margin-top:15px;box-shadow: 1px 2px 1px #BBB;">
			<div class="gallery-wrapper">
			
				
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 0 )" data-toggle="modal" data-target="#influencer-modal">
						<img ng-src="img/pbit-assets-v4/{{influence_user.id}}/1.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 1 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/2.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 2 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/3.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 3 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/4.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 4 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/5.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 5 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/6.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 6 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/7.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 7 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/8.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 8 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/9.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" ng-click="selectGallery( influence_user.id, 9 )" data-toggle="modal" data-target="#influencer-modal">
						<img src="img/pbit-assets-v4/{{influence_user.id}}/10.jpg">
					</a>
				</div>
			</div>
		</div>
		</div> 
	</div>
</div>


<div class="modal fade influencer-modal" id="influencer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12">
      		<div class="col-md-6">
      			<img src="img/pbit-assets-v4/{{influence_user.id}}/{{gallery.index}}.jpg" class="img-responsive">
      		</div>
      		<div class="col-md-6 inception">
      			<p style="text-align:center;margin-bottom: 50px;"><!-- INCEPTION IN A CONCERT --></p>
      			<p><span ng-bind="gallery.likes">5,000</span> Likes</p>
      			<p><span ng-bind="gallery.comments">2,123</span> Comments</p>
      			<p><span ng-bind="gallery.engagements">64</span>% Engagements</p>
      			<button>REQUEST FOR USE</button>
      		</div>
      	</div>
      	<div class="col-md-12" style="padding: 0 30px;margin-top:20px;">
      		<br><span style="font-size: 16px;'">ENGAGERS</span><br><br>
      		<div class="engagers-wrapper">
      			<div ng-repeat="img in gallery.images" class="engagers">
      				<img src="img/pbit-assets-v4/{{influence_user.id}}/{{gallery.index}}/{{img}}.jpg">
      				<span>{{img}}</span>
      			</div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>
</div>
<script>
    (function($){
        $(window).load(function(){
            $(".influencers-left-menu").mCustomScrollbar({
              theme: "dark-thick",
              axis: "yx"
          });
        });
    })(jQuery);
</script>