<form ng-submit="createCampaign()">
<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
  <section style="font-size: 20px!important;" class="campaign-form" hidden>
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 20px;">
      Create Campaign now!
      <small class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s">Get started by filling out the form below.</small>
      </h1>
  </section>
  <section style="font-size: 20px!important;" class="campaign-intro-select">
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 10px;">
      Select your Social media Platform to Advertise :
      </h1>
  </section>
  <section style="font-size: 20px!important;" class="campaign-pay-select" hidden>
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 10px;">
      You can pay now to activate the campaign or later :
      </h1>
  </section>
</div>
<div class="row bs-wizard" style="border-bottom:0;">
    <div class="col-xs-4 bs-wizard-step bs-wizard-step-1 complete">
      <div class="text-center bs-wizard-stepnum">Step 1</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="javascript::void(0)" class="bs-wizard-dot" ng-click="choose('step-1')"></a>
      <div class="bs-wizard-info text-center">choose platform</div>
    </div>
    
    <div class="col-xs-4 bs-wizard-step bs-wizard-step-2"><!-- complete -->
      <div class="text-center bs-wizard-stepnum">Step 2</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="javascript::void(0)" class="bs-wizard-dot" ng-click="choose('step-2')"></a>
      <div class="bs-wizard-info text-center">campaign brief</div>
    </div>
    
    <div class="col-xs-4 bs-wizard-step bs-wizard-step-3"><!-- complete -->
      <div class="text-center bs-wizard-stepnum">Step 3</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="javascript::void(0)" class="bs-wizard-dot" ng-click="choose('step-3')"></a>
      <div class="bs-wizard-info text-center">payment</div>
    </div>
</div>
<div class="row campaign-platform text-center" id="advertise-option" style="height: 600px;">
  <div class="content-post">
  <div class="text-title">
    <span style="font-size: 50px">Pick where you want to advertise on :</span>
  </div>
  <div class="col-md-12 text-center" style="margin-top: 50px;">
    <div class="col-md-5 col-xs-5 col-sm-5 wow fadeInUp" data-wow-duration="0.9s" data-delay="0.9s" style="visibility: visible; animation-duration: 0.9s; animation-name: fadeInUp;">
      <img src="../../img/ads-icons/facebook.svg" class="img-responsive pull-right" style="width: 100px;height: auto;cursor:pointer" ng-click="choose('facebook')">
      <div class="col-md-12 col-xs-12 col-sm-12 text-right">
        <p style="margin-right: 5px;">Facebook</p>
      </div>
    </div>
    <div class="col-md-2 col-xs-2 col-sm-2  wow fadeInUp" data-wow-duration="1.5s" data-delay="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
      <span style="font-size: 70px;">Or</span>
    </div>
    <div class="col-md-5 col-xs-5 col-sm-5 wow fadeInUp" data-wow-duration="2s" data-delay="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
      <img src="../../img/ads-icons/instagram.svg" class="img-responsive pull-left" style="width: 100px;height: auto;cursor:pointer" ng-click="choose('instagram')">
      <div class="col-md-12 col-xs-12 col-sm-12 text-left">
        <p style="margin-left: 5px;">Instagram</p>
      </div>
    </div>
  </div>

  <div class="bottom-text-container">
    <div class="col-md-6 col-md-offset-3 text-center">
      <div class="col-md-10 custom-money-container" style="margin-top:45px;">
        <section class="custom-check-container pull-left" style="margin-left: 80px;">
          <span class="glyphicon glyphicon-ok custom-glyphicon-ok"></span>  
        </section>
        <h2>100% money back guranteed
        <br>
          <span style="font-weight: normal;font-size: 12px;">A profound campaign or your money back</span>
        </h2>
      </div>
    </div>
  </div>
  </div>
</div>

<div class="row campaign-form-fillup" style="background-color: #E7EAEF;" hidden>
<section class="content" id="campaign-brief" >
<div class="row text-center">
  <p id="campaign-head-title">Campaign Brief :</p>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-md-4 col-xs-4 col-sm-4">
      <p style="margin-top: 45%;text-align: right;">Campaign Objective <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
    </div>
    <div class="col-md-8 col-xs-8 col-sm-8">
        <div class="col-md-3">
          <label for="optionsRadios1">
            <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 150px; width: 100%; display: block;">
            <input required type="radio" name="optionsRadios" id="optionsRadios1" value="App Install" ng-model="campaign.campaign_objective"/>
            App Install
          </label>
        </div>
        <div class="col-md-3">
          <label for="optionsRadios2">
            <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 150px; width: 100%; display: block;">
            <input required type="radio" name="optionsRadios" id="optionsRadios2" value="Page Like" ng-model="campaign.campaign_objective"/>
            Page Like
          </label>
        </div>
        <div class="col-md-3">
          <label for="optionsRadios3">
            <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 150px; width: 100%; display: block;">
            <input required type="radio" name="optionsRadios" id="optionsRadios3" value="Drive to Website" ng-model="campaign.campaign_objective"/>
            Drive to Website
          </label>
        </div>
    </div>
  </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Campaign Name<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-8 col-xs-8 col-sm-8" style="margin-top: 30px;">
          <input type="text" class="form-control" ng-model="campaign.campaign_name" style="width: 72%;" required>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Landing Page Url <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-8 col-xs-8 col-sm-8" style="margin-top: 30px;">
          <input type="text" class="form-control" ng-model="campaign.landing_page" style="width: 72%;" required>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Target Country <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-2" style="margin-top: 30px;">
        <country-select ng-model="campaign.target_country" required></country-select>
      </div>
      <div class="col-md-4" style="margin-top: 30px;">
        <span style="margin-top: 10px;" class="pull-left">Language used for ads <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> : </span>
        <country-select ng-model="campaign.target_language" style="width: 43%;" required></country-select>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Target Age <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-1" style="margin-top: 30px;">
        <input type="number" class="form-control" style="width: 110%;" value="18" ng-model="temp.minimum_age" required>
      </div>
      <div class="col-md-1" style="margin-top: 30px;">
      <span class="glyphicon glyphicon-minus" style="margin-top: 10px;"></span><span class="glyphicon glyphicon-minus" style="margin-top: 10px;margin-left: -5px;"></span>
      </div>
      <div class="col-md-1" style="margin-top: 30px;">
        <input type="number" class="form-control" style="width: 110%;" value="80" ng-model="temp.maximum_age" required>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Gender <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-4 col-xs-4 col-sm-4" style="margin-top: 30px;">
        <div class="btn-group pull-left" role="group" aria-label="...">
          <button type="button" class="btn btn-default" id="state_1" ng-click="getGender('All', '1')">All</button>
          <button type="button" class="btn btn-default" id="state_2" ng-click="getGender('Men', '2')">Men</button>
          <button type="button" class="btn btn-default" id="state_3" ng-click="getGender('Women', '3')">Women</button>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Brief Detail <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-6 col-xs-6 col-sm-6 text-left" style="margin-top: 30px;">
        <textarea class="form-control" ng-model="campaign.guideline_ad" style="width: 95%; height: 150px;" required></textarea>
        <p style="margin-left: 10px;font-size: 10px; margin-top: 5px">Describe your aims and requirements in detail here - the more specific, the better.
        <br />
        Tell the designers what is required, but also let te=hem know where they're free to be creative
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px;">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="col-md-4 col-xs-4 col-sm-4">
        <p style="text-align: right;">Upload Image <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i> :</p>
      </div>
      <div class="col-md-4">
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
          <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="customer" file-model=""></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            <p class="pull-left" style="font-size: 10px;margin-top: 5px;">E.g. Your current logo, photos. illustrations, content, layout ideas etc.</p> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px; margin-bottom: 70px;">
    <div class="col-md-12 col-xs-12 col-sm-12 text-center">
      <button class="btn btn-large" style="background-color: #16AD8F;color: white;font-size: 20px;" ng-click="createCampaign()"> Next </button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 10%;">
        <div class="col-md-9 custom-money-container" style="margin-top:45px;">
            <section class="custom-check-container pull-left" style="margin-left: 80px;">
              <span class="glyphicon glyphicon-ok custom-glyphicon-ok"></span>  
            </section>
            <h2>100% money back guranteed
            <br>
              <span style="font-weight: normal;font-size: 12px;">A profound campaign or your money back</span>
            </h2>
        </div>
    </div>
  </div>
</div>
</section>
</div>
<div class="row payment-method-campaign" hidden>
  <div class="payment-method">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h3 class="pull-left">Total</h3>
          <h3 class="pull-right">$250.00 USD</h3>
        </div>
        <div class="col-md-6">
          <section style="margin-top: 20px;">
            <div class="col-md-12">
              <div class="bottom-text custom-money-container">
                <div class="icon-check-wrapper" style="background-color: #5A2C94;width: 30px;height: 30px;border-radius: 50%;text-align: center;float: left;margin-right: 15px;">
                  <i class="glyphicon glyphicon-ok custom-glyphicon-ok"></i>
                </div>
                <div class="text-quote">
                  <h2>100% money back gurantee</h2>
                  <p>A design you love or your money back</p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  <div class="payment-details-container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4">
          <h3 class="pull-left">Payment details</h3>
        </div>
        <div class="col-md-6">
          <h4>How would you like to pay?</h4>
          <br />
          <ul class="payment-choices">
            <div class="radio">
              <div class="form-group">
                  <label for="choice-1">
                  <li>
                    <input type="radio" name="choice-1" style="float: left;display: inline-block;margin-right: 10px;width: 20px;height: 50px;" onclick="showLess()"> 
                    <!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> -->
                      <!-- <input type="hidden" name="cmd" value="_s-xclick"> -->
                      <!-- <input type="hidden" name="hosted_button_id" value="ECDRRWE9L6HCS"> -->
                      <input type="image" src="https://www.paypalobjects.com/en_GB/SG/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
                      <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                    <!-- </form> -->
                </label>
              </div>
              <div class="form-group">
                <label for="choice-2">
                  <li style="margin-top: 10px;">
                    <input type="radio" name="choice-1" style="float: left;display: inline-block;width: 20px; height: 50px;" onclick="showMore()">
                    <img src="../../img/visa.svg" class="img-responsive" style="float: left;margin-left: 10px;"> 
                    <img src="../../img/master.svg" class="img-responsive" style="float: left;margin-left: 10px;"><img src="../../img/american.svg" class="img-responsive" style="float: left;margin-left: 10px;"></li>
                </label>
              </div>
            </div>
          </ul>

          <div class="row more-details wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s" hidden>
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span style="font-size: 20px;">  Credit Card Details</span>                  
                  </div>
                  <div class="panel-body">
                    <div class="form-group">
                       <label>Name of Card</label>
                       <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                       <label>Card Number</label>
                       <input type="text" class="form-control">
                    </div>
                   <div class="form-group">
                     <label>Security Code</label>
                     <input type="text" class="form-control">
                   </div>
                  <div class="form-group">
                 <label>Card Expiry</label>
                 <br />
                <select placeholder="- month -" class="form-control" style="width: 50%;float: left;margin-right: 5px;">
                  <option disabled="disabled" selected="selected" value="">
                  - month -
                  </option><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select>
                  <select placeholder="- year -" class="form-control" style="width: 45%;"><option disabled="disabled" selected="selected" value="">
                - year -
                  </option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option><option value="2026">2026</option><option value="2027">2027</option><option value="2028">2028</option><option value="2029">2029</option><option value="2030">2030</option>
                </select>
               </div>
  
              <div class="form-group">
                <input type="checkbox"><span style="font-size: 20px;margin-left: 5px;"> Save this card for next time</span>
              </div>
            </div>
            <div class="panel-heading">
              <span style="font-size: 20px;"> Billing Address </span>
            </div>
              <div class="panel-body">
                <div class="form-group">
                  <label>Address</label>
                  <input type="text" class="form-control">
                </div>
              <div class="form-group">
                  <label>Address Line 2</label>
                  <input type="text" class="form-control">
                </div>
              <div class="form-group">
                  <label>City</label>
                  <input type="text" class="form-control">
                </div>
              <div class="form-group">
                  <label>State/Province</label>
                  <input type="text" class="form-control">
                </div><div class="form-group">
                  <label>ZipCode</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                <label>Country</label>
                  <select placeholder=" - Select Country - " class="form-control"><option disabled="disabled" value="">
                 - Select Country - 
                    </option><option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island (Bouvetoya)</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="VG">British Virgin Islands</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CD">DR Congo</option><option value="CG">Congo</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="CI">Cote D'Ivoire</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FO">Faeroe Islands</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadaloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard and McDonald Islands</option><option value="VA">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KP">North Korea</option><option value="KR">South Korea</option><option value="KW">Kuwait</option><option value="KG">Kyrgyz Republic</option><option value="LA">Lao People's Democratic Republic</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libyan Arab Jamahiriya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao</option><option value="MK">Macedonia</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia</option><option value="MD">Moldova</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="AN">Netherlands Antilles</option><option value="NL">Netherlands</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH" selected="selected">Philippines</option><option value="PN">Pitcairn Island</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russian Federation</option><option value="RW">Rwanda</option><option value="SH">St. Helena</option><option value="KN">St. Kitts and Nevis</option><option value="LC">St. Lucia</option><option value="PM">St. Pierre and Miquelon</option><option value="VC">St. Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SK">Slovakia (Slovak Republic)</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard &amp; Jan Mayen Islands</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syrian Arab Republic</option><option value="TW">Taiwan</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau (Tokelau Islands)</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="VI">US Virgin Islands</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="UM">United States Minor Outlying Islands</option><option value="US">United States of America</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela</option><option value="VN">Viet Nam</option><option value="WF">Wallis and Futuna Islands</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option><option value="MF">St. Martin (French part)</option><option value="JE">Jersey</option><option value="IM">Isle of Man</option><option value="GG">Guernsey</option><option value="BL">Saint Barthélemy</option><option value="AX">Åland Islands</option><option value="EU">Europe</option><option value="AP">Asia/Pacific Region</option><option value="K1">Kosovo</option></select>
                  </div>
                </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4">
          <h3>Contact details</h3>
        </div>
        <div class="col-md-6">
          <div class="form-group">
             <label>Full Name</label>
             <input type="text" class="form-control">
          </div>
          <div class="form-group">
             <label>Company name</label>
             <input type="text" class="form-control">
          </div>
          <div class="form-group">
             <label>Country code</label>
             <select placeholder=" - Select Country - " class="form-control"><option disabled="disabled" selected="selected" value="">
                 - Select Country - 
                  </option><option value="AF">Afghanistan (+93)</option><option value="AL">Albania (+355)</option><option value="DZ">Algeria (+213)</option><option value="AS">American Samoa (+1)</option><option value="AD">Andorra (+376)</option><option value="AO">Angola (+244)</option><option value="AI">Anguilla (+1)</option><option value="AQ">Antarctica (+672)</option><option value="AG">Antigua and Barbuda (+1)</option><option value="AR">Argentina (+54)</option><option value="AM">Armenia (+374)</option><option value="AW">Aruba (+297)</option><option value="AU">Australia (+61)</option><option value="AT">Austria (+43)</option><option value="AZ">Azerbaijan (+994)</option><option value="BS">Bahamas (+1)</option><option value="BH">Bahrain (+973)</option><option value="BD">Bangladesh (+880)</option><option value="BB">Barbados (+1)</option><option value="BY">Belarus (+375)</option><option value="BE">Belgium (+32)</option><option value="BZ">Belize (+501)</option><option value="BJ">Benin (+229)</option><option value="BM">Bermuda (+1)</option><option value="BT">Bhutan (+975)</option><option value="BO">Bolivia (+591)</option><option value="BA">Bosnia and Herzegovina (+387)</option><option value="BW">Botswana (+267)</option><option value="BR">Brazil (+55)</option><option value="IO">British Indian Ocean Territory (+246)</option><option value="VG">British Virgin Islands (+1)</option><option value="BN">Brunei Darussalam (+673)</option><option value="BG">Bulgaria (+359)</option><option value="BF">Burkina Faso (+226)</option><option value="BI">Burundi (+257)</option><option value="KH">Cambodia (+855)</option><option value="CM">Cameroon (+237)</option><option value="CA">Canada (+1)</option><option value="CV">Cape Verde (+238)</option><option value="KY">Cayman Islands (+1)</option><option value="CF">Central African Republic (+236)</option><option value="TD">Chad (+235)</option><option value="CL">Chile (+56)</option><option value="CN">China (+86)</option><option value="CX">Christmas Island (+618)</option><option value="CC">Cocos (Keeling) Islands (+61)</option><option value="CO">Colombia (+57)</option><option value="KM">Comoros (+269)</option><option value="CD">DR Congo (+243)</option><option value="CG">Congo (+242)</option><option value="CK">Cook Islands (+682)</option><option value="CR">Costa Rica (+506)</option><option value="CI">Cote D'Ivoire (+225)</option><option value="HR">Croatia (+385)</option><option value="CU">Cuba (+53)</option><option value="CY">Cyprus (+357)</option><option value="CZ">Czech Republic (+420)</option><option value="DK">Denmark (+45)</option><option value="DJ">Djibouti (+253)</option><option value="DM">Dominica (+1)</option><option value="DO">Dominican Republic (+1)</option><option value="EC">Ecuador (+593)</option><option value="EG">Egypt (+20)</option><option value="SV">El Salvador (+503)</option><option value="GQ">Equatorial Guinea (+240)</option><option value="ER">Eritrea (+291)</option><option value="EE">Estonia (+372)</option><option value="ET">Ethiopia (+251)</option><option value="FO">Faeroe Islands (+298)</option><option value="FK">Falkland Islands (Malvinas) (+500)</option><option value="FJ">Fiji (+679)</option><option value="FI">Finland (+358)</option><option value="FR">France (+33)</option><option value="GF">French Guiana (+594)</option><option value="PF">French Polynesia (+689)</option><option value="GA">Gabon (+241)</option><option value="GM">Gambia (+220)</option><option value="GE">Georgia (+995)</option><option value="DE">Germany (+49)</option><option value="GH">Ghana (+233)</option><option value="GI">Gibraltar (+350)</option><option value="GR">Greece (+30)</option><option value="GL">Greenland (+299)</option><option value="GD">Grenada (+1)</option><option value="GP">Guadaloupe (+590)</option><option value="GU">Guam (+1)</option><option value="GT">Guatemala (+502)</option><option value="GN">Guinea (+224)</option><option value="GW">Guinea-Bissau (+245)</option><option value="GY">Guyana (+592)</option><option value="HT">Haiti (+509)</option><option value="VA">Holy See (Vatican City State) (+379)</option><option value="HN">Honduras (+504)</option><option value="HK">Hong Kong (+852)</option><option value="HU">Hungary (+36)</option><option value="IS">Iceland (+354)</option><option value="IN">India (+91)</option><option value="ID">Indonesia (+62)</option><option value="IR">Iran (+98)</option><option value="IQ">Iraq (+964)</option><option value="IE">Ireland (+353)</option><option value="IL">Israel (+972)</option><option value="IT">Italy (+39)</option><option value="JM">Jamaica (+1)</option><option value="JP">Japan (+81)</option><option value="JO">Jordan (+962)</option><option value="KZ">Kazakhstan (+7)</option><option value="KE">Kenya (+254)</option><option value="KI">Kiribati (+686)</option><option value="KP">North Korea (+850)</option><option value="KR">South Korea (+82)</option><option value="KW">Kuwait (+965)</option><option value="KG">Kyrgyz Republic (+996)</option><option value="LA">Lao People's Democratic Republic (+856)</option><option value="LV">Latvia (+371)</option><option value="LB">Lebanon (+961)</option><option value="LS">Lesotho (+266)</option><option value="LR">Liberia (+231)</option><option value="LY">Libyan Arab Jamahiriya (+218)</option><option value="LI">Liechtenstein (+423)</option><option value="LT">Lithuania (+370)</option><option value="LU">Luxembourg (+352)</option><option value="MO">Macao (+853)</option><option value="MK">Macedonia (+389)</option><option value="MG">Madagascar (+261)</option><option value="MW">Malawi (+265)</option><option value="MY">Malaysia (+60)</option><option value="MV">Maldives (+960)</option><option value="ML">Mali (+223)</option><option value="MT">Malta (+356)</option><option value="MH">Marshall Islands (+692)</option><option value="MQ">Martinique (+596)</option><option value="MR">Mauritania (+222)</option><option value="MU">Mauritius (+230)</option><option value="YT">Mayotte (+262)</option><option value="MX">Mexico (+52)</option><option value="FM">Micronesia (+691)</option><option value="MD">Moldova (+373)</option><option value="MC">Monaco (+377)</option><option value="MN">Mongolia (+976)</option><option value="ME">Montenegro (+382)</option><option value="MS">Montserrat (+1)</option><option value="MA">Morocco (+212)</option><option value="MZ">Mozambique (+258)</option><option value="MM">Myanmar (+95)</option><option value="NA">Namibia (+264)</option><option value="NR">Nauru (+674)</option><option value="NP">Nepal (+977)</option><option value="AN">Netherlands Antilles (+599)</option><option value="NL">Netherlands (+31)</option><option value="NC">New Caledonia (+687)</option><option value="NZ">New Zealand (+64)</option><option value="NI">Nicaragua (+505)</option><option value="NE">Niger (+227)</option><option value="NG">Nigeria (+234)</option><option value="NU">Niue (+683)</option><option value="NF">Norfolk Island (+672)</option><option value="MP">Northern Mariana Islands (+1)</option><option value="NO">Norway (+47)</option><option value="OM">Oman (+968)</option><option value="PK">Pakistan (+92)</option><option value="PW">Palau (+680)</option><option value="PS">Palestinian Territory (+970)</option><option value="PA">Panama (+507)</option><option value="PG">Papua New Guinea (+675)</option><option value="PY">Paraguay (+595)</option><option value="PE">Peru (+51)</option><option value="PH">Philippines (+63)</option><option value="PL">Poland (+48)</option><option value="PT">Portugal (+351)</option><option value="PR">Puerto Rico (+1)</option><option value="QA">Qatar (+974)</option><option value="RE">Reunion (+262)</option><option value="RO">Romania (+40)</option><option value="RU">Russian Federation (+7)</option><option value="RW">Rwanda (+250)</option><option value="SH">St. Helena (+290)</option><option value="KN">St. Kitts and Nevis (+1)</option><option value="LC">St. Lucia (+1)</option><option value="PM">St. Pierre and Miquelon (+508)</option><option value="VC">St. Vincent and the Grenadines (+1)</option><option value="WS">Samoa (+685)</option><option value="SM">San Marino (+378)</option><option value="ST">Sao Tome and Principe (+239)</option><option value="SA">Saudi Arabia (+966)</option><option value="SN">Senegal (+221)</option><option value="RS">Serbia (+381)</option><option value="SC">Seychelles (+248)</option><option value="SL">Sierra Leone (+232)</option><option value="SG">Singapore (+65)</option><option value="SK">Slovakia (Slovak Republic) (+421)</option><option value="SI">Slovenia (+386)</option><option value="SB">Solomon Islands (+677)</option><option value="SO">Somalia (+252)</option><option value="ZA">South Africa (+27)</option><option value="ES">Spain (+34)</option><option value="LK">Sri Lanka (+94)</option><option value="SD">Sudan (+249)</option><option value="SR">Suriname (+597)</option><option value="SJ">Svalbard &amp; Jan Mayen Islands (+47)</option><option value="SZ">Swaziland (+268)</option><option value="SE">Sweden (+46)</option><option value="CH">Switzerland (+41)</option><option value="SY">Syrian Arab Republic (+963)</option><option value="TW">Taiwan (+886)</option><option value="TJ">Tajikistan (+992)</option><option value="TZ">Tanzania (+255)</option><option value="TH">Thailand (+66)</option><option value="TL">Timor-Leste (+670)</option><option value="TG">Togo (+228)</option><option value="TK">Tokelau (Tokelau Islands) (+690)</option><option value="TO">Tonga (+676)</option><option value="TT">Trinidad and Tobago (+1)</option><option value="TN">Tunisia (+216)</option><option value="TR">Turkey (+90)</option><option value="TM">Turkmenistan (+993)</option><option value="TC">Turks and Caicos Islands (+1)</option><option value="TV">Tuvalu (+688)</option><option value="VI">US Virgin Islands (+1)</option><option value="UG">Uganda (+256)</option><option value="UA">Ukraine (+380)</option><option value="AE">United Arab Emirates (+971)</option><option value="GB">United Kingdom (+44)</option><option value="UM">United States Minor Outlying Islands (+1)</option><option value="US">United States of America (+1)</option><option value="UY">Uruguay (+598)</option><option value="UZ">Uzbekistan (+998)</option><option value="VU">Vanuatu (+678)</option><option value="VE">Venezuela (+58)</option><option value="VN">Viet Nam (+84)</option><option value="WF">Wallis and Futuna Islands (+681)</option><option value="EH">Western Sahara (+212)</option><option value="YE">Yemen (+967)</option><option value="ZM">Zambia (+260)</option><option value="ZW">Zimbabwe (+263)</option><option value="MF">St. Martin (French part) (+590)</option><option value="JE">Jersey (+44)</option><option value="IM">Isle of Man (+44)</option><option value="GG">Guernsey (+44)</option><option value="BL">Saint Barthélemy (+590)</option><option value="AX">Åland Islands (+358)</option><option value="EU">Europe (+388)</option><option value="K1">Kosovo (+381)</option></select>
          </div>
          <div class="form-group">
             <label>Phone number</label>
             <input type="text" class="form-control">
          </div>
          <div class="form-group text-right">
            <a ui-sref="preview-ad" class="btn btn-large btn-primary" style="font-size: 20px;margin-bottom: 10px; box-shadow: 0px 0px 4px black;margin-right: 10px;" ng-click="saveAds()">SAVE NOW</a>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
              <input type="hidden" name="cmd" value="_s-xclick">
              <input type="hidden" name="hosted_button_id" value="ECDRRWE9L6HCS">
              <!-- <input type="image" src="https://www.paypalobjects.com/en_GB/SG/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online."> -->
              <button class="btn btn-large" style="background-color: #01BC8C;color: white;font-size: 20px;box-shadow: 0px 0px 4px black;" ng-click="payNow()">PAY AND LAUNCH</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

<div class="modal fade" id="paypalCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <img src="../../img/credit/paypal.png">
      </div>
      <div class="modal-body">
        <section style="width: 100%;height: auto;background-color: #E1E1E1;">
          <span style="font-size: 30px;padding: 10px;color: #777777;">Adhero Ads</span>
          <span class="pull-right" style="font-size: 20px;padding: 10px;color: #504949;">Total: <span style="color: #965454;font-size: 25px;">$250</span></span>
        </section>
        <section>
          <div class="form-group">
            <div class="col-md-6">
              <label>First Name</label>
              <input type="text" class="form-control">
            </div>
            <div class="col-md-6">
              <label>Last Name</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
              <div class="col-md-8">
                <label>Cardholder Address</label>
                <input type="text" class="form-control">
              </div>
              <div class="col-md-4">
                <label>Card Expiry</label>
                <input type="date" class="form-control">
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
              <label>Card Number</label>
              <input type="text" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Security Code</label>
              <input type="number" class="form-control">
            </div>
          </div>
        </section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn custom-button pull-right" data-dismiss="modal" ng-click="payNow()">Pay</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function () {
    // $('[data-toggle="tooltip"]').tooltip('show')
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  })
</script>