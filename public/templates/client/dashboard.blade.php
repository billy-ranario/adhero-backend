<div>
  <div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
      <section style="font-size: 20px!important;">
          <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 20px;">
          Dashboard
          <small class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s">Control panel.</small>
          </h1>
          <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s" style="font-size:10px;background-color: #E7EAEF;margin:0">
            <li><a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
            </li>
          </ol>
      </section>
  </div>
  <div class="row" style="display:none">
    <div class="">
      <div class="col-md-3" style="margin-top: 10px;">
        <div class="panel panel-default">
          <div class="panel-body">
            <section>
              <p style="font-size: 20px;">The Winning Ad Found!</p>
            </section>
            <section class="col-md-5">
              <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 60px; width: 60px; display: block; border-radius: 50%;">
            </section>
            <section class="col-m-2">
              <p style="margin-top: 20px;font-size: 18px;">Ads A</p>
            </section>
            <section class="col-md-12" style="margin-left: -10px; margin-top: 5px;">
            Confidence percentage: <span style="font-size: 30px;margin-left: 20px;margin-top: -12px;position: absolute;">87%</span>
            </section>
          </div>
        </div>
        <div class="panel panel-default" style="margin-top: -15px;">
          <div class="panel-body">
            <div class="col-md-12 col-sm-12 col-xs-12 remove-side-padding">
              <p><span style="font-size: 20px;font-weight: bold;">3</span> <span style="margin-left: 2px;">days</span> <span style="margin-left: 15px;font-weight: bold;font-size: 20px;">5</span> <span style="margin-left: 2px;">Minutes</span> <span style="font-size: 20px;font-weight: bold;margin-left: 15px;">39</span> <span>Hours</span></p>
            </div>
          </div>
          <p style="margin-left: 30px; margin-top: -20px;">RUNTIME</p>
        </div>
      </div>

      <div class="col-md-9" ng-if="userType == 'customer'" style="margin-top: 10px;">
      <div class="box box-info true-campaign">
        <div class="box-header with-border">
          <h3 class="box-title">Campaign Name: Sample Campaign</h3>
          <div class="box-tools pull-right">
            <select class="form-control">
              <option>Result</option>
              <option>Cost</option>
              <option>Lead Conversion</option>
              <option>Cost per Lead</option>
            </select>
            <!-- <button class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button> -->
          </div>
        </div>
        <div class="box-body" style="width: 100%;">
          <div class="chart">
            <canvas id="dashboard-report" style="100%; height: 152px;"></canvas>
            <div id="legend-dashboard"></div>
          </div>
        </div>
      </div>

      <div class="box box-info false-campaign" hidden>
        <div class="box-header with-border">
          <h3 class="box-title">Campaign</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="chart text-center" style="margin-bottom: 50px;">
            <button class="btn btn-large" style="font-size: 50px;color: white;font-weight: bold;background-color: #725391;box-shadow: 0px 0px 10px black;margin-top: 50px;" ng-click="createMyCampaign()">Launch Campaign Now!</button>
          </div>
        </div>
      </div>
    </div>
  </div>
    

    </div>

  <div class="row" ng-if="userType == 'customer'" style="display:none">
  <section class="content">
      <!--/row-->
      <div class="row ">
          <div class="col-md-8 col-sm-6">
              <!-- <div class="panel panel-border">
                  <div class="panel-heading text-center" style="background-color: #01BC8C;color: white;border-bottom: 1px solid #8C8888;">
                    <span class="glyphicon glyphicon-chevron-right pull-right" style="font-size: 20px;"><br><i style="font-size: 10px;">Next</i></span>
                                <span class="glyphicon glyphicon-chevron-left pull-left" style="font-size: 20px;"><br><i style="font-size:10px;">Previous</i></span>
                    <span style="font-size: 20px;
                                  ">Active Campaign :</span><br>
                    <span class="">Date Start: <br></span></div>
                  <div class="panel-body">
                      <div class="table-scrollable text-center">
                    <span style="
                      font-size: 50px;
                      font-weight: bold;
                      color: gray;
                  "> No Active Campaign </span>
                  </div>
                  </div>
              </div> -->
              <div class="panel panel-border">
                  <div class="panel-heading text-center" style="background-color: #01BC8C;color: white;border-bottom: 1px solid #8C8888;">
                    <span class="glyphicon glyphicon-chevron-right pull-right" style="font-size: 20px;"><br><i style="font-size: 10px;">Next</i></span>
                    <span class="glyphicon glyphicon-chevron-left pull-left" style="font-size: 20px;"><br><i style="font-size:10px;">Previous</i></span>
                    <span style="font-size: 20px;">Active Campaign : Campaign Name</span><br>
                    <span class="">Date Start: <br>
                    <span> 16/10/2015  <span> 23:10 GMT+8</span>
                    </span></span>
                  </div>
  <div class="panel-body">
      <div class="table-scrollable">
        <table class="table table-hover">
          <thead>
              <tr>
                <th>#</th>
                <th>Variables</th>
                <th>Ads A <br> <a href="#" style="font-size: 12px;text-decoration: underline;font-style: italic;">Preview</a></th>
                 <th>Ads B <br> <a href="#" style="font-size: 12px;text-decoration: underline;font-style: italic;">Preview</a></th>
                <th>Ads C <a href="#" style="font-size: 12px;text-decoration: underline;font-style: italic;">Preview</a>
                    <i class="livicon" data-name="trophy" data-size="50" data-c="#fff" data-hc="#fff" data-loop="true" id="livicon-201" style="width: 40px; height: 40px;/* color: black; */margin-left: 5px;/* margin-top: 5px; */"><svg height="40" version="1.1" width="50" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.640625px;background-color: #F89A14;/* width: 40px; *//* height: 40px; */" id="canvas-for-livicon-201"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M27.4,4H24.026999999999997C24.026999999999997,4,23.948999999999998,3.648,23.996,3.098C24.043,2.548,23.544999999999998,2.057,22.884999999999998,1.9999999999999998H9.115C8.455,2.057,7.957000000000001,2.548,8.004,3.098C8.051,3.648,7.973,4,7.973,4H4.6C4.27,4,4,4.269,4,4.6V11.6C4,12.926,5.068,14.158999999999999,6.301,14.648C7.406000000000001,15.086,8.715,15.519,8.987,16.044C10,18,11.057,18.527,12,19C12.807,19.402,14,20,14,22V25.4C14,25.729999999999997,13.808,26.186999999999998,13.553,26.4C12.949000000000002,26.901999999999997,11.81,27.724,10.596,27.945C10.27,28.004,10,28.27,10,28.6V30H22V28.6C22,28.270000000000003,21.73,28.004,21.404,27.946C20.189,27.725,19.049,26.901000000000003,18.445,26.399C18.191,26.188,18,25.73,18,25.4V22C18,20,19.193,19.402,20,19C20.943,18.527,22,18,23.014,16.043C23.285,15.518999999999998,24.594,15.085999999999999,25.7,14.646999999999998C26.932,14.158,28,12.926,28,11.6V4.6C28,4.269,27.73,4,27.4,4ZM6.998,12.666C6.447,12.298,6,11.463,6,10.8V6H8L9,14L6.998,12.666ZM19.002,11.872L19.709,16L16.000999999999998,14.05L12.294,16L13.001000000000001,11.872L10,8.947L14.146,8.343L16,4.587L17.854,8.343L22,8.947L19.002,11.872ZM26,10.8C26,11.463000000000001,25.553,12.298,25.002,12.666L23,14L24,6H26V10.8Z" transform="matrix(1.5625,0,0,1.5625,0,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                </svg></i>
          </th>
        </tr>
    </thead>
      <tbody>
          <tr>
            <td>1</td>
            <td>Result</td>
            <td><span>100,000</span><br> <span style="font-size: 10px;"> Website Clicks </span></td>
            <td><span>200,000</span><br> <span style="font-size: 10px;"> Website Clicks </span></td>
            <td>
              <span>300,000</span><br> <span style="font-size: 10px;"> Website Clicks </span>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Cost</td>
            <td><span>$1</span><br> <span style="font-size: 10px;">per Website Click</span></td>
            <td><span>$2</span><br> <span style="font-size: 10px;">per Website Click</span></td>
            <td><span>$3</span><br> <span style="font-size: 10px;">per Website Click</span></td>
          </tr>
          <tr>
            <td>3</td>
            <td>Lead Conversion</td>
            <td>100</td>
            <td>200</td>
            <td>300</td>
          </tr>
          <tr>
            <td>4</td>
            <td>Cost per Lead</td>
            <td>$1</td>
            <td>$2</td>
            <td>$3</td>
          </tr>
      </tbody>
    </table>
    </div>
  </div>
  </div>
  </div>
  <div class="col-md-4">
     <div class="col-lg-12 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
              <!-- Trans label pie charts strats here-->
              <div class="lightbluebg no-radius" style="">
                <div class="panel-body squarebox square_boxs" style="background-color: #5F9DD6;margin-bottom: 10px;border-radius: 5px;color: white;padding: 0px;">
                      <div class="col-xs-12 pull-left nopadmar">
                          <div class="row">
                              <div class="square_box col-xs-7 text-left" style="margin-top: 5px;">
                                  <span style="font-size: 12px;">Active Campaign Today</span>
                                  <div class="number" id="myTargetElement1" style="font-size: 40px;font-weight: bold;">7</div>
                              </div>
                              <div class="col-xs-5 text-left">
                                <i class="livicon  pull-right" data-name="eye-open" data-l="true" data-c="#fff" data-hc="#fff" data-s="70" id="livicon-48" style="padding-left:15px;"><svg height="70" version="1.1" width="70" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.5px;" id="canvas-for-livicon-48" cz-backup-style="overflow: hidden; position: relative; left: -0.5px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M29.379,14.958C26.633,12.368,22,7.6,16,7.6S5.366,12.368,2.621,14.958C1.7930000000000001,15.739,1.794,16.264,2.622,17.045C5.367,19.637,10,24.4,16,24.4S26.633,19.631999999999998,29.378999999999998,17.043C30.207,16.261,30.207,15.738,29.379,14.958ZM15.996,21.5C12.96,21.5,10.496,19.037,10.496,16C10.496,12.965,12.96,10.5,15.996,10.5C19.033,10.5,21.496000000000002,12.965,21.496000000000002,16C21.496,19.037,19.033,21.5,15.996,21.5ZM13,16C13,17.656,14.345,19,16,19C17.656,19,19,17.656,19,16V15.975C18.68,16.3,18.242,16.5,17.75,16.5C16.785,16.5,16,15.716,16,14.75C16,14.055,16.41,13.449,16.996,13.17C16.686999999999998,13.06,16.349999999999998,12.999,15.999999999999998,12.999C14.345,12.999,13,14.345,13,16Z" stroke-width="0" transform="matrix(2.1875,0,0,2.1875,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></i>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-xs-6">
                                  <small class="stat-label">Last Week</small>
                                  <h4 id="myTargetElement1.1">98000</h4>
                              </div>
                              <div class="col-xs-6 text-right">
                                  <small class="stat-label">Last Month</small>
                                  <h4 id="myTargetElement1.2">396000</h4>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
  <div class="col-lg-12 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
              <!-- Trans label pie charts strats here-->
              <div class="palebluecolorbg no-radius">
                  <div class="panel-body squarebox square_boxs" style="background-color: #01BC8C;color: white;border-radius: 5px;margin-bottom: 10px;padding: 0px;">
                      <div class="col-xs-12 pull-left nopadmar">
                          <div class="row">
                              <div class="square_box col-xs-7 pull-left">
                                  <span style="font-size: 11px;">Pending Campaign Today</span>
                                  <div class="number" id="myTargetElement4" style="font-size: 40px;font-weight: bold;">8</div>
                              </div>
                             <div class="col-xs-5 text-left">
                                <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff" data-hc="#fff" data-s="70" id="livicon-51" style="width: 70px; height: 70px;"><svg height="70" version="1.1" width="70" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-livicon-51"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M8.672,21.086C8.573,20.894,8.524000000000001,20.755,8.524000000000001,20.755S8.092,21.009999999999998,8.092,20.291S8.524000000000001,20.755,8.956000000000001,17.979C8.956000000000001,17.979,10.154000000000002,17.619999999999997,9.914000000000001,14.639H9.627C9.627,14.639,9.770000000000001,14.007,9.864,13.210999999999999C9.860000000000001,12.880999999999998,9.870000000000001,12.529999999999998,9.901000000000002,12.144999999999998L9.939000000000002,11.688999999999998C9.918000000000001,11.161999999999999,9.832000000000003,10.683999999999997,9.627000000000002,10.375999999999998C8.907000000000002,9.297999999999998,8.619000000000003,8.578999999999997,7.036000000000002,8.063999999999998C5.453,7.55,6.028,7.652,4.878,7.705C3.726,7.757,2.766,8.423,2.766,8.785C2.766,8.785,2.0460000000000003,8.837,1.758,9.145C1.487,9.434999999999999,1.053,10.709999999999999,1.001,11.163V11.464C1.0479999999999998,12.163,1.259,14.086,1.4699999999999998,14.540000000000001L1.1839999999999997,14.643C0.9459999999999997,17.623,2.143,17.984,2.143,17.984C2.5749999999999997,20.756,3.0069999999999997,19.575000000000003,3.0069999999999997,20.294C3.0069999999999997,21.014,2.5749999999999997,20.757,2.5749999999999997,20.757S2.1919999999999997,21.889000000000003,1.2319999999999998,22.299000000000003C1.171,22.322,1.093,22.356,1,22.395V27.999H1.575C1.546,26.631999999999998,1.652,24.865,2.3209999999999997,24.151C2.677,23.771,3.844,23.146,8.672,21.086Z" opacity="1" stroke-width="0" transform="matrix(2.1875,0,0,2.1875,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M31,11.389C30.959,10.984,30.873,10.623,30.707,10.376C29.988,9.296999999999999,29.699,8.578999999999999,28.118000000000002,8.064C26.534000000000002,7.55,27.110000000000003,7.652,25.958000000000002,7.704C24.808000000000003,7.755999999999999,23.847,8.422,23.847,8.783999999999999C23.847,8.783999999999999,23.128,8.835999999999999,22.839000000000002,9.143999999999998C22.568,9.435999999999998,22.130000000000003,10.719999999999999,22.081000000000003,11.168999999999999H22.114000000000004L22.194000000000003,12.145999999999999C22.214000000000002,12.393999999999998,22.215000000000003,12.613,22.221000000000004,12.837C22.311000000000003,13.549999999999999,22.430000000000003,14.282,22.551000000000002,14.539L22.264000000000003,14.642C22.026000000000003,17.622,23.225,17.983,23.225,17.983C23.657,20.755,24.088,19.574,24.088,20.293C24.088,21.012999999999998,23.654,20.756,23.654,20.756S23.601,20.908,23.492,21.118C28.262,23.151999999999997,29.419,23.773,29.77,24.153C30.44,24.866999999999997,30.544999999999998,26.631999999999998,30.516,28.000999999999998H30.996V22.319999999999997C30.977999999999998,22.312999999999995,30.956999999999997,22.302999999999997,30.942999999999998,22.298C29.983999999999998,21.886999999999997,29.598999999999997,20.755999999999997,29.598999999999997,20.755999999999997S29.166999999999998,21.012999999999998,29.166999999999998,20.292999999999996C29.166999999999998,19.573999999999995,29.598999999999997,20.755999999999997,30.029999999999998,17.982999999999997C30.029999999999998,17.982999999999997,30.834999999999997,17.735999999999997,30.993,16.009999999999998V14.696999999999997C30.993,14.677999999999997,30.993,14.660999999999998,30.991,14.641999999999998H30.701999999999998C30.701999999999998,14.641999999999998,30.916999999999998,13.687999999999997,30.993,12.648999999999997V11.389999999999997H31Z" opacity="1" stroke-width="0" transform="matrix(2.1875,0,0,2.1875,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M21.14,21.271C20.01,20.787,19.557000000000002,19.452,19.557000000000002,19.452S19.047,19.755000000000003,19.047,18.907C19.047,18.058,19.557000000000002,19.452,20.067,16.178C20.067,16.178,21.48,15.753,21.199,12.238000000000001H20.859C20.859,12.238000000000001,21.709000000000003,8.479000000000001,20.859,7.206000000000001C20.01,5.933000000000002,19.671000000000003,5.085000000000001,17.801000000000002,4.4780000000000015C15.932000000000002,3.8710000000000013,16.614,3.9920000000000018,15.255000000000003,4.054000000000001C13.896000000000003,4.115000000000001,12.763000000000002,4.903000000000001,12.763000000000002,5.326000000000001C12.763000000000002,5.326000000000001,11.914000000000001,5.387000000000001,11.575000000000001,5.751000000000001C11.235000000000001,6.115000000000001,10.669,7.811000000000002,10.669,8.236S10.952,11.510000000000002,11.235000000000001,12.116L10.898000000000001,12.238C10.615000000000002,15.753,12.030000000000001,16.178,12.030000000000001,16.178C12.539000000000001,19.451,13.049000000000001,18.058,13.049000000000001,18.907C13.049000000000001,19.755,12.539000000000001,19.452,12.539000000000001,19.452S12.087000000000002,20.787000000000003,10.955000000000002,21.271C9.823000000000002,21.754,3.540000000000002,24.361,3.0290000000000017,24.907C2.5180000000000016,25.455,2.576000000000002,28,2.576000000000002,28H29.52C29.52,28,29.579,25.455,29.067,24.907C28.556,24.36,22.272,21.754,21.14,21.271Z" stroke-width="0" transform="matrix(2.1875,0,0,2.1875,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></i>
                             </div>
                          </div>
                          <div class="row">
                              <div class="col-xs-6">
                                  <small class="stat-label">Last Week</small>
                                  <h4 id="myTargetElement4.1">56000</h4>
                              </div>
                              <div class="col-xs-6 text-right">
                                  <small class="stat-label">Last Month</small>
                                  <h4 id="myTargetElement4.2">219864</h4>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
  <div class="col-lg-12 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
              <!-- Trans label pie charts strats here-->
              <div class="redbg no-radius" style="color: white;border-radius: 5px;">
                  <div class="panel-body squarebox square_boxs" style="background-color: #EF6F6C;border-radius: 5px;padding: 0px!important; padding: 0px;">
                      <div class="col-xs-12 pull-left nopadmar">
                          <div class="row">
                              <div class="square_box col-xs-7 pull-left">
                                  <span style="font-size: 11px;">Today's Payout</span>
                                  <div class="number" id="myTargetElement2" style="font-size: 29px;font-weight: bold;">$250</div>
                              </div>
                              <div class="col-xs-5 text-left">
                               <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#fff" data-hc="#fff" data-s="70" id="livicon-49" style="width: 70px; height: 70px;"><svg height="70" version="1.1" width="70" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-livicon-49" cz-backup-style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M13.745,18.354V19.419999999999998C13.315999999999999,19.319999999999997,13.101999999999999,19.151999999999997,13.101999999999999,18.913999999999998C13.103,18.732,13.2,18.4,13.745,18.354ZM14.251,20.514V21.662C14.899999999999999,21.599,14.947999999999999,21.275,14.947999999999999,21.073999999999998C14.948,20.865,14.715,20.678,14.251,20.514ZM18,20C18,22.209,16.209,24,14,24S10,22.209,10,20S11.791,16,14,16S18,17.791,18,20ZM15.937,20.951C15.950999999999999,20.705000000000002,15.700999999999999,20.099,15.376,19.912C15.117999999999999,19.764,14.6,19.599,14.296,19.543V18.381C14.6,18.4,14.7,18.6,14.788,18.75C14.870000000000001,18.941,15.017,19.037,15.226,19.037H15.773000000000001C15.755,18.572,15.501000000000001,17.698999999999998,14.297,17.573999999999998L14.3,17C14.3,17,13.941,16.987,13.804,16.987H13.790000000000001V17.573999999999998C12.499,17.598999999999997,12.247000000000002,18.503999999999998,12.231000000000002,18.968999999999998C12.2,19.9,12.6,20.1,13.746,20.4V21.6C13.591000000000001,21.573,13.146,21.5,13.094000000000001,20.911H12.164000000000001C12.201000000000002,21.423000000000002,12.646,22.501,13.746000000000002,22.501V23H14.246000000000002V22.5C15.5,22.5,15.9,21.6,15.937,20.951Z" transform="matrix(2.1875,0,0,2.1875,0,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M13.745,18.354V19.419999999999998C13.315999999999999,19.319999999999997,13.101999999999999,19.151999999999997,13.101999999999999,18.913999999999998C13.103,18.732,13.2,18.4,13.745,18.354ZM14.251,20.514V21.662C14.899999999999999,21.599,14.947999999999999,21.275,14.947999999999999,21.073999999999998C14.948,20.865,14.715,20.678,14.251,20.514ZM18,20C18,22.209,16.209,24,14,24S10,22.209,10,20S11.791,16,14,16S18,17.791,18,20ZM15.937,20.951C15.950999999999999,20.705000000000002,15.700999999999999,20.099,15.376,19.912C15.117999999999999,19.764,14.6,19.599,14.296,19.543V18.381C14.6,18.4,14.7,18.6,14.788,18.75C14.870000000000001,18.941,15.017,19.037,15.226,19.037H15.773000000000001C15.755,18.572,15.501000000000001,17.698999999999998,14.297,17.573999999999998L14.3,17C14.3,17,13.941,16.987,13.804,16.987H13.790000000000001V17.573999999999998C12.499,17.598999999999997,12.247000000000002,18.503999999999998,12.231000000000002,18.968999999999998C12.2,19.9,12.6,20.1,13.746,20.4V21.6C13.591000000000001,21.573,13.146,21.5,13.094000000000001,20.911H12.164000000000001C12.201000000000002,21.423000000000002,12.646,22.501,13.746000000000002,22.501V23H14.246000000000002V22.5C15.5,22.5,15.9,21.6,15.937,20.951Z" transform="matrix(2.1875,0,0,2.1875,21.875,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M13.745,18.354V19.419999999999998C13.315999999999999,19.319999999999997,13.101999999999999,19.151999999999997,13.101999999999999,18.913999999999998C13.103,18.732,13.2,18.4,13.745,18.354ZM14.251,20.514V21.662C14.899999999999999,21.599,14.947999999999999,21.275,14.947999999999999,21.073999999999998C14.948,20.865,14.715,20.678,14.251,20.514ZM18,20C18,22.209,16.209,24,14,24S10,22.209,10,20S11.791,16,14,16S18,17.791,18,20ZM15.937,20.951C15.950999999999999,20.705000000000002,15.700999999999999,20.099,15.376,19.912C15.117999999999999,19.764,14.6,19.599,14.296,19.543V18.381C14.6,18.4,14.7,18.6,14.788,18.75C14.870000000000001,18.941,15.017,19.037,15.226,19.037H15.773000000000001C15.755,18.572,15.501000000000001,17.698999999999998,14.297,17.573999999999998L14.3,17C14.3,17,13.941,16.987,13.804,16.987H13.790000000000001V17.573999999999998C12.499,17.598999999999997,12.247000000000002,18.503999999999998,12.231000000000002,18.968999999999998C12.2,19.9,12.6,20.1,13.746,20.4V21.6C13.591000000000001,21.573,13.146,21.5,13.094000000000001,20.911H12.164000000000001C12.201000000000002,21.423000000000002,12.646,22.501,13.746000000000002,22.501V23H14.246000000000002V22.5C15.5,22.5,15.9,21.6,15.937,20.951Z" transform="matrix(2.1875,0,0,2.1875,13.125,-10.9375)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M26,23.999C25.303,24.328999999999997,26.033,26.904999999999998,25.199,27.61C24.705000000000002,28.214,22.385,27.852,22.136000000000003,27.967C21.419000000000004,27.807,22.439000000000004,25.487,21.597,25.619C20.247,26.006,16.628,26.029,16.085,25.978C15.937000000000001,25.962000000000003,16.085,27.806,15.571000000000002,27.966C14.634000000000002,27.997,13.727000000000002,28.025000000000002,13.353000000000002,27.950000000000003C12.164000000000001,27.983000000000004,12.915000000000001,25.688000000000002,12.612000000000002,25.454000000000004C12.430000000000001,25.395000000000003,11.276000000000002,25.120000000000005,11.100000000000001,25.052000000000003C10.802000000000001,25.130000000000003,10.835,26.652000000000005,10.399000000000001,27.009000000000004C9.670000000000002,27.333000000000006,7.687000000000001,26.999000000000002,7.408000000000001,26.427000000000003C6.963000000000001,24.906000000000002,8.218000000000002,23.243000000000002,7.965000000000002,22.989000000000004C6.980000000000001,22.290000000000003,6.710000000000002,22.737000000000005,6.001000000000001,21.837000000000003C4.574000000000002,20.267000000000003,3.6940000000000013,21.544000000000004,2.397000000000001,20.382000000000005C1.604,19.31,2.2,16.419,2.46,16.08C3.004,15.372999999999998,3.55,16.287999999999997,4,15.085999999999999C4.189,12.447999999999999,5.332,10.052,7.371,8.204999999999998C6.382,7.194,4.564,4.929,6.667,3.733C8.704,3.318,12.581,6.0600000000000005,12.581,6.0600000000000005S14.931,5.3340000000000005,17.519,5.3340000000000005C24.871,5.3340000000000005,29.999,10.178,29.999,16.151C30,19.741,29.211,21.938,26,23.999L26,23.999ZM7.719,10.968C7.719,11.464,8.318,12.187,8.813,12.187C9.309000000000001,12.187,10.369,11.818,10.188,10.843C10.186,10.83,7.719,10.473,7.719,10.968ZM23.25,8.869C21.795,8.372,20.041,7.88,16.924,8.024C16.445,8.024,16.055,8.540999999999999,16.055,9.020999999999999S16.445999999999998,9.889999999999999,16.924,9.889999999999999C16.924,9.889999999999999,20.35,9.573999999999998,23.25,10.735999999999999C23.73,10.735999999999999,24.049,10.338999999999999,24.119,9.863999999999999C24.213,9.243,23.25,8.869,23.25,8.869Z" opacity="1" stroke-width="0" transform="matrix(2.1875,0,0,2.1875,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path></svg></i>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-xs-6">
                                  <small class="stat-label" style="font-size: 11px;">Last Week</small>
                                  <h4 id="myTargetElement2.1" style="font-weight: bold;">$500</h4>
                              </div>
                              <div class="col-xs-6 text-right">
                                  <small class="stat-label" style="font-size: 11px;">Last Month</small>
                                  <h4 id="myTargetElement2.2" class="" style="font-weight: bold;padding: -4px;">$1000</h4>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
    </div>
  </div>
  </section>
  </div>


  <!-- Create Campaign For First Time Users -->
  <div class="row">
      <div class="col-md-12">
        <div class="panel custom-panel-dashboard">
            <div class="panel-body text-center">
              <div class="campaign-wrapper">
                <!-- <form action="https://www.paypal.com/cgi-bin/webscr" target="PPDGFrame" class="standard">
                  <label for="buy">Buy Now:</label>
                  <input type="image" id="submitBtn" value="Pay with PayPal" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif">
                  <input id="type" type="hidden" name="expType" value="mini">
                  <input id="paykey" type="hidden" name="paykey" value="ECDRRWE9L6HCS">
                </form> -->
                  <!--script type="text/javascript" charset="utf-8">
                  var dgFlowMini = new PAYPAL.apps.DGFlowMini({trigger: 'submitBtn'});
                  </script-->
                <h3 class="remove-margin" style="color:#777">Create a campaign now</h3>
                <p style="color:#B1B6BE;width:60%;margin:15px auto 25px;">
                  Let's Create Your First Ad!
                </p>
                <div class="input-group custom-input-group-create-campaign">
                  <input type="text" class="form-control" placeholder="Your Campaign Name..." ng-model="create.campaign_name">
                  <span class="input-group-btn">
                    <button class="btn custom-button" ng-click="createCampaign()">Create Your First Campaign</button>
                  </span>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
</div>

<div hidden>
  <div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 10px;">
        Dashboard
        <small class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s">Control panel.</small>
        </h1>
    </section>
  </div>

  <div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-header">
          <h2>Dashboard for creative</h2>
        </div>
        <div class="panel-body">
          <p>Content dashboad for creative dashboard</p>
        </div>
    </div>
  </div>
</div>
