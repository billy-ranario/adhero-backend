<md-dialog aria-label="Download Preview">
  <md-dialog-content>
      <div class="col-md-8" style="margin-bottom: 10px;">
          <h2>Mobile Plan</h2>
          <h4>Australia, Male, Female</h4>
        <div class="row text-center">
          <h4 style="margin-left: 70px;font-weight: bold;" class="pull-left">Sample photo</h4>
          <img src="../img/brief/aaaa.PNG" style="width: auto;height: 300px;">
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div class="col-md-5 col-md-offset-1">
            <section class="text-center"><button class="btn btn-success" style="background: #70AD47!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Subject</button></section>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;">Elements</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">N/A</span>
              <span style="font-weight: bold;margin-top: 5px;">N/A</span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center" style="margin-bottom: 10px;">
              <p style="text-decoration: underline;font-weight: bold;margin-top:30px;">Objects</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Car</span>
              <span><img src="../img/brief/black.png" style="width: auto;height: 40px;margin-right: 25px;"></span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center">
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Grey</span>
              <span class="pull-right"><div class="bar" style="width: 50px;height: 20px;background: gray;border: 2px solid gray;margin-right: 45px;"></div></span>
            </div>
          </div>
          <div class="col-md-6" style="margin-bottom: 5px;">
            <section class="text-center"><button class="btn btn-success" style="background: #4472C4!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Background</button></section>
            <div>
              <!-- <span class="pull-left" style="font-weight:bold;">Sky <br/><br /> White</span> -->
              <!-- <span class="pull-right" style="margin-right: 25px;"> -->
              <!-- <img src="../img/pbit-assets-v2/cloud.png" style="width: auto;height: 30px;"> -->
              <!-- <br /> -->
              <!-- <img src="../img/pbit-assets-v2/mountain.png" style="width: auto;height: 30px;margin-left: 5px;"></span> -->
              <span class="pull-left" style="font-weight:bold;margin-left: 85px;">Indoor</span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;margin-top:15px;">Colour</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;margin-bottom: 5px;">White</span>
              <span class="pull-right"><div style="width: 67px;height: 20px;background: white;border: 2px solid gray;margin-right: 65px;margin-top: 5px;margin-bottom: 30px;"></div></span>
            </div>
            <div class="clear-both"></div>
            <div>
              <span class="pull-left" style="font-weight:bold;">Temperature: <br/> Warm (6500k) </span>
              <span class="pull-right" style="margin-right: 25px;"><img src="../img/pbit-assets-v2/temp.png" style="width: auto;height: 25px;"></span>
            </div>
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div>
            <section><h5 style="margin:20px;font-weight: bold;font-size: 30px;">Angle &amp; Perspective</h5></section><div>
            <div class="col-md-3">
              <img src="../img/pbit-assets-v2/camera.png" style="width: auto; height: 100px;">
            </div>
              <div class="col-md-6">
                <div class="text-center">
                  <p style="margin:0;">Distance</p>
                  <span>3m</span>
                  <img src="../img/pbit-assets-v2/arrow.png" style="width: auto; height: 20px;">
                </div>
              </div>
              <div class="col-md-3">
                <img src="../img/pbit-assets-v2/man.png" style="width: auto; height: 100px;">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-5" style="padding: 0;">
              <span class="pull-left" style="margin-right: 20px;margin-top: 5px;"><img src="../img/pbit-assets-v2/angle.png" style="width: auto; height: 100px;"></span>
              <span class="pull-right" style="margin-right: 15px;margin-top: 10px;"><h3>Angle: <br /> 60&deg;</h3></span>
            </div>
            <div class="col-md-6 col-md-offset-1" style="padding: 0">
              <h3 class="pull-right text-right" style="margin-top: 20px;"><span>Focal length:</span> <br /><span>70mm</span></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="text-right">
          <h2>Increase sales</h2>
          <h4 class="theme-title">Sports Photo Taking</h4>
        </div>
        <div class="row text-center">
          <h4 style="margin-top: 16px; font-size: 20px;"><b>More Examples</b></h4>
        </div>
        <div class="row text-center" style="margin: 0; width: 0">
          <div class="image-container-dl" style="margin-left: 20px;">
            <img src="../img/brief/bbbb.PNG" style="width: auto;height: 300px;">
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <h4 style="margin-left: 5px;">Misc.</h4>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:10px;">Emotions to evoke:</p>
          </section>
          <div>
            <span class="pull-left" style="font-weight: bold;margin-left: 5px;">Awe</span>
            <span class="pull-right"><img src="../img/pbit-assets-v2/emoticons.png" style="width: auto;height: 35px;margin-right: 25px;"></span>
          </div>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Aspect ratio:</p>
            <p class="pull-left" style="font-weight: bold;text-align: left;margin-left: 5px;">16 : 9</p>
          </section>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Lightning:</p>
            <span class="pull-left" style="margin: 5px;font-weight: bold;"><p>Artificial</p></span>
            <p class="pull-right"><img src="../img/pbit-assets-v2/sun.png" style="width: auto;height: 30px;margin-right: 30px;"></p>
          </section>
          <div class="clear-both"></div>
        </div>
      </div>
      <div class="clear-both"></div>
      <div class="row text-right" style="margin: 0;">
        <a href="{{ url }}downloadBrief/0" class="btn btn-primary" style="margin-top: 30px;margin-right: 20px;margin-bottom: 10px;">Download</a>
      </div>
  </md-dialog-content>
</md-dialog>












<md-dialog aria-label="Download Preview">
  <md-dialog-content>
      <div class="col-md-8" style="margin-bottom: 10px;">
          <h2>Mobile Plan</h2>
          <h4>Australia, Male, Female</h4>
        <div class="row text-center">
          <h4 style="margin-left: 70px;font-weight: bold;" class="pull-left">Sample photo</h4>
          <img src="../img/brief2/main-img.PNG" style="width: auto;height: 300px;">
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div class="col-md-5 col-md-offset-1">
            <section class="text-center"><button class="btn btn-success" style="background: #70AD47!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Subject</button></section>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;">Elements</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Human</span>
              <span><img src="../img/brief2/black(1).png" style="width: auto;height: 40px;margin-right: 25px;"></span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center" style="margin-bottom: 10px;">
              <p style="text-decoration: underline;font-weight: bold;margin-top:30px;">Objects</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Ball</span>
              <span><img src="../img/brief2/circle.PNG" style="width: auto;height: 40px;margin-right: 25px;"></span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center">
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">White</span>
              <span class="pull-right"><div class="bar" style="width: 50px;height: 20px;background: white;border: 2px solid gray;margin-right: 45px;"></div></span>
            </div>
          </div>
          <div class="col-md-6" style="margin-bottom: 5px;">
            <section class="text-center"><button class="btn btn-success" style="background: #4472C4!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Background</button></section>
            <div>
              <!-- <span class="pull-left" style="font-weight:bold;">Sky <br/><br /> White</span> -->
              <!-- <span class="pull-right" style="margin-right: 25px;"> -->
              <!-- <img src="../img/pbit-assets-v2/cloud.png" style="width: auto;height: 30px;"> -->
              <!-- <br /> -->
              <!-- <img src="../img/pbit-assets-v2/mountain.png" style="width: auto;height: 30px;margin-left: 5px;"></span> -->
              <span class="pull-left" style="font-weight:bold;margin-left: 85px;">Stadium</span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;margin-top:15px;">Colour</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;margin-bottom: 5px;">Green</span>
              <span class="pull-right"><div style="width: 67px;height: 20px;background: green;border: 2px solid gray;margin-right: 65px;margin-top: 5px;margin-bottom: 30px;"></div></span>
            </div>
            <div class="clear-both"></div>
            <div>
              <span class="pull-left" style="font-weight:bold;">Temperature: <br/> Cool (6500k) </span>
              <span class="pull-right" style="margin-right: 25px;"><img src="../img/pbit-assets-v2/temp.png" style="width: auto;height: 25px;"></span>
            </div>
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div>
            <section><h5 style="margin:20px;font-weight: bold;font-size: 30px;">Angle &amp; Perspective</h5></section><div>
            <div class="col-md-3">
              <img src="../img/pbit-assets-v2/camera.png" style="width: auto; height: 100px;">
            </div>
              <div class="col-md-6">
                <div class="text-center">
                  <p style="margin:0;">Distance</p>
                  <span>10m</span>
                  <img src="../img/pbit-assets-v2/arrow.png" style="width: auto; height: 20px;">
                </div>
              </div>
              <div class="col-md-3">
                <img src="../img/pbit-assets-v2/man.png" style="width: auto; height: 100px;">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-5" style="padding: 0;">
              <span class="pull-left" style="margin-right: 20px;margin-top: 5px;"><img src="../img/pbit-assets-v2/angle.png" style="width: auto; height: 100px;"></span>
              <span class="pull-right" style="margin-right: 15px;margin-top: 10px;"><h3>Angle: <br /> 90&deg;</h3></span>
            </div>
            <div class="col-md-6 col-md-offset-1" style="padding: 0">
              <h3 class="pull-right text-right" style="margin-top: 20px;"><span>Focal length:</span> <br /><span>24mm</span></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="text-right">
          <h2>Increase sales</h2>
          <h4>Rugby Action Shots</h4>
        </div>
        <div class="row text-center">
          <h4 style="margin-top: 16px; font-size: 20px;"><b>More Examples</b></h4>
        </div>
        <div class="row text-center" style="margin: 0; width: 0">
          <div class="image-container-dl" style="margin-left: 35px;">
            <img src="../img/brief2/right-img.PNG" style="width: auto;height: 300px;">
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <h4 style="margin-left: 5px;">Misc.</h4>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:10px;">Emotions to evoke:</p>
          </section>
          <div>
            <span class="pull-left" style="font-weight: bold;margin-left: 5px;">Excitement</span>
            <span class="pull-right"><img src="../img/pbit-assets-v2/emoticons.png" style="width: auto;height: 35px;margin-right: 25px;"></span>
          </div>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Aspect ratio:</p>
            <p class="pull-left" style="font-weight: bold;text-align: left;margin-left: 5px;">16 : 9</p>
          </section>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Lightning:</p>
            <span class="pull-left" style="margin: 5px;font-weight: bold;"><p>Natural</p></span>
            <p class="pull-right"><img src="../img/pbit-assets-v2/sun.png" style="width: auto;height: 30px;margin-right: 30px;"></p>
          </section>
          <div class="clear-both"></div>
        </div>
      </div>
      <div class="clear-both"></div>
      <div class="row text-right" style="margin: 0;">
        <a href="{{ url }}downloadBrief/1" class="btn btn-primary" style="margin-top: 30px;margin-right: 20px;margin-bottom: 10px;">Download</a>
      </div>
  </md-dialog-content>
</md-dialog>










<md-dialog aria-label="Download Preview">
  <md-dialog-content>
      <div class="col-md-8" style="margin-bottom: 10px;">
          <h2>Mobile Plan</h2>
          <h4>Australia, Male, Female</h4>
        <div class="row text-center">
          <h4 style="margin-left: 70px;font-weight: bold;" class="pull-left">Sample photo</h4>
          <img src="../img/brief3/main-img2.PNG" style="width: auto;height: 300px;">
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div class="col-md-5 col-md-offset-1">
            <section class="text-center"><button class="btn btn-success" style="background: #70AD47!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Subject</button></section>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;">Elements</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Human</span>
              <span><img src="../img/brief3/black(1).png" style="width: auto;height: 40px;margin-right: 25px;"></span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center" style="margin-bottom: 10px;">
              <p style="text-decoration: underline;font-weight: bold;margin-top:30px;">Objects</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Guitar</span>
              <span><img src="../img/brief3/music.png" style="width: auto;height: 40px;margin-right: 25px;"></span>
            </div>
            <div class="clear-both"></div>
            <!-- <div class="text-center">
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;">Red</span>
              <span class="pull-right"><div class="bar" style="width: 50px;height: 20px;background: red;border: 2px solid gray;margin-right: 45px;"></div></span>
            </div> -->
          </div>
          <div class="col-md-6" style="margin-bottom: 5px;">
            <section class="text-center"><button class="btn btn-success" style="background: #4472C4!important;border: none;border-radius: 0px;margin: 5px;margin-bottom: 20px;">Background</button></section>
            <div>
              <!-- <span class="pull-left" style="font-weight:bold;">Sky <br/><br /> White</span> -->
              <!-- <span class="pull-right" style="margin-right: 25px;"> -->
              <!-- <img src="../img/pbit-assets-v2/cloud.png" style="width: auto;height: 30px;"> -->
              <!-- <br /> -->
              <!-- <img src="../img/pbit-assets-v2/mountain.png" style="width: auto;height: 30px;margin-left: 5px;"></span> -->
              <span class="pull-left" style="font-weight:bold;margin-left: 85px;"> </span>
            </div>
            <div class="clear-both"></div>
            <div class="text-center">
              <p style="text-decoration: underline;font-weight: bold;margin-top:15px;">Colour</p>
              <span class="pull-left" style="font-weight: bold;margin-top: 5px;margin-bottom: 5px;">Red</span>
              <span class="pull-right"><div style="width: 67px;height: 20px;background: red;border: 2px solid gray;margin-right: 65px;margin-top: 5px;margin-bottom: 30px;"></div></span>
            </div>
            <div class="clear-both"></div>
            <div>
              <span class="pull-left" style="font-weight:bold;">Temperature: <br/> Cool (6500k) </span>
              <span class="pull-right" style="margin-right: 25px;"><img src="../img/pbit-assets-v2/temp.png" style="width: auto;height: 25px;"></span>
            </div>
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <div>
            <section><h5 style="margin:20px;font-weight: bold;font-size: 30px;">Angle &amp; Perspective</h5></section><div>
            <div class="col-md-3">
              <img src="../img/pbit-assets-v2/camera.png" style="width: auto; height: 100px;">
            </div>
              <div class="col-md-6">
                <div class="text-center">
                  <p style="margin:0;">Distance</p>
                  <span>5m</span>
                  <img src="../img/pbit-assets-v2/arrow.png" style="width: auto; height: 20px;">
                </div>
              </div>
              <div class="col-md-3">
                <img src="../img/pbit-assets-v2/man.png" style="width: auto; height: 100px;">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-5" style="padding: 0;">
              <span class="pull-left" style="margin-right: 20px;margin-top: 5px;"><img src="../img/pbit-assets-v2/angle.png" style="width: auto; height: 100px;"></span>
              <span class="pull-right" style="margin-right: 15px;margin-top: 10px;"><h3>Angle: <br /> 70&deg;</h3></span>
            </div>
            <div class="col-md-6 col-md-offset-1" style="padding: 0">
              <h3 class="pull-right text-right" style="margin-top: 20px;"><span>Focal length:</span> <br /><span>135mm</span></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="text-right">
          <h2>Increase sales</h2>
          <h4>Acoustic Guitar</h4>
        </div>
        <div class="row text-center">
          <h4 style="margin-top: 16px; font-size: 20px;"><b>More Examples</b></h4>
        </div>
        <div class="row text-center" style="margin: 0; width: 0">
          <div class="image-container-dl" style="margin-left: 35px;">
            <img src="../img/brief3/right-img.PNG" style="width: auto;height: 300px;">
          </div>
        </div>
        <div class="row" style="margin: 0;border: 1px solid rgb(225, 225, 225);border-radius: 3px;margin-left: 5px;margin-top: 5px;">
          <h4 style="margin-left: 5px;">Misc.</h4>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:10px;">Emotions to evoke:</p>
          </section>
          <div>
            <span class="pull-left" style="font-weight: bold;margin-left: 5px;">Serenity</span>
            <span class="pull-right"><img src="../img/pbit-assets-v2/emoticons.png" style="width: auto;height: 35px;margin-right: 25px;"></span>
          </div>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Aspect ratio:</p>
            <p class="pull-left" style="font-weight: bold;text-align: left;margin-left: 5px;">16 : 9</p>
          </section>
          <div class="clear-both"></div>
          <section class="text-center">
            <p style="font-weight: bold;text-decoration: underline;padding:30px;padding-bottom: 10px;">Lightning:</p>
            <span class="pull-left" style="margin: 5px;font-weight: bold;"><p>Artificial</p></span>
            <p class="pull-right"><img src="../img/pbit-assets-v2/sun.png" style="width: auto;height: 30px;margin-right: 30px;"></p>
          </section>
          <div class="clear-both"></div>
        </div>
      </div>
      <div class="clear-both"></div>
      <div class="row text-right" style="margin: 0;">
        <a href="{{ url }}downloadBrief/2" class="btn btn-primary" style="margin-top: 30px;margin-right: 20px;margin-bottom: 10px;">Download</a>
      </div>
  </md-dialog-content>
</md-dialog>
