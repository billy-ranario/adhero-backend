<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size:15px;margin:10px;">
      Choose Ad
      <br />
      <small>Comment and Pick your ads for A/B test</small>
      </h1>
      <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin:0;">
        <li><a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
        </li>
      </ol>
    </section>
</div>


<div class="row" style="margin:30px 0 0;">
<div class="col-md-12 remove-side-padding" style="margin-bottom:30px;">
   <!--  <md-content>
      <md-tabs md-dynamic-height md-border-bottom>
        <md-tab label="one">
          <md-content class="md-padding">
            <h1 class="md-display-2">Tab One</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue. Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus. In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec, feugiat ultricies mi.</p>
          </md-content>
        </md-tab>
        <md-tab label="two">
          <md-content class="md-padding">
            <h1 class="md-display-2">Tab Two</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue. Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus. In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec, feugiat ultricies mi. Aliquam erat volutpat. Nam placerat, tortor in ultrices porttitor, orci enim rutrum enim, vel tempor sapien arcu a tellus. Vivamus convallis sodales ante varius gravida. Curabitur a purus vel augue ultrices ultricies id a nisl. Nullam malesuada consequat diam, a facilisis tortor volutpat et. Sed urna dolor, aliquet vitae posuere vulputate, euismod ac lorem. Sed felis risus, pulvinar at interdum quis, vehicula sed odio. Phasellus in enim venenatis, iaculis tortor eu, bibendum ante. Donec ac tellus dictum neque volutpat blandit. Praesent efficitur faucibus risus, ac auctor purus porttitor vitae. Phasellus ornare dui nec orci posuere, nec luctus mauris semper.</p>
            <p>Morbi viverra, ante vel aliquet tincidunt, leo dolor pharetra quam, at semper massa orci nec magna. Donec posuere nec sapien sed laoreet. Etiam cursus nunc in condimentum facilisis. Etiam in tempor tortor. Vivamus faucibus egestas enim, at convallis diam pulvinar vel. Cras ac orci eget nisi maximus cursus. Nunc urna libero, viverra sit amet nisl at, hendrerit tempor turpis. Maecenas facilisis convallis mi vel tempor. Nullam vitae nunc leo. Cras sed nisl consectetur, rhoncus sapien sit amet, tempus sapien.</p>
            <p>Integer turpis erat, porttitor vitae mi faucibus, laoreet interdum tellus. Curabitur posuere molestie dictum. Morbi eget congue risus, quis rhoncus quam. Suspendisse vitae hendrerit erat, at posuere mi. Cras eu fermentum nunc. Sed id ante eu orci commodo volutpat non ac est. Praesent ligula diam, congue eu enim scelerisque, finibus commodo lectus.</p>
          </md-content>
        </md-tab>
        <md-tab label="three">
          <md-content class="md-padding">
            <h1 class="md-display-2">Tab Three</h1>
            <p>Integer turpis erat, porttitor vitae mi faucibus, laoreet interdum tellus. Curabitur posuere molestie dictum. Morbi eget congue risus, quis rhoncus quam. Suspendisse vitae hendrerit erat, at posuere mi. Cras eu fermentum nunc. Sed id ante eu orci commodo volutpat non ac est. Praesent ligula diam, congue eu enim scelerisque, finibus commodo lectus.</p>
          </md-content>
        </md-tab>
      </md-tabs>
    </md-content> -->
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist" id="preview-ad-tabs" style="background:#E7EAEF;text-align:center;">
    <li tabContent="tab-1" role="presentation">
        <a href="javascript:void()" aria-controls="tab-1" role="tab" data-toggle="tab">Ready to Launch Campaign</a>
    </li>
    <li tabContent="tab-2" role="presentation" class="active">
        <a href="javascript:void()" aria-controls="tab-2" role="tab" data-toggle="tab">Pending Campaign</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content" style="background:#fff">
    <div role="tabpanel" class="tab-pane" id="tab-1">
        <div class="panel-body" style="margin-top: 10px; height: 430px; margin-bottom: 15px;margin-right:10px;">
          <div class="panel-body panel-list" ng-repeat="campaign in campaignList | filter: 'in-progress'" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
            <div class="col-md-2" style="margin-left: -20px;">
              <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 80px; width: 100px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
           </div>
          <div class="col-md-4 text-center">
            <p style="font-size:16px;text-shadow:1px 1px 3px #CCC;font-weight:bold;color:#317298;"> {{ campaign.campaign_name }} </p>           
              <span>3</span>
              <br>
              <span>Variations</span>
          </div>
            <div class="col-md-4">
              <p style="margin-left: -5px;">Brief Submitted: <span ng-bind="campaign.date_created | date"></span> </p>
              <p style="margin-left: -5px;">Last Update: <span ng-bind="campaign.date_created | date"></span> </p>
              <button class="btn btn-xs btn-success">1st feedback</button>
            </div>
            <div class="col-md-2 text-center">
              <button class="btn btn-default" disabled>Ready</button>
              <br>
              <a class="btn btn-primary" style="margin-top: 15px;" ng-click="goChoose(campaign.campaign_name)">Choose Ads</a>
            </div>
          </div>
        </div>
      </div>

    <div role="tabpanel" class="tab-pane active" id="tab-2">
      
            <div class="panel-body" style="margin-top: 10px; height: 430px; margin-bottom: 15px;margin-right:10px;">
                <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;"
                     ng-repeat="campaign in campaignList | filter: 'pending'">
                  <div class="col-md-2" style="margin-left: -20px;">
                    <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 80px; width: 100px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                 </div>
                <div class="col-md-4 text-center">
                  <p style="font-size:16px;text-shadow:1px 1px 3px #CCC;font-weight:bold;color:#317298;" ng-bind="campaign.campaign_name"> </p>           
                    <span>3</span>
                    <br>
                    <span>Variations</span>
                </div>
                  <div class="col-md-4">
                    <p style="margin-left:-5px;">Brief Submitted: <span ng-bind="campaign.date_created | date"></span> </p>
                  </div>
                  <div class="col-md-2" style="margin-top: -20px;">
                    <button class="btn btn-success" style="margin-top: 15px;">Not Paid</button>
                    <br />
                    <a class="btn custom-btn-pending" style="margin-top: 15px;"
                       ng-click="doPayment(campaign.id)">Deposit</a>
                  </div>
                </div>
            </div>
           </div>
        </div>     
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="paypalDeposit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <img src="../../img/credit/paypal.png">
      </div>
      <div class="modal-body">
        <section style="width: 100%;height: auto;background-color: #E1E1E1;">
          <span style="font-size: 30px;padding: 10px;color: #777777;">Adhero Ads</span>
          <span class="pull-right" style="font-size: 20px;padding: 10px;color: #504949;">Total: <span style="color: #965454;font-size: 25px;">$250</span></span>
        </section>
        <section>
          <div class="form-group">
            <div class="col-md-6">
              <label>First Name</label>
              <input type="text" class="form-control">
            </div>
            <div class="col-md-6">
              <label>Last Name</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
              <div class="col-md-8">
                <label>Cardholder Address</label>
                <input type="text" class="form-control">
              </div>
              <div class="col-md-4">
                <label>Card Expiry</label>
                <input type="date" class="form-control">
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
              <label>Card Number</label>
              <input type="text" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Security Code</label>
              <input type="number" class="form-control">
            </div>
          </div>
        </section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn custom-button pull-right" ng-click="deposit(id_campaign)">Pay</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $( 'ul#preview-ad-tabs li' ).click( function () {
        var element = $( this );
        var tabID   = element.attr( 'tabContent' );
        // Remove Active Class
        $( 'ul#preview-ad-tabs li' ).removeClass( 'active' );
        $( '.tab-pane' ).removeClass( 'active' );
        
        // Add Active Class to specific element
        $( '#' + tabID ).addClass( 'active' );
        element.addClass( 'active' );
    } );
</script>
