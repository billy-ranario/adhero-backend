<div predict-view>
<!-- <div class="col-md-2" style="width: 20%;">
	<div class="navigation">
		<ul class="nav">
			<div class="triangle" style="background:#000;"></div>
			<li><a ui-sref="home"><i class="fa fa-home"></i></a></li>
			<li style="background:#000;"><a ui-sref="saved-results"><i class="fa fa-flask"></i></a></li>
			<li><a ui-sref="summary"><i class="fa fa-bar-chart"></i></a></li>
		</ul>
	</div>
</div>
 -->
<div class="sub-content-wrapper col-md-9">
	<div class=" predict-view-wrapper">
		<section class="alert-msgs-box">
			<div class="alert alert-info msg msg002" id="predict-msg002">
				<p>
					<strong>
						Our image selection algorithm has assessed all of the images that you have used in the past and what your audience is engaging
						with to determine what will work best for your new campaign. 
					</strong>
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div>
			<!-- <div class="alert alert-info msg msg002" id="predict-msg002">
				<p>
					<strong>Predict your next campaigns performance! </strong>Give us the details below and let us do the work.
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div> -->
			<div class="alert alert-warning msg msg01" id="predict-msg001">
				<p>
					<strong>First Time?</strong>
					Let us show you how it works.
					<button type="button" class="close" ng-click="close('msg001')"><span aria-hidden="true">&times;</span></button>
				</p>
				<button class="btn btn-sm btn-warning btn-yellow demo-btn" ng-click="useraction('demo')">Demo</button>
				<button class="btn btn-sm btn-default expert-btn" ng-click="useraction('skip')">Skip</button>
			</div>
		</section>

		<section class="loading-predict-container" hidden>
			<div class="progress">
			  <div class="progress-bar progress-bar-info progress-bar-striped" id="predict-progress"role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			    <span class="sr-only">20% Complete</span>
			  </div>
			</div>
			<h4>Estimated time... 24 hours</h4>
			<section class="predict-quote-container">
				<div class="col-md-12">
					<div class="col-md-2">
						<img src="../img/avatar.png" class="predict-img-qoute img-responsive">
					</div>
					<div class="col-md-10">
						<div class="panel panel-default" style="border-radius: 10px; margin-top: 10px;border-color: none;">
							<div class="panel-body predict-panel-default">
								<i class="glyphicon glyphicon-triangle-left custom-glyphicon-triangle-left-predict"></i>
								<section class="predict-qoute-text">
									<h3>"During the last 6 months, your audience has been responding to content that is "inspirational"</h3>
								</section>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="row text-center">
				<section class="btn-container">
					<a href="javascript:void(0)" ui-sref="home" class="btn back-to-dasboard-btn">Back to Dashboard</a>
					<a href="javascript:void(0)" class="btn demo-images-btn" ng-click="demoImages()">Demo Images</a>
				</section>
			</section>
		</section>
		
		<section class="content-manager">
			<div class="row">
				<div class="col-md-7 headline-wrapper">
					<h3 class="headline-text">What is your message about?</h3>
					<p>Enter the headline you plan to use</p>
					<textarea class="form-control headline-text-input" ng-model="predictObject.headingtext" placeholder="Enter headline text here..."></textarea>			
				</div>
				<div class="col-md-5 media-platform-wrapper">
					<h3 class="headline-text">Pick what libraries you want to receive recommendations from:</h3>
					<div class="target-recommendatations">
						<md-checkbox ng-click="toggleBoxLib( 'stock_photos' )" ng-if="!predictObject.demo" value="stock_photos">Stock photos</md-checkbox>						
 						<md-checkbox ng-click="toggleBoxLib( 'user_generated' )" ng-if="!predictObject.demo" value="user_generated">User generated content</md-checkbox>	
 						<md-checkbox ng-click="toggleBoxLib( 'user_library' )" ng-if="!predictObject.demo" value="user_library">Your library ( Upload )</md-checkbox>

 						<!-- Demo Purposes -->
						<md-checkbox ng-click="toggleBoxLib( 'stock_photos' )"  ng-checked="predictObject.demo" ng-if="predictObject.demo" value="stock_photos">Stock photos</md-checkbox>						
 						<md-checkbox ng-click="toggleBoxLib( 'user_generated' )"  ng-checked="predictObject.demo" ng-if="predictObject.demo" value="user_generated">User generated content</md-checkbox>	
 						<md-checkbox ng-click="toggleBoxLib( 'user_library' )"  ng-if="predictObject.demo" value="user_library">Your library ( Upload )</md-checkbox>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="target-wrapper">
						<h3 class="headline-text">Target Customer:</h3>
						<div class="target-option-wrapper">
							<!-- Target Customers -->
							<div class="btn-group btn-action geography-option">
								<select class="form-control btn btn-primary" ng-model="predictObject.targetcustomer">
									<option value="Australia">Australia</option>
								</select>
							</div>
							<div class="btn-group btn-action geography-option">
								<select class="form-control btn btn-primary" ng-model="predictObject.targetAge">
									<option value="Kid">Kid</option>
									<option value="Young Adult">Young Adult</option>
									<option value="Kid and Young Adult">Kid and Young Adult</option>
									<option value="Mature Adult">Mature Adult</option>
									<option value="Teen and Young Adult">Teen and Young Adult</option>
									<option value="Elderly">Elderly</option>
									<option value="Teen Adult, Young Adult and Elderly">Teen Adult, Young Adult and Elderly</option>
								</select>
							</div>
						</div>	
						<div class="target-gender">
							<md-checkbox ng-if="!predictObject.demo" ng-click="toggleBoxGender( 'Male' )" value="Male">Male</md-checkbox>						
	 						<md-checkbox ng-if="!predictObject.demo" ng-click="toggleBoxGender( 'Female' )" value="Female">Female</md-checkbox>						

	 						<!-- Demo Purposes -->
							<md-checkbox ng-checked="predictObject.demo" ng-if="predictObject.demo" ng-click="toggleBoxGender( 'Male' )" value="Male">Male</md-checkbox>						
	 						<md-checkbox ng-if="predictObject.demo" ng-click="toggleBoxGender( 'Female' )" value="Female">Female</md-checkbox>						
						</div>
						
					</div>
					<div class="media-platform-wrapper">
						<h3 class="headline-text">Channel to Publish:</h3>
						<div class="social-media-icons">
							<a href="javascript:void(0);" ng-click="socialPlatform( 'facebook' )" id="sm-fb" ><img src="../img/sprites/facebook.png" class="icon-media"></a>
							<a href="javascript:void(0);" ng-click="socialPlatform( 'instagram' )" id="sm-insta" style="width:58px;"><img src="../img/sprites/Instagram-icon.png" class="icon-media"></a>
							<!-- <a href="javascript:void(0);" ng-click="socialPlatform( 'linkedin' )" id="sm-in" ><img src="../img/sprites/linkedin.png" class="icon-media"></a> -->
						</div>
					</div>	
					<!-- <button class="btn btn-success pull-right btn-create-predict" ng-click="createPredict()">Submit</button>	 -->			<div class="">
					<div class="form-group">
						<h3 class="headline-text">Product:</h3>
						<select class="btn btn-primary" ng-model="predictObject.product">
							<option value="Prepaid/Postpaid">Prepaid/Postpaid</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<h3 class="headline-text">Season / Event:</h3>
						<div class="optionlist-wrapper geography-option">
							<select class="btn btn-primary" ng-change="selectEvent()" ng-model="predictObject.event">
								<option value="Spring">Spring</option>
								<option value="Summer">Summer</option>
								<option value="Autumn">Autumn</option>
								<option value="Winter">Winter</option>
								<option value="Australia Day">Australia Day</option>
								<option value="Australian Open">Australian Open</option>
								<option value="Formula One">Formula One</option>
								<option value="Others">Others</option>
							</select>
							<div class="input-other" ng-if="showInput" style="width:300px;">
								<input type="text" class="form-control" ng-model="predictObject.otherEvent" placeholder="Specify Season/Event">
							</div>
						</div>
					</div>
					<div class="form-group">
						<h3 class="headline-text">Campaign Type:</h3>
						<select class="btn btn-primary" ng-model="predictObject.campaigntype">
							<option value="Promote an offer">Promote an offer</option>
							<option value="Upgrade to new plan">Upgrade to new plan</option>
							<option value="Get new subscriber">Get new subscriber</option>
						</select>
						<button class="btn btn-success pull-right" ng-click="createPredict()">Save this experiment</button>	
					</div>
				</div>
			</div>
		</section>
	</div>
</div>