<div class="col-md-12" style="padding: 15px 55px;">
	<div class="col-md-12">
		<div class="button-opt text-center" style="margin-top:30px;">
			<button id="influencers" class="btn btn-lg btn-theme active" ng-click="actionView( 'influencers' )">Influencers</button>
			<button id="audiences" class="btn btn-lg btn-theme" ng-click="actionView( 'audiences' )">Audiences</button>
		</div>
		<div class="option-bar overflow" ng-if="defaultView" style="margin-top:30px;">
			<span class="pull-left eng-title">Your Influencers</span>
			<div class="dropdown-option pull-right">
				View By: 
				<select>
					<option value="Followers">Followers</option>
					<option value="Option 1">Option 1</option>
					<option value="Option 2">Option 2</option>
				</select>
			</div>
		</div>
		<br>

		<div class="image-list" style="margin-top:30px;text-align:center;" ng-if="defaultView">
			<div class="col-md-1"></div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/1">
					<img src="../img/pbit-assets-v4/Amanda Wong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Amanda Wong</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/2">
					<img src="../img/pbit-assets-v4/Andrea Chong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Andrea Chong</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/3">
					<img src="../img/pbit-assets-v4/Bella Koh.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Bella Koh</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/4">
					<img src="../img/pbit-assets-v4/Nellie Lim.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Nellie Lim</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/5">
					<img src="../img/pbit-assets-v4/Nicole Wong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Nicole Wong</span>
					</div>
				</a>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="image-list" style="margin-top:30px;overflow: hidden;background: #fff;padding: 30px 15px;box-shadow: 1px 2px 1px #BBB;" ng-if="!defaultView">
			<div class="col-md-4">
				<div class="col-md-12 text-center">
					<label>Audience Affinity</label> <br>
					<div id="doughnut-graph" style="margin-left: -40px; width: 100%; margin-top: -80px;"></div>
				</div>
				<div class="col-md-12" style="position:relative;top:-50px;">
					<div class="col-md-12">
						<h4 style="color: #9E9FA4;">Theme</h4>
						<table class="table custom-table">
							<tbody>
								<tr ng-repeat="list in labels">
									<td><span class="legend-box" style="background: {{ list.color }};"></span> <span ng-bind="list.label" style="margin-left: 10px;"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="table-responsive">
					<table class="table audiences">
						<thead>
							<tr>
								<th>Ranking</th>
								<th>Username</th>
								<th>Followers</th>
								<th>Posts</th>
								<th>Affinities</th>
								<th>Other Interests</th>
							</tr>
						</thead>
						<tbody >
							<tr ng-repeat="profile in profileInfo">
								<td>
									<span ng-bind="$index + 1"></span> &nbsp;
									<img src="{{profile.img_url}}" alt="no image" class="audience-img">
								</td>
								<td>
									<span ng-bind="profile.username"></span>
								</td>
								<td>
									<span ng-bind="profile.followers"></span>
								</td>
								<td>
									<span ng-bind="profile.posts"></span>
								</td>
								<td>
									<span ng-bind="profile.affinities"></span>
								</td>
								<td>
									<span ng-bind="profile.other_int"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>