
<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
       <!--  <a ui-sref="campaign-report" class="wow fadeInUp pull-left" data-wow-duration="0.5s" data-wow-delay="0.5s" class="pull-left" style="margin-top:10px;margin-left:15px;">
          <i class="fa fa-arrow-circle-left"></i>
        </a> -->
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px;margin: 10px 0 0 10px;">
        Campaign Report
        <br />
        <small>Report of your Ad Creation Campaign using AdHero</small>
        </h1>
        <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin: 0;">
          <li><a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
        </ol>
    </section>
</div>
<div class="row">
    <div class="right-panel-content">
      <div class="col-md-8 col-sm-8">
          <div class="panel panel-default active-campaign-panel" style="border-radius: 0px;margin-top: 10px;">
              <div class="panel-head text-left" style="border:2px solid #16ad8f;background:#16ad8f;color: #fff;">
                  <p style="padding: 10px;font-size: 20px; margin:0;">Active Campaigns</p>
              </div>
              <div class="panel-body" style="margin-top: 10px; height: 430px; overflow: scroll; overflow-x: hidden;margin-bottom: 15px;margin-right:10px;">
                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <!-- <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;" ng-click="viewDetails()">DETAIL REPORT</button> -->
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                     <!--  <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;">DETAIL REPORT</button> -->
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                     <!--  <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;">DETAIL REPORT</button> -->
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                     <!--  <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;">DETAIL REPORT</button> -->
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                     <!--  <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;">DETAIL REPORT</button> -->
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                     <!--  <button class="btn btn-primary btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #317298;">DETAIL REPORT</button> -->
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- <div class="col-md-4" style="margin-top: 10px;">
          <div class="panel panel-default" style="border-radius: 0px;">
              <div class="panel-body"><p style="padding: 85px;">Active Campaigns</p></div>
          </div>
          <div class="panel panel-default" style="border-radius: 0px;">
              <div class="panel-body"><p style="padding: 81px;">Pending Campaigns</p></div>
          </div>
      </div>   -->
      <div class="col-md-8 col-sm-8">
          <div class="panel panel-default pending-campaign-panel" style="border-radius: 0px;margin-top: 10px;">
              <div class="panel-head text-left" style="border:2px solid #3C8DBC;background: #3C8DBC;color:#fff;">
                  <p style="padding:10px;font-size:20px;margin:0;">Pending Campaigns</p>
              </div>
              <div class="panel-body" style="margin-top: 10px; height: 430px; overflow: scroll; overflow-x: hidden;margin-bottom: 15px;margin-right:10px;">
                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>

                  <div class="panel-body panel-list" style="background-color:#F1F1F1;border:1px solid #E7E8E8; margin-bottom: 5px;">
                    <div class="col-md-2" style="margin-left: -20px;">
                      <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 70px; width: 70px; display: block;border: 1px solid #CCC; /* border-radius: 50%; */" class="__web-inspector-hide-shortcut__">
                   </div>
                  <div class="col-md-4">
                    <p style="font-size:16px;font-weight:bold;color:#317298;"> Campaign Name </p>           
                      <span>3</span>
                      <span style="padding-left: 85px;">1,500</span>
                      <br>
                      <span>Variations</span><span style="padding-left: 35px;">Vistors</span>
                  </div>
                    <div class="col-md-4">
                      <p style="margin-left: -5px;">Date Started: 28/08/08 23:50 </p>
                    <span style="margin-left: 70px;">7150</span><br>
                    <span style="margin-left: 50px;">Conversions</span>
                  </div>
                    <div class="col-md-2">
                      <button class="btn btn-success btn-large" style="margin-top: 15px;border:none;margin-left: -5px;border-bottom:4px solid #069252;">LAUNCH</button>
                    </div>
                  </div>
              </div>
          </div>
      </div>
     <!--  <div class="col-md-4" style="margin-top: 10px;">
          <div class="panel panel-default" id="campaign-result" style="border-radius: 0px;">
              <div class="panel-header">
                  <div class="form-group">
                  <label>Archieve:</label>
                  <div class="input-group pull-right" style="width: 240px;">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="campaign-result-date">
                  </div>
                </div>
              </div>
              <table class="table table-bordered table-hovered">
              <thead>
              <tr>
                <th style="width:30px">#</th>
                <th>Ad Name</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>1</td>
                <td>Update software</td>
                <td><button class="btn btn-success btn-xs">Completed</button></td>
              </tr>
              <tr>
                <td>2</td>
                <td>Clean database</td>
                <td><button class="btn btn-success btn-xs">Pending</button></td>
              </tr>
              <tr>
                <td>3</td>
                <td>Cron job running</td>
                <td><button class="btn btn-danger btn-xs">Cancel</button></td>
              </tr>
              <tr>
                <td>4</td>
                <td>Fix and squish bugs</td>
                <td><button class="btn btn-danger btn-xs">Cancel</button></td>
              </tr>
              <tr>
                <td>5</td>
                <td>Fix and squish bugs</td>
                <td><button class="btn btn-primary btn-xs">Active</button></td>
              </tr>
            </tbody>
          </table>
              <div class="panel-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                  <li><a href="#">«</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">»</a></li>
                </ul>
              </div>
          </div>
      </div> -->
    </div>
</div>

<script type="text/javascript">
    $(function( ){
        $('#campaign-result-date').daterangepicker();
    });
</script>

<script type="text/javascript" src="assets/js/transformicons.js"></script> 
<script>transformicons.add('.tcon')</script>  
<script type="text/javascript">
  !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});
</script> 
