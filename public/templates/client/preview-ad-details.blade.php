<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
  <section style="font-size: 20px!important;">
    <a href="javascript:void(0)" class="wow fadeInUp pull-left" data-wow-duration="0.5s" data-wow-delay="0.5s" ng-click="getBack()" class="pull-left" style="margin-top:10px;margin-left:15px;">
      <i class="fa fa-arrow-circle-left"></i>
    </a>
    <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px;margin: 10px 0 0 10px;">
    Choose Your Ads
    <br />
    <small>Comment &amp; Pick your Ads</small>
    </h1>
    <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin:0;">
      <li><a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
    </ol>
  </section>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top: 10px;">
    <div class="panel panel-default" style="border-radius: 0px;">
      <div class="panel-heading" style="border-radius: 0px;background-color: #28A6A0;color: white;">
        <p style="padding: 10px;font-size: 20px;margin: 0;">Campaign: 
          <span style="font-size: 15px;" ng-bind="campaignName"></span>
          <small class="pull-right" style="font-size: 10px;margin-top: 13px;">
          After you approve the ads, it will be plublish on your Facebook advertise account
          </small>
        </p>
      </div>
      <div class="panel-body">
        <div class="col-md-4 text-center panel-box" style="">
          <img src="../../img/live_campaigns/1.jpg" ng-click="togglePreview()" class="thumbnail" style="margin: 10px auto;width: auto;height: 400px;cursor: pointer;">
          <span style="font-size: 32px;">Ad A</span>
          <!-- <select class="form-control" style="margin: 10px auto;">
            <option value="Approve">Approve</option>
            <option value="Reject">Reject</option>
            <option value="Edit">Edit</option>
          </select>
          <textarea class="form-control" style="height: 150px;" placeholder="Write a comment..."></textarea> -->
        </div>
        <div class="col-md-4 text-center panel-box" style="">
          <img src="../../img/live_campaigns/2.jpg" ng-click="togglePreview()" class="thumbnail" style="margin: 10px auto;width: auto;height: 400px;cursor: pointer;">
          <span style="font-size: 32px;">Ad B</span>
         <!--  <select class="form-control" style="margin: 10px auto;">
            <option value="Approve">Approve</option>
            <option value="Reject">Reject</option>
            <option value="Edit">Edit</option>
          </select>
          <textarea class="form-control" style="height: 150px;" placeholder="Write a comment..."></textarea> -->
        </div>
        <div class="col-md-4 text-center panel-box" style="">
          <img src="../../img/live_campaigns/3.jpg" ng-click="togglePreview()" class="thumbnail" style="margin: 10px auto;width: auto;height: 400px;cursor: pointer;">
          <span style="font-size: 32px;">Ad C</span>
          <!-- <select class="form-control" style="margin: 10px auto;">
            <option value="Approve">Approve</option>
            <option value="Reject">Reject</option>
            <option value="Edit">Edit</option>
          </select>
          <textarea class="form-control" style="height: 150px;" placeholder="Write a comment..."></textarea> -->
        </div>
        <div class="col-md-12 text-center">
          <button class="btn custom-button" ng-click="launch()" style="margin-top: 20px;font-size: 20px;">LAUNCH!</button>
        </div>
      </div>
    </div>
  </div>

<!-- modals -->

<div class="modal right fade" tabindex="-1" role="dialog" id="ads-preview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="remove-margin" style="color:#A1A1A1;font-size:20px;margin-top:3px;">Post Details</h2>
      </div>
      <div class="modal-body">
        <!-- <div class="row"> -->
          <div class="preview-container">
               <img src="../../img/live_campaigns/1.jpg" class="thumbnail wow slideInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" style="height: 400px;margin:auto;">
               <ul class="list-unstyled right-preview-wrapper" style="margin-top:15px;">
                    <li>
                       <div class="col-md-12 remove-side-padding">
                         <div class="pull-left wow slideInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" style="color:#777">
                              <h3 class="remove-margin">930</h3>
                              <small>Estimated reach for $25</small>
                          </div>
                          <div class="pull-right wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.5s">
                              <button class="btn btn-info">Promote</button>
                          </div>
                       </div>   
                    </li>
                    <li>
                       <div class="col-md-12 remove-side-padding">
                         <div class="pull-left wow slideInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" style="color:#777">
                              <h3 class="remove-margin">84</h3>
                              <small>People reached to date</small>
                          </div>
                          <div class="pull-right">
                              
                          </div>
                       </div>   
                    </li>
                    <li>
                       <div class="col-md-12 remove-side-padding">
                         <div class="pull-left wow slideInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" style="color:#777">
                              <h3 class="remove-margin">29</h3>
                              <small>Post Engagements</small>
                          </div>
                          <div class="pull-right">
                              
                          </div>
                       </div>   
                    </li>
               </ul>
            </div>
        <!-- </div> -->
      </div>
    </div>
  </div>
</div>