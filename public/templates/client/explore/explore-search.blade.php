<div class="panel panel-default">
  <div class="panel-body">
    <section class="text-center">
      <p style="color: #777575;font-size: 18px;">Find Inspiration in a Minute</p>
      <span style="color: #B5B1B1;font-size: 14px;"> Simple Search and Get Powerful Results</span>
      <br />
      <div class="inner-addon right-addon">
        <i class="glyphicon glyphicon-search"></i>
        <input type="text" class="form-control" placeholder="Enter Keyword e.g Thanksgiving" style="border-radius: 50px;margin: 40px auto;"/>
      </div>
    </section>
  </div>
</div>