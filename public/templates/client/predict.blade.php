
<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px;margin: 10px 0 0 10px;">
        Predict
        </h1>
        <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin: 0;">
          <li>
            <a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
        </ol>
    </section>
</div>
<div class="row" style="margin-top: 10px;" hidden>
  <div class="col-md-10 col-md-offset-1">
    <div class="panel-group custom-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="step-1">
          <h4 class="panel-title">
            <p class="pull-right step-1-button steps-predict wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              EDIT
            </p>
            <section class="step-container">
              <span>1</span>
            </section>
            <p class="step-description"> OBJECTIVES <span class="step-second-description step-1"> - Give us an idea about what you want to achieve with this ad. </span> </p>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="step-1">
          <div class="panel-body">
          <form ng-submit="gotStep_2()">
            <div class="form-group custom-form-group-predict">
              <span>1.1 What do you want this ad to do for you? <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i></span>
              <br />
              <!-- <md-input-container>
                <md-select ng-model="ctrl.sample_1" style="width: 50%; padding: 10px;">
                  <md-option >Increase engagement on an existing post on your page</md-option>
                  <md-option >Increase page likes</md-option>
                  <md-option >Increase the page views of a landing page that you indicate</md-option>
                  <md-option >Encourage sign-ups or purchases on your own website</md-option>
                  <md-option >Encourage the installs on your app</md-option>
                  <md-option >Increase the amount of people using your app</md-option>
                  <md-option >Increase page likes</md-option>
                  <md-option >Notify and get people to attend an event</md-option>
                  <md-option >Provide a coupon, discount, or deal</md-option>
                  <md-option >Promote a video you have made</md-option>
                </md-select>
              </md-input-container> -->
              <select class="form-control" style="border-radius: 5px;margin-left:40px;width:45%;display:inline-block;margin-top: 10px;font-weight: normal;">
                <option>Increase engagement on an existing post on your page</option>
                <option>Increase page likes</option>
                <option>Increase the page views of a landing page that you indicate</option>
                <option>Encourage sign-ups or purchases on your own website</option>
                <option>Encourage the installs on your app</option>
                <option>Increase the amount of people using your app</option>
                <option>Increase page likes</option>
                <option>Notify and get people to attend an event</option>
                <option>Provide a coupon, discount, or deal</option>
                <option>Promote a video you have made</option>
              </select>
            </div>
            <div class="form-group custom-form-group-predict">
              <span>1.2 What do you want people to do when they see your ad?</span>
              <br />
             <!--  <md-input-container>
                <md-select ng-model="ctrl.sample_2" style="width: 50%; padding: 10px;">
                  <md-option >Click to learn more</md-option>
                  <md-option >Shop on your website</md-option>
                  <md-option >Download your app</md-option>
                  <md-option >Book a reservation</md-option>
                  <md-option >Sign up</md-option>
                  <md-option >Use your app</md-option>
                  <md-option >Open the link you’ve provided</md-option>
                  <md-option >Like your Facebook page</md-option>
                  <md-option >Play your Facebook game</md-option>
                  <md-option >Watch your video</md-option>
                </md-select>
              </md-input-container> -->
              <select class="form-control" style="border-radius: 5px;margin-left:40px;width:45%;display:inline-block;margin-top: 10px;font-weight: normal;">
                <option>Click to learn more</option>
                <option>Shop on your website</option>
                <option>Download your app</option>
                <option>Book a reservation</option>
                <option>Sign up</option>
                <option>Use your app</option>
                <option>Open the link you’ve provided</option>
                <option>Like your Facebook page</option>
                <option>Play your Facebook game</option>
                <option>Watch your video</option>
              </select>
            </div>
            <div class="form-group text-right">
              <button class="btn custom-button activate-step-1" type="submit">Let's move on</button>
            </div>
          </div>
          </form>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="step-2">
          <h4 class="panel-title">
            <p class="pull-right step-2-button steps-predict wow fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.7s" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              EDIT
            </p>
            <section class="step-container">
              <span>2</span>
            </section>
            <p class="step-description"> CONTENT <span class="step-second-description step-2" hidden> - Tell us more about the text and visual content you're using for this ad.</span> </p>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step-2">
          <div class="panel-body">
            <form ng-submit="gotStep_3()">
            <div class="form-group">
              <span>2.1 What kind of message do you want to convey with your ad? You want to…</span>
              <br>
              <!-- <md-input-container>
                <md-select ng-model="ctr.sample_3" style="width: 50%; padding: 10px;">
                  <md-option >Make an announcement</md-option>
                  <md-option >Provide a reaction about a current event or new information</md-option>
                  <md-option >Invite people to participate in an event</md-option>
                  <md-option >Make an announcement</md-option>
                  <md-option >Make a special offer, provide a discount, or give away freebies</md-option>
                  <md-option >Inform people about a contest you are holding</md-option>
                  <md-option >Encourage a conversation on your post</md-option>
                  <md-option >Provide new information about your brand or product</md-option>
                  <md-option >Talk about how your company wants to help make the world a better place</md-option>
                </md-select>
              </md-input-container> -->
              <select class="form-control" style="border-radius: 5px;margin-left:40px;width:65%;display:inline-block;margin-top: 10px;font-weight: normal;">
                <option>Make an announcement</option>
                <option>Provide a reaction about a current event or new information</option>
                <option>Invite people to participate in an event</option>
                <option>Make a special offer, provide a discount, or give away freebies</option>
                <option>Inform people about a contest you are holding</option>
                <option>Encourage a conversation on your post</option>
                <option>Provide new information about your brand or product</option>
                <option>Talk about how your company wants to help make the world a better place</option>
              </select>
            </div>
            <div class="form-group">
              <span>2.2 What kind of information is included in your message? Check all that apply</span>
              <br>
              <div class="radio-float">
                <md-checkbox> Your brand </md-checkbox>
                <md-checkbox> Product features </md-checkbox>
                <md-checkbox> Product benefits </md-checkbox>
                <md-checkbox> Product availability </md-checkbox>
                <md-checkbox> Product location </md-checkbox>
                <md-checkbox> 
                  Product feature comparisons
                  <!-- <a href="javacript:void(0)" style="display:inline-block; margin-left: 5px;font-weight:normal;" data-toggle="tooltip" data-placement="right" title="This is when you compare the features of your product against another, similar product"> What's this? </a>  -->
                  
                  <md-tooltip md-visible="tooltip.showTooltip" md-direction="left">
                    This is when you compare the features of your product against another, similar product
                  </md-tooltip>
                </md-checkbox>
                <md-checkbox> Your product's price </md-checkbox>
              </div>
              <div class="radio-float">
                <md-checkbox> Pricing comparisons </md-checkbox>
                <md-checkbox> 
                  Deals or special offers
                  <a href="javacript:void(0)" style="display:inline-block; margin-left: 5px;font-weight:normal;" data-toggle="tooltip" data-placement="right" title="This includes discounts, freebies, buy one take one deals, and the like"> What's this? </a>
                </md-checkbox>
                <md-checkbox> Proven facts, scientific data, or expert opinions </md-checkbox>
                <md-checkbox> Social proof </md-checkbox>
                </div>
            </div>
            <div class="form-group">
              <span>2.3 What kind of image format does your ad have?</span>
              <br>
              <md-radio-group>
                <md-radio-button value="Banana" ng-click="SingleImage()"> Single image </md-radio-button>
                <md-radio-button value="Mango" ng-click="CarouselImage()"> Image carousel</md-radio-button>
              </md-radio-group>
              <section class="singleImageSelection">
              
                <section style="padding: 10px;" class="singleImageQuestion_1 p" hidden>
                  <span>Is your image illustrated or realistic?</span>
                  <br>
                  <md-radio-group>
                    <md-radio-button value="Realistic"> Realistic </md-radio-button>
                    <md-radio-button value="Illustrate"> Illustrate </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="Realistic" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="Realistic" name="real_optionsRadios">
                    Realistic
                  </label>
                  <label for="Illustrate" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="Illustrate" name="real_optionsRadios">
                   Illustrate
                  </label> -->
                </section>
                
                <section style="padding: 10px;" class="singleImageQuestion_2" hidden>
                  <span>What is the main subject of your image?</span>
                  <br>
                  <md-radio-group>
                    <md-radio-button value="person" ng-click="Realistic()"> A person </md-radio-button>
                    <md-radio-button value="animal" > An animal </md-radio-button>
                    <md-radio-button value="object" ng-click="object()"> An object </md-radio-button>
                    <md-radio-button value="scenic" > A scenic picture </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="person" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="person" name="subject_optionsRadios">
                    A person
                  </label>
                  <label for="animal" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="animal" name="subject_optionsRadios">
                   An animal
                  </label>
                  <label for="object" style="font-weight: normal;margin-left: 40px;" >
                    <input type="radio" id="object" name="subject_optionsRadios">
                   An object
                  </label>
                  <label for="scenic" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="scenic" name="subject_optionsRadios">
                   A scenic picture
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_2_featured" hidden>
                  <span>Choose the person/people featured in the image</span>
                  <br>
                  <md-radio-group>
                    <md-radio-button value="male"> Male </md-radio-button>
                    <md-radio-button value="female" > Female </md-radio-button>
                    <md-radio-button value="both" > Both </md-radio-button>
                    <md-radio-button value="family" > Family </md-radio-button>
                    <md-radio-button value="group" > Group </md-radio-button>
                    <md-radio-button value="visible" > Only body parts are visible (like a hand or a smile) </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="male" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="male" name="featured_optionsRadios">
                   Male
                  </label>
                  <label for="female" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="female" name="featured_optionsRadios">
                   Female
                  </label>
                  <label for="both" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="both" name="featured_optionsRadios">
                   Both
                  </label>
                  <label for="family" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="family" name="featured_optionsRadios">
                   Family
                  </label>
                  <label for="group" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="group" name="featured_optionsRadios">
                   Group
                  </label>
                  <label for="parts" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="parts" name="featured_optionsRadios">
                   Only body parts are visible (like a hand or a smile)
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_2_people" hidden>
                  <span>Tell us more about the people in the picture</span>
                  <br>
                  <md-radio-group >
                    <md-radio-button value="kids">They’re kids</md-radio-button>
                    <md-radio-button value="teens"> They’re teens </md-radio-button>
                    <md-radio-button value="adults"> They’re adults </md-radio-button>
                    <md-radio-button value="elderly"> They’re elderly </md-radio-button>
                    <md-radio-button value="celebrities"> They’re celebrities </md-radio-button>
                    <md-radio-button value="mix"> They’re a mix of two or more of the above </md-radio-button>
                  </md-radio-group>
                 <!--  <label for="_1" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_1" name="about_optionsRadios">
                   They’re kids
                  </label>
                  <label for="_2" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_2" name="about_optionsRadios">
                   They’re teens
                  </label>
                  <label for="_3" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_3" name="about_optionsRadios">
                   They’re adults
                  </label>
                  <label for="_4" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_4" name="about_optionsRadios">
                   They’re elderly
                  </label>
                  <label for="_5" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_5" name="about_optionsRadios">
                   They’re celebrities
                  </label>
                  <label for="_6" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_6" name="about_optionsRadios">
                   They’re a mix of two or more of the above
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_2_ethnic" hidden>
                  <span>What ethnic group does the person in the image belong to?</span>
                  <br>
                  <md-radio-group >
                    <md-radio-button value="asian">Asian</md-radio-button>
                    <md-radio-button value="caucasian"> Caucasian </md-radio-button>
                    <md-radio-button value="african"> African </md-radio-button>
                    <md-radio-button value="latino"> Latino </md-radio-button>
                    <md-radio-button value="middle-eastern"> Middle-Eastern </md-radio-button>
                    <md-radio-button value="ethnicities"> There are several ethnicities represented in the image </md-radio-button>
                    <md-radio-button value="canno-determined"> Cannot be determined </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="_7" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_7" name="group_optionsRadios">
                   Asian
                  </label>
                  <label for="_8" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_8" name="group_optionsRadios">
                   Caucasian
                  </label>
                  <label for="_9" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_9" name="group_optionsRadios">
                   African
                  </label>
                  <label for="_10" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_10" name="group_optionsRadios">
                   Latino
                  </label>
                  <label for="_11" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_11" name="group_optionsRadios">
                   Middle-Eastern
                  </label>
                  <label for="_12" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_12" name="group_optionsRadios">
                   There are several ethnicities represented in the image
                  </label>
                  <label for="_13" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_13" name="group_optionsRadios">
                   Cannot be determined
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_2_objects" hidden>
                  <span>What kind of object is it?</span>
                  <br>
                  <md-radio-group >
                    <md-radio-button value="product">The product</md-radio-button>
                    <md-radio-button value="other"> Other </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="_14" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_14" name="object_optionsRadios">
                   The product
                  </label>
                  <label for="_15" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_15" name="object_optionsRadios">
                   Other
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_2_pictures" hidden>
                  <span>What kind of scene is in the picture?</span>
                  <br>
                  <md-radio-group>
                    <md-radio-button value="urban">An urban landscape</md-radio-button>
                    <md-radio-button value="rural"> A rural landscape </md-radio-button>
                    <md-radio-button value="nature-scene"> A nature scene </md-radio-button>
                  </md-radio-group>
                  <!-- <label for="_16" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_16" name="scene_optionsRadios">
                   An urban landscape
                  </label>
                  <label for="_17" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_17" name="scene_optionsRadios">
                   A rural landscape
                  </label>
                  <label for="_18" style="font-weight: normal;margin-left: 40px;">
                    <input type="radio" id="_18" name="scene_optionsRadios">
                   A nature scene
                  </label> -->
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_3" hidden>
                  <span>What is the predominant color in your image?</span>
                  <br>
                  <!-- <md-input-container>
                    <md-select ng-model="ctrl.sample_4" style="width: 50%; padding: 10px;">
                      <md-option >Red</md-option>
                      <md-option >Orange</md-option>
                      <md-option >Yellow</md-option>
                      <md-option >Brown</md-option>
                      <md-option >White</md-option>
                      <md-option >Green</md-option>
                      <md-option >Blue</md-option>
                      <md-option >Grey</md-option>
                      <md-option >Purple</md-option>
                      <md-option >Black</md-option>
                      <md-option >Off-White</md-option>
                      <md-option >Pink</md-option>
                      <md-option >Tan</md-option>
                    </md-select>
                  </md-input-container> -->
                  <select class="form-control" style="border-radius: 5px;margin-left:40px;width:20%;display:inline-block;margin-top: 10px;font-weight: normal;">
                    <option>Red</option>
                    <option>Orange</option>
                    <option>Yellow</option>
                    <option>Brown</option>
                    <option>White</option>
                    <option>Green</option>
                    <option>Blue</option>
                    <option>Grey</option>
                    <option>Purple</option>
                    <option>Black</option>
                    <option>Grey</option>
                    <option>Off-White</option>
                    <option>Pink</option>
                    <option>Tan</option>
                  </select>
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_4" hidden>
                  <span>Is your logo part of the image that you used? If so, where in your image is it located?</span>
                  <br>
                 <!--  <md-input-container>
                    <md-select ng-model="position" style="width: 50%; padding: 10px;">
                      <md-option>Top</md-option>
                      <md-option>Bottom</md-option>
                      <md-option>Left</md-option>
                      <md-option>Right</md-option>
                      <md-option>Lower Right</md-option>
                      <md-option>Lower Left</md-option>
                      <md-option>Upper Right</md-option>
                      <md-option>Upper Left</md-option>
                    </md-select>
                  </md-input-container> -->
                  <select class="form-control" style="border-radius: 5px;margin-left:40px;width:20%;display:inline-block;margin-top: 10px;font-weight: normal;">
                    <option>Top</option>
                    <option>Bottom</option>
                    <option>Left</option>
                    <option>Right</option>
                    <option>Lower Right</option>
                    <option>Lower Left</option>
                    <option>Upper Right</option>
                    <option>Upper Left</option>
                    <option>Center</option>
                    <option>The logo is not within the image</option>
                  </select>
                </section>
                <section style="padding: 10px;" class="singleImageQuestion_5" hidden>
                  <span>Does the image have a text overlay that indicates a price or special offer?</span>
                  <br>
                  <select class="form-control" style="border-radius: 5px;margin-left:40px;width:40%;display:inline-block;margin-top: 10px;font-weight: normal;">
                    <option>Yes, it has both the price and a special offer</option>
                    <option>It only has a special offer</option>
                    <option>It only shows a price</option>
                    <option>No, it has neither</option>
                  </select>
                </section>
              </section>
              
              <section style="padding: 10px;" class="carouselQuestion_1" hidden>
                <span>How are the images used in the carousel?</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;">
                  <option>Each image is a separate item in a catalogue-style carousel</option>
                  <option>Each image is a fragment of one large image that highlights one product</option>
                  <option>Each image showcases a unique product feature</option>
                  <option>The images are set in a sequence in order to tell a story</option>
                </select>
              </section>
                <!-- </section> -->
              <!-- </section> -->
            </div>
            <div class="form-group text-right">
              <!-- role="tab" id="step-3" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" -->
              <button type="submit" class="btn custom-button activate-step-1">Onto the next one</button>
            </div>
          </form>

          </div>
        </div>
      </div>

      <div class="panel panel-default">
      <!-- id="step-3" role="tab" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" -->
        <div class="panel-heading" role="tab">
          <h4 class="panel-title">
            <p class="pull-right step-3-button steps-predict wow fadeInRight" data-wow-duration="0.9s" data-wow-delay="0.9s" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
              EDIT
            </p>
            <section class="step-container">
              <span>3</span>
            </section>
            <p class="step-description"> APPROACH <span class="step-second-description step-3" hidden> - Let's talk about how your ad conveys your message</span> </p>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
          <div class="panel-body">
          <form ng-submit="gotStep_4()">
            <div class="form-group">
             <section style="padding: 10px;" class="carouselQuestion_1">
                <span>3.1 What kind of statements and sentiments does your ad have? Check all that apply.</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;" required>
                  <option>Funny statements or jokes</option>
                  <option>Excited statements and exclamation points</option>
                  <option>An offer to help with something</option>
                  <option>A message that inspires hope</option>
                  <option>Something that tells people you understand how they feel or what they go through in a certain situation</option>
                  <option>Questions</option>
                  <option>Statements that inspire immediate action and may include words like “instant,” “now,” “limited time,” and “exclusive”</option>
                  <option>A declarative statement</option>
                  <option>A declarative statement</option>
                  <option>A command or request to take action</option>
                  <option>Something that says “Things are not the way you thought” and how they could be better with your product</option>
                  <option>Statements that discuss the risks and possible negative outcomes that may happen in a world without your product</option>
                </select>
              </section>
            </div>
            <div class="form-group">
             <section style="padding: 10px;" class="carouselQuestion_1">
                <span>3.2 What is the point-of-view used in your ad copy?</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;" required>
                  <option>It uses the first person, with words like “I,” “we,” and “our.”</option>
                  <option>It uses the second person and focuses on words like “you” and “your.”</option>
                  <option>It uses the third person, with words like “he,” “she,” and “they.”</option>
                </select>
              </section>
            </div>
            <div class="form-group">
             <section style="padding: 10px;" class="carouselQuestion_1">
                <span>3.3 Which of these fit best with your brand’s personality?</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;" required>
                  <option>Remarkable facts</option>
                  <option>Emojis/emoticons</option>
                  <option>Humor</option>
                  <option>Philanthropic posts</option>
                  <option>Small talk</option>
                  <option>Seasonal and holiday posts</option>
                  <option>Messages your friends are most likely to share</option>
                </select>
              </section>
            </div>
            <div class="form-group">
             <section style="padding: 10px;" class="carouselQuestion_1">
                <span>3.3 What kind of emotion is evident in your ad’s image?</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;" required>
                  <option>Laughter</option>
                  <option>Joy</option>
                  <option>Awe</option>
                  <option>Excitement</option>
                  <option>Sadness</option>
                  <option>Pain</option>
                  <option>Frustration</option>
                  <option>Anger</option>
                  <option>Calm</option>
                  <option>Thoughtfulness</option>
                  <option>Other</option>
                </select>
              </section>
            </div>
            <div class="form-group">
             <section style="padding: 10px;" class="carouselQuestion_1">
                <span>3.5 Is the image you’re using an action shot, or does it show someone or something that is posed or stationary?</span>
                <br>
                <select class="form-control" style="border-radius: 5px;margin-left:40px;width:50%;display:inline-block;margin-top: 10px;font-weight: normal;" required>
                  <option>Action shot</option>
                  <option>Posed/stationary subject</option>
                </select>
              </section>
            </div>
            <div class="form-group text-right">
              <button type="submit" class="btn custom-button activate-step-1">Onto the next one...</button>
            </div>
          </form>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading" role="tab">
        <!-- id="step-4" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" -->
          <h4 class="panel-title">
            <p class="pull-right step-4-button steps-predict wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="1.1s" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
              EDIT
            </p>
            <section class="step-container">
              <span>4</span>
            </section>
            <p class="step-description"> STRUCTURE <span class="step-second-description step-4" hidden> - We just need a little more info about your ad</span> </p>
          </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step-4">
          <div class="panel-body">
            <form ng-submit="showPredict()">
              <div class="form-group">
               <section style="padding: 10px;" class="carouselQuestion_1">
                  <span>4.1 How many words is your headline made up of?</span>
                  <br>
                  <section style="padding: 10px;">
                    <!-- <label for="word_1" style="font-weight: normal;">
                      <input type="radio" id="word_1" name="word_1" required>
                      1-5 words
                    </label>
                    <label for="word_2" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_2" name="word_1" required>
                      6-10 words
                    </label>
                    <label for="word_3" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_3" name="word_1" required>
                      >10 words
                    </label> -->
                    <md-radio-group>
                      <md-radio-button value="urban">1-5 words</md-radio-button>
                      <md-radio-button value="rural"> 6-10 words </md-radio-button>
                      <md-radio-button value="nature-scene"> > 10 words </md-radio-button>
                    </md-radio-group>
                  </section>
                </section>
              </div>
              <div class="form-group">
               <section style="padding: 10px;" class="carouselQuestion_1">
                  <span>4.1 How many words is your text made up of?</span>
                  <br>
                  <section style="padding: 10px;">
                    <md-radio-group>
                      <md-radio-button value="urban">1-6 words</md-radio-button>
                      <md-radio-button value="rural"> 6-15 words </md-radio-button>
                      <md-radio-button value="nature-scene"> > 15 words </md-radio-button>
                    </md-radio-group>
                    <!-- <label for="word_4" style="font-weight: normal;">
                      <input type="radio" id="word_4" name="word_4" required>
                      1-6 words
                    </label>
                    <label for="word_5" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_5" name="word_4" required>
                      6-15 words
                    </label>
                    <label for="word_6" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_6" name="word_4" required>
                      >15 words
                    </label> -->
                  </section>
                </section>
              </div>
              <div class="form-group">
               <section style="padding: 10px;" class="carouselQuestion_1">
                  <span>4.1 How many words is your description made up of?</span>
                  <br>
                  <section style="padding: 10px;">
                    <md-radio-group>
                      <md-radio-button value="urban"> 0 </md-radio-button>
                      <md-radio-button value="rural"> 1-18 words </md-radio-button>
                      <md-radio-button value="nature-scene"> > 18 words </md-radio-button>
                    </md-radio-group>
                    <!-- <label for="word_7" style="font-weight: normal;">
                      <input type="radio" id="word_7" name="word_7" required>
                      0
                    </label>
                    <label for="word_8" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_8" name="word_7" required>
                      1-18 words
                    </label>
                    <label for="word_9" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_9" name="word_7" required>
                      >18 words
                    </label> -->
                  </section>
                </section>
              </div>
              <div class="form-group">
               <section style="padding: 10px;" class="carouselQuestion_1">
                  <span>4.1 Does your ad image have a text overlay? If it does, how much of your ad is made up of text?</span>
                  <br>
                  <section style="padding: 10px;">
                    <md-radio-group>
                      <md-radio-button value="urban"> 0% </md-radio-button>
                      <md-radio-button value="rural"> 10% words </md-radio-button>
                      <md-radio-button value="nature-scene"> 20% </md-radio-button>
                      <md-radio-button value="nature-scene"> > 20% </md-radio-button>
                    </md-radio-group>
                    <!-- <label for="word_10" style="font-weight: normal;">
                      <input type="radio" id="word_10" name="word_10" required>
                      0%
                    </label>
                    <label for="word_11" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_11" name="word_10" required>
                      10%
                    </label>
                    <label for="word_12" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_12" name="word_10" required>
                      20%
                    </label>
                    <label for="word_13" style="font-weight: normal;margin-left: 40px;">
                      <input type="radio" id="word_13" name="word_10" required>
                      >20%
                    </label> -->
                  </section>
                </section>
              </div>
              <div class="form-group text-right">
                <button type="submit" class="btn custom-button activate-step-1" >How did my Ad do?</button>
              </div>
          </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="row show-predict-loading" hidden>
  <div class="col-md-10 col-md-offset-1">
    <div class="panel default-panel">
      <div class="panel-heading" style="border-bottom: 1px solid #ddd;">
        <h4 style="margin-left: 10px;color: #515763;">Predict and Suggestions</h4>
      </div>
      <div class="panel-body text-center predict-loading-data">
        <span style="padding: 10px;">
          You can chill out and let Adhero's machine learning algorithm give you the lowdown on how to this Post will perform.
          And then, we'll give you tips to make t even better! (Note, our prediction is a fair estimate based on a lot of Math you can ask us about, it isn't absolute - give us a break, we ain't Nustradamus!)
        </span>

        <section class="text-center" style="margin-top: 20px; margin-bottom: 15px;">
          <!-- <img src="../img/loading-predict.gif" style="width: 60px;"> -->
          <md-progress-circular md-mode="indeterminate" md-diameter="50" style="margin: 0 auto"></md-progress-circular>
        </section>
      </div>
      <section class="prediction-container-main" >
        <div hidden class="panel-body result-predict-data" style="background: #CBEEF4;color: #86888C;font-size: 18px;padding: 25px;">
          <section class="prediction-container text-center" style="padding: 0;margin: 0;">
            <p style="margin: 0;">
              <i class="glyphicon glyphicon-info-sign"></i>
              We inspected the elements of your Ad and predict Engagement in the same range displayed below.
              <br>
              Switch between the tabs to get an estimate of the Likes, Comments and Shares.
            </p>
          </section>
        </div>
        <div class="panel-body predict-result-view">
          <section class="text-center">
            <div class="btn-group" role="group" aria-label="..." style="margin-top: 15px;">
              <button type="button" class="btn btn-default default-predic-status" id="predict_state_1" ng-click="getPredictState('cost_conversion','predict_state_1')">Cost of Conversion</button>
              <button type="button" class="btn btn-default default-predic-status" id="predict_state_2" ng-click="getPredictState('ctr','predict_state_2')">CTR</button>
              <button type="button" class="btn btn-default default-predic-status" id="predict_state_3">Likes</button> <!--ng-click="getPredictState('likes','predict_state_3')"-->
              <button type="button" class="btn btn-default default-predic-status" id="predict_state_4">Comments</button> <!-- ng-click="getPredictState('comments','predict_state_4')" -->
              <button type="button" class="btn btn-default default-predic-status" id="predict_state_5">Shares</button> <!-- ng-click="getPredictState('shares','predict_state_5')" -->
            </div>
          </section>
          <section class="marker-bar-container-main">
            <section class="predict-status-result-container">
              <section ng-repeat="result in allResult" style="position: absolute;margin-left: {{ result.mark | number:0 }}%; color: {{ result.color }}; transition: all 1.5s;" class="marker-indicator text-center">
                <p> {{ $index + 1 | ordinal }} {{ result.type }} Predictive</p>
                <span class="icon-marker"></span>
              </section>
              <section onclick="$('.suggestion_1').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" style="position: absolute;margin-left: 0%; color: #B3AEAE; transition: all 2s;margin-top: -19px;" class="marker-indicator text-center suggestion-marker-1" hidden>
                <span class="icon-marker-suggest"></span>
                <br>
                <p style="font-size: 12px;margin-top: -5px;">1</p>
              </section>
              <section onclick="$('.suggestion_2').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" style="position: absolute;margin-left: 0%; color: #B3AEAE; transition: all 2s;margin-top: -19px;" class="marker-indicator text-center suggestion-marker-2" hidden>
                <span class="icon-marker-suggest"></span>
                <br>
                <p style="font-size: 12px;margin-top: -5px;">2</p>
              </section>
              <section onclick="$('.suggestion_3').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" style="position: absolute;margin-left: 0%; color: #B3AEAE; transition: all 2s;margin-top: -19px;" class="marker-indicator text-center suggestion-marker-3" hidden>
                <span class="icon-marker-suggest"></span>
                <br>
                <p style="font-size: 12px;margin-top: -5px;">3</p>
              </section>
              <section onclick="$('.suggestion_4').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" style="position: absolute;margin-left: 0%; color: #B3AEAE; transition: all 2s;margin-top: -19px;" class="marker-indicator text-center suggestion-marker-4" hidden>
                <span class="icon-marker-suggest"></span>
                <br>
                <p style="font-size: 12px;margin-top: -5px;">4</p>
              </section>
            </section>            
            <section class="marker-bar-container"></section>
          </section>
          <section class="level-container-prediction">
            <span class="pull-left">Low</span>
            <span class="pull-right">High</span>
          </section>
          <section class="predict-status-information-container">
            <p>Do you want to maximize Engagement for your Ad?</p>
            <!-- <button class="btn custom-violet-button" ng-click="letsDoit()">Let's do it!</button> -->
            <md-button class="md-raised custom-violet-button" ng-click="letsDoit()">Let's do it!</md-button>
          </section>
          <section class="suggestions-container" id="suggestion_accordion" role="tablist" aria-multiselectable="true">
            <section class="suggestion_1" hidden>
            <p class="wow fadeInUp" data-wow-delay="0.5" data-wow-duration="0.5">Still want to improve your ad? Here are our recommendations:</p>
            <section class="suggestion-details wow fadeInUp" id="suggestion-details-1" data-wow-delay="0.7" data-wow-duration="0.7">
              <section class="step-container">
                <span>1</span>
              </section>
              <p class="suggestions-text">
                <span>Use the second person plural point of view, so that readers feel like you're taling to them.</span>
                <label style="display:none;" class="label label-success pull-right" ng-click="previewSuggestion( 'suggestion_1_collapse' )" id="s1">Show</label>
              </p>
            </section>
            <section class="collapsed-section collapse in" id="suggestion_1_collapse" style="overflow:hidden">
                <br />
                <p style="color: #7F8186;">How other brands have done it:</p>
                <hr />
                <a href="javascript:void(0)" ng-click="costPreview('step_1')" ng-if="cost">See how this has worked for you before</a>
                <a href="javascript:void(0)" ng-click="ctrPreview('step_1')" ng-if="ctr">See how this has worked for you before</a>
                <a href="javascript:void(0)" ng-click="likePreview('step_1')" ng-if="likes">See how this has worked for you before</a>
                <a href="javascript:void(0)" ng-click="commentPreview('step_1')" ng-if="comments">See how this has worked for you before</a>
                <a href="javascript:void(0)" ng-click="sharePreview('step_1')" ng-if="shares">See how this has worked for you before</a>
                <div class="col-md-12 text-center" style="margin-bottom: 20px;margin-top: 50px">
                  <div class="row ads-wrapper-output wow fadeInUp" data-wow-delay="0.9" data-wow-duration="0.9" style="margin-bottom:70px;display:flex;align-items:center">
                      <div class="col-md-5">
                          <img src="../../img/Ins. 1a.JPG" class="img-responsive">
                      </div>
                      <div class="col-md-7">
                          <div class="optimized-cost" style="display:flex;justify-content:center;align-items:center;border:2px solid #000;min-height:100px;">
                              <h2 style="margin:0">Optimized Cost Per Conversion: $0.65</h2>
                          </div>
                      </div>
                  </div>
                  <div class="row ads-wrapper-output wow fadeInUp" data-wow-delay="1.2" data-wow-duration="1.2" style="margin-bottom:70px;display:flex;align-items:center">
                      <div class="col-md-5">
                          <img src="../../img/Ins. 1b.JPG" class="img-responsive">
                      </div>
                      <div class="col-md-7">
                          <div class="optimized-cost" style="display:flex;justify-content:center;align-items:center;border:2px solid #000;min-height:100px;">
                              <h2 style="margin:0">Optimized Cost Per Conversion: $0.65</h2>
                          </div>
                      </div>
                  </div>
                  <div class="btn-group next-indicator-buttons wow fadeInUp" data-wow-delay="1.5" data-wow-duration="1.5" role="group" aria-label="..." style="margin-top: 20px;">
                    <button onclick="$('.suggestion_1').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" type="button" class="btn btn-default" ng-click="nextSuggestion( 'suggestion_2_collapse' , 'suggestion_1_collapse' , 'suggestion-details-2', 'suggestion-marker-1',  '10%')">Accept</button>
                    <button type="button" class="btn btn-default">Reject</button>
                  </div>                  
                </div>
            </section>
            </section>
            <section class="suggestion-details wow fadeInUp" data-wow-delay="0.5" data-wow-duration="0.5" id="suggestion-details-2" hidden>
              <section class="step-container">
                <span>2</span>
              </section>
              <p class="suggestions-text">
                <span>Keep your headline below 10 words. 5 - 6 words is best.</span>
                <label style="display:none;" class="label label-success pull-right" ng-click="previewSuggestion( 'suggestion_2_collapse' )" id="s2">Show</label>
              </p>
            </section>
            <section class="suggestion_2 collapsed-section collapse" id="suggestion_2_collapse" style="overflow:hidden">
              <p style="color: #7F8186;font-weight: bold;">How other brands have done it:</p>
              <br />
              <hr />
              <a href="javascript:void(0)" ng-click="costPreview('step_2')" ng-if="cost">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="ctrPreview('step_2')" ng-if="ctr">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="likePreview('step_2')" ng-if="likes">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="commentPreview('step_2')" ng-if="comments">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="sharePreview('step_2')" ng-if="shares">See how this has worked for you before</a> 
              <div class="col-md-12 text-center" style="margin-bottom: 20px;margin-top: 50px">
                <div class="row ads-wrapper-output wow fadeInUp" data-wow-delay="0.7" data-wow-duration="0.7" style="margin-bottom:70px;display:flex;align-items:center">
                    <div class="col-md-5">
                        <img src="../../img/Ins. 1c.JPG" class="img-responsive">
                    </div>
                    <div class="col-md-7">
                        <div class="optimized-cost" style="display:flex;justify-content:center;align-items:center;border:2px solid #000;min-height:100px;">
                            <h2 style="margin:0">Optimized Cost Per Conversion: $0.85</h2>
                        </div>
                    </div>
                </div>
                <div class="row ads-wrapper-output wow fadeInUp" data-wow-delay="0.9" data-wow-duration="0.9" style="margin-bottom:70px;display:flex;align-items:center">
                    <div class="col-md-5">
                        <img src="../../img/Ins. 1d.JPG" class="img-responsive">
                    </div>
                    <div class="col-md-7">
                        <div class="optimized-cost" style="display:flex;justify-content:center;align-items:center;border:2px solid #000;min-height:100px;">
                            <h2 style="margin:0">Optimized Cost Per Conversion: $0.45</h2>
                        </div>
                    </div>
                </div>
                <div class="btn-group next-indicator-buttons wow fadeInUp" data-wow-delay="1.2" data-wow-duration="1.2" role="group" aria-label="..." style="margin-top: 20px;">
                  <button onclick="$('.suggestion_2').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" type="button" class="btn btn-default" ng-click="nextSuggestion( 'suggestion_3_collapse' , 'suggestion_2_collapse' , 'suggestion-details-3', 'suggestion-marker-2',  '30%' )">Accept</button>
                  <button type="button" class="btn btn-default">Reject</button>
                </div>   
              </div>
            </section>
            <section class="suggestion-details wow fadeInUp" data-wow-delay="0.5" data-wow-duration="0.5" id="suggestion-details-3" hidden>
              <section class="step-container">
                <span>3</span>
              </section>
              <p class="suggestions-text">
                <span>Invite your readers to take action by using statements that tell them waht to do.</span>
                <label style="display:none;" class="label label-success pull-right" ng-click="previewSuggestion( 'suggestion_3_collapse' )" id="s3">Show</label>
              </p>
            </section>
            <section class="suggestion_3 collapsed-section collapse" id="suggestion_3_collapse" style="overflow:hidden">
              <p style="color: #7F8186;font-weight: bold;">How other brands have done it:</p>
              <br />
              <hr />
              <a href="javascript:void(0)" ng-click="costPreview('step_3')" ng-if="cost">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="ctrPreview('step_3')" ng-if="ctr">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="likePreview('step_3')" ng-if="likes">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="commentPreview('step_3')" ng-if="comments">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="sharePreview('step_3')" ng-if="shares">See how this has worked for you before</a>
              <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.7" data-wow-duration="0.7">
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Liberty Mutual Insurance ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Farmers Insurance Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
              </div>
              <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.9" data-wow-duration="0.9">
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Insurance Discounts Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Cincinnati Insurance Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="btn-group next-indicator-buttons" role="group" aria-label="..." style="margin-top: 20px;">
                  <button onclick="$('.suggestion_3').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" type="button" class="btn btn-default" ng-click="nextSuggestion( 'suggestion_4_collapse' , 'suggestion_3_collapse' , 'suggestion-details-4', 'suggestion-marker-3',  '60%' )">Accept</button>
                  <button type="button" class="btn btn-default">Reject</button>
                </div>   
              </div>
            </section>
            <section class="suggestion-details wow fadeInUp" id="suggestion-details-4" data-wow-delay="0.5" data-wow-duration="0.5" hidden>
              <section class="step-container">
                <span>4</span>
              </section>
              <p class="suggestions-text">
                <span>Use a realistic image instead of an illustrated one.</span>
                <label style="display:none;" class="label label-success pull-right" ng-click="previewSuggestion( 'suggestion_4_collapse' )" id="s4">Show</label>
              </p>
            </section>
            <section class="suggestion_4 collapsed-section collapse" id="suggestion_4_collapse" style="overflow:hidden">
              <br />
              <a href="javascript:void(0)" ng-click="costPreview('step_4')" ng-if="cost">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="ctrPreview('step_4')" ng-if="ctr">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="likePreview('step_4')" ng-if="likes">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="commentPreview('step_4')" ng-if="comments">See how this has worked for you before</a>
              <a href="javascript:void(0)" ng-click="sharePreview('step_4')" ng-if="shares">See how this has worked for you before</a>
              <hr />
              <p style="color: #7F8186;font-weight: bold;">How other brands have done it:</p>
              <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.7" data-wow-duration="0.7">
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Cincinnati Insurance Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Farmers Insurance Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
              </div>
              <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.9" data-wow-duration="0.9">
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Insurance Discounts Ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="col-md-6">
                  <img src="../../img/predict_images/Fake Liberty Mutual Insurance ad.png" class="thumbnail custom-thumbnail-suggestion-2">
                </div>
                <div class="btn-group next-indicator-buttons" role="group" aria-label="..." style="margin-top: 20px;">
                  <button type="button" class="btn btn-default" ng-click="nextSuggestion( 'final-suggestion' , 'suggestion_4_collapse' , 'suggestion-details-4', 'suggestion-marker-4',  '90%' )">Accept</button>
                  <button type="button" class="btn btn-default">Reject</button>
                </div>
                <button type="button" class="btn custom-button email-add-button wow fadeInUp" data-wow-delay="0.5" data-wow-duration="0.5" ng-click="showConfirm()">Send to my email <span class="glyphicon glyphicon-envelope" style="margin-left: 10px;"></span></button>
              </div>
            </section>
            <section class="text-center next-indicator" hidden>
                  <!-- <span onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" class="glyphicon glyphicon-pencil to-top-indicator" style="font-size: 30px;color: #6A6C6F;" hidden>
                  <br />
                  <span>Edit Post</span> -->
                  <button onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});" class="btn custom-button to-top-indicator" hidden>Edit post <span class="glyphicon glyphicon-pencil" style="margin-left: 10px;"></span></button>
                  </span>
              </section>
            </section>
        </div>
      </section>
    </div>
  </div>
</div>


<div class="row qustionnaires" hidden>
    <div class="col-md-10 col-md-offset-1 financial">
      <div class="panel panel-default">
        <div class="panel-heading custom-heading-questionnaire">
          <section class="objective wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s">
            <p>Objective</p>
          </section>
          <section class="selection-industry wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s">
            <p>Select Industry</p>
            <div class="row selection-questionnaire-cat">
              <div class="col-md-3 icon-container-questionnaire wow fadeInUp insurance-container" data-wow-delay="0.5s" data-wow-duration="0.5s" ng-click="getCategory('insurance')">
                <span class="icon-insurance"></span>
                <p>INSURANCE</p>
              </div>
              <div class="col-md-3 icon-container-questionnaire wow fadeInUp financial-container" data-wow-delay="0.7s" data-wow-duration="0.7s" ng-click="getCategory('financial')">
                <span class="icon-financial"></span>
                <p>FINANCIAL SERVICES</p> 
              </div>
              <div class="col-md-3 icon-container-questionnaire wow fadeInUp travel-container" data-wow-delay="0.9s" data-wow-duration="0.9s" ng-click="getCategory('travel')">
                <span class="icon-travel"></span>
                <p>TRAVEL</p>  
              </div>
              <div class="col-md-3 icon-container-questionnaire wow fadeInUp consumer-container" data-wow-delay="1.1s" data-wow-duration="1.5s" ng-click="getCategory('consumer')">
                <span class="icon-Untitled-1_copy"></span>
                <p>CONSUMER GOODS</p>
              </div>
            </div>
          </section>
        </div>
        <div class="panel-body panel-industry-category" hidden data-wow-duration="0.5s" data-wow-delay="0.5s">
          <section>
            <p>Who is your target audience? (Check all that apply)</p>
            <md-radio-group>
              <md-radio-button class="questionnaire-radio-button" value="existing_customer"> Existing customers </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="new_customer"> New customers </md-radio-button>
            </md-radio-group>
          </section>
          <section class="form-group"  style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Location</p>
            </div>
           <country-select ng-model="campaign.target_country" class="form-control" style="float: left;display: inline-block;width: 40%;"></country-select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Age Range</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">1</option>
              <option value="1">2</option>
              <option value="1">3</option>
              <option value="1">4</option>
            </select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Gender</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">Male</option>
              <option value="1">Female</option>
            </select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Marital Status</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">Single</option>
              <option value="1">Married</option>
              <option value="1">Divorce</option>
            </select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Family</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">1</option>
              <option value="1">2</option>
              <option value="1">3</option>
              <option value="1">4</option>
            </select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Income Bracket</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">$1000</option>
              <option value="1">$2000</option>
              <option value="1">$3000</option>
              <option value="1">$4000</option>
            </select>
          </section>
          <section class="form-group" style="margin-left: 10px;">
            <div style="text-align:left;width:150px; float:left">
              <p style="display: inline-block;margin: 8px;">Other Status</p>
            </div>
            <select class="form-control" style="float: left;display: inline-block;width: 40%;">
              <option value="1">1</option>
              <option value="1">1</option>
              <option value="1">1</option>
              <option value="1">1</option>
            </select>
          </section>
          <section>
            <p>What will you be promoting with your sponsored content?</p>
            <md-radio-group>
              <md-radio-button class="questionnaire-radio-button" value="facebook_page"> A Facebook page </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="landing"> A website/landing page </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="post"> A specific Facebook post </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="p_s_f"> A product, service, or feature </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="discount"> A discount or special offer </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="event"> An event </md-radio-button>
              <md-radio-button class="questionnaire-radio-button" value="app"> An app </md-radio-button>
            </md-radio-group>
          </section>
          <section>
            <p>Choose the words that best describe your brand personality. Check all that apply.</p>
            <section style="margin-left: 20px;">
              <md-checkbox> Funny </md-checkbox>
              <md-checkbox> Straight-forward </md-checkbox>
              <md-checkbox> Friendly/casual </md-checkbox>
              <md-checkbox> Formal </md-checkbox>
              <md-checkbox> Reserved </md-checkbox>
              <md-checkbox> Excitable/Energetic </md-checkbox>
              <md-checkbox> Efficient </md-checkbox>
              <md-checkbox> Laid back </md-checkbox>
              <md-checkbox> Adventurous </md-checkbox>
              <md-checkbox> Consistent/dependable </md-checkbox>
            </section>
          </section>
          <div class="row quesions-category-row">
            <section>
              <p>Which of these best describes your ad content?</p>
              <h4 class="financial-title" ng-if="industry_category.financial">Financial</h4>
              <h4 class="insurance-title" ng-if="industry_category.insurance">Insurance</h4>
              <h4 class="travel-title" ng-if="industry_category.travel">Travel</h4>
              <h4 class="consumer-title" ng-if="industry_category.consumer">Consumer Goods</h4>

              <md-radio-group class="financial-data" ng-if="industry_category.financial">
                <md-radio-button class="questionnaire-radio-button" value="new_product"> New product, service, or offer </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="special_rates"> Special rates </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="events"> Event </md-radio-button>
              </md-radio-group>

              <md-radio-group class="travel-data" ng-if="industry_category.travel">
                <md-radio-button class="questionnaire-radio-button" value="new_product"> Full travel package </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="special_rates"> Tours </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="events"> Accomodations </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="events"> Transportation </md-radio-button>
              </md-radio-group>

              <md-radio-group class="insurance-data" ng-if="industry_category.insurance">
                <md-radio-button class="questionnaire-radio-button" value="new_product"> New product or offer </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="special_rates"> New/special features of existing products </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="events"> Event </md-radio-button>
              </md-radio-group>

              <md-radio-group class="consumer-goods-data" ng-if="industry_category.consumer">
                <md-radio-button class="questionnaire-radio-button" value="new_product"> Free shipping/delivery </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="special_rates"> Discount or deal/Sale </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="events"> New product </md-radio-button>
              </md-radio-group>
            </section>
            <section>
              <p>Which of these applies to your offer?</p>
              <md-radio-group>
                <md-radio-button class="questionnaire-radio-button" value="limited_stock"> Limited time offer </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="limited_stock_consumer"> Limited stock (consumer goods only) </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="online"> Online only </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="weekly"> Weekly or daily </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="invitation"> Invitation or announcement (for events and new products) </md-radio-button>
              </md-radio-group>
            </section>
            <section>
              <p>What image format are you planning to use?</p>
              <md-radio-group>
                <md-radio-button class="questionnaire-radio-button" value="single image"> Single Image </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="carousel image"> Carousel Image  <span class="coming_soon">Coming Soon!</span></md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="video"> Video <span class="coming_soon">Coming Soon!</span> </md-radio-button>
              </md-radio-group>
            </section>
            <section>
              <p>What kind of emotion is evident in your ad's image?</p>
              <md-radio-group>
                <md-radio-button class="questionnaire-radio-button" value="laughter"> Laughter </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="joy"> joy </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="awe"> Awe </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="excitement"> Excitement </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="sadnes"> Sadness </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="pain"> Pain </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="frustration"> Frustration </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="anger"> Anger </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="calm"> Calm </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="thoughtful"> Thoughtfulness </md-radio-button>
                <md-radio-button class="questionnaire-radio-button" value="other"> Other </md-radio-button>
              </md-radio-group>
            </section>
          </div>
          <div class="row predict-button-trigger text-center">
            <button class="btn custom-btn-Predict predict-btn-trigger" ng-click="predictResult()">PREDICT</button>
            <md-progress-circular class="predict-progress" md-mode="indeterminate" md-diameter="30" style="margin: 0 auto" hidden></md-progress-circular>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row prediction-container" style="margin-top: 20px;">
  <div class="col-md-12">
    <div class="col-md-5 col-md-offset-1 prediction-cursor-box">
      <div class="panel panel-default">
        <div class="panel-body text-center no-padding" ng-click="category('image-category')">
          <h1>IMAGE</h1>
        </div>
      </div>
    </div>
    <div class="col-md-5 prediction-cursor-box">
      <div class="panel panel-default">
        <div class="panel-body text-center no-padding" ng-click="category('text-category')">
          <h1>TEXT</h1>
        </div>
      </div>
    </div>
  </div>
  <!-- image-category -->
  <div class="col-md-12 image-category">
    <div class="col-md-4 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading no-border-color">
          <h4>CAMPAIGN DESCRIPTION</h4>
          <h6>TARGET AUDIENCE:</h6>
          <div class="row">
            <div class="col-md-6">
              <img src="../../img/avatar2.png" style="width: 100%;">
            </div>
            <div class="col-md-6">
              <h6>Female</h6>
              <h6>Aged 25-35</h6>
              <h6>Philippines</h6>
              <h6>Married with kids</h6>
              <h6>Ave. Income: $100,000/year</h6>
            </div>
          </div>
        </div>
        <div class="panel-body gray-color no-padding">
          <h6 style="margin-left: 5px;">OBJECTIVE: <span style="font-weight: bold;"> INCREASE CTR </span></h6>
        </div>
      </div>
      <div class="row">
        <section class="custom-row-predict-image-elements">
          <h4>IMAGE ELEMENTS <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>You may edit the algorithm suggestion below by selecting the variations in the elements</h6>
          <h6>Calculated effects of the elements changes according to industries and geography</h6>
        </section>
        <p class="recommended-image-elements-p">Our Recommended Image Elements: </p>
        <section class="custom-row-predict-text-elements" style="margin-top: 0;padding-top: 0;">
          <h4>IMAGE FOCUS <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>Human focus has a passive impact of 24% on the performance of sponsored content</h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="human"> Human </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="product"> Product </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="nature"> Nature </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>IMAGE MOVEMENT <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>Stationary image has a passive impact of 20% on the performance of sponsored content</h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="action_shot"> Action Shot </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="stationary"> Stationary </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>EMOTION <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>Passive emotion has a passive impact of 34% on the performance of sponsored content</h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="positve"> Positve </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="neutral"> Neutral </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="negative"> Negative </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>EYE CONTACT <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>Direct eye contact has a passive impact of 33% on the performance of sponsored content</h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="direct"> Direct </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="between_subjects"> Between subjects </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> No </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>BACKGROUND COLOR <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>Blue background has a passive impact of 27% on the performance of sponsored content</h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="direct"> Blue </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="between_subjects"> Green </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> White </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Red </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Brown </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Orange </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Gray </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Pink </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Yellow </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Black </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="no"> Purple </md-radio-button>
            </md-radio-group>
          </section>
        </section>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-body">
          <md-content>
            <md-tabs md-dynamic-height md-border-bottom>
              <md-tab label="Sample Images">
                <md-content class="md-padding">
                <section>
                  <h6>Below are serveral images with the elements that will work best for your campaign and target audience.
                  <br />
                  If you want to use any of these images in your sponsored content, click and image to select it.
                  </h6>
                </section>
                <section class="col-md-12 images-gallery">
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/1.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/2.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/3.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/4.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/5.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/6.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/7.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/8.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/9.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/10.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/11.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-4 custom-col-md-4">
                    <img src="../img/predict_gallery_images/12.jpg" class="img-responsive">
                  </div>
                </section>
                <section class="next-predict-container text-center">
                  <button class="btn btn-medium custom-button margin-top-20px">Next</button>
                </section>
                </md-content>
              </md-tab>
              <md-tab label="Content Overview" ng-click="imageFocusImageGraph()">
                <md-content class="md-padding">
                  <div class="row">
                    <div class="col-md-6">
                      <section class="wheel-of-emotions-container">
                        
                      </section>
                    </div>
                    <div class="col-md-6">
                      <div class="panel panel-default">
                        <div class="panel-heading custom-panel-heading-overview">
                          <h4>The Elements in your Content have been tested</h4>
                        </div>
                        <div class="panel-body no-padding">
                          <section class="subject-line-container">
                            <p class="subject-line-first pull-left">XXXXXXX times</p>
                          </section>
                        </div>
                      </div>

                      <div class="panel panel-default custom-panel-default-graph-container">
                        <div class="panel-body no-padding">
                          <div id="imageFocusImageBarGraph" style="height: 300px; width: 100%;"></div>
                          <section class="image-focus-graph-text">
                            <h6>Based on your industry, images using Human as image focus produces the best performance.</h6>
                          </section>
                        </div>
                      </div>
                    </div>
                  </div>
                </md-content>
              </md-tab>
            </md-tabs>
          </md-content>
        </div>
      </div>
    </div>
  </div>

  <!-- text category -->
  <div class="col-md-12 text-category" hidden>
    <div class="col-md-4 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading no-border-color">
          <h4>CAMPAIGN DESCRIPTION</h4>
          <h6>TARGET AUDIENCE:</h6>
          <div class="row">
            <div class="col-md-6">
              <img src="../../img/avatar2.png" style="width: 100%;">
            </div>
            <div class="col-md-6">
              <h6>Female</h6>
              <h6>Aged 25-35</h6>
              <h6>Philippines</h6>
              <h6>Married with kids</h6>
              <h6>Ave. Income: $100,000/year</h6>
            </div>
          </div>
        </div>
        <div class="panel-body gray-color no-padding">
          <h6 style="margin-left: 5px;">OBJECTIVE: <span style="font-weight: bold;"> INCREASE CTR </span></h6>
        </div>
      </div>
      <div class="row">
        <section class="custom-row-predict-image-elements">
          <h4>TEXT ELEMENTS <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6>You may edit the algorithm suggestion below by selecting the variations in the elements</h6>
          <h6>Calculated effects of the elements changes according to industries and geography</h6>
        </section>
        <p class="recommended-image-elements-p">Our Recommended Text Elements: </p>
        <section class="custom-row-predict-text-elements" style="margin-top: 0;padding-top: 0;">
          <h4>MESSAGE TYPE <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6></h6>
          <section class="custom-radio-buttons-container" style="float: left;margin-bottom: 10px;">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="announcement"> Announcement </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="special_offer"> Special Offer </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="production_promotion"> Production Promotion </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="reaction"> Reaction </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="invitation"> Invitation </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="conversation"> Conversation </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="social_responsibility"> Social Responsibility </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="content"> Contest </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <hr />
        <section class="custom-row-predict-text-elements">
          <h4>APPROACH <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6></h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="first_person"> 1st person </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="second_person"> 2nd person </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="third_person"> 3rd person </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>POINT-OF-VIEW <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6></h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="positve"> Positve </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="neutral"> Neutral </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="negative"> Negative </md-radio-button>
            </md-radio-group>
          </section>
        </section>
        <section class="custom-row-predict-text-elements">
          <h4>PRODUCT BENEFIT <i class="glyphicon glyphicon-info-sign custom-info-sign"></i></h4>
          <h6></h6>
          <section class="custom-radio-buttons-container">
            <md-radio-group>
              <md-radio-button class="custom-radio-buttons" value="guarantee"> Guarantee </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="long_coverage"> Long coverage</md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="cash_assistance"> Cash assistance </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="flexibility"> Flexibility </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="wide_age_coverage"> Wide age coverage </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="savings"> Savings </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="family_coverage"> Family coverage </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="convenience"> Convenience </md-radio-button>
              <md-radio-button class="custom-radio-buttons" value="wide_geographic_coverage"> Wide geographic coverage </md-radio-button>
            </md-radio-group>
          </section>
        </section>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-body no-padding">
          <md-content>
            <md-tabs md-dynamic-height md-border-bottom>
              <md-tab label="SAMPLE TEXTS">
                <md-content class="md-padding">
                <section>
                  <h6>We've analyzed your campaign brief and your ad's performance history and these are our top recommendations.
                  If you wish to use one of these entries, simply click on the text and click Next.
                  </h6>
                </section>
                <section class="table-list-text">
                  <div class="row">
                    <div class="col-md-3 col-md-offset-9 text-center">
                      <h5> Performance <br /> Matrix</h5>
                    </div>
                  </div>
                  <div class="row custom-table-row">
                      <div class="col-md-1">
                        <span>1.</span> 
                      </div>
                      <div class="col-md-9">
                        <span>Specializing in divorce case for 20 years. Divorce is extremely hard, let us help. Contact us today.</span>
                      </div>
                      <div class="col-md-1 text-center">
                        0.89
                      </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>2.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Jasper's Market is now open downtown. Like our page for the latest news and special invites!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.84
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>3.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Jasper's Market features a large selection of organic produce to help you meet all of your family's cooking needs. Stop by today.</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.89
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>4.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>With HootSuite Pro, your business has the power to manage social media, measures success, and save time. Try it FREE for 30 days!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.82
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>5.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Start saving time on social. Manage all your social networks from a simple dashboard with HootSuite Pro. Try it FREE for 30 days!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.79
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>6.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Deliciously wholesome snacks, delivered monthly. Try a free sample of five of our tastiest snacks, just pay $7.95 for shipping!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.76
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>7.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Happy President's Day! Enjoy 40% off with code 40FORU through Tuesday, Feb 17.</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.76
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>8.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Spend $50 and get $10 off your first order. Get your offer now!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.74
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>9.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Planning a vacation? Save up to 40% on Luxury hotels. Exclusive deals today!</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.73
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>10.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Dropbox for Business is the DropBox you know and love with more business features you need. Join the 100,000+ business today.</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.71
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>11.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Still have an adjustible rate mortgage? Don't wait for rates to rise. Refinance to a fixed rate today.</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.70
                    </div>
                  </div>
                  <div class="row custom-table-row">
                    <div class="col-md-1">
                      <span>12.</span> 
                    </div>
                    <div class="col-md-9">
                      <span>Save big on your first order of our locally sourced groceries! Jasper's Market delivers within the city. Order online today.</span>
                    </div>
                    <div class="col-md-1 text-center">
                      0.89
                    </div>
                  </div>
                </section>
                <section class="next-predict-container text-center">
                  <button class="btn btn-medium custom-button margin-top-20px">Next</button>
                </section>
                </md-content>
              </md-tab>
              <md-tab label="Content Overview" md-active="false" ng-click="imageFocusGraph()">
                <md-content class="md-padding">
                  <div class="row">
                    <div class="col-md-6">
                      <section class="wheel-of-emotions-container">
                        
                      </section>
                    </div>
                    <div class="col-md-6">
                      <div class="panel panel-default custom-panel-default-subject-line">
                        <div class="panel-heading custom-panel-heading-overview">
                          <h4>SUBJECT LINE LENGTH</h4>
                        </div>
                        <div class="panel-body no-padding">
                          <section class="subject-line-container">
                            <p class="subject-line-first pull-left">Maximum Character Count</p>
                            <p class="subject-line-second pull-right">120</p>
                          </section>
                          <section class="subject-line-container">
                            <p class="subject-line-first pull-left">Minimum Character Count</p>
                            <p class="subject-line-second pull-right">58</p>
                          </section>
                          <section class="subject-line-container">
                            <p class="subject-line-first pull-left">Average Character Count</p>
                            <p class="subject-line-second pull-right">104</p>
                          </section>
                          <section class="subject-line-container">
                            <p class="subject-line-first pull-left">Average Word Count</p>
                            <p class="subject-line-second pull-right">120</p>
                          </section>
                        </div>
                      </div>

                      <div class="panel panel-default custom-panel-default-graph-container">
                        <div class="panel-body no-padding">
                          <div id="imageFocusBarGraph" style="height: 300px; width: 100%;"></div>
                          <section class="image-focus-graph-text">
                            <h6>Based on your industry, images using Human as image focus produces the best performance.</h6>
                          </section>
                        </div>
                      </div>
                    </div>
                  </div>
                </md-content>
              </md-tab>
            </md-tabs>
          </md-content>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modals -->

<!-- #commentsPreview -->
<div class="modal right fade" tabindex="-1" role="dialog" id="commentsPreview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="commentschartContainer" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that used the 2nd person POV gained more Comments than those that didn't.</li>
                <li>The ads that used the 1st person point-of-view cost an average of 87 comments.</li>
                <li>The ads that used the 2nd person point-of-view cost an average of 139 comments.</li>
                <li>The ads that used the 3rd person point-of-view cost an average of 34 comments.</li>
                <li>In total, your past ads have gained an average of 90 Comments each.</li>
              </ul>
            </div>
          </div>
        </div>

        <p style="color: #7F8186; font-size: 15px;">The data collected from previous Facebook ad campaigns show that ads that have images with your brand incoporated into them perform better than those that don't.</p>
        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section>          
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="commentsPreview2" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="commentsContainer2" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines more than 10 words long had higher cost per click than those with 10 words or less.</li>
                <li>The average cost per click for your ads with headlines 1 - 5 words long is $0.73</li>
                <li>The average cost per click for your ads with headlines 6 - 10 words long is $0.54</li>
                <li>The average cost per click for your ads with headlines longer than 10 words is $1.48</li>
                <li>In total, your past ads' average cost per click is $0.92</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with headlines less than 10 words long:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="commentsreview3" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="commentschartContainer3" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines more than 10 words long had higher cost per click than those with 10 words or less.</li>
                <li>The average cost per click for your ads with headlines 1 - 5 words long is $0.73</li>
                <li>The average cost per click for your ads with headlines 6 - 10 words long is $0.54</li>
                <li>The average cost per click for your ads with headlines longer than 10 words is $1.48</li>
                <li>In total, your past ads' average cost per click is $0.92</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads that included a request, command, or call to action in the copy:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="commentsreview4" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="commentschartContainer4" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>The average cost per click for your ads with illustrated images is higher than those with realistic images </li>
                <li>The ads with realistic images had an average CPC of $0.50 </li>
                <li>The ads with illustrated images had an average CPC of $0.75</li>
                <li>In total average CPC of your ads was $0.63</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with realistic images:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: #commentsPreview -->
<!-- #cpcPreview -->
<div class="modal right fade" tabindex="-1" role="dialog" id="cpcPreview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="CpcchartContainer" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that used the second person point-of-view have lower costs per click.</li>
                <li>The ads that used the 1st person point-of-view cost an average of $0.75 per click.</li>
                <li>The ads that used the 2nd person point-of-view cost an average of $0.50 per click.</li>
                <li>The ads that used the 3rd person point-of-view cost an average of $1.48 per click.</li>
                <li>All of your ads cost ads an average of $0.91 per click.</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="cpcPreview2" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="CpcchartContainer2" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines more than 10 words long had higher cost per click than those with 10 words or less.</li>
                <li>The average cost per click for your ads with headlines 1 - 5 words long is $0.73</li>
                <li>The average cost per click for your ads with headlines 6 - 10 words long is $0.54</li>
                <li>The average cost per click for your ads with headlines longer than 10 words is $1.48</li>
                <li>In total, your past ads' average cost per click is $0.92</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with headlines less than 10 words long:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="cpcPreview3" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="CpcchartContainer3" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines more than 10 words long had higher cost per click than those with 10 words or less.</li>
                <li>The average cost per click for your ads with headlines 1 - 5 words long is $0.73</li>
                <li>The average cost per click for your ads with headlines 6 - 10 words long is $0.54</li>
                <li>The average cost per click for your ads with headlines longer than 10 words is $1.48</li>
                <li>In total, your past ads' average cost per click is $0.92</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads that included a request, command, or call to action in the copy:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="cpcPreview4" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="CpcchartContainer4" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>The average cost per click for your ads with illustrated images is higher than those with realistic images </li>
                <li>The ads with realistic images had an average CPC of $0.50 </li>
                <li>The ads with illustrated images had an average CPC of $0.75</li>
                <li>In total average CPC of your ads was $0.63</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with realistic images:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: #cpcPreview -->

<!-- #ctrPreview -->
<div class="modal right fade" tabindex="-1" role="dialog" id="ctrPreview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="ctrchartContainer" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that used the second person point-of-view have generated higher clicks through rates than your ads that use first or third person in the text.</li>
                <li>The ads that used the 1st person in the text have an average of 1.3% CTR.</li>
                <li>The ads that used the 2nd person in the text have an average of 0.54% CTR.</li>
                <li>The ads that used the 3rd person in the text have an average of 0.06% CTR.</li>                
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="ctrPreview2" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="ctrchartContainer2" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines that were 10 words or less performed better than those with longer headlines</li>
                <li>The ads with headlines at 1 - 5 words had an average click-through rate 0.89%</li>
                <li>The ads with headlines at 6 - 10 words had an average click-through rate f 1.3%</li>
                <li>The ads with headlines longer than 10 words had an average click-through rate of 0.06%</li>
                <li>The average click-through rate for your past ads is 0.75%</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="ctrPreview3" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="cdtrchartContainer3" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that include a request or call to action in the text have performed better than other</li>
                <li>The ads with a request, command or call to action had an average click-through rate of 1.10%</li>
                <li>The ads that posed questions in the text had an average click-through rate of 0.08%</li>
                <li>The ads that included funny statements had an average click-through rate of 0.16%</li>
                <li>The ads with other kinds of statements had an average click-through rate of 0.7%</li>
                <li>The average click-through rate of your past ads is 0.69%</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads that included a request, command, or call to action in the copy:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="ctrPreview4" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="ctrchartContainer4" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>The average cost per click for your ads with illustrated images is higher than those with realistic images </li>
                <li>The ads with realistic images had an average CPC of 1.5% </li>
                <li>The ads with illustrated images had an average CPC of 0.54%</li>
                <li>In total average CPC of your ads was 0.92%</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with realistic images:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: #ctrPreview -->

<!-- #likesPreview -->
<div class="modal right fade" tabindex="-1" role="dialog" id="likesPreview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="likeschartContainer" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that used the second person point-of-view gained more likes than the other ads.</li>
                <li>The ads that used the 1st person gained an average of 121 likes.</li>
                <li>The ads that used the 2nd person gained an average of 253 likes.</li>
                <li>The ads that used the 3rd person gained an average of 65 likes.</li>                
                <li>In total, your past ads have gained an average of 146 likes.</li>                
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="likesPreview2" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="likeschartContainer2" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines that were 10 words or less performed better than those with longer headlines</li>
                <li>The ads with headlines at 1 - 5 words long received an average of 124 likes </li>
                <li>The ads with headlines at 6 - 10 words long received an average of 258 likes </li>
                <li>The ads with headlines longer than 10 words long received an average of 69 likes </li>
                <li>Your past ads have received an average of 150 Likes each</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads less than 10 words long:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="likesPreview3" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="likeschartContainer3" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that include a request or call to action in the cpoy gained ahigh average number Likes</li>
                <li>The ads that with funny statements in the copy gained an average number of Likes</li>
                <li>The ads that included a call to action in the copy gained an average numer of Likes</li>
                <li>On average, your past ads gained 146 Likes each</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads that included a request, command, or call to action in the copy:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="likesPreview4" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="likeschartContainer4" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads with realistic images gained more Likes than those with illustrated images </li>
                <li>The ads with realistic images gained an average of 253 Likes each </li>
                <li>The ads with illustrated images gained an average of 121 Likes each</li>
                <li>In total average Likes of your past ads gained is 187</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with realistic images:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: #likesPreview -->

<!-- share preview -->
  <div class="modal right fade" tabindex="-1" role="dialog" id="sharePreview" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="sharechartContainer" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that used the 2nd person POV gained more Comments than those that didn't.</li>
                <li>The ads that used the 1st person point-of-view cost an average of 87 comments.</li>
                <li>The ads that used the 2nd person point-of-view cost an average of 139 comments.</li>
                <li>The ads that used the 3rd person point-of-view cost an average of 34 comments.</li>
                <li>In total, your past ads have gained an average of 90 Comments each.</li>
              </ul>
            </div>
          </div>
        </div>

        <p style="color: #7F8186; font-size: 15px;">The data collected from previous Facebook ad campaigns show that ads that have images with your brand incoporated into them perform better than those that don't.</p>
        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Your most successful ads with this element.</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
            <div class="col-md-12" style="margin-bottom:10px;">
              <img src="../../img/predict_images/sample_1.png" style="width:100%;">
            </div>
            <div class="col-md-12" style="margin-bottom:10px;">
              <img src="../../img/predict_images/sample_2.png" style="width:100%;">
            </div>
            <div class="col-md-12" style="margin-bottom:10px;">
              <img src="../../img/predict_images/sample_3.png" style="width:100%;">
            </div>
          </section>          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" tabindex="-1" role="dialog" id="sharePreview2" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="sharechartContainer2" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads headlines that were 10 words or less performed better than those with longer headlines</li>
                <li>The ads with headlines at 1 - 5 words long received an average of 124 likes </li>
                <li>The ads with headlines at 6 - 10 words long received an average of 258 likes </li>
                <li>The ads with headlines longer than 10 words long received an average of 69 likes </li>
                <li>Your past ads have received an average of 150 Likes each</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads less than 10 words long:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="sharePreview3" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="sharechartContainer3" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads that include a request or call to action in the cpoy gained ahigh average number Likes</li>
                <li>The ads that with funny statements in the copy gained an average number of Likes</li>
                <li>The ads that included a call to action in the copy gained an average numer of Likes</li>
                <li>On average, your past ads gained 146 Likes each</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads that included a request, command, or call to action in the copy:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal right fade" tabindex="-1" role="dialog" id="sharePreview4" aria-labelledby="myModalLabel">
  <div class="modal-dialog custom-modal-dialog-preview">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close pull-left" style="margin-top: 0px!important;margin-right: 10px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>How this has worked for you in the past</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <div id="sharechartContainer4" style="height: 300px; width: 100%;"></div>
          </div>
          <div class="col-md-12">
            <div class="graph-desc">
              <p>We collected some data on your past ads' insights. We've found that: </p>
              <ul>
                <li>Your past ads with realistic images gained more Likes than those with illustrated images </li>
                <li>The ads with realistic images gained an average of 253 Likes each </li>
                <li>The ads with illustrated images gained an average of 121 Likes each</li>
                <li>In total average Likes of your past ads gained is 187</li>
              </ul>
            </div>
          </div>
        </div>

        <hr />
        <p style="margin: 0;font-size: 15px;color: #7F8186;">Here are your most successful past ads with realistic images:</p>
        <div class="col-md-12" style="margin-top: 20px;">
          <section>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_1.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_2.png" style="width:100%;">
              </div>
              <div class="col-md-12" style="margin-bottom:10px;">
                <img src="../../img/predict_images/sample_3.png" style="width:100%;">
              </div>
          </section> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end of share preview -->