<!-- <div class="col-md-2" style="width: 20%;">
	<div class="navigation">
		<ul class="nav">
			<div class="triangle" style="background:#000;"></div>
			<li><a ui-sref="home"><i class="fa fa-home"></i></a></li>
			<li style="background:#000;"><a ui-sref="saved-results"><i class="fa fa-flask"></i></a></li>
			<li><a ui-sref="summary"><i class="fa fa-bar-chart"></i></a></li>
		</ul>
	</div>
	<div class="main-desc-wrapper">
		<div class="experiment-description">
			<h3 style="">Parameters:</h3>
			<div class="description">
				<label>Product:</label>
				<p>Mobile Plan</p>
			</div>
			<div class="description">
				<label>Audience:</label>
				<p>Australia, Male &amp; Female</p>
			</div>
			<div class="description">
				<label>Objective:</label>
				<p>Increase sales</p>
			</div>
			<div class="description">
				<label>Category:</label>
				<p style="margin:0">#Health</p>
				<p style="margin:0">#Food</p>
				<p style="margin:0">#Summer</p>
			</div>
		</div>
	</div>
</div> -->

<div class="sub-content-wrapper col-md-9" predict-result>
	<div class=" predict-view-wrapper">
		<section class="predict-result-container">			
			<div class="row">
				<div class="col-md-12">
					<div class="gallery-title">
						<!-- <h1 class="no-margin">Category: <span ng-bind="galleryImageData.theme"></span></h1> -->
						<h3 class="no-margin">Interests: <span style="font-size:18px;padding:10px;display:inline-block;">#Health #Food #Summer</span></h3>
					</div>
					<div class="alert alert-info msg msg002">
						<p>
							<strong>
								Based on a library of 3,231,938 images, PrecisionBit has classified the themes that are driving the highest engagement.
							</strong>
							<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
						</p>
					</div>
					<br>
				</div>
				<div class="col-md-6">
					<div class="gallery-preview-wrapper">

						<h2 class="pull-left no-margin" ng-if="galleryImageData.themeTitle">Theme: <span style="color:#28A6A0;" ng-bind="galleryImageData.themeTitle"></span></h2>
						<h2 class="pull-left no-margin" ng-if="!galleryImageData.themeTitle">Theme: Phone</h2>	

						<div class="gallery-preview">
							<img src="{{imgPreview}}" width="100%" class="img img-responsive display-image">
						</div>
						<div class="gallery-thumbnail">
							<!-- <div id="owl-example" class="owl-carousel">
								<div ng-repeat="img in galleryImageData.galleryImages"><img ng-src="{{img}}" ng-click="previewGalleryImg( img , 1 )" class="img img-thumbnail display-image"></div>
							</div> -->
							<div owl-carousel="galleryImageData.galleryImages">
								<div class="item"><img ng-src="{{item}}" ng-click="previewGalleryImg( item , $index )" class="img img-thumbnail display-image"></div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="image-elements text-left">
								<h3>Image Elements</h3>
								<p>
									<span class="element-attribut">Object : </span>
									<span class="element-value" ng-if="galleryImageData.elemObject" ng-bind="galleryImageData.elemObject"></span>
									<span class="element-value" ng-if="!galleryImageData.elemObject">No value</span> <!-- if no value -->
								</p>
								<p>
									<span class="element-attribut">Scene : </span>
									<span class="element-value" ng-if="galleryImageData.elemScene" ng-bind="galleryImageData.elemScene"></span>
									<span class="element-value" ng-if="!galleryImageData.elemScene">No value</span> <!-- if no value -->
								</p>
								<p>
									<span class="element-attribut">Emotion : </span>
									<span class="element-value" ng-if="galleryImageData.elemEmotion" ng-bind="galleryImageData.elemEmotion"></span>
									<span class="element-value" ng-if="!galleryImageData.elemEmotion">No value</span> <!-- if no value -->
								</p>
								<p>
									<span class="element-attribut">Composition : </span>
									<span class="element-value" ng-if="galleryImageData.elemComposition" ng-bind="galleryImageData.elemComposition"></span>
									<span class="element-value" ng-if="!galleryImageData.elemComposition">No value</span> <!-- if no value -->
								</p>
								<p>
									<span class="element-attribut">Lighting : </span>
									<span class="element-value" ng-if="galleryImageData.elemLighting" ng-bind="galleryImageData.elemLighting"></span>
									<span class="element-value" ng-if="!galleryImageData.elemLighting">No value</span> <!-- if no value -->
								</p>
							</div>
						</div>

						<div class="col-md-6 text-center">
							<h2>Engagement Score: <span ng-bind="galleryImageData.engaging_score"></span></h2>
							<br>
							<a href="javascript:void(0)" ng-click="downloadPreview($event)" class="btn btn-lg btn-primary">VIEW BRIEF</a>
						</div>


					</div>
				</div>

				<div class="col-md-6">
					<div class="title-heading">
						<!-- <h3 class="no-margin">Themes</h3> -->
					</div>

					<div class="theme-lists">

						<div class="list-wrapper" ng-repeat="theme in themeData" id="{{theme.id}}">
							<div class="col-md-12 text-center" ng-click="themePreview( theme.id, false )" style="outline:none;">
								<div class="theme-title">
									<label ng-bind="theme.themeTitle"></label>
								</div>
							</div>
							<div class="col-md-12 theme-list-gallery">
								<div class="badge-wrapper" ng-click="themePreview( theme.id, false )" style="outline:none;">
									<img ng-src="../img/badge/{{$index + 1}}.png" width="75">
								</div>
								<div class="text-center" ng-click="themePreview( theme.id, false )" style="outline:none;">
									<p>Engagement Score: <span ng-bind="theme.engaging_score"></span></p>
								</div>
								<div class="gallery-thumbnail">
									<div owl-carousel="theme.galleryImages">
										<div class="item"><img ng-src="{{item}}" ng-click="previewGalleryImg( item , true, theme.id )" class="img img-thumbnail display-image"></div>
									</div>
								</div>
							</div>
						</div>



					</div>

				</div>

			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(".owl-carousel").owlCarousel();
		$("[name='overlay-switch']").bootstrapSwitch( 'size' , 'mini' );
	});
</script>