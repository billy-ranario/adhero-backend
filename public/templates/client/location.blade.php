<div class="col-md-12 location" style="padding: 15px 55px;">
	<div class="col-md-12 no-padding-left" style="margin-bottom:10px;">
		<span class="">SEARCH BY LOCATION</span>
	</div>
	<br >
	<div class="col-md-7 col-sm-12 col-xs-12 no-padding" >
		<!-- <div class="map-container" map-lazy-load="https://maps.google.com/maps/api/js" >
		  <ng-map center="Singapore" zoom="11" style="height: 340px;"></ng-map>
		</div> -->
		<div id="map" style="height: 340px;width: 98%"></div>
	</div>
	<div class="col-md-5 col-sm-12 col-xs-12  no-padding">
		<div class="right-wrapper">
			<div class="col-md-12 no-padding">
				<div class="form-inline search-wrapper">
					<input type="text" class="form-control search-location" placeholder="Search...">
					<button type="submit" class="location-btn">Submit</button>
				</div>
			</div>
			<div class="col-md-12 no-padding">
				<table class="table table-responsive location-table" style="box-shadow: 1px 2px 1px #BBB;">
					<thead>
						<tr>
							<th>Location</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>ION Orchard</td>
							<td>Completed</td>
							<td><a ui-sref="location-result" href="">Show Result</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>

    var map;
    var myLatLng = {lat: 1.3006821, lng: 103.7907011};
    var pinIcon = new google.maps.MarkerImage(
		    "img/home_logo.png",
		    null, /* size is determined at runtime */
		    null, /* origin is 0,0 */
		    null, /* anchor is bottom center of the scaled image */
		    new google.maps.Size(80, 60)
		);

		function initializeMap() {
			console.log("Initializing map !");
    	map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 11
	    });

	    var marker = new google.maps.Marker({
		    position: myLatLng,
		    map: map,
		    animation: google.maps.Animation.DROP,
		    icon: pinIcon
		  });
	  }

	  initializeMap();
    
</script>