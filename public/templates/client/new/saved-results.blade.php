<div class="col-md-8">
	<div class="results-container row">
		<div class="col-md-12">
	   		<a href="javascript:void(0)" ng-click="showPredictionForm()" class="btn custom-btn-prediction">New Prediction</a>
			<h2>Saved Results</h2>
			<br>
			<div class="dropdown">
			  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    Sort by date/time
				    <span class="caret"></span>
			  	</button>
			  
			  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="javascript:void(0)" ng-click="sortResult( 'latest' )">Latest</a></li>
					<li><a href="javascript:void(0)" ng-click="sortResult( 'oldest' )">Oldest</a></li>
			  	</ul>
			</div>

			<hr>
			<table class="table table-hover table-bordered table-striped table-bordered" >
				<thead>
					<th>Topic</th>
					<th style="width:320px">Audience Target: Country, Gender</th>
					<th>Date</th>
					<th>Status</th>
				</thead>
				<tbody>
					<tr ng-repeat="list in clientObject.predict | orderBy:'+':statusView" id="{{list.predict_id}}">
						<td>
							<span ng-bind="list.hashtags"></span>
						</td>
						<td>
							<span ng-bind="list.country"></span>,
							<span ng-bind="list.gender">
								<!-- <span ng-bind="gender"></span> -->
							</span>
						</td>
						<td>
							<span ng-bind="list.date_created"></span>
						</td>
						<td class="text-center">
							<span class="pull-left" ng-bind="list.status"></span>
							<button ng-if="list.status == 'Completed' "class="btn btn-primary btn-sm" ng-click="showresult(list)">Show Result</button>
							<button class="btn btn-danger btn-sm pull-right" ng-click="deleteExperiment( $event, list )">Delete</button>
						</td>
					</tr>
				</tbody>
			</table>	
		</div>
	</div>
</div>
<div class="col-md-4" id="predict-form-content">
	<form>		
		<div class="content-heading">
			<label>Product</label>
		</div>
		<div layout-gt-sm="row">
			<md-input-container class="md-block" flex-gt-sm>
				<label style="font-weight:normal">Campaign Name</label>
				<input type="text" ng-model="predictObject.campaign_name">
			</md-input-container>
		</div>
		<div layout-gt-sm="row">
			<md-input-container class="md-block" flex-gt-sm>
				<label style="font-weight:normal">Product Name</label>
				<input type="text" ng-model="predictObject.product_name">
			</md-input-container>
		</div>

		<div class="content-heading">
			<label>Audience (optional)</label>
		</div>
		<div layout-gt-sm="row">
		<md-input-container class="md-block" flex-gt-sm style="width:50%;padding-right:5px;">
				<label style="font-weight:normal">Country</label>
				<input type="text" ng-model="predictObject.country">
			</md-input-container>

			<md-select placeholder="Gender" ng-model="predictObject.gender" md-on-open="" style="width:50%;padding-left:5px;">
				<md-option value="Male">Male</md-option>
				<md-option value="Female">Female</md-option>
				<md-option value="Both">Both</md-option>
			</md-select>
		</div>

		<div class="content-heading">
			<label>Objective</label>
		</div>
		<div layout-gt-sm="row">
			<md-select placeholder="Objective" ng-model="predictObject.objective" md-on-open="" style="width:100%">
				<md-option value="Clicks to Website">Clicks to Website: Send people to your website.</md-option>
				<md-option value="Website Conversions">Website Conversions: Increase conversions on your website. <!-- You'll need a conversion pixel for your website before you can create this ad. --></md-option>
				<md-option value="Page Post Engagement">Page Post Engagement: Boost your posts.</md-option>
				<md-option value="Page Likes">Page Likes: Promote your Page and get Page likes to connect. <!-- with more of the people who matter to you. --></md-option>
				<md-option value="App Installs">App Installs: Get installs of your app.</md-option>
				<md-option value="App Engagement">App Engagement: Increase engagement in your app.</md-option>
				<md-option value="Offer Claims">Offer Claims: Create offers for people to redeem in your store.</md-option>
				<md-option value="Local Awareness">Local Awareness: Reach people near your business.</md-option>
				<md-option value="Event Responses">Event Responses: Raise attendance at your event.</md-option>
				<md-option value="Video Views">Video Views: Create ads that get more people to view a video.</md-option>
				<md-option value="Increase Sales">Increase Sales</md-option>
			</md-select>
		</div>
		<div class="col-md-12 no-padding social-media-publish">
			<p>Publish your work in:</p>
			<button>
				<i class="fa fa-facebook"></i> | <span>Facebook</span>
			</button>
			<button>
				<i class="fa fa-instagram"></i> | <span>Instagram</span>
			</button>
			<br>
			<br>
		</div>
		<div class="content-heading">
			<label>Fill in your hashtag groups</label>
		</div>
		<div class="col-md-12 no-padding">
			<md-input-container class="md-block col-md-4 no-padding" style="padding:0 5px !important" flex-gt-sm>
				<label style="font-weight:normal">#hashtag</label>
				<input type="text" ng-model="predictObject.hashtag[0]">
			</md-input-container>
			<md-input-container class="md-block col-md-4 no-padding" style="padding:0 5px !important" flex-gt-sm>
				<label style="font-weight:normal">#hashtag</label>
				<input type="text" ng-model="predictObject.hashtag[1]">
			</md-input-container>
			<md-input-container class="md-block col-md-4 no-padding" style="padding:0 5px !important" flex-gt-sm>
				<label style="font-weight:normal">#hashtag</label>
				<input type="text" ng-model="predictObject.hashtag[2]">
			</md-input-container>

			<md-input-container class="md-block col-md-6 no-padding" style="padding:0 5px !important" flex-gt-sm>
				<label style="font-weight:normal">#hashtag</label>
				<input type="text" ng-model="predictObject.hashtag[3]">
			</md-input-container>
			<md-input-container class="md-block col-md-6 no-padding" style="padding:0 5px !important" flex-gt-sm>
				<label style="font-weight:normal">#hashtag</label>
				<input type="text" ng-model="predictObject.hashtag[4]">
			</md-input-container>
		</div>
		<div class="col-md-12 no-padding formSubmit">
			<button type="submit" class="submit">Submit</button>
		</div>
	</form>
</div>