<div class="sub-content-wrapper col-md-10" predict-result>
	<div class=" predict-view-wrapper">
	<section class="predict-result-container">			
			<div class="row custom-predict-row text-center">
				<p>We've sourced the best photos that will engage your target audience</p>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<section class="predict-image-container">

							<img src="../img/{{defaultImage}}" ng-if="!predictResult" class="img-responsive img-thumbnail">
							<img ng-if="predictResult" ng-src="http://52.77.17.243/upload_get_client/{{ predictResult.filename }}" class="img-responsive img-thumbnail">

							<section class="text-center text-predict-image-container">
								<h3 ng-if="!predictResult.headingtext">New phone. Own it.</h3>
								<h3 ng-show="predictResult.headingtext" ng-bind="predictResult.headingtext"></h3>


							</section>

							<!-- Dropdown for Image Tag inputs -->
							<ul class="nav pull-right">
								<li class="dropdown" id="menuLogin">
									<a class="dropdown-toggle btn btn-default" href="javascript:void(0)" data-toggle="dropdown" id="navLogin">Image Tags</a>
									<div class="dropdown-menu" style="padding:17px;min-width: 300px;">
										<form class="form" id="formLogin"> 
											<label ng-if="!predictResult.img_tags">No tags available</label>
											<div class="tag-content" ng-repeat="tag in predictResult.img_tags">
												<p ng-bind="tag"></p>
											</div>												
										</form>
									</div>
								</li>
							</ul>
							<!-- End: Dropdown for Image Tag inputs -->

						</section>
						<section class="predict-info-container">
							<section class="list-predict-info" style="margin-top: 10px;">
								<i class="glyphicon glyphicon-info-sign custom-glyphicon-info-sign"></i>
								<h4>Predict CTR: 
									<span ng-if="!predictResult.client_upload_data[0].ctr">4-5</span>
									<span ng-if="predictResult.client_upload_data[0].ctr" ng-bind="predictResult.client_upload_data[0].ctr"></span>									
									%
								</h4>
							</section>
							<section class="list-predict-info">
								<i class="glyphicon glyphicon-info-sign custom-glyphicon-info-sign"></i>
								<h4>Emotional Response: 									
									<span ng-if="!predictResult.client_upload_data[0].emotional_response">20</span>
									<span ng-if="predictResult.client_upload_data[0].emotional_response" ng-bind="predictResult.client_upload_data[0].emotional_response"></span>									
								</h4>
							</section>
							<section class="list-predict-info">
								<i class="glyphicon glyphicon-info-sign custom-glyphicon-info-sign"></i>
								<h4>Aesthetic Score: 
									<span ng-if="!predictResult.client_upload_data[0].aesthetic_score">3.5</span>
									<span ng-if="predictResult.client_upload_data[0].aesthetic_score" ng-bind="predictResult.client_upload_data[0].aesthetic_score"></span>									
								</h4>
							</section>
						</section>
						<section class="chart-container">
							<canvas class="pull-left" id="myChart" width="150" height="150"></canvas>
							<h5>
								<span ng-if="!predictResult">75</span>
								<span ng-if="predictResult.client_upload_data[0].engaging_score" ng-bind="predictResult.client_upload_data[0].engaging_score"></span>
								% of your audience find this photo engaging
							</h5>							
						</section>
					</div>
					<div class="col-md-6">
						<section class="image-result-show-container">
							<div class="panel panel-default custom-default-image-result-show-container">
								<div class="panel-heading text-center">
									<h4><span style="color: #56585F;">Curated Images</span> <i class="glyphicon glyphicon-info-sign custom-info-show-images"> </i></h4>
								</div>

								<!-- If for demo, show demo images -->
								<div class="panel-body" ng-if="!predictResult">
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo1.png" class="img-responsive thumbnail img-thumbnail" ng-click="showPicture('photo1.png')">
										</div>
										<div class="col-md-6">
											<img src="../img/photo2.png" class="img-responsive thumbnail img-thumbnail" ng-click="showPicture('photo2.png')">
										</div>
									</div>
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo3.jpg" class="img-responsive thumbnail img-thumbnail" ng-click="showPicture('photo3.jpg')">
										</div>
										<div class="col-md-6">
											<img src="../img/photo4.jpg" class="img-responsive thumbnail img-thumbnail" ng-click="showPicture('photo4.jpg')">
										</div>
									</div>
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo1.png" class="img-responsive thumbnail img-thumbnail">
										</div>
										<div class="col-md-6">
											<img src="../img/photo2.png" class="img-responsive thumbnail img-thumbnail">
										</div>
									</div>
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo1.png" class="img-responsive thumbnail img-thumbnail">
										</div>
										<div class="col-md-6">
											<img src="../img/photo2.png" class="img-responsive thumbnail img-thumbnail">
										</div>
									</div>
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo1.png" class="img-responsive thumbnail img-thumbnail">
										</div>
										<div class="col-md-6">
											<img src="../img/photo2.png" class="img-responsive thumbnail img-thumbnail">
										</div>
									</div>
									<div class="row row-divider">
										<div class="col-md-6">
											<img src="../img/photo1.png" class="img-responsive thumbnail img-thumbnail">
										</div>
										<div class="col-md-6">
											<img src="../img/photo2.png" class="img-responsive thumbnail img-thumbnail">
										</div>
									</div>
								</div>

								<!-- If not for demo, show real images -->
								<div class="panel-body" ng-if="predictResult">
									<div class="col-md-6" ng-repeat="img in clientImageObject" style="height:140px;" style="margin-top: 20px!important;">
										<img src="http://52.77.17.243/upload_get_client/{{ img.filename }}" class="img-responsive thumbnail img-thumbnail" ng-click="showPicture(img)">
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>