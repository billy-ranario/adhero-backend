<div upload-client>
	<div class="col-md-10 sub-content-wrapper" style="background:#ECF0F1;border-color:#ECF0F1;padding:0;overflow:visible;">
		<div class="images-container row" style="background:#ECF0F1;border-color:#ECF0F1;padding:0;overflow:visible;">
			<div class="uploaded-images col-md-12" style="overflow:visible;padding:0;">
				
				<div class="row" style="overflow:visible;">
					<div class="col-md-12" style="overflow:visible;">


						<div class="row" id="imgupload-wrapper" hidden>
							<div class="col-lg-12">
								<div class="row">
									<div class="col-lg-3">
										<div class="form-group select-grp" hidden>
									 		<label>Select Group</label><a class="hide-select" href="" style="font-size:12px;"> Back</a>
									 		<select class="form-control group-name">
									 			<option ng-repeat="cat in userImageUploads | unique: 'category' ">{{cat.category}}</option>
									 		</select>
									 	</div>	
								 		<div class="form-group create-grp">
									 		<label>Create New Group</label> or <a class="shw-select" href="" style="font-size:12px;"> Select</a>
									 		<input type="text" class="form-control group-name">
									 	</div>	

									</div>
								</div>
							 	<form enctype="multipart/form-data">
								    <div class="form-group">
								      <div id="kv-error-1" style="margin-top:10px;display:none"></div>
								      <div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
								      <input ng-model="search" id="input-id" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
								    </div>
								  </form>
							</div>
						</div>
						<div class="row upload-img-options" style="overflow:visible;padding-left:15px;padding-right:15px;">
							<div class="btn-group" style="width: 22%;">
								<i class="glyphicon glyphicon-search" style="position: absolute;left:0;padding: 10px;pointer-events: none;"></i>
							    <input ng-model="search" type="text" class="form-control" placeholder="Search" style="padding-left: 30px;" />
							</div>

							<div class="filters-wrapper">
								<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-radius: 4px;">
									   <i class="fa fa-filter"></i>
									   Filters
									</button>
									<ul class="dropdown-menu">
									  <li ng-repeat="(key, value) in userImageUploads | groupBy: 'category'  "><a href="" ng-click="getCategoryImages(key)">{{key}}</a></li>
									</ul>

								</div>

								<div class="btn-group">
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-radius: 4px;">
								    <i class="fa fa-exchange" style="transform:rotate(90deg);"></i>
								    	<span class="sortType">A - Z</span> 
								    <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a ng-click="sortBy('filename',false)">A - Z</a></li>
								    <li><a ng-click="sortBy('filename',true)">Z - A</a></li>
								    <li><a ng-click="sortBy('timeCreated',true)">Newest</a></li>
								    <li><a ng-click="sortBy('timeCreated',false)">Oldest</a></li>
								  </ul>
								</div>

								<div class="btn-group" >
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-radius: 4px;">
								    <i class="fa fa-file-image-o"></i>
								    <span class="view-type">Gallery</span> <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a ng-click="changeView('filmstrip')">Filmstrip</a></li>
								    <li><a ng-click="changeView('gallery')">Gallery</a></li>
								    <li><a ng-click="changeView('list')">List</a></li>
								  </ul>
								</div>
							</div>

							<div class="btn-group" style="width: 17.666667%;">
								<button class="form-control" ng-click="showUploadInput()" style="">Upload Image</button>	
							</div>
							
						</div>						
						<div class="images">

							<div class="gallery-category-images" hidden>
								<label class="gallery-category-title" style="padding-left:20px;">
									<span class="category-title">Group Name</span> <a ng-click="backToCategory()" style="font-weight:500;font-size:10px;">Back</a>
								</label>
								<br>
								<a href="" class="groups grp" data-filename="{{img.filename}}" ng-repeat="img in CategoryImages | filter:search | orderBy:sortType:sortReverse">

									<div class="group-img">
										<img src="http://52.77.17.243/get_client_personal_upload/{{img.filename}}" class="img-responsive img-thumbnail">
									</div>
									<!-- <div class="group-title">
										Group Name
										{{ img.filename }}

									</div> -->
									<label class="delete-wrapper" ng-click="deleteImg($event, img)">								
										<i class="glyphicon glyphicon-trash"></i>								
									</label>
								</a>

							</div>
							
							<div class="gallery">
								<div class="groups grp"  data-category="{{key}}" ng-repeat="(key, value) in userImageUploads | filter:search | orderBy:sortType:sortReverse | groupBy: 'category'  ">
									<div class="group-img">
										<a href="" ng-click="getCategoryImages(key)">
											<img src="img/4.png" class="img-responsive img-thumbnail">
										</a>
										
									</div>
									<div class="group-title">
										<a href="" ng-click="getCategoryImages(key)">
											<!-- Group Name -->
											{{ key }}
										</a>
									</div>
									<div class="group-status">
										<span>Images : {{ value.length }}</span>
										<!-- <span>{{ img.timeCreated | date:"MM/dd/yyyy 'at' h:mma" }}</span> -->
										<label class="delete-wrapper" ng-click="deleteGroup($event, value[0])">								
											<i class="glyphicon glyphicon-trash"></i>								
										</label>
									</div>
								</div>

							</div>

							<div class="list" style="display:none;">
								<ul style="">
									<li class="grp" data-category="{{key}}" ng-repeat="(key, value) in userImageUploads | filter:search | orderBy:sortType:sortReverse | groupBy: 'category'  ">
										<!-- <label class="delete-wrapper" ng-click="deleteImg($event, img)">								
											<i class="glyphicon glyphicon-trash"></i>								
										</label> -->
										<a href="javascript:void(0)" ng-click="getCategoryImages(key)" id="{{img}}" >
											{{ key }}
											<span> Images : {{ value.length }}</span>
										</a>
									</li>
								</ul>
							</div>

							<div class="filmstrip" style="display:none;">
								<a href="" ng-click="getCategoryImages(key)" class="strips grp" data-category="{{key}}" ng-repeat="(key, value) in userImageUploads | filter:search | orderBy:sortType:sortReverse | groupBy: 'category'  ">
									<div class="top" >
										<!-- <label class="delete-wrapper" ng-click="deleteImg($event, img)">								
											<i class="glyphicon glyphicon-trash"></i>								
										</label> -->
										<span class="group-title" style="font-weight: 600;">{{ key }}</span>
										<span>Images : {{ value.length }}</span>
									</div>
									<div class="bottom">
										<div class="strip-img">

											<img ng-repeat="img in value | limitTo:4" src="telstra_images/{{img.filename}}" class="img-thumbnail">
										</div>	
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})	
</script>