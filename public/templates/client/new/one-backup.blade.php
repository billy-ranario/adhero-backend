<!-- <div class="home col-md-12 custom-ui-view">

	<a ui-sref="predict">
		<div class="predict">
			<div class="icon"><i class="fa fa-flask"></i></div>
			<div class="title">Predict</div>
			<div class="desc">Look at which images works best for you</div>
		</div>
	</a>
	
	<a ui-sref="summary">	
		<div class="report">
			<div class="icon"><i class="fa fa-bar-chart"></i></div>
			<div class="title">Report</div>
			<div class="desc">Analyse and compare your historical images</div>
		</div>
	</a>
</div> -->

<div class="home col-md-12 custom-ui-view" home>
	<div class="col-md-12 engagement">
		<span class="eng-title">Your Engagement</span><br>

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-comments" style="background:#D84D52;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#D84D52;">209,412</p>
				<p class="desc"> Posts </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-user" style="background:#F57268;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#F57268;">209,412</p>
				<p class="desc"> Users </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-file-text-o" style="background:#F6D561;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#F6D561;">2,926,971</p>
				<p class="desc"> Reads </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-eye" style="background:#53BCFF;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#53BCFF;">1,982,572</p>
				<p class="desc"> Impressions </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<img src="img/theme.png" style="background:#946EB7;padding: 8px;width: 50px;height: 50px;margin: 10px 0;border-radius: 50%;">
			</div>
			<div class="col-md-7 specs" >
				<p class="digits" style="color:#946EB7;">24</p>
				<p class="desc"> Theme </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos" >
			<div class="col-md-5 eng-icon">
				<img src="img/shake.png" style="background:#323D7B;padding: 8px;width: 50px;height: 50px;margin: 10px 0;border-radius: 50%;">
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#323D7B;">14.3%</p>
				<p class="desc"> Engagements </p>
			</div>
		</div>	
	</div>

	<div class="col-md-12 engage-btm">
		<div class="col-md-6 left" style="padding-left: 0;">
			<br>
			<div class="col-md-6 left-box" style="margin-left:0;">
				<p class="box-title" style="padding:0;height:28px;"><span style="line-height: 2.428571;margin-left: 15px;">HASHTAG TRACKING</span> <a href="" class="button" style="float:right;padding: 5px 10px;border-left: 1px solid #F0F0F0;"><i class="fa fa-plus" style="color: #5f6b64;"></i></a> </p>
				<p class="desc" style="color:#D54E48;">#Gameofthrones</p>

				<div class="tags-wrapper">
					<span class="tags">#hajiwoninSG</span>	
					<span class="tags">#SG50</span>	
					<span class="tags">#BranStarkinSG</span>	
					<span class="tags">#OOTD</span>	
				</div>
				<div class="tags-wrapper first-word-cloud"></div>
			</div>
 
			<div class="col-md-6 left-box" style="margin-right:0;">
				<p class="box-title">TOP TRENDING HASHTAGS <a href="" style="float:right;"><i class="fa fa-caret-down" style="color: #5f6b64;"></i></a> </p>
				<p class="desc" style="color:#4CC2F2;">#SG50</p>
				<div class="tags-wrapper">
					<span class="tags">#Music</span>	
					<span class="tags">#TBT</span>	
					<span class="tags">#Cray</span>	
					<span class="tags">#Singapore</span>
				</div>
				<div class="tags-wrapper second-word-cloud"></div>
			</div>
		</div>
		<div class="col-md-6 right" style="padding-right:5px;">
			<span>Influencers</span><br>
			<div class="col-md-12 influencers" style="">
				<table class="table table-striped table-condensed">
					<thead>
						<tr style="background:#FFAD30;color:#fff;">
							<th style="width:10%"></th>
							<th>Name</th>
							<th>Posts</th>
							<th>Followers</th>
							<th>Following
								<!-- data-toggle="dropdown" -->
								<a href="javascript:void(0)" class="dropdown-toggle settings-gear" id="trigger-menu">
                  <i class="fa fa-cog" style="color:#fff"></i>
                </a>
	               <ul class="dropdown-menu table-influ">
			            <li><a href="javascript:void(0)">By Name</a></li>
			            <li><a href="javascript:void(0)">By Followers</a></li>
			            <li><a href="javascript:void(0)">By Score</a></li>
			            <li><a href="javascript:void(0)">By Interest</a></li>
			        </ul>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width:10%"><img src="img/pbit-assets-v4/Amanda Wong.jpg" style="width:100%;"></td>
							<td>Amanda Wong</td>
							<td>1,170</td>
							<td>37,800</td>
							<td>683</td>
						</tr>
						<tr>
							<td style="width:10%"><img src="img/pbit-assets-v4/Andrea Chong.jpg" style="width:100%;"></td>
							<td>Andrea Chong</td>
							<td>1,887</td>
							<td>245,000</td>
							<td>562</td>
						</tr>
						<tr>
							<td style="width:10%"><img src="img/pbit-assets-v4/Bella Koh.jpg" style="width:100%;"></td>
							<td>Bella Koh</td>
							<td>2369</td>
							<td>67,000</td>
							<td>1,114</td>
						</tr>
						<tr>
							<td style="width:10%"><img src="img/pbit-assets-v4/Nellie Lim.jpg" style="width:100%;"></td>
							<td>Nellie Lim</td>
							<td>2,402</td>
							<td>42,700</td>
							<td>669</td>
						</tr>
						<tr>
							<td style="width:10%"><img src="img/pbit-assets-v4/Nicole Wong.jpg" style="width:100%;"></td>
							<td>Nicole Wong</td>
							<td>969</td>
							<td>49,600</td>
							<td>473</td>
						</tr>
					</tbody>	
				</table>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function( ) {

		$('#trigger-menu').click(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('#trigger-menu').hover(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('body').click(function( ) {
			$('.table-influ').slideUp(100);
			$('.settings-gear').removeClass('rotate');
		});
	});
</script>