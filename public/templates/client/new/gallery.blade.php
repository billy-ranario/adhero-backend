<div class="container" style="background:#fff;padding: 45px 70px;box-shadow: 1px 2px 1px #BBB;margin-bottom: 40px;">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="buttons-wrapper col-md-4 col-sm-4 col-sm-4 text-center col-md-offset-4">
					<button class="btn btn-white btn-lg">ION Orchard</button>
				</div>
				<div class="buttons-wrapper col-md-4 col-sm-4 col-sm-4 text-right">
					<button class="btn btn-white ">03/14</button>
					<button class="btn btn-white ">04/14</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top:50px;">
			<div class="col-md-3 col-sm-3 col-xs-12 text-center">
				<div class="heading-left" style="margin-bottom:60px;border-radius: 5px;">
					<h3 style="margin: 5px 0 0 0;">Query</h3>
				</div>
				<div class="heading-left" style="border-radius: 5px;">
					<h3 style="margin: 5px 0 0 0;">Tags</h3>
				</div>
				<ul class="list-unstyled galler-tags">
					<li>Foodie</li>
					<li>Time with others</li>
					<li>Relaxation alone</li>
					<li>Travel Adventure</li>
					<li>Retail Therapy</li>
				</ul>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12 location-gal">
				<div class="gallery-images">
					<div class="col-md-12">
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/1.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/1.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/2.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/2.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/3.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/3.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/4.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/4.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/5.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/5.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/6.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/6.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/7.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/7.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/8.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/8.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/9.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/9.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6">
							<img src="img/location-gallery/10.jpg" width="100%">
							<div class="detail">
								<img src="img/location-gallery/User_Pics/10.jpg" style="border: 1px solid #fff;border-radius: 50%;width: 26px;margin-right: 3px;">
								<span>Name</span>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>