<div class="sub-content-wrapper col-md-10" predict-view>
	<div class=" predict-view-wrapper">
		<section class="alert-msgs-box">
			<div class="alert alert-info msg msg002" id="predict-msg002">
				<p>
					<strong>Predict your next campaigns performance! </strong>Give us the details below and let us do the work.
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div>
			<div class="alert alert-warning msg msg01" id="predict-msg001">
				<p>
					<strong>First Time?</strong>
					Let us show you how it works.
					<button type="button" class="close" ng-click="close('msg001')"><span aria-hidden="true">&times;</span></button>
				</p>
				<button class="btn btn-sm btn-warning btn-yellow demo-btn" ng-click="useraction('demo')">Demo</button>
				<button class="btn btn-sm btn-default expert-btn" ng-click="useraction('skip')">Skip</button>
			</div>
			<div class="alert alert-success msg msg004" id="predict-msg004" hidden>
				<p>
					<strong>Thanks for the details! </strong>Hang on while we locate the best images for you
					<button type="button" class="close" ng-click="close('msg004')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div>
		</section>

		<section class="loading-predict-container" hidden>
			<div class="progress">
			  <div class="progress-bar progress-bar-info progress-bar-striped" id="predict-progress"role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			    <span class="sr-only">20% Complete</span>
			  </div>
			</div>
			<h4>Estimated time... 24 hours</h4>
			<section class="predict-quote-container">
				<div class="col-md-12">
					<div class="col-md-2">
						<img src="../img/avatar.png" class="predict-img-qoute img-responsive">
					</div>
					<div class="col-md-10">
						<div class="panel panel-default" style="border-radius: 10px; margin-top: 10px;border-color: none;">
							<div class="panel-body predict-panel-default">
								<i class="glyphicon glyphicon-triangle-left custom-glyphicon-triangle-left-predict"></i>
								<section class="predict-qoute-text">
									<h3>"During the last 6 months, your audience has been responding to content that is "inspirational"</h3>
								</section>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="row text-center">
				<section class="btn-container">
					<a href="javascript:void(0)" ui-sref="home" class="btn back-to-dasboard-btn">Back to Dashboard</a>
					<a href="javascript:void(0)" class="btn demo-images-btn" ng-click="demoImages()">Demo Images</a>
				</section>
			</section>
		</section>
		
		<section class="content-manager">
			<div class="row">
				<div class="col-md-7 headline-wrapper">
					<p class="text-heading">Headline Text:</p>
					<textarea class="form-control headline-text-input" ng-model="predictObject.headingtext" placeholder="Enter headline text here..."></textarea>
				</div>
				<div class="col-md-5 media-platform-wrapper">
					<p class="text-heading">Select your platform:</p>
					<div class="social-media-icons">
						<a href="javascript:void(0);" ng-click="socialPlatform( 'facebook' )" id="sm-fb" ><img src="../img/sprites/facebook.png" class="icon-media"></a>
						<a href="javascript:void(0);" ng-click="socialPlatform( 'instagram' )" id="sm-insta" style="width:58px;"><img src="../img/sprites/Instagram-icon.png" class="icon-media"></a>
						<a href="javascript:void(0);" ng-click="socialPlatform( 'linkedin' )" id="sm-in" ><img src="../img/sprites/linkedin.png" class="icon-media"></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-7 target-wrapper">
					<label class="text-heading">Target demographies:</label>
					<br>
					<br>
					<div class="target-option-wrapper">
						<!-- Geography -->
						<div class="btn-group btn-action geography-option">
							<select class="form-control btn btn-primary" ng-model="predictObject.geography">
								<option value="">Select Geography</option>
								<option value="Singapore">Singapore</option>
								<option value="Philippines">Philippines</option>
								<option value="Korea">Korea</option>
								<option value="San Francisco">San Francisco</option>
							</select>
						</div>
						<!-- Age -->
						<div class="btn-group btn-action age-min-option">
							<input type="number" ng-model="predictObject.ageMin" class="form-control btn btn-primary" placeholder="Age Minimum">
						</div>
						<!-- Geography -->
						<div class="btn-group btn-action age-max-option">
							<input type="number" ng-model="predictObject.ageMax" class="form-control btn btn-primary" placeholder="Age Maximum">
						</div>
					</div>	
					<div class="target-gender">
						<md-radio-group ng-model="predictObject.targetgender">
							<md-radio-button value="Male">Male</md-radio-button>
							<md-radio-button value="Female">Female</md-radio-button>
							<md-radio-button value="All">Male &amp; Female</md-radio-button>
						</md-radio-group>
					</div>
					
				</div>
				<div class="col-md-5">
					<label>Pick what libraries you want to receive recommendations from:</label><br><br>

					<div class="target-gender">
						<md-radio-group ng-model="predictObject.image_lib">
							<md-radio-button value="stock_photos">Stock photos</md-radio-button>
							<md-radio-button value="user_generated">User generated content</md-radio-button>
							<md-radio-button value="user_library">Your library ( Upload )</md-radio-button>
						</md-radio-group>
					</div>
					<button class="btn btn-success pull-right" ng-click="createPredict()">Submit</button>	
				</div>
			</div>
		</section>
	</div>
</div>
</div>