<section class="alert-msgs-box">
	<div class="alert alert-info msg msg002" id="predict-msg002">
		<p>
			<strong>Predict your next campaigns performance! </strong>Give us the details below and let us do the work.
			<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
		</p>
	</div>
	<div class="alert alert-warning msg msg01" id="predict-msg001">
		<p>
			<strong>First Time?</strong>
			Let us show you how it works.
			<button type="button" class="close" ng-click="close('msg001')"><span aria-hidden="true">&times;</span></button>
		</p>
		<button class="btn btn-sm btn-warning btn-yellow demo-btn" ng-click="useraction('demo')">Demo</button>
		<button class="btn btn-sm btn-default expert-btn" ng-click="useraction('skip')">Skip</button>
	</div>
	<div class="alert alert-success msg msg004" id="predict-msg004" hidden>
		<p>
			<strong>Thanks for the details! </strong>Hang on while we locate the best images for you
			<button type="button" class="close" ng-click="close('msg004')"><span aria-hidden="true">&times;</span></button>
		</p>
	</div>
</section>