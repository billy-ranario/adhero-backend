
<div class="home col-md-12 no-padding custom-ui-view" home>
	<div class="col-md-12 top-content-wrapper">
		<div class="col-md-12">
			<h4 style="color:#999;"><b>Your Engagements</b></h4>
		</div>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>209,000</p>
				<p>Posts</p>
				<!-- <a href="">View More</a> -->
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>1,203,192</p>
				<p>Users</p>
				<!-- <a href="">View More</a> -->
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>9,321</p>
				<p>Reads</p>
				<!-- <a href="">View More</a> -->
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>200,192</p>
				<p>Impressions</p>
				<!-- <a href="">View More</a> -->
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>412</p>
				<p>Themes</p>
				<!-- <a href="">View More</a> -->
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>45.6%</p>
				<p>Engagements</p>
				<!-- <a href="">View More</a> -->
			</div>
		</div>
	</div>


	<div class="col-md-12 one-bottom-wrapper" >
		<div class="col-md-6 ">
			<div class="col-md-12 one-left-wrapper">
				<div class="col-md-6">
					<p>HASHTAG TRACKING <a href="" class="button" ><i class="fa fa-plus" style="margin-right:5px;color: #5f6b64;"></i></a></p>

					<p>#Gameofthrones</p>
					<p>#hajiwoninSG</p>
					<p>#SG50</p>
					<p>#BranStarkinSG</p>
					<p>#OOTD</p>
				</div>
				<div class="col-md-6">
					<p>TOP TRENDING HASHTAGS <a href="" ><i class="fa fa-caret-up" style="margin-right:5px;color: #5f6b64;"></i></a> </p>

					<p>#SG50</p>
					<p>#Music</p>
					<p>#TBT</p>
					<p>#Cray</p>
					<p>#Singapore</p>
				</div>
			</div>
		</div>

		<div class="col-md-6 one-right">
			<div class="col-md-12 no-padding">
				<p style="font-size:17px;color: #999;font-weight: 600;"><span>Influencers</span> <a href="" ><i class="fa fa-caret-up" style="margin-right:5px;color: #5f6b64;"></i></a> </p>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Amanda Wong.jpg') no-repeat;background-position: center; ">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Amanda Wong.jpg" class="img-avatar">
						<p>Amanda Wong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>1,170</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>37.8%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Andrea Chong.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Andrea Chong.jpg" class="img-avatar">
						<p>Andrea Chong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>1,887</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>24.5%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Bella Koh.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Bella Koh.jpg" class="img-avatar">
						<p>Bella Koh</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>2369</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>67%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Nellie Lim.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Nellie Lim.jpg" class="img-avatar">
						<p>Nellie Lim</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>2,402</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>42.7%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Nicole Wong.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Nicole Wong.jpg" class="img-avatar">
						<p>Nicole Wong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>969</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>49.6%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>




		</div>
	</div>
	

</div>


<script type="text/javascript">
	$(function( ) {

		$('#trigger-menu').click(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('#trigger-menu').hover(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('body').click(function( ) {
			$('.table-influ').slideUp(100);
			$('.settings-gear').removeClass('rotate');
		});

		$(function(){
		    var $window = $(window),
		        $header = $('.main-header'),
		        $this   = $(this); // <-----here you can cache your selectors

		    $window.on('scroll', function(){
		       if($this.scrollTop() > 0 && $window.innerWidth() > 990 ){
		           $('.top-content-wrapper').addClass('top-content-wrapper-fix');
		           $('.one-left-wrapper').addClass('one-left-wrapper-fix');
		       }else{
		           $('.top-content-wrapper').removeClass('top-content-wrapper-fix');
		           $('.one-left-wrapper').removeClass('one-left-wrapper-fix');
		       }
		    }).scroll();
		});
	});
</script>