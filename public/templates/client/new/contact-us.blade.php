<div class="col-md-10" >
	<div class="contact-container row" >
		<div class="col-md-12" >
			<div class="row">
				<div class="col-md-7">
					<h4 style="padding:10px;background:#17A085;color:#FFF;border-radius:5px;">Contact Us</h4>
				</div>
				
				<div class="col-md-7" style="font-size:12px;">
					<p>We value your opinion and your feedback is important to us. Leave us a message below !</p>
					<p>For urgent inquiries, please contact <a href="#">enquiries@precisionbit.com</a></p>

					<hr style="border-top:1px solid #ccc;">

					<form>
						<div class="form-group">
						    <label>Name</label>
						    <input type="text" class="form-control">
						</div>
						<div class="form-group">
						    <label>Email</label>
						    <input type="email" class="form-control">
						</div>
						<div class="form-group">
						    <label>Message</label>
						    <textarea class="form-control" cols="10" rows="10"></textarea>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
