<div class="col-md-2" style="width: 12.666667%;" graph-plotly>
	<div class="navigation">

		<ul class="nav">
			<div class="triangle" style="background:#000;top:124px;"></div>
			<li><a ui-sref="home"><i class="fa fa-home"></i></a></li>
			<li><a ui-sref="predict"><i class="fa fa-flask"></i></a></li>
			<li style="background:#000;"><a ui-sref="summary"><i class="fa fa-bar-chart"></i></a></li>
		</ul>

	</div>
</div>		

<div class="col-md-10 summ" style="z-index:99;">
	<div class="sub-content-wrapper isFirstTime" style="padding-top:0px;">
		<div class="row">
			<div class="col-lg-4 ">
				<div class="top" hidden>
					<div class="header">
						<h4 style="font-weight:600;margin: 0px 0px 10px 0px;"><a href="" ui-sref="report-telstra" style="color:#222;"><i class="fa fa-search" ></i> Best Performing Visual Elements</a></h4>
					</div>
					<div class="body">
						<ul>
							<li><span class="top-0"></span> <span class="top-0-ctr"></span> <a href="" ><i class="icon-chart" style="color:#286090;"></i></a> </li>
							<li><span class="top-1"></span> <span class="top-1-ctr"></span> <a href="" ><i class="icon-chart" style="color:#449D44;"></i></a> </li>
							<li><span class="top-2"></span> <span class="top-2-ctr"></span> <a href="" ><i class="icon-chart" style="color:#5BC0DE;"></i></a> </li>
							<li><span class="top-3"></span> <span class="top-3-ctr"></span> <a href="" ><i class="icon-chart" style="color:#F0AD4E;"></i></a> </li>
							<li><span class="top-4"></span> <span class="top-4-ctr"></span> <a href="" ><i class="icon-chart" style="color:#D9534F;"></i></a> </li>
						</ul>
					</div>
					<div class="footer">
						<button class="btn btn-default 1-yr" ng-click="filter('1 year')">1 Year</button>
						<button class="btn btn-default 6-mnth actives" ng-click="filter('6 months')">6 Months</button>
						<button class="btn btn-default lifetime" ng-click="filter('lifetime')">Lifetime</button>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<button ui-sref="report-telstra" class="btn btn-default" style="float:right;margin:10px 10px 0 0;background:#28A6A0;color:#FFF;">Details</button>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 ">
				<div class="bottom" hidden>
					<div class="row">
						<div class="col-md-12">
							<div class="header">
								<h4 style="font-weight:600;margin: 0px 0px 10px 5px;"><a href="" ui-sref="report-comp	" style="color:#222;"><i class="fa fa-eye" style="margin-right: 5px;"></i> Best Performing Competitor Visual Elements</a></h4>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<ul style="color: #777;font-weight: 600;">
								<li><br></li>
								<li class="bottom-0"></li>
								<li class="bottom-1"></li>
								<li class="bottom-2"></li>
								<li class="bottom-3"></li>
								<li class="bottom-4"></li>
							</ul>
						</div>		
						<div class="col-md-3">
							<ul style="text-align:center;font-weight:600;color:#3FA158;">
								<li style="color:#222;">Avg. Engagement Score</li>
								<li>1240</li>
								<li>940</li>
								<li>840</li>
								<li>740</li>
								<li>637</li>
							</ul>
						</div>		
						<div class="col-md-3 gallery">
							<ul style="text-align:center;">
								<li style="font-weight:600;">Gallery</li>
								<li>
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
								</li>
								<li>
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
								</li>
								<li>
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
								</li>
								<li>
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
								</li>
								<li>
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
									<img src="telstra_images/2.jpg">
								</li>
							</ul>
						</div>		
						<div class="col-md-3 engage">
							<ul style="text-align:center;">
								<li style="font-weight:600;">Engagement Score</li>
								<li>
									<img src="img/engagement.png">
								</li>
								<li>
									<img src="img/engagement.png">
								</li>
								<li>
									<img src="img/engagement.png">
								</li>
								<li>
									<img src="img/engagement.png">
								</li>
								<li>
									<img src="img/engagement.png">
								</li>
							</ul>
						</div>		
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="footer">
								<button class="btn btn-default">1 Month</button>
								<button class="btn btn-default">3 Months</button>
								<button class="btn btn-default actives">6 Months</button>
								<button class="btn btn-default">1 Year</button>
								<button class="btn btn-default">Lifetime</button>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
	
	$(function(){
		// $('#myModal').modal('show');

		$('#telstra-btn').click(function(){
			$('.summ').css('z-index',0);
			$('.competitors').css('display','none');
			$('.telstra').css('z-index',99);
		});

		$('#hide-summary').click(function(){
			$('.isFirstTime').fadeOut();
		});

		$( "#date-start" ).datepicker();
		$( "#date-end" ).datepicker();
		$('[data-toggle="tooltip"]').tooltip();

		$('.prev-img-info').hover(
  			function(){
  				$('.details-info').show();
  			},
  			function(){
  				$('.details-info').hide();
  			}
  		);
	})
</script>