<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;
">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 20px;">
        Contact Us!
        <small class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s"></small>
        </h1>
        <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s" style="font-size: 10px;background-color: #E7EAEF;margin:0">
          <li><a  class="btn btn-medium header-title-section custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
        </ol>
    </section>
    <section class="text-right">
     
    </section>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-primary" id="hidepanel1" style="margin-top: 20px;">
      <div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="clock" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-48" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-livicon-48"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M16,2C8.269,2,2,8.269,2,16S8.269,30,16,30S30,23.731,30,16S23.731,2,16,2ZM16.691,26.965L16.5,24.101H15.5L15.309,26.965C9.788,26.621,5.379,22.213,5.036,16.692L7.9,16.5V15.5L5.035,15.309C5.3790000000000004,9.788,9.786999999999999,5.379,15.308,5.036L15.5,7.9H16.5L16.691,5.035C22.212,5.3790000000000004,26.621,9.786999999999999,26.964,15.308L24.101,15.5V16.5L26.965,16.691C26.621,22.213,22.213,26.621,16.691,26.965Z" transform="matrix(0.5,0,0,0.5,0,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z" transform="matrix(0.5,0,0,0.5,0,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></i>
          Contact Us!
      </h3>
      </div>
      <div class="panel-body">
          <form class="form-horizontal" action="#" method="post">
              <fieldset>
                  <!-- Name input-->
                  <div class="form-group">
                      <label class="col-md-3 control-label" for="name">Name</label>
                      <div class="col-md-9">
                          <input id="name" name="name" type="text" placeholder="Your name" class="form-control"></div>
                  </div>
                  <!-- Email input-->
                  <div class="form-group">
                      <label class="col-md-3 control-label" for="email">Your E-mail</label>
                      <div class="col-md-9">
                          <input id="email" name="email" type="text" placeholder="Your email" class="form-control"></div>
                  </div>
                  <!-- Message body -->
                  <div class="form-group">
                      <label class="col-md-3 control-label" for="message">Your message</label>
                      <div class="col-md-9">
                          <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-3 control-label" for="name">File Upload</label>
                      <div class="col-md-9">
                          <!-- <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                              <div class="form-control" data-trigger="fileinput">
                                  <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                  <span class="fileinput-filename"></span>
                              </div>
                              <span class="input-group-addon btn btn-default btn-file">
                                  <span class="fileinput-new">Select file</span>
                                  <span class="fileinput-exists">Change</span>
                                  <input type="file" name="..."></span>
                              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          </div> -->
                          <div class="input-group">
                            <input type="text" class="form-control" aria-label="Text input with segmented button dropdown">
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-default">Select file</button>
                            </div>
                          </div>
                      </div>
                  </div>
                  <!-- Form actions -->
                  <div class="form-group">
                      <div class="col-md-12 text-right">
                          <a href="#" class="btn btn-responsive btn-primary btn-sm" role="button">Send Email</a>
                      </div>
                  </div>
              </fieldset>
          </form>
      </div>
  </div>
</div>
  </div>
  
<!-- <h1 class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">
  Contact Us
</h1>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-header">
        <section class="text-center">
          <h4>Questions: </h4>
        </section>
      </div>
      <div class="panel-body">
        <textarea class="form-control"></textarea>
      </div>
      <div class="panel-footer text-center">
        <button class="btn btn-large btn-primary">Send</button>
      </div>
    </div>
  </div>
</div> -->