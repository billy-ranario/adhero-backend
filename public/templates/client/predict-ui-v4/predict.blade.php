<div ng-controller="predictController" brief-predict style="padding: 15px 55px;">
	<div class="col-md-12 top-trigger-experiment" id="experiment-table" style="margin-top: 30px;">		
		<div class="dropdown">
		  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    Sort by date/time
			    <span class="caret"></span>
		  	</button>
		  
		  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<li><a href="javascript:void(0)" ng-click="sortResult( 'latest' )">Latest</a></li>
				<li><a href="javascript:void(0)" ng-click="sortResult( 'oldest' )">Oldest</a></li>
		  	</ul>

		  	<a href="javascript:void(0)" ng-click="showPredictionForm( 'create' )" class="pull-right btn custom-btn-prediction">New Brief</a>
			<a href="javascript:void(0)" ng-click="showPredictionForm( 'cancel' )" class="pull-right btn custom-btn-prediction-cancel" hidden>Cancel</a>
		</div>

		<hr>
		<table class="table table-hover table-bordered table-striped table-bordered" >
			<thead>
				<th>Topic</th>
				<th style="width:320px">Audience Target: Country, Gender</th>
				<th>Date</th>
				<th>Status</th>
			</thead>
			<tbody>
				
				<tr>
					<td>
						#GameofThrones
					</td>
					<td>
						<span>Singapore</span>,
						<span>
							<!-- <span ng-bind="gender"></span> -->
							Both
						</span>
					</td>
					<td>
						<span>04-28-2016 04:58 PM</span>
					</td>
					<td class="text-center">
						<span class="pull-left">Completed</span>

						<a href="javascript:void(0)" ui-sref="predict-results-template">
							<i class="fa fa-eye" style="color:#A19FA0; padding: 10px;"></i>
						</a>
						<a href="javascript:void(0)" >
							<i class="fa fa-trash" style="color:#A19FA0"></i>
						</a>
					</td>
				</tr>
				<tr>
					<td style="box-shadow: 1px 2px 1px #BBB;">
						#SG50
					</td>
					<td style="box-shadow: 1px 2px 1px #BBB;">
						<span>Singapore</span>,
						<span>
							<!-- <span ng-bind="gender"></span> -->
							Both
						</span>
					</td>
					<td style="box-shadow: 1px 2px 1px #BBB;">
						<span>04-29-2016 04:58 PM</span>
					</td>
					<td class="text-center" style="box-shadow: 0px 2px 1px #BBB;">
						<span class="pull-left">Completed</span>

						<a href="javascript:void(0)" ui-sref="predict-results-template2">
							<i class="fa fa-eye" style="color:#A19FA0; padding: 10px;"></i>
						</a>
						<a href="javascript:void(0)" >
							<i class="fa fa-trash" style="color:#A19FA0"></i>
						</a>
					</td>
				</tr>
				<tr ng-repeat="list in clientObject.predict | orderBy:'+':statusView" id="{{list.predict_id}}">
					<td style="box-shadow: 1px 2px 1px #BBB;">
						<span ng-repeat="hash in list.hashtags">
							<span ng-bind="hash.hash"></span><span ng-if="!$last">, </span>
						</span>
					</td>
					<td style="box-shadow: 1px 2px 1px #BBB;">
						<span ng-bind="list.country"></span>,
						<span ng-bind="list.gender">
							<!-- <span ng-bind="gender"></span> -->
						</span>
					</td>
					<td style="box-shadow: 1px 2px 1px #BBB;">
						<span ng-bind="list.date_created"></span>
					</td>
					<td class="text-center" style="box-shadow: 0px 2px 1px #BBB;">
						<span class="pull-left" ng-bind="list.status"></span>

						<a href="javascript:void(0)" ng-if="list.status == 'Completed' " ng-click="showresult(list)">
							<i class="fa fa-eye" style="color:#A19FA0; padding: 10px;"></i>
						</a>
						<a href="javascript:void(0)" ng-click="deleteExperiment( $event, list )">
							<i class="fa fa-trash" style="color:#A19FA0"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>	
	</div>

	<div class="col-md-4" id="predict-form-content" style="box-shadow: 1px 2px 1px #BBB;">
		<form>		
			<div class="content-heading">
				<label>Product</label>
			</div>
			<div layout-gt-sm="row">
				<md-input-container class="md-block" flex-gt-sm>
					<label style="font-weight:normal">Campaign Name</label>
					<input type="text" ng-model="predictObject.campaign_name">
				</md-input-container>
			</div>
			<div layout-gt-sm="row">
				<md-input-container class="md-block" flex-gt-sm>
					<label style="font-weight:normal">Product Name</label>
					<input type="text" ng-model="predictObject.product_name">
				</md-input-container>
			</div>

			<div class="content-heading">
				<label>Audience (optional)</label>
			</div>
			<div layout-gt-sm="row">
			<md-input-container class="md-block" flex-gt-sm style="width:50%;padding-right:5px;">
					<label style="font-weight:normal">Country</label>
					<input type="text" ng-model="predictObject.country">
				</md-input-container>

				<md-select placeholder="Gender" ng-model="predictObject.gender" md-on-open="" style="width:50%;padding-left:5px;">
					<md-option value="Male">Male</md-option>
					<md-option value="Female">Female</md-option>
					<md-option value="Both">Both</md-option>
				</md-select>
			</div>

			<div class="content-heading">
				<label>Objective</label>
			</div>
			<div layout-gt-sm="row">
				<md-select placeholder="Objective" ng-model="predictObject.objective" md-on-open="" style="width:100%">
					<md-option value="Clicks to Website">Clicks to Website: Send people to your website.</md-option>
					<md-option value="Website Conversions">Website Conversions: Increase conversions on your website. <!-- You'll need a conversion pixel for your website before you can create this ad. --></md-option>
					<md-option value="Page Post Engagement">Page Post Engagement: Boost your posts.</md-option>
					<md-option value="Page Likes">Page Likes: Promote your Page and get Page likes to connect. <!-- with more of the people who matter to you. --></md-option>
					<md-option value="App Installs">App Installs: Get installs of your app.</md-option>
					<md-option value="App Engagement">App Engagement: Increase engagement in your app.</md-option>
					<md-option value="Offer Claims">Offer Claims: Create offers for people to redeem in your store.</md-option>
					<md-option value="Local Awareness">Local Awareness: Reach people near your business.</md-option>
					<md-option value="Event Responses">Event Responses: Raise attendance at your event.</md-option>
					<md-option value="Video Views">Video Views: Create ads that get more people to view a video.</md-option>
					<md-option value="Increase Sales">Increase Sales</md-option>
				</md-select>
			</div>
			<div class="col-md-12 no-padding social-media-publish">
				<p>Publish your work in:</p>
				<button ng-click="selectedMedia('Facebook')" id="Facebook" class="btn-social-exp">
					<i class="fa fa-facebook"></i> | <span>Facebook</span>
				</button>
				<button ng-click="selectedMedia('Instagram')" id="Instagram" class="btn-social-exp">
					<i class="fa fa-instagram"></i> | <span>Instagram</span>
				</button>
				<br>
				<br>
			</div>
			<div class="content-heading">
				<label>Fill in your hashtag groups</label>
			</div>
			<div class="col-md-12 no-padding">
				<md-input-container class="md-block col-md-4 no-padding" style="padding:0 5px !important" flex-gt-sm ng-repeat="tags in hashtags">
					<label style="font-weight:normal">#hashtag</label>
					<input type="text" ng-model="tags.hash">
				</md-input-container>
			</div>
			<div class="col-md-12 no-padding formSubmit">
				<button type="submit" class="submit" ng-click="createExperiment()">Submit</button>
			</div>
		</form>
	</div>
</div>