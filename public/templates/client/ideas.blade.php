<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 20px;">
        Ads Ideas
        <br />
        <small>Here are some example of ads</small>
        </h1>
        <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="1s" data-wow-delay="0.5s" style="font-size:10px;background-color: #E7EAEF;margin:0">
          <li><a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
        </ol>
    </section>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top: 10px;">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-md-12">
          <div class="form-group text-center">
            <p style="font-size: 30px;">Need Ideas About your Next Facebook Ad ?</p>
            <small>We search and study ads that circulated in Facebook and evaluate with our Engine</small>
            <br>
            <small>Find out how others advertise on Facebook and Why!</small>
          </div>
        </div>
        <div class="awe-wrapper">
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control custom-form-control">
                <option value="0"> All Placements</option>
                <option value="newsfeed-desktop">Newsfeed Desktop</option>
                <option value="newsfeed-mobile">Newsfeed Mobile</option>
                <option value="right-column">Right Column</option> 
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control custom-form-control">
                <option class="level-0 sf-item-0" value="0"> All Industries</option>
                <option value="b2b">B2B</option>
                <option value="b2c">B2C</option>
                <option value="brand">Brand</option>
                <option value="ecommerce">eCommerce</option>
                <option class="level-0 sf-item-295" value="education" data-sf-cr="_sft_295" data-sf-hide-empty="1">Education</option>
                <option class="level-0 sf-item-230" value="entertainment" data-sf-cr="_sft_230" data-sf-hide-empty="1">Entertainment</option>
                <option class="level-0 sf-item-296" value="family" data-sf-cr="_sft_296" data-sf-hide-empty="1">Family</option>
                <option class="level-0 sf-item-222" value="fashion" data-sf-cr="_sft_222" data-sf-hide-empty="1">Fashion &amp; Beauty</option>
                <option class="level-0 sf-item-256" value="finance" data-sf-cr="_sft_256" data-sf-hide-empty="1">Finance</option>
                <option class="level-0 sf-item-221" value="food" data-sf-cr="_sft_221" data-sf-hide-empty="1">Food</option>
                <option class="level-0 sf-item-283" value="games" data-sf-cr="_sft_283" data-sf-hide-empty="1">Games</option>
                <option class="level-0 sf-item-270" value="health-fitness" data-sf-cr="_sft_270" data-sf-hide-empty="1">Health &amp; Fitness</option>
                <option class="level-0 sf-item-227" value="car" data-sf-cr="_sft_227" data-sf-hide-empty="1">Home &amp; Car</option>
                <option class="level-0 sf-item-231" value="local-business" data-sf-cr="_sft_231" data-sf-hide-empty="1">Local Business</option>
                <option class="level-0 sf-item-234" value="marketing" data-sf-cr="_sft_234" data-sf-hide-empty="1">Marketing</option>
                <option class="level-0 sf-item-266" value="other" data-sf-cr="_sft_266" data-sf-hide-empty="1">Other</option>
                <option class="level-0 sf-item-269" value="real-estate" data-sf-cr="_sft_269" data-sf-hide-empty="1">Real Estate</option>
                <option class="level-0 sf-item-233" value="services" data-sf-cr="_sft_233" data-sf-hide-empty="1">Services</option>
                <option class="level-0 sf-item-226" value="startup" data-sf-cr="_sft_226" data-sf-hide-empty="1">Startup</option>
                <option class="level-0 sf-item-223" value="technology" data-sf-cr="_sft_223" data-sf-hide-empty="1">Technology</option>
                <option class="level-0 sf-item-232" value="travel" data-sf-cr="_sft_232" data-sf-hide-empty="1">Travel</option> 
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control custom-form-control">
                <option class="level-0 sf-item-0" value="0"> All Objectives</option>
                <option class="level-0 sf-item-257" value="awareness" data-sf-cr="_sft_257" data-sf-hide-empty="0">Awareness</option>
                <option class="level-0 sf-item-218" value="content-promotion" data-sf-cr="_sft_218" data-sf-hide-empty="0">Content Promotion</option>
                <option class="level-0 sf-item-216" value="event" data-sf-cr="_sft_216" data-sf-hide-empty="0">Event</option>
                <option class="level-0 sf-item-213" value="lead-generation" data-sf-cr="_sft_213" data-sf-hide-empty="0">Lead Generation</option>
                <option class="level-0 sf-item-215" value="mobile-app" data-sf-cr="_sft_215" data-sf-hide-empty="0">Mobile App</option>
                <option class="level-0 sf-item-217" value="page-likes" data-sf-cr="_sft_217" data-sf-hide-empty="0">Page Likes</option>
                <option class="level-0 sf-item-214" value="sale" data-sf-cr="_sft_214" data-sf-hide-empty="0">Sale</option> 
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control custom-form-control">
                <option class="level-0 sf-item-0" value="0"> All Attributes</option>
                <option class="level-0 sf-item-268" value="carousel-ad" data-sf-cr="_sft_268" data-sf-hide-empty="1">Carousel Ad</option>
                <option class="level-0 sf-item-210" value="has-humans" data-sf-cr="_sft_210" data-sf-hide-empty="1">Has Humans</option>
                <option class="level-0 sf-item-209" value="is-illustration" data-sf-cr="_sft_209" data-sf-hide-empty="1">Is Illustration</option>
                <option class="level-0 sf-item-208" value="is-photo" data-sf-cr="_sft_208" data-sf-hide-empty="1">Is Photo</option>
                <option class="level-0 sf-item-212" value="is-video" data-sf-cr="_sft_212" data-sf-hide-empty="1">Is Video</option>
                <option class="level-0 sf-item-260" value="promo" data-sf-cr="_sft_260" data-sf-hide-empty="1">Promo</option>
                <option class="level-0 sf-item-211" value="retargeting" data-sf-cr="_sft_211" data-sf-hide-empty="1">Retargeting</option>
                <option class="level-0 sf-item-207" value="split-test" data-sf-cr="_sft_207" data-sf-hide-empty="1">Split Test</option>
                <option class="level-0 sf-item-259" value="we-dont-like" data-sf-cr="_sft_259" data-sf-hide-empty="1">We Don't Like!</option>
                <option class="level-0 sf-item-258" value="we-like" data-sf-cr="_sft_258" data-sf-hide-empty="1">We Like!</option> 
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
          <button class="btn custom-button" style="font-size: 30px;width: 50%;">Find Ads Now!</button>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <p style="font-size: 20px;margin-left: 50px;margin-bottom: 20px; color: #777777;">Latest Ads Examples</p>

  <div class="col-md-12">
    <div class="col-md-12" style="width: 90%;margin-left: 5%;padding: 20px;;border-top: 1px solid #d0d0d0;">
      <div class="col-md-4 wow fadeInUp" ng-repeat="sample_ad in sample_ads" data-wow-duration="0.{{ $index }}" data-wow-delay="{{ $index }}" ng-click="getViewAd(sample_ad)">
        <!-- <button id="ad_{{sample_ad.id}}" class="btn adBtnDetails" style="position: absolute;margin-left: 60%;margin-top: 10px;background: #28A6A0;color: white;border-radius: 0px;">Click for Details</button>
        <img ng-src="../../img/{{sample_ad.img}}" class="thumbnail custom-thumbnail-ad" style="width: 80%;">
          <p style="margin: 0;margin-left: 80px;margin-bottom: 10px;">{{ sample_ad.name }}</p> -->
          <div class="panel panel-default text-center">
            <button id="ad_{{sample_ad.id}}" class="btn adBtnDetails" style="position: absolute;margin-left: 60%;margin-top: 10px;background: #28A6A0;color: white;border-radius: 0px;">Click for Details</button>
            <img ng-src="../../img/live_campaigns/{{sample_ad.img}}" imageonload class="thumbnail custom-thumbnail-ad" style="margin: 10px auto;width: 60%;border-radius: 0px;" src="../../img/ad_sample_1.png">
              <p style="margin: 0;margin-bottom: 10px;" class="ng-binding">{{ sample_ad.name }}</p>
            </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="adsView" tabindex="-1" role="dialog" aria-labelledby="adsView">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header custom-modal-header-view-ads">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
        <p ng-bind="details_campaign.name" style="color: white;font-size: 30px;margin: 0px;text-shadow: 0px 0px 2px #777777;"></p>
      </div>
      <div class="modal-body">
        <img ng-src="../../img/live_campaigns/{{details_campaign.img}}" class="thumbnail custom-thumbnail-ad" style="margin-left: 50px;width: auto;height: 500px;border-radius: 0px;" src="../../img/ad_sample_1.png">
        <section class="adds-details">
          <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" rowspan="1" colspan="4"> <span style="color: #3e4553;">Page:</span> <a href="{{details_campaign.link_page}}" style="color: #3b5998;" target="_blank"> {{ details_campaign.link_title }}</a></th>
            </thead>
            <tbody>
              <tr role="row" class="odd">
                <td style="color: #777777;">Placement</td>
                <td rowspan="1" colspan="4"> {{ details_campaign.placement }}</td>
              </tr>
              <tr role="row" class="odd">
                <td style="color: #777777;">Industries</td>
                <td rowspan="1" colspan="4"> {{ details_campaign.industries }}</td>
              </tr>
              <tr role="row" class="odd">
                <td style="color: #777777;">Objectives</td>
                <td rowspan="1" colspan="4"> {{ details_campaign.objectives }}</td>
              </tr>
              <tr role="row" class="odd">
                <td style="color: #777777;">Attibutes</td>
                <td rowspan="1" colspan="4"> {{ details_campaign.attributes }}</td>
              </tr>
            </tbody>
          </table>
        </section>
      </div>
    </div>
  </div>
</div>