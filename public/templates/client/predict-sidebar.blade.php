<div class="sidebar-menu-wrapper">
  <ul class="list-unstyled" id="accordion" role="tablist" aria-multiselectable="true">
    <li>
      <div class="heading-list">
        <button ng-click="hidePedictMenu()" class="close pull-right" style="margin-top: 10px!important;margin-right: 10px;"><span aria-hidden="true">&times;</span></button>
        <h2>Predict </h2>
        <small>Predict your ads now</small>
      </div>
    </li>
    <li>
     <!--  -->
      <a ui-sref="predict">
      <div class="list-content">
        <div class="list-icon">
          <span class="icon-monitor" style="font-size: 50px;"></span>
        </div>
        <div class="list-title">
          <h3>Predict</h3>
          <!-- <small>Adhero run A/B test for your campaign</small> -->
        </div>
      </div>
      </a>
    </li>
    <li>
    <!-- ui-sref="winning-report" -->
      <a class="predict-menu-trigger" ng-click="showSubMenu()">
        <div class="list-content">
          <div class="list-icon">
           <span class="glyphicon glyphicon-search" style="font-size: 50px;"></span>
           </div>
          <div class="list-title">
            <h3>Explore</h3>
          </div>
          <span class="trigger-arrow-glyp glyphicon glyphicon-chevron-right pull-right"></span>
        </div>
      </a>
    </li>
    <li class="explore-sub-menu" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      <a href="javascript:void(0)">STRUCTURE</a>
      <ul class="structure-components panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
        <li><a href="javascript:void(0)"> Component 1 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
      </ul>
    </li>
    <li class="explore-sub-menu" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <a href="javascript:void(0)">TONE</a>
      <ul class="structure-components panel-collapse collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
        <li><a href="javascript:void(0)"> Component 1 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
      </ul>
    </li>
    <li class="explore-sub-menu" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
      <a href="javascript:void(0)">INTENT</a>
      <ul class="structure-components panel-collapse collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingThree">
        <li><a href="javascript:void(0)"> Component 1 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
      </ul>
    </li>
    <li class="explore-sub-menu" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
      <a href="javascript:void(0)">CONTEXT</a>
      <ul class="structure-components panel-collapse collapse" id="collapseFour" role="tabpanel" aria-labelledby="headingFour">
        <li><a href="javascript:void(0)"> Component 1 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
        <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
      </ul>
    </li>
  </ul>    
</div>