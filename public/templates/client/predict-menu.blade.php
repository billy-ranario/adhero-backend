
<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
  <section style="font-size: 20px!important;" navigation-campaign>
    <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin: 0;">
      <li>
        <a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
      </li>
    </ol>
    <a href="javascript:void(0)" class="show-side-bar-menu pull-left" ng-click="showSideBarMenu()">
      <span class="glyphicon glyphicon-th-list"></span>
    </a>
  </section>
</div>
<div class="row wow slideInLeft" id="pop-sidebar" data-wow-duration="0.5s" data-wow-delay="0.5s">
  <div class="col-md-4 sidebar-reports-container remove-side-padding">
    <div class="sidebar-menu-wrapper">
      <ul class="list-unstyled">
        <li>
          <div class="heading-list">
            <button ng-click="hidePedictMenu()" class="close pull-right" style="margin-top: 10px!important;margin-right: 10px;"><span aria-hidden="true">&times;</span></button>
            <h2>Predict </h2>
            <small>Predict your ads now</small>
          </div>
        </li>
        <li>
         <!--  -->
          <a ui-sref="predict">
          <div class="list-content">
            <div class="list-icon">
              <span class="icon-monitor" style="font-size: 50px;"></span>
            </div>
            <div class="list-title">
              <h3>Predict</h3>
              <!-- <small>Adhero run A/B test for your campaign</small> -->
            </div>
          </div>
          </a>
        </li>
        <li ng-click="showElements('search')">
        <!-- ui-sref="winning-report" -->
          <a class="predict-menu-trigger" ng-click="showSubMenu()">
            <div class="list-content">
              <div class="list-icon">
               <span class="glyphicon glyphicon-search" style="font-size: 50px;"></span>
               </div>
              <div class="list-title">
                <h3>Explore</h3>
              </div>
              <span class="trigger-arrow-glyp glyphicon glyphicon-chevron-right pull-right"></span>
            </div>
          </a>
        </li>
        <li ng-click="showExploreGraphsStrucuture('engagement','structure')" class="explore-sub-menu" >
          <a ng-click="showElements('structure')" href="javascript:void(0)">STRUCTURE</a>
          <ul class="structure-components panel-collapse collapse" id="structure" role="tabpanel" aria-labelledby="headingOne">
            <li><a href="javascript:void(0)"> Length of a Post <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <!-- <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li> -->
          </ul>
        </li>
        <li ng-click="showExploreGraphsTone('engagement','tone')" class="explore-sub-menu" >
          <a ng-click="showElements('tone')" href="javascript:void(0)">TONE</a>
          <ul class="structure-components panel-collapse collapse" id="tone" role="tabpanel" aria-labelledby="headingTwo">
            <li><a href="javascript:void(0)"> Question <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <!-- <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li> -->
          </ul>
        </li>
        <li ng-click="showExploreGraphsIntent('engagement','intent')" class="explore-sub-menu" >
          <a ng-click="showElements('intent')" href="javascript:void(0)">INTENT</a>
          <ul class="structure-components panel-collapse collapse" id="intent" role="tabpanel" aria-labelledby="headingThree">
            <li><a href="javascript:void(0)"> Brand Product <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <!-- <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li> -->
          </ul>
        </li>
        <li ng-click="showExploreGraphsContext('engagement','context')" class="explore-sub-menu" >
          <a ng-click="showElements('context')" href="javascript:void(0)">CONTEXT</a>
          <ul class="structure-components panel-collapse collapse" id="context" role="tabpanel" aria-labelledby="headingFour">
            <li><a href="javascript:void(0)"> Mobile App <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <!-- <li><a href="javascript:void(0)"> Component 2 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 3 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li>
            <li><a href="javascript:void(0)"> Component 4 <span class="glyphicon glyphicon-chevron-right custom-explore-right-arrow"></span></a></li> -->
          </ul>
        </li>
      </ul>    
    </div>
    

  </div>

  <div class="col-md-8 right-content-container" style="margin-top: 6px;">
    <div class="col-md-10 col-md-offset-1 static-element-indicator" id="element-search">
      <div class="panel panel-default">
        <div class="panel-body">
          <section class="text-center">
            <p style="color: #777575;font-size: 18px;">Find Inspiration in a Minute</p>
            <span style="color: #B5B1B1;font-size: 14px;"> Simple Search and Get Powerful Results</span>
            <br />
            <div class="inner-addon right-addon">
              <i class="glyphicon glyphicon-search"></i>
              <input type="text" class="form-control search-input-explore" placeholder="Enter Keyword e.g Thanksgiving" style="border-radius: 50px;margin: 40px auto;"/>
            </div>
          </section>
        </div>
      </div>
    </div>
    <div class="col-md-10 col-md-offset-1 static-element-indicator" style="margin-top: 20px;" id="element-structure" hidden>
      <div class="panel panel-default">
          <div class="panel-heading text-center color-adhero-green">
            <span style="font-size: 20px;color: white;">Length of Post</span>
          </div>
          <div class="panel-body">
            <p class="element-text-explore">The length of your Post is the number of characters like alphabets, numerals, spaces and special characters. For example, the number of characters in the Post Unmetric is awesome is 19. Check out a chart with insights on how the length affects different engagement metrics below.</p>
            <hr />
            <h4 class="element-text-explore-insights">LET'S GET SOME INSIGHT</h4>
            <hr />
            <section>
              <md-radio-group ng-model="data.group1">
                <md-radio-button value="Default" ng-click="showExploreGraphsStrucuture('Engagement')" class="explore-element-radio-button" value="engagement"> Engagement </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsStrucuture('Likes')" class="explore-element-radio-button" value="likes"> Likes </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsStrucuture('Comments')" class="explore-element-radio-button" value="comments"> Comments </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsStrucuture('Shares')" class="explore-element-radio-button" value="shares"> Shares </md-radio-button>
              </md-radio-group>
            </section>
            <hr class="clear-both" />
            <div id="explore_engagement_stucture" style="height: 300px; width: 100%;"></div>
          </div>
      </div>
    </div>
    <div class="col-md-10 col-md-offset-1 static-element-indicator" style="margin-top: 20px;" id="element-tone" hidden>
      <div class="panel panel-default">
          <div class="panel-heading text-center color-adhero-green">
            <span style="font-size: 20px;color: white;">Question</span>
          </div>
          <div class="panel-body">
            <p class="element-text-explore">Your Post poses a question and kindly asks people to respond. Below, you'll find examples of Posts meant with Questions and a chart showcasing how it affects different engagement metrics.</p>
            <section class="explore-tabs">
              <md-content>
                <md-tabs md-dynamic-height md-border-bottom>
                  <md-tab label="Example 1">
                    <md-content class="md-padding">
                    <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>Alright fans, we've got two vintage FIAT vehicles going head to head this week. Tell us which one you'd rather take for a cruise down the highway.</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/tone_1.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>235</td>
                            <td>33</td>
                            <td>17</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 2">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                        <a href="javacript:void(0)">
                          <img src="../../img/adhero_image.png" class="explore-avatar">
                        </a>
                        <a href="javascript:void(0)">
                          <span class="explore-avatar-title">Adhero</span>
                          <span class="sep">&nbsp;•&nbsp;</span>
                          <span>Wed, Oct 16 2013 at 11:00 PM</span>
                        </a>
                      </section>
                      <section class="explore-container-description-avatar">
                        <p>This 1991 Nissan Sentra SE has been in the Seller family for more than 22 years, and tried-and-true has traveled over 604K miles with them! How many miles you have racked up on YOUR Nissan? Tell us in the comments below for a chance to win a Nissan fan pack! RULES: http://rul.es/hh4V9V</p>
                      </section>
                      <section class="explore-container-image-avatar-preview">
                        <a href="javascript:void(0)"><img src="../../img/explore_images/tone_2.jpg"></a>
                      </section>
                      <section class="explore-container-table-status">
                        <table>
                          <thead>
                            <tr class="explore-tr-th">
                              <th>LIKES</th>
                              <th>COMMENTS</th>
                              <th>SHARES</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="explore-tr-td">
                              <td>1661</td>
                              <td>721</td>
                              <td>195</td>
                            </tr>
                          </tbody>
                        </table>
                      </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 3">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>Which Hyundai do you love? Tell us for a chance at a new ride: http://bit.ly/LoveMyHyundai</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/tone_3.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>4212</td>
                            <td>816</td>
                            <td>207</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                </md-tabs>
              </md-content>
            </section>
            <h4 class="element-text-explore-insights">LET'S GET SOME INSIGHT</h4>
            <hr />
            <section>
              <md-radio-group ng-model="data.group2">
                <md-radio-button value="Default" ng-click="showExploreGraphsTone('Engagement')" class="explore-element-radio-button" value="engagement"> Engagement </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsTone('Likes')" class="explore-element-radio-button" value="likes"> Likes </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsTone('Comments')" class="explore-element-radio-button" value="comments"> Comments </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsTone('Shares')" class="explore-element-radio-button" value="shares"> Shares </md-radio-button>
              </md-radio-group>
            </section>
            <hr class="clear-both" />
            <div id="explore_engagement_tone" style="height: 300px; width: 100%;"></div>
          </div>
      </div>
    </div>
    <div class="col-md-10 col-md-offset-1 static-element-indicator" style="margin-top: 20px;" id="element-intent" hidden>
      <div class="panel panel-default">
          <div class="panel-heading text-center color-adhero-green">
            <span style="font-size: 20px;color: white;">Brand Product</span>
          </div>
          <div class="panel-body">
            <p class="element-text-explore">Your Post's talking about your Brand's Product - it could be a new product, a concept, an old product from the archives. Get a better idea with examples of Posts mentioning a Brand Product and a chart showcasing how it affects different engagement metrics.</p>
            <section class="explore-tabs">
              <md-content>
                <md-tabs md-dynamic-height md-border-bottom>
                  <md-tab label="Example 1">
                    <md-content class="md-padding">
                    <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>I am the possible. #BMWi8</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/intent_1.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>36630</td>
                            <td>702</td>
                            <td>8410</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 2">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                        <a href="javacript:void(0)">
                          <img src="../../img/adhero_image.png" class="explore-avatar">
                        </a>
                        <a href="javascript:void(0)">
                          <span class="explore-avatar-title">Adhero</span>
                          <span class="sep">&nbsp;•&nbsp;</span>
                          <span>Wed, Oct 16 2013 at 11:00 PM</span>
                        </a>
                      </section>
                      <section class="explore-container-description-avatar">
                        <p>The 2015 Nissan Altima is no one-trick pony! Impressive acceleration, fuel economy, safety AND a satisfying ride all make it a top choice among family sedans. Read more at Edmunds.com: http://nssn.co/alx1n1</p>
                      </section>
                      <section class="explore-container-image-avatar-preview">
                        <a href="javascript:void(0)"><img src="../../img/explore_images/intent_2.jpg"></a>
                      </section>
                      <section class="explore-container-table-status">
                        <table>
                          <thead>
                            <tr class="explore-tr-th">
                              <th>LIKES</th>
                              <th>COMMENTS</th>
                              <th>SHARES</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="explore-tr-td">
                              <td>834</td>
                              <td>23</td>
                              <td>57</td>
                            </tr>
                          </tbody>
                        </table>
                      </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 3">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>Get live video from the Tokyo Motor Show showing new Lexus cars. Send your requests to #LexusInTokyo on Twitter. Find out more. http://bit.ly/1amsCsa</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/intent_3.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>27703</td>
                            <td>169</td>
                            <td>663</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                </md-tabs>
              </md-content>
            </section>
            <h4 class="element-text-explore-insights">LET'S GET SOME INSIGHT</h4>
            <hr />
            <section>
              <md-radio-group ng-model="data.group3">
                <md-radio-button value="Default" ng-click="showExploreGraphsIntent('Engagement')" class="explore-element-radio-button" value="engagement"> Engagement </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsIntent('Likes')" class="explore-element-radio-button" value="likes"> Likes </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsIntent('Comments')" class="explore-element-radio-button" value="comments"> Comments </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsIntent('Shares')" class="explore-element-radio-button" value="shares"> Shares </md-radio-button>
              </md-radio-group>
            </section>
            <hr class="clear-both" />
            <div id="explore_engagement_intent" style="height: 300px; width: 100%;"></div>
          </div>
      </div>
    </div>
    <div class="col-md-10 col-md-offset-1 static-element-indicator" style="margin-top: 20px;" id="element-context" hidden>
      <div class="panel panel-default">
          <div class="panel-heading text-center color-adhero-green">
            <span style="font-size: 20px;color: white;">Mobile App</span>
          </div>
          <div class="panel-body">
            <p class="element-text-explore">Your Post's talking about a mobile/tablet app developed by your Brand. Check out examples of Posts mentioning a Mobile App and a chart showcasing how it affects different engagement metrics, below.</p>
            <section class="explore-tabs">
              <md-content>
                <md-tabs md-dynamic-height md-border-bottom>
                  <md-tab label="Example 1">
                    <md-content class="md-padding">
                    <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>We took the F-TYPE Coupé on a road trip like no other. See the F-TYPE on the edge of the extreme. To read the full story, download our Jaguar Magazine iPad app: http://bit.ly/1eflvBI</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/context_1.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>26494</td>
                            <td>176</td>
                            <td>1247</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 2">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                        <a href="javacript:void(0)">
                          <img src="../../img/adhero_image.png" class="explore-avatar">
                        </a>
                        <a href="javascript:void(0)">
                          <span class="explore-avatar-title">Adhero</span>
                          <span class="sep">&nbsp;•&nbsp;</span>
                          <span>Wed, Oct 16 2013 at 11:00 PM</span>
                        </a>
                      </section>
                      <section class="explore-container-description-avatar">
                        <p>Come home to a warm welcome or, if you prefer, a cool reception. Nest Learning Thermostats now work with the Digital DriveStyle app with Drive Kit Plus for the iPhone. Now, when you head home in your Mercedes-Benz, your Nest will adjust the temperature accordingly, ensuring a comfortable arrival as you step into your home. Learn more in this video: http://mbusa.us/0tgzMa/</p>
                      </section>
                      <section class="explore-container-image-avatar-preview">
                        <a href="javascript:void(0)"><img src="../../img/explore_images/context_2.jpg"></a>
                      </section>
                      <section class="explore-container-table-status">
                        <table>
                          <thead>
                            <tr class="explore-tr-th">
                              <th>LIKES</th>
                              <th>COMMENTS</th>
                              <th>SHARES</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="explore-tr-td">
                              <td>15280</td>
                              <td>154</td>
                              <td>398</td>
                            </tr>
                          </tbody>
                        </table>
                      </section>
                    </md-content>
                  </md-tab>
                  <md-tab label="Example 3">
                    <md-content class="md-padding">
                      <section class="explore-container-images-preview">
                      <a href="javacript:void(0)">
                        <img src="../../img/adhero_image.png" class="explore-avatar">
                      </a>
                      <a href="javascript:void(0)">
                        <span class="explore-avatar-title">Adhero</span>
                        <span class="sep">&nbsp;•&nbsp;</span>
                        <span>Sat, Apr 26 2014 at 06:01 AM</span>
                      </a>
                    </section>
                    <section class="explore-container-description-avatar">
                      <p>Haven't used the Aston Martin Configurator app yet? Here's a quick guide to the features: http://astnmrt.in/Qp8tMv</p>
                    </section>
                    <section class="explore-container-image-avatar-preview">
                      <a href="javascript:void(0)"><img src="../../img/explore_images/context_3.jpg"></a>
                    </section>
                    <section class="explore-container-table-status">
                      <table>
                        <thead>
                          <tr class="explore-tr-th">
                            <th>LIKES</th>
                            <th>COMMENTS</th>
                            <th>SHARES</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="explore-tr-td">
                            <td>19141</td>
                            <td>90</td>
                            <td>701</td>
                          </tr>
                        </tbody>
                      </table>
                    </section>
                    </md-content>
                  </md-tab>
                </md-tabs>
              </md-content>
            </section>
            <h4 class="element-text-explore-insights">LET'S GET SOME INSIGHT</h4>
            <hr />
            <section>
              <md-radio-group ng-model="data.group4">
                <md-radio-button value="Default" ng-click="showExploreGraphsContext('Engagement')" class="explore-element-radio-button" value="engagement"> Engagement </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsContext('Likes')" class="explore-element-radio-button" value="likes"> Likes </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsContext('Comments')" class="explore-element-radio-button" value="comments"> Comments </md-radio-button>
                <md-radio-button ng-click="showExploreGraphsContext('Shares')" class="explore-element-radio-button" value="shares"> Shares </md-radio-button>
              </md-radio-group>
            </section>
            <hr class="clear-both" />
            <div id="explore_engagement_context" style="height: 300px; width: 100%;"></div>
          </div>
      </div>
    </div>
    <!-- <section ui-view></section> -->
  </div>
</div>

<script type="text/javascript">
  $(function(){
    $('.search-input-explore').click(function(){
      $('.inner-addon').addClass('inner-addon_focus');
    });
  });
</script>