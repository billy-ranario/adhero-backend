<md-dialog aria-label="Mango (Fruit)" image-show>
  <md-dialog-content>
    <div class="md-dialog-content">
      <img ng-src="{{ image }}" style="margin: auto; max-width: 100%;">
    </div>
  </md-dialog-content>
</md-dialog>