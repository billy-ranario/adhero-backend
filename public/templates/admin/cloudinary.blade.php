  <div class="col-md-6 col-md-offset-3" cloudinary-list style="margin-top: 50px;">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3>Image List in Cloudinary Api</h3>
    </div>
    <div class="panel-body">
      <ul class="list-group">
        <li class="list-group-item text-center">
          <input type="text" class="form-control" ng-model="keyword">
        </li>
        <li class="list-group-item text-left">
          <button class="btn btn-primary">Upload</button>
        </li>
        <li class="list-group-item" ng-repeat="list in images | filter: keyword" id="image_{{list.public_id}}">
          <md-checkbox ng-click="select(list)">
          <span ng-bind="list.date | date" class="pull-left"></span>
          <br />
          <img ng-src="{{ list.secure_url }}" class="pull-left">
          <i id="image_{{list.public_id}}" class="glyphicon glyphicon-trash pull-right custom-list-image-delete" ng-click="remove(list)"></i>
          <img src="../img/loading-sm.gif" class="loading-images pull-right" id="image_loader_{{list.public_id}}">
          <div class="clear-both"></div>
          </md-checkbox>

        </li>
        <li class="list-group-item">
            <md-checkbox ng-click="selectedAll(images)" ng-checked="status == true">Select All</md-checkbox>
        </li>
        <li ng-if="selected_ids" class="list-group-item text-center" ng-click="deleteMultiple(selected)"><button class="btn btn-danger load-images-btn">Delete</button><img src="../img/loading-sm.gif" class="loading-images"></li>
        <li class="list-group-item text-center" ng-click="loadMoreImages(next_cursor)"><button class="btn btn-primary load-images-btn">Load More</button><img src="../img/loading-sm.gif" class="loading-images"></li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    cloudinary.setCloudName('dew9c3qc2');

    
  });
</script>