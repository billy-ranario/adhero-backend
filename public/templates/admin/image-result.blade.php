<div class="col-md-11 col-md-offset-1">
  	<div class="panel panel-default image-resut-panel-default">
  		<div class="panel-body">
  			<div class="row" style="margin-bottom: 20px;">
  				<!-- <a href="http://localhost:8000/api/admin/report/clarifai" class="btn btn-download pull-right">Download as CSV</a> -->
          <!-- <button type="button" ng-csv="getArray()" csv-header="['Image', 'Tags', 'Score']" filename="clarifai.csv" class="btn btn-download pull-right">Download as CSV</button>
          <img src="../img/loading-sm.gif" class="loading-images"> -->
          <a href="javascript:void(0)" ui-sref="table-clarifai" type="button" class="btn btn-download pull-right">View in Clarifai Tags and Score Using Table</a>
  			</div>
  			<div class="row" ng-repeat="img in clientImageObject">
  				<div class="col-md-1" style="width: 5%!important">
  					<section class="image-result-id">
	  					<h3><span ng-bind="$index + 1"></span></h3>
	  				</section>
  				</div>
  				<div class="col-md-2">
  					<section class="image-result-holder text-center">
                <a href="javascript:void(0)" image-load-cg image="img"></a>
	  					<button class="btn btn-danger" ng-click="deleteImage(img)"><i class="glyphicon glyphicon-trash"></i></button>
	  				</section>
  				</div>
  				<div class="col-md-4">
  					<section class="image-result-text-holder">
	  					<h2>Clarifai API </h2>
	  					<div class="container-table">
	  						<table class="table">
		  						<thead>
		  							<tr>
		  								<th>Tags</th>
		  								<th>Score</th>
		  							</tr>
		  						</thead>
		  						<tbody>
		  							<tr ng-repeat="tags in img.clarifai.data">
		  								<td><span ng-bind="tags.class"></span></td>
		  								<td><span ng-bind="tags.probability | number:2"></span></td>
		  							</tr>
		  						</tbody>
		  					</table>
	  					</div>
	  				</section>
  				</div>
  				<div class="col-md-5">
  					<section class="image-result-text-holder">
	  					<h2>Graymatic API </h2>
	  					<div class="container-table">
		  					<table class="table">
		  						<thead>
		  							<tr>
		  								<th>Aesthetics</th>
		  								<th>Attributes</th>
		  								<th>Color</th>
		  							</tr>
		  						</thead>
		  						<tbody>
		  							<tr ng-repeat="data in img.graymatic">
		  								<td>
		  									<p>Aesthetic Social: <span ng-bind="data.aesthetics.Aesthetic_Social" class="bold-data"></span></p>
		  									<p>Aesthetic Value: <span ng-bind="data.aesthetics.aesthetic_value" class="bold-data"></span></p>
		  									<p>Animation Index: <span ng-bind="data.aesthetics.animation_index" class="bold-data"></span></p>
		  									<p>Arousal: <span ng-bind="data.aesthetics.arousal" class="bold-data"></span></p>
		  									<p>Hue: <span ng-bind="data.aesthetics.hue" class="bold-data"></span></p>
		  									<p>Image Quality: <span ng-bind="data.aesthetics.image_quality" class="bold-data"></span></p>
		  									<p>Object: <span ng-bind="data.aesthetics.object" class="bold-data"></span></p>
		  									<p>Valence: <span ng-bind="data.aesthetics.valence" class="bold-data"></span></p>
		  								</td>
		  								<td>
		  									<p>Genre: <span ng-bind="data.attributes[0].genre[0]" class="bold-data"></span></p>
		  									<!-- <p>Genre: <span ng-bind="data.attributes[0].object" class="bold-data"></span></p> -->
		  								</td>
		  								<td>
		  									<p ng-repeat="color_info in data.color.colorpalette.colors">
		  										<!-- <p ng-repeat="color in color_info">
		  											<span ng-bind="color[0]"></span>
		  										</p> -->
		  										<span ng-bind="color_info[1]"></span>:
		  										<span ng-bind="color_info[0]" class="bold-data"></span>
		  									</p>
		  								</td>
		  							</tr>
		  						</tbody>
		  					</table>
	  					</div>
	  				</section>
  				</div>
  				<div class="clear-both"></div>
  				<hr class="hr-custom" />
  			</div>
        <div class="text-center">
          <button class="btn btn-primary load-images-btn" ng-click="next(next_page)">Load More</button>
          <img src="../img/loading-sm.gif" class="loading-images">
          <button class="btn btn-success back-to-top" onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});">Back to Top</button>
        </div>
  		</div>
  	</div>
</div>