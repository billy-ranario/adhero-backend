<div class="col-md-11 col-md-offset-1" immaga-data>
    <div class="panel panel-default image-resut-panel-default">
      <div class="panel-body">
        <div class="row" style="margin-bottom: 20px;">
          <button class="btn btn-download pull-right">Download as CSV</button>
        </div>
        <!-- ng-repeat="img in clientImageObject" -->
        <div class="row" ng-repeat="img in imagga">
          <div class="col-md-1" style="width: 0%!important">
            <section class="image-result-id">
              <h3><span ng-bind="$index + 1"></span></h3>
            </section>
          </div>
          <div class="col-md-2">
            <section class="image-result-holder text-center">
                <a href="javascript:void(0)" image-load image="img"></a>
              <button class="btn btn-danger" ng-click="deleteImage(img)"><i class="glyphicon glyphicon-trash"></i></button>
            </section>
          </div>
          <div class="col-md-5">
            <section class="image-result-text-holder">
              <h2>Immaga Color</h2>
              <section>
                <p>Result Info</p>
                <p>Color Percent Threshold: <span ng-bind="img.result.colors.results[0].info.color_percent_threshold"></span>%</p>
                <p>Color Variance: <span ng-bind="img.result.colors.results[0].info.color_variance"></span></p>
                <p>Object Percentage: <span ng-bind="img.result.colors.results[0].info.object_percentage"></span></p>
              </section>
              <div class="container-table">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Background Colors</th>
                      <th>Foreground Colors</th>
                      <th>Image Colors</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr>
                      <td>
                        <section ng-repeat="bc in img.result.colors.results[0].info.background_colors">
                          <p>Closest Palette Color: "<span ng-bind="bc.closest_palette_color"></span>"</p>
                          <p>Closest Palette Color HTML Code: "<span ng-bind="bc.closest_palette_color_html_code"></span>"</p>
                          <p>Closest Palette Color Parent: "<span ng-bind="bc.closest_palette_color_parent"></span>"</p>
                          <p>Closest Palette Color Distance: <span ng-bind="bc.closest_palette_distance"></span></p>
                          <p>Closest Palette Color Distance: <span ng-bind="bc.closest_palette_distance"></span></p>
                          <p>Percentage: <span ng-bind="bc.percentage"></span></p>
                          <hr />
                        </section>
                      </td>
                      <td>
                        <section ng-repeat="fc in img.result.colors.results[0].info.foreground_colors">
                          <p>Closest Palette Color: "<span ng-bind="fc.closest_palette_color"></span>"</p>
                          <p>Closest Palette Color HTML Code: "<span ng-bind="fc.closest_palette_color_html_code"></span>"</p>
                          <p>Closest Palette Color Parent: "<span ng-bind="fc.closest_palette_color_parent"></span>"</p>
                          <p>Closest Palette Color Distance: <span ng-bind="fc.closest_palette_distance"></span></p>
                          <p>Closest Palette Color Distance: <span ng-bind="fc.closest_palette_distance"></span></p>
                          <p>Percentage: <span ng-bind="fc.percentage"></span></p>
                          <hr />
                        </section>
                      </td>
                      <td>
                        <section ng-repeat="ic in img.result.colors.results[0].info.image_colors">
                          <p>Closest Palette Color: "<span ng-bind="ic.closest_palette_color"></span>"</p>
                          <p>Closest Palette Color HTML Code: "<span ng-bind="ic.closest_palette_color_html_code"></span>"</p>
                          <p>Closest Palette Color Parent: "<span ng-bind="ic.closest_palette_color_parent"></span>"</p>
                          <p>Closest Palette Color Distance: <span ng-bind="ic.closest_palette_distance"></span></p>
                          <p>Closest Palette Color Distance: <span ng-bind="ic.closest_palette_distance"></span></p>
                          <p>Percentage: <span ng-bind="ic.percent"></span></p>
                          <hr />
                        </section>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </section>
          </div>
          <div class="col-md-4">
            <section class="image-result-text-holder">
              <h2>Imagga Tags </h2>
              <section class="image-result-text-holder">
              <div class="container-table" style="height: 370px!important;">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Confidence</th>
                      <th>tag</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="tags in img.result.tags.results[0].tags">
                      <td><span ng-bind="tags.confidence | number:2"></span></td>
                      <td><span ng-bind="tags.tag"></span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </section>
            </section>
          </div>
          <div class="clear-both"></div>
          <hr class="hr-custom" />
        </div>
      </div>
    </div>
</div>