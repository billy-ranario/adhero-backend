<div class="col-md-11 col-md-offset-1" table-clarfai>
    <div class="panel panel-default image-resut-panel-default">
      <div class="panel-body">
        <div class="row" ng-repeat="img in clientImageObject">
          <div class="col-md-12" id="image_{{img.image_id}}">
            
          </div>
          <div class="col-md-1 col-md-offset-2" style="width: 5%!important">
            <section class="image-result-id">
              <h3><span ng-bind="$index + 1"></span></h3>
            </section>
          </div>
          <div class="col-md-3">
            <section class="image-result-holder text-center">
                <a href="javascript:void(0)" image-load-cg image="img"></a>
                <span ng-bind="img.clarifai.image"></span>
            </section>
          </div>
          <div class="col-md-4">
            <table class="table">
              <thead>
                <tr>
                  <th>Tags</th>
                  <th>Score</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="list in img.clarifai.data">
                  <td><span ng-bind="list.class"></span></td>
                  <td><span ng-bind="list.probability"></span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="clear-both"></div>
          <hr class="hr-custom" />
        </div>
        <div class="text-center bottom-call">
          <button class="btn btn-primary load-images-btn" ng-click="next(next_page)">Load More</button>
          <img src="../img/loading-sm.gif" class="loading-images">
          <button class="btn btn-success back-to-top" onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});">Back to Top</button>
        </div>
      </div>
    </div>
</div>