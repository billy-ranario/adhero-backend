var app = angular.module('admin', ['ui.router','ngStorage', 'countrySelect', 'ngFileUpload', 'angular-loading-bar', 'ngMaterial', 'ngSanitize', 'ngCsv'])


app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/admin/dashboard.blade.php',
      controller: 'homeController'
    })
    .state('image-result', {
      url: '/image-result',
      templateUrl: 'templates/admin/image-result.blade.php',
      controller: 'imageCtrl'
    })
    .state('client', {
      url: '/client',
      templateUrl: 'templates/admin/client.blade.php'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'templates/admin/settings.blade.php'
    })
    .state('client-telstra', {
      url: '/client-telstra',
      templateUrl: 'templates/admin/clients/client-telstra.blade.php',
      controller: 'clientController'
    })
    .state('image-result-immaga', {
      url: '/image-result-immaga',
      templateUrl: 'templates/admin/image-result-2.blade.php',
    })
    .state('admin-server-images', {
      url: '/admin-server-images',
      templateUrl: 'templates/admin/admin-images-server-list.blade.php',
    })
    .state('admin-server-images-cloudinary', {
      url: '/admin-server-images-cloudinary',
      templateUrl: 'templates/admin/cloudinary.blade.php',
    })
    .state('table-clarifai', {
      url: '/table-clarifai',
      templateUrl: 'templates/admin/table-clarifai.blade.php',
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/error/500-error.blade.php'
    });
    // .state('upload', {
    //   url: '/upload',
    //   controller: 'uploadCtrl',
    //   templateUrl: 'templates/upload.blade.php',
    // });
    $urlRouterProvider.otherwise('/');
    // $locationProvider.html5Mode(true);
});
