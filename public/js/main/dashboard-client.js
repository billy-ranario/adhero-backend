var app = angular.module('client', ['ui.router','ngStorage', 'countrySelect', 'angular-loading-bar', 'ordinal', 'ngMaterial'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/client/new/one.blade.php',
      controller: 'dashboardController'
    })
    .state('report', {
      url: '/report',
      templateUrl: 'templates/client/new/three.blade.php',
    })
    .state('create-brief', {
      url: '/create-brief',
      templateUrl: 'templates/client/create-campaign.blade.php',
      controller: 'createCampaignController'
    })
    .state('preview-ad', {
      url: '/preview-ad',
      templateUrl: 'templates/client/preview-ad.blade.php',
      controller: 'previewAdController'
    })
    .state('campaign-report', {
      url: '/campaign-report',
      templateUrl: 'templates/client/ads-result.blade.php',
      controller: 'adsResultController'
    })
    .state('campaign-result', {
      url: '/campaign-result',
      templateUrl: 'templates/client/campaign-result.blade.php',
      controller: 'previewAdController'
    })
    .state('winning-report', {
      url: '/winning-report',
      templateUrl: 'templates/client/winning-result.blade.php',
      controller: 'previewAdController'
    })
    // jrran90
    .state('payment', {
      url: '/payment/:id',
      templateUrl: 'templates/client/payment.blade.php',
      controller: 'paymentController'
    })
    .state('contact', {
      url: '/contact',
      templateUrl: 'templates/client/contact.blade.php',
      controller: 'contactController'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'templates/client/settings.blade.php',
      controller: 'settingsController'
    })
    .state('preview-ad-details', {
      url: '/preview-ad-details',
      templateUrl: 'templates/client/preview-ad-details.blade.php',
      controller: 'previewAdController'
    })
    .state('ideas', {
      url: '/ideas',
      templateUrl: 'templates/client/ideas.blade.php',
      controller: 'ideasController'
    })
    .state('predict', {
      url: '/predict',
      templateUrl: 'templates/client/predict.blade.php',
      controller: 'predictController'
    })
    .state('predict-menu', {
      url: '/predict-menu',
      templateUrl: 'templates/client/predict-menu.blade.php',
      controller: 'predictController'
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/error/500-error.blade.php'
    });

    $urlRouterProvider.otherwise('/');
    // $locationProvider.html5Mode(true);
});

app.controller('userInfo', function( $scope, $http, $localStorage, serverUrl ){
  $scope.getUserInfo = function getUserInfo( ) {
    $http.get( serverUrl.url + 'sessionUserInfo')
    .success(function( response ){
      console.log( response );
      $localStorage.user = response;
      $scope.user = response;
    });
  }

  $scope.logout = function logout( ) {
    $localStorage.$reset();
  }

  // $scope.getUserInfo();
});