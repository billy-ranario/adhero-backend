  var app = angular.module('client', ['ui.router','ngStorage', 'countrySelect', 'angular-loading-bar', 'ordinal', 'ngMaterial', 'graph', 'upload', 'predict','angular.filter', 'page', 'user', 'brief', 'notification', 'image', 'report', 'd3', 'ngMap', 'chart.js', 'influencers_info'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

	$stateProvider
    .state('home', {
      url: '/',
      views: {
        'main': {
          templateUrl: 'templates/client/new/one.blade.php',
          controller: 'dashboardController'
        }
    }
  })
  .state('summary', {
    url: '/summary',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'ReportSummaryCtrl'
        },
        'main': {
          templateUrl: 'templates/client/new/three.blade.php',
          controller: 'ReportSummaryCtrl'
        }
    }
  })  
  .state('report-telstra', {
    url: '/report-telstra',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'reportController'
        },
        'main': {
          templateUrl: 'templates/client/new/four.blade.php',
          controller: 'reportController'
        }
    }
  })
  .state('report-comp', {
    url: '/report-comp',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'reportController'
        },
        'main': {
          templateUrl: 'templates/client/new/five.blade.php',
          controller: 'reportController'
        }
    }
  })
  .state('predict', {
    url: '/predict',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict.blade.php',
          controller: 'predictResultController'
        }
    }
  })
  .state('gallery', {
    url: '/gallery',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/new/gallery.blade.php',
          controller: 'galleryController'
        }
    }
  })
  .state('predict-result', {
    url: '/predict-result',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('predict-results-template', {
    url: '/predict-results-template',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results-template.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('predict-results-template2', {
    url: '/predict-results-template2',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results-template2.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('location', {
    url: '/location',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/location.blade.php',
          'controller': 'locationController'
        }
    }    
  })
  .state('location-result', {
    url: '/location-result',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/location-result.blade.php',
          'controller': 'locationController'
        }
    }    
  })
  .state('explist', {
    url: '/explist',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/new/explist.blade.php'
        }
    }
  })
  .state('influencers', {
    url: '/influencers',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/influencers.blade.php',
          controller: 'influencersCtrl'
        }
    }
  })
  .state('influencer-info', {
    url: '/influencer-info/:id',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: ''
        },
        'main': {
          templateUrl: 'templates/client/influencer-info.blade.php',
          controller: 'influencersCtrl'
        }
    }
  })
  .state('upload', {
    url: '/upload',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'UploadCtrl'
        },
        'main': {
          templateUrl: 'templates/client/new/upload.blade.php',
          controller: 'UploadCtrl'
        }
    }
  })
  .state('saved-results', {
    url: '/saved-results',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/saved-results.blade.php',
          controller: 'predictController'
      }
    }
  })
  .state('contact-us', {
    url: '/contact-us',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/contact-us.blade.php',
          controller: 'predictController'
      }
    }
  })

  .state('faq', {
    url: '/faq',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/faq.blade.php',
          controller: 'predictController'
      }
    }
  });
  // .state('predict-result', {
  //   url: '/predict-result',
  //   templateUrl: 'templates/client/new/predict-data-result.blade.php',
  // });


  $urlRouterProvider.otherwise('/');
});