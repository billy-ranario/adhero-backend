var app = angular.module('home', [ ]);

app.controller('emailController',[
  "$scope", 
  "$http",
  "serverUrl", 
  "nodeUrl",
  function( $scope, $http, serverUrl, nodeUrl ){
  console.log( 'running home' );
  $scope.mail = {};
  $scope.key = "";


  if(localStorage.emailTrigger == "true") {
    $(window).load(function(){
        $('.form-content').animatescroll();

        setTimeout(function() {
          localStorage.removeItem("emailTrigger");
        }, 500);
     });
  };

  $scope.getKey = function getKey( ){
    $http.get( nodeUrl.url + 'get_key' )
    .success(function(response){
      $scope.key = response;
    });
  };

  $scope.sendEmail = function sendEmail( ) {
    console.log( $scope.mail );
    var btnElement = $( '#btn-submit' );
    var data = {
      key         : $scope.key,
      message     : {
          "html": "<p>Company Name: "+ $scope.mail.company_name +"</p><p>" + $scope.mail.message + "</p>",
          "text": "PrecisionBit",
          "subject": "PrecisionBit Request Demo",
          "from_email": $scope.mail.email,
          "from_name": $scope.mail.firstname + ' ' + $scope.mail.lastname,
          "to": [
              {
                  "email": 'hadi@precisionbit.com',
                  "name": "PrecisionBit",
                  "type": "to"
              }
          ],
      }
    }
    btnElement.addClass( 'btn-disable' ).attr( 'disabled' );
    btnElement.find( 'strong' ).text( 'Sending...' );
    $http.post( 'https://mandrillapp.com/api/1.0/messages/send.json' , data ) 
      .success( function success ( response ) {
        console.log( response );
          btnElement.removeClass( 'btn-disable' );
          btnElement.find( 'strong' ).text( 'Submit' );

          sweetAlert("", "Your request was successfully sent!", "success");
      } )
      .error( function( err ) {
        console.log( err );
        sweetAlert("", "Please provide a verified email address", "error");
      });
  };
  $scope.getKey( );
}]);