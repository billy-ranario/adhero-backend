<?php header("Content-type: application/javascript"); ?>
var app = angular.module('app', ['ui.router','ngStorage', 'countrySelect', 'ngFileUpload', 'angular-loading-bar'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/dashboard.blade.php',
      controller: 'dashboardController'
    })
    .state('create-brief', {
      url: '/create-brief',
      templateUrl: 'templates/create-campaign.blade.php',
      controller: 'createCampaignController'
    })
    .state('preview-ad', {
      url: '/preview-ad',
      templateUrl: 'templates/preview-ad.blade.php',
      controller: 'previewAdController'
    })
    .state('campaign-report', {
      url: '/campaign-report',
      templateUrl: 'templates/ads-result.blade.php',
      controller: 'adsResultController'
    })
    .state('campaign-result', {
      url: '/campaign-result',
      templateUrl: 'templates/campaign-result.blade.php',
      controller: 'previewAdController'
    })
    .state('winning-report', {
      url: '/winning-report',
      templateUrl: 'templates/winning-result.blade.php',
      controller: 'previewAdController'
    })

    // jrran90
    .state('payment', {
      url: '/payment/:id',
      templateUrl: 'templates/payment.blade.php',
      controller: 'paymentController'
    })


    .state('contact', {
      url: '/contact',
      templateUrl: 'templates/contact.blade.php',
      controller: 'contactController'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'templates/settings.blade.php',
      controller: 'settingsController'
    })
    .state('preview-ad-details', {
      url: '/preview-ad-details',
      templateUrl: 'templates/preview-ad-details.blade.php',
      controller: 'previewAdController'
    })
    .state('ideas', {
      url: '/ideas',
      templateUrl: 'templates/ideas.blade.php',
      controller: 'ideasController'
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/500-error.blade.php',
    })
    .state('creative-brief', {
      url: '/creative-brief',
      templateUrl: 'templates/creative-brief.blade.php',
      controller: 'creativeBriefController'
    })
    .state('revenues', {
      url: '/revenues',
      templateUrl: 'templates/revenues.blade.php',
      controller: 'revenuesController'
    });

    $urlRouterProvider.otherwise('/');
    // $locationProvider.html5Mode(true);
});

app.controller('userInfo', function( $scope, $http, $localStorage, serverUrl ){
  $scope.getUserInfo = function getUserInfo( ) {
    $http.get( serverUrl.url + 'sessionUserInfo')
    .success(function( response ){
      console.log( response );
      $localStorage.user = response;
      $scope.user = response;
      // var session = <?php  ?>;
      // console.log( session );
    });
  }

  $scope.logout = function logout( ) {
    $localStorage.$reset();
  }

  $scope.getUserInfo();
});