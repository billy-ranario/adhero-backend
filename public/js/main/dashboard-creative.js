var app = angular.module('creative', ['ui.router','ngStorage', 'countrySelect', 'ngFileUpload', 'angular-loading-bar'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/creative/dashboard.blade.php',
      controller: 'dashboardController'
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/error/500-error.blade.php',
    })
    .state('creative-brief', {
      url: '/creative-brief',
      templateUrl: 'templates/creative/creative-brief.blade.php',
      controller: 'creativeBriefController'
    })
    .state('creative-brief-details', {
      url: '/creative-brief-details',
      templateUrl: 'templates/creative/creative-brief-detail.blade.php',
      controller: 'creativeBriefDetailController'
    })
    .state('revenues', {
      url: '/revenues',
      templateUrl: 'templates/creative/revenues.blade.php',
      controller: 'revenuesController'
    })
    .state('designers', {
      url: '/designers',
      templateUrl: 'templates/creative/designers.blade.php',
      controller: 'designersController'
    })
    .state('creatives', {
      url: '/creatives',
      templateUrl: 'templates/creative/creatives.blade.php',
      controller: 'creativesController'
    });

    $urlRouterProvider.otherwise('/revenues');
    // $locationProvider.html5Mode(true);
});

app.controller('userInfo', function( $scope, $http, $localStorage, serverUrl ){
  $scope.getUserInfo = function getUserInfo( ) {
    $http.get( serverUrl.url + 'sessionUserInfo')
    .success(function( response ){
      console.log( response );
      $localStorage.user = response;
      $scope.user = response;
      // var session = <?php  ?>;
      // console.log( session );
    });
  }

  $scope.logout = function logout( ) {
    $localStorage.$reset();
  }

  $scope.getUserInfo();
});