app
  .service('createService',[
    function service( ) {
      var campaign_name = "";

      return {
        createCampaignName: function createCampaignName( response ) {
          if( response ) {
            campaign_name = response;
            console.log( response );
          } else {
            return campaign_name;
          }
        }
      }
    }
]);
app
  .service('netWork',[
    function service( ) {
      var network = {};

      return {
        loadNetwork: function loadNetwork( response ) {
          if( response ) {
            network = response;
            console.log( response );
          } else {
            return network;
          }
        }
      }
    }
]);
app
  .service('campaignHolder',[
    function service( ) {
      var campaign = {};

      return {
        campaignName: function campaignName( response ) {
          if( response ) {
            campaign = response;
            console.log( response );
          } else {
            return campaign;
          }
        }
      }
    }
]);
app
  .service('predictService',[
    function service( ) {

      var predict = {
        localStorageSet: function setStorage ( key , value ) {
          localStorage.setItem( key , JSON.stringify( value ) );
        },
        localStorageGet: function getStorage ( key ) {
          return JSON.parse( localStorage.getItem( key ) );
        },
        set: function setData ( key , value ) {
          predict.localStorageSet ( key , value );
        },
        imageData: function imageData( ) {        
          return predict.localStorageGet ( 'completeImageData' );
        },
        completeImageData: function completeImageData( ) {
          return predictData.completeImageData;
        },
        userData: function userData( response ) {
          if( response ) {
            userDataObj = response;
            console.log( response );
          } else {
            return userDataObj;
          }
        },
        showPredictResult: function userData( ) {
          return predictData.showPredictResult;
        },
        clearPredictResult: function clearPredictResult(  ) {
          showPredictResultObj = {};
        },
        predictStatus: function predictStatus( data ) {
          if( data ) {
            predict_status = data;
          } else {
            return predict_status;
          }
        },
        predictSampleBg: function predictSampleBg( data ) {
          if( data ) {
            sample_bg = data;
          } else {
            return sample_bg;
          }
        },
        predictGetResult: function predictGetResult( data ) {
          if( data ) {
            result_predict = data;
          } else { 
            return result_predict;
          }
        },
        showResult: function showResult( data ) {
          if( data ) {
            resultData = data;
          } else { 
            return resultData;
          }
        }
      };

      return predict;
    }
]);

app
  .service('previewImageService', [
    function service( ) {
      console.log('service running');
      var image = "";

      return {
        imageView: function imageView( data ) {
          if( data ) {
            image = data;
          } else {
            return image;
          }
        } 
      }
    }
]);