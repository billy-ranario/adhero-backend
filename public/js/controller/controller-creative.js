app
  .controller('dashboardController',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$rootScope",
    "$state",
    "createService",
    "netWork",
    function controller( $http, $scope, serverUrl, $localStorage, $rootScope, $state, createService, netWork ) {
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.dashboard-menu').addClass('active');
      netWork.loadNetwork('home');
   
    }
])
.controller('creativeBriefController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    "$state",
    function controller( $http, $scope, serverUrl, $rootScope, netWork, $state ) {
      netWork.loadNetwork('creative-brief');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.briefs-menu').addClass('active');

      $scope.number = [
      {
        id: 1
      },
      {
        id: 1
      },{
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      }];

      $scope.viewBriefDetails = function viewBriefDetails() {
        $state.go('creative-brief-details');
      }
    }
])
.controller('creativeBriefDetailController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('creative-brief-details');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.briefs-menu').addClass('active');
      $scope.CurrentDate = new Date();
    }
])
.controller('revenuesController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('revenues');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.revenues-menu').addClass('active');
      $scope.CurrentDate = new Date();
    }
])
.controller('designersController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('designers');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.designers-menu').addClass('active');
    }
])
.controller('creativesController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('creatives');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.creatives-menu').addClass('active');
    }
]);