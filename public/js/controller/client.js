app
	.controller('dashboardController', [
		"$http",
		"$scope",
		function controller( $http, $scope ){
			console.log('dashboardController');
			$('.menu-header-global').removeClass('active-menu');
			$('.fa-icon').removeClass('active-menu-icon');
		  	$('.home-a').addClass('active-menu');
		  	$('.home-a span').addClass('active-menu-icon');
		}
	])
	.controller('reportController', [
		"$http",
		"$scope",
		"Report",
		"reportService",
		"$state",
		function controller( $http, $scope, Report, reportService, $state ){
			console.log('report running');
			// console.log('running');
			$( '.predict-submenu-list' ).fadeOut();
			$('.treeview').removeClass('active');
			$('.report-menu').addClass('active');
			// Animate Actions
			$scope.animateAction = function animateAction( id ) {
				// $( '.panel-title > a i' ).removeClass( 'rotate' );
				// $( '#link-' + id ).find('i').addClass( 'rotate' );
				$('.collapse' ).collapse( 'hide' );
				$('#collapse-' + id ).collapse( 'show' );
				if ( $( '#link-' + id ).attr( 'aria-expanded' ) == 'true' ) {
					$( '#link-' + id ).attr( 'aria-expanded' , 'false' );
					$( '.panel-title > a i' ).removeClass( 'rotate' );
				}else if( $( '#link-' + id ).attr( 'aria-expanded' ) == 'false' ) {
					$( '#link-' + id ).attr( 'aria-expanded' , 'true' );
				}
				// console.log( $( '#link-' + id ).attr( 'aria-expanded' ) )
			}

			/**
			 * This is just a function, that's it!!
			 *
			 * @param  var type, var field
			 */
			/*function tagType(type) {
				Report.show(type)
					  .then(function (res) {
					  	angular.forEach(res.data.result, function (value) {
					  		for (var i=0; i<value.field.length; i++) {
					  			if (value.field[i]=="emotions") {
					  				console.log(value.field[i]);
					  			} 
					  		}
					  	});
					  });
			}*/
			var emotions;
			var emotion_list;

			/**
			 * Plot Report (default filter is - all)
			 *
			 * @param  var reportType, var filter
			 */		
			$scope.plotReport = function (reportType, filter) {
        		console.log(reportType, filter);
				var myPlot = document.getElementById('my-graph');
				var thisPlot = document.getElementById('avePlot');
				$scope.avgCalculator = function (data,key) {
					var sum = {};
					sum['total'] = 0;
					var length = {};
					length['total'] = 0;
					var avg = {};
					for (i=0;i<data.length;i++) {
						for(var j = 0; j < data[i]['value'].length; j++){
							if ( $.inArray( data[i]['value'][j], key ) != -1  ){
								if (data[i]['value'][j] in sum){
									length[data[i]['value'][j]] +=1;
									sum[data[i]['value'][j]] += parseFloat(data[i].unique_ctr); 	
								}
								else{
									length[data[i]['value'][j]] =1;
									sum[data[i]['value'][j]] = parseFloat(data[i].unique_ctr); 	
								}
							}
						}
						sum['total'] += parseFloat(data[i].unique_ctr);
						length['total'] +=1;
						
					};

					for ( key in sum){
						avg[key] = sum[key]/length[key]
					}
					return avg
				}

				$scope.meanDraw = function(total_avg, dataObj){
					var list = [];
					for (var key in total_avg) {
						if (key != 'total') list.push(key);
					} 
					var avgtotal = {
						name: 'Avg',
						y: Array.apply(null, Array(list.length)).map(function(){return 0.43}),
						x: list,
						mode: 'lines',
						text: 'Average ' + String(Math.round(total_avg['total']*100)/100) + '%',
						hoverinfo:"text",
						line: {
							color: 'Gray',
							width: 1,
							dash: 'dot',
							connectgaps: false
							}
					}
					dataObj.push(avgtotal);
				}
				$scope.constructorAvg = function(dataObj, total_avg) {
					var list = []
					for (i=0;i<dataObj.length;i++){
						if(dataObj[i].name){
							var template = dataObj[i];
							var label_avg = 'Average CTR: '+ String(Math.round(parseFloat(total_avg[template.name])*100)/100)+'%';
			  				list.push({
			  					name: template.name,
								y: [total_avg[template.name]],
								x: [template.name],
								mode: 'markers',
								type: 'scatter',
								text: label_avg, 
								hoverinfo:"text",
								showlegend: false,
								marker : {
									symbol: "star",
									color: 'white',
									line: {
										width:2,
										color:'black'
									},
									size:10
								}
							});
						}
					}
					return dataObj.concat(list);
				}
				switch ( reportType ) {
					case 'emotions':
						$scope.reportType = reportType;

						$scope.title = "Image Emotions";
						$scope.predict = {};
						document.querySelector(".tag-val .option-selected").innerHTML = "Emotions";
						

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();
							  		// reportService.clearGraph();

							  		var t1=[],t2=[],t3=[],t4=[],t5=[],
							  			t6=[],t7=[],t8=[],t9=[],t10=[],t11=[],t12=[],t13=[],t14=[];

							  		var arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],arrCtr5=[],
							  			arrCtr6=[],arrCtr7=[],arrCtr8=[],arrCtr9=[];

							  		var arrAds=[],arrAds2=[],arrAds3=[],arrAds4=[],arrAds5=[],
							  			arrAds6=[],arrAds7=[],arrAds8=[],arrAds9=[],arrAds10=[],
							  			arrAds11=[],arrAds12=[],arrAds13=[],arrAds14=[];

							  		var arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[],arrHoverValue4=[],arrHoverValue5=[],
							  			arrHoverValue6=[],arrHoverValue7=[],arrHoverValue8=[],arrHoverValue9=[];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[];

							  		var tags = [],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[];
							  		
							  		emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		

							  		angular.forEach(res.data.result, function(value,key) {
							  			if (value.value[0] == "Acceptance") {
							  				tags.push([value.type,value.ad_id]);
							  				
							  				var arrX = [];
							  				arrCtr.push(parseFloat(value.unique_ctr));
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);
							  				avg[0] += parseFloat(value.unique_ctr);



							  				arrCtr_count[0]++;
											arrHoverValue.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[0]);

							  				t1 = {
							  					name: value.value[0],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue, 
												img: arrAds, 
												overalldata: overalldata, 
												value: tags,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])

								  			}

							  			} else if (value.value[0] == "Anticipation") {

							  				tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);
							  				
							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

											arrHoverValue2.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[0]);
							  				t2 = {
							  					name: value.value[0],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												// text: arrHoverValue2,
												// img: arrAds2,
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Interest") {

							  				tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);
  				
							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				arrHoverValue3.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[0]);
							  				t3 = {
							  					name: value.value[0],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												// text: arrHoverValue3,
												// img: arrAds3,
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Joy") {

							  				tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);
			  				
							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				arrHoverValue4.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[0]);
							  				t4 = {
							  					name: value.value[0],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												// text: arrHoverValue4,
												// img: arrAds4,
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "N/A") {

							  				tags5.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);
							  				
							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

							  				arrHoverValue5.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[0]);
							  				t5 = {
							  					name: value.value[0],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												// text: arrHoverValue5,
												// img: arrAds5,
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Pensiveness") {
							  				
							  				tags6.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);
			  				
							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				arrHoverValue6.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[0]);
							  				t6 = {
							  					name: value.value[0],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Serenity") {

							  				tags7.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);
							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);
							  				
							  				avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

							  				arrHoverValue7.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[0]);
							  				t7 = {
							  					name: value.value[0],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Surprise") {

							  				tags8.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);
							  				
							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;


							  				arrHoverValue8.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[0]);
							  				t8 = {
							  					name: value.value[0],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Trust") {

							  				tags9.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);
	
							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

							  				arrHoverValue9.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[0]);
							  				t9 = {
							  					name: value.value[0],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Pensiveness") {
							  				
							  				tags10.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);
							  				
							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

							  				arrHoverValue10.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[0]);
							  				t10 = {
							  					name: value.value[0],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Sadness") {

							  				tags11.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);
							  				
							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

							  				arrHoverValue11.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[0]);
							  				t11 = {
							  					name: value.value[0],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Serenity") {
							  				
							  				tags12.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);

							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

							  				arrHoverValue12.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[0]);
							  				t12 = {
							  					name: value.value[0],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												// text: arrHoverValue12,
												// img: arrAds12,
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Surprise") {
							  				
							  				tags13.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);

							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

							  				arrHoverValue13.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[0]);
							  				t13 = {
							  					name: value.value[0],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Trust") {

							  				tags14.push([value.type,value.ad_id]);

							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);
							  				
							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

							  				arrHoverValue14.push(["<b>Emotion:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[0]);
							  				t14 = {
							  					name: value.value[0],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){

							  				computeAvg();
							  			}

						  				
							  		});



									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;

												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );

						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });

									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8,t9];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
										$('.chart-btm-box').show();
									// })

									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);
									
									var layout = {
										title: $scope.title,
										// xaxis: {title: 'Emotions'},
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest',
									};
									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8,t9];
									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];
								  	dataObj = $scope.constructorAvg(dataObj, total_avg);

								  	$scope.meanDraw(total_avg, dataObj);
								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});


								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
								  	// 		var side_images = [];

								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");
								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {

								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											// 				li.innerHTML = '<a class="thumbnail" ><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;
								  	// 				}
								  	// 			}
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}


								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Emotions';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  			
								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  			
								  			

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});

								  	// 		});
								  	// 	});

							

							  });

						break;
					case 'indoor/outdoor':

						$scope.reportType = reportType;

						$scope.title = "Indoor Outdoor";
						document.querySelector(".tag-val .option-selected").innerHTML = "Indoor/Outdoor";

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		// reportService.clearGraph();

							  		var t1=[],t2=[],t3=[],
							  			arrCtr=[],arrCtr2=[],arrCtr3=[],
										arrAds=[],arrAds2=[],arrAds3=[];

									var arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[];

									var overalldata=[],overalldata2=[],overalldata3=[];

									emotion_list = [];
							  		var avg = [0,0,0];
							  		var arrCtr_count = [0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var tags = [],tags2=[],tags3=[];
							  		

							  		angular.forEach(res.data.result, function(value,key) {
							  			if (value.value[1] == "Indoor") {

							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

											arrHoverValue.push(["<b>Indoor/Outdoor:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);



							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[1]);
							  				t1 = {
							  					name: value.value[1],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}

							  			} else if (value.value[1] == "Outdoor") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

											arrHoverValue2.push(["<b>Indoor/Outdoor:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);



							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[1]);
							  				t2 = {
							  					name: value.value[1],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}

							  			// } else if (value.value[1] == "NIL") {


							  			} else if (value.value[1] == "N/A") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

											arrHoverValue3.push(["<b>Indoor/Outdoor:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);



							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[1]);
							  				t3 = {
							  					name: value.value[1],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}


											if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}							  			
								  		}

								  		if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_all += parseFloat(avg[x]);
						  						
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			

									
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,

										// xaxis: { title: 'Indoor/Outdoor' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3];
								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									
								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											// console.log(data);
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;
								  	// 				}
								  	// 			}
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Indoor Outdoor';
								  			
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});

								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });

						break;
					case 'scenario':

						$scope.reportType = reportType;					

						document.querySelector(".tag-val .option-selected").innerHTML = "Scenario";
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();
							  	$scope.title = "Scenario";
							  		var t1=[],t2=[],t3=[],t4=[],t5=[],t6=[],t7=[],t8=[],t9=[],t10=[],
							  			t11=[],t12=[],t13=[],t14=[],t15=[],t16=[],t17=[],t18=[],t19=[],t20=[],
							  			t21=[],t22=[],t23=[];

							  		var arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],arrCtr5=[],
							  			arrCtr6=[],arrCtr7=[],arrCtr8=[],arrCtr9=[],arrCtr10=[],
										arrCtr11=[],arrCtr12=[],arrCtr13=[],arrCtr14=[],arrCtr15=[],
										arrCtr16=[],arrCtr17=[],arrCtr18=[],arrCtr19=[],arrCtr20=[],
										arrCtr21=[],arrCtr22=[],arrCtr23=[];

							  		var arrAds=[],arrAds2=[],arrAds3=[],arrAds4=[],arrAds5=[],
							  			arrAds6=[],arrAds7=[],arrAds8=[],arrAds9=[],arrAds10=[],
							  			arrAds11=[],arrAds12=[],arrAds13=[],arrAds14=[],arrAds15=[],
							  			arrAds16=[],arrAds17=[],arrAds18=[],arrAds19=[],arrAds20=[],
							  			arrAds21=[],arrAds22=[],arrAds23=[];

							  		var arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[],arrHoverValue4=[],arrHoverValue5=[],
							  			arrHoverValue6=[],arrHoverValue7=[],arrHoverValue8=[],arrHoverValue9=[],arrHoverValue10=[],
							  			arrHoverValue11=[],arrHoverValue12=[],arrHoverValue13=[],arrHoverValue14=[],arrHoverValue15=[],
							  			arrHoverValue16=[],arrHoverValue17=[],arrHoverValue18=[],arrHoverValue19=[],arrHoverValue20=[],
							  			arrHoverValue21=[],arrHoverValue22=[],arrHoverValue23=[];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[],overalldata15=[]
							  			,overalldata16=[],overalldata17=[],overalldata18=[],overalldata19=[],overalldata20=[]
							  			,overalldata21=[],overalldata22=[],overalldata23=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'scenario');

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[],tags15=[]
							  			,tags16=[],tags17=[],tags18=[],tags19=[],tags20=[]
							  			,tags21=[],tags22=[],tags23=[];	
							  		

							  		angular.forEach(res.data.result, function(value,key) {
							  	

							  			if (value.value[0] == "Amusement Park") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);

							  				

											arrHoverValue.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

											avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[0]);
							  				t1 = {
							  					name: value.value[0],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "At the frontyard") {
							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr);
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);

							  				

											arrHoverValue2.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");											arrHoverValue2.


											avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				for (var i = 0; i<arrCtr2.length; i++) {
							  					arrX.push(value.value[0]);
							  				}

							  				t2 = {
							  					name: value.value[0],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Balcony") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);

							  				
											arrHoverValue3.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");											arrHoverValue3.


											avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[0]);
							  				t3 = {
							  					name: value.value[0],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}


											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}				

							  			} else if (value.value[0] == "Café") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr);
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);

							  				
											arrHoverValue4.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");											arrHoverValue4.

											avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[0]);
							  				t4 = {
							  					name: value.value[0],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}


											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}					

							  			} else if (value.value[0] == "Car") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);

							  				
											arrHoverValue5.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");											arrHoverValue5.


											avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[0]);
							  				t5 = {
							  					name: value.value[0],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "Cinema") {
							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				

							  				
											arrHoverValue6.push("<b>Scenario::</b> " + value.value[0] +"<br><b>CTR: </b>"+parseFloat(value.unique_ctr).toFixed(2)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");						

											arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);
											avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[0]);
							  				t6 = {
							  					name: value.value[0],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}

							  			} else if (value.value[0] == "City") {

							  				
							  				
											tags7.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);
							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);

							  				
											arrHoverValue7.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");										

											avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[0]);
							  				t7 = {
							  					name: value.value[0],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};
							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Garden") {

							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);

											arrHoverValue8.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

											avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[0]);
							  				t8 = {
							  					name: value.value[0],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Home") {

							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);


							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

											arrHoverValue9.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[0]);
							  				t9 = {
							  					name: value.value[0],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "In a car") {

							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);


							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

											arrHoverValue10.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[0]);
							  				t10 = {
							  					name: value.value[0],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "In a crowd") {

							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);


							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

											arrHoverValue11.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[0]);
							  				t11 = {
							  					name: value.value[0],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "In a room") {

							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);


							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

											arrHoverValue12.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[0]);
							  				t12 = {
							  					name: value.value[0],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Jungle") {

							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);


							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

											arrHoverValue13.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[0]);
							  				t13 = {
							  					name: value.value[0],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "NIL") {

							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);


							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

											arrHoverValue14.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[0]);
							  				t14 = {
							  					name: value.value[0],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "On a hammock") {

							  				
							  				
											tags15.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr15.push(value.unique_ctr);
							  				arrAds15.push(value.ad_number);
							  				overalldata15.push(value);


							  				avg[14] += parseFloat(value.unique_ctr);
							  				arrCtr_count[14]++;

											arrHoverValue15.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr15.length; i++) arrX.push(value.value[0]);
							  				t15 = {
							  					name: value.value[0],
												y: arrCtr15,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue15,
												img: arrAds15,
												overalldata: overalldata15,
												value: tags15,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "On a tree") {

							  				
							  				
											tags16.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr16.push(value.unique_ctr);
							  				arrAds16.push(value.ad_number);
							  				overalldata16.push(value);


							  				avg[15] += parseFloat(value.unique_ctr);
							  				arrCtr_count[15]++;

											arrHoverValue16.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr16.length; i++) arrX.push(value.value[0]);
							  				t16 = {
							  					name: value.value[0],
												y: arrCtr16,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue16,
												img: arrAds16,
												overalldata: overalldata16,
												value: tags16,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "On the stairs") {

							  				
							  				
											tags17.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr17.push(value.unique_ctr);
							  				arrAds17.push(value.ad_number);
							  				overalldata17.push(value);


							  				avg[16] += parseFloat(value.unique_ctr);
							  				arrCtr_count[16]++;

											arrHoverValue17.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr17.length; i++) arrX.push(value.value[0]);
							  				t17 = {
							  					name: value.value[0],
												y: arrCtr17,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue17,
												img: arrAds17,
												overalldata: overalldata17,
												value: tags17,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Park") {

							  				
							  				
											tags18.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr18.push(value.unique_ctr);
							  				arrAds18.push(value.ad_number);
							  				overalldata18.push(value);


							  				avg[17] += parseFloat(value.unique_ctr);
							  				arrCtr_count[17]++;

											arrHoverValue18.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr18.length; i++) arrX.push(value.value[0]);
							  				t18 = {
							  					name: value.value[0],
												y: arrCtr18,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue18,
												img: arrAds18,
												overalldata: overalldata18,
												value: tags18,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Party") {

							  				
							  				
											tags19.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr19.push(value.unique_ctr);
							  				arrAds19.push(value.ad_number);
							  				overalldata19.push(value);


							  				avg[18] += parseFloat(value.unique_ctr);
							  				arrCtr_count[18]++;

											arrHoverValue19.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr19.length; i++) arrX.push(value.value[0]);
							  				t19 = {
							  					name: value.value[0],
												y: arrCtr19,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue19,
												img: arrAds19,
												overalldata: overalldata19,
												value: tags19,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Road") {

							  				
							  				
											tags20.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr20.push(value.unique_ctr);
							  				arrAds20.push(value.ad_number);
							  				overalldata20.push(value);


							  				avg[19] += parseFloat(value.unique_ctr);
							  				arrCtr_count[19]++;

											arrHoverValue20.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr20.length; i++) arrX.push(value.value[0]);
							  				t20 = {
							  					name: value.value[0],
												y: arrCtr20,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue20,
												img: arrAds20,
												overalldata: overalldata20,
												value: tags20,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Sea") {

							  				
							  				
											tags21.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr21.push(value.unique_ctr);
							  				arrAds21.push(value.ad_number);
							  				overalldata21.push(value);


							  				avg[20] += parseFloat(value.unique_ctr);
							  				arrCtr_count[20]++;

											arrHoverValue21.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr21.length; i++) arrX.push(value.value[0]);
							  				t21 = {
							  					name: value.value[0],
												y: arrCtr21,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue21,
												img: arrAds21,
												overalldata: overalldata21,
												value: tags21,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Supermarket") {

							  				
							  				
											tags22.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr22.push(value.unique_ctr);
							  				arrAds22.push(value.ad_number);
							  				overalldata22.push(value);


							  				avg[21] += parseFloat(value.unique_ctr);
							  				arrCtr_count[21]++;

											arrHoverValue22.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr22.length; i++) arrX.push(value.value[0]);
							  				t22 = {
							  					name: value.value[0],
												y: arrCtr22,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue22,
												img: arrAds22,
												overalldata: overalldata22,
												value: tags22,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Urban") {

							  				
							  				
											tags23.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr23.push(value.unique_ctr);
							  				arrAds23.push(value.ad_number);
							  				overalldata23.push(value);


							  				avg[22] += parseFloat(value.unique_ctr);
							  				arrCtr_count[22]++;

											arrHoverValue23.push("<b>Scenario:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>");

							  				for (var i = 0; i<arrCtr23.length; i++) arrX.push(value.value[0]);
							  				t23 = {
							  					name: value.value[0],
												y: arrCtr23,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue23,
												img: arrAds23,
												overalldata: overalldata23,
												value: tags23,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			}		

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}					  			
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  		
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,
										// xaxis: { title: 'Salient Object' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23];
									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];
								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],							  				
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								 		// 	}
								 		// 	var image_info = [];
								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  	// 				// do not include the clicked image
								  	// 				// to avoid redundancy
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {

								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i],point.data.value[i][0],point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;

								  	// 				}

								  	// 			}

								  	// 			// display 6 list
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;

								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Scenario';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 ) - (-.5)) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 ) - (-.5)) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ) - (-.5)) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ) - (-.5)) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });
						break;
					case 'layout':

						$scope.reportType = reportType;					

						$scope.title = "Layout";
						document.querySelector(".tag-val .option-selected").innerHTML = "Layout";
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();


							  		var t1=[],t2=[],
							  			arrCtr=[],arrCtr2=[],
							  			arrAds=[],arrAds2=[],
							  			arrHoverValue=[],arrHoverValue2=[];



									emotion_list = [];
							  		var avg = [0,0];
							  		var arrCtr_count = [0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var overalldata=[],overalldata2=[];

							  		var tags=[],tags2=[];


							  		var total_avg = $scope.avgCalculator(res.data.result,'layout');
							  		

							  		angular.forEach(res.data.result, function(value,key) {
							  			if (value.value[2] == "Portrait") {
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);



							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				arrHoverValue.push(["<b>Layout:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[2]);

							  				t1 = {
							  					name: value.value[2],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}


							  			} else if (value.value[2] == "Landscape") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);



							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				arrHoverValue2.push(["<b>Layout:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[2]);

							  				t2 = {
							  					name: value.value[2],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
								  		}

							  			if( key == (res.data.result.length -1) ){
							  				// computeAvg();
							  			}
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,
										// xaxis: { title: 'Layout' },
										yaxis: {
											title: 'UNIQUE CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();



								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
								  	// 		var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];


								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {


								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);


											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											  			

											//   			img_ctr++;
								  	// 				}
								  	// 			}
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}
								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Layout';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  				

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 		// $('.prev-img-info').tooltip(options);

								  	// 	});

							  });
						break;
					case 'weather':

						$scope.reportType = reportType;					

						$scope.title = "Weather";
						document.querySelector(".tag-val .option-selected").innerHTML = "Weather";
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1=[],t2=[],t3=[],t4=[],
							  			arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],
							  			arrAds=[],arrAds2=[],arrAds3=[],arrAds4=[];

							  		var arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[],arrHoverValue4=[];	

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[];


									emotion_list = [];
							  		var avg = [0,0,0,0];
							  		var arrCtr_count = [0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'weather');

							  		var tags=[],tags2=[],tags3=[],tags4=[];
							  		

							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[3] == "Clear") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);



							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				arrHoverValue.push(["<b>Weather:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);




							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[3]);
							  				t1 = {
							  					name: value.value[3],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			} else if (value.value[3] == "Cloudy") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);



							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				arrHoverValue2.push(["<b>Weather:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);




							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[3]);
							  				t2 = {
							  					name: value.value[3],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			// } else if (value.value[3] == "NIL") {


							  			} else if (value.value[3] == "Not Outdoors") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);



							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				arrHoverValue3.push(["<b>Weather:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);




							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[3]);
							  				t3 = {
							  					name: value.value[3],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}			
							  			} else if (value.value[3] == "Sunset") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);



							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				arrHoverValue4.push(["<b>Weather:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);




							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[3]);
							  				t4 = {
							  					name: value.value[3],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}


											if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}							  			
								  		}

								  		if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,


										// xaxis: { title: 'Weather' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4];


									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									



								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  	// 				// do not include the clicked image
								  	// 				// to avoid redundancy
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {

								  			
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;
								  	// 				}

								  	// 			}

								  	// 			// display 6 list
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Weather';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });
						break;
					case 'gender':

						$scope.reportType = reportType;

						$scope.title = "Gender";
						document.querySelector(".tag-val .option-selected").innerHTML = "Gender";
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  	

							  		var t1=[],t2=[],t3=[],t4=[],
							  			arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],
							  			arrAds=[],arrAds2=[],arrAds3=[],arrAds4=[],
							  			arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[],arrHoverValue4=[];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[];


									emotion_list = [];
							  		var avg = [0,0,0,0];
							  		var arrCtr_count = [0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'gender');

							  		var tags=[],tags2=[],tags3=[],tags4=[];
							  		
							  		
							  		angular.forEach(res.data.result, function(value,key) {
							  			
							  			if (value.value[0] == "Both") {


							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

											arrHoverValue.push(["<b>Gender:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[0]);
							  				t1 = {
							  					name: value.value[0],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			// } else if (value.value[0] == "F") {


							  			} else if (value.value[0] == "Female") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

											arrHoverValue2.push(["<b>Gender:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[0]);

							  				t2 = {
							  					name: value.value[0],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			// } else if (value.value[0] == "M") {


							  			} else if (value.value[0] == "Male") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

											arrHoverValue3.push(["<b>Gender:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[0]);
							  				t3 = {
							  					name: value.value[0],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}




											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}				
							  			// } else if (value.value[0] == "NIL") {


							  			} else if (value.value[0] == "N/A") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

											arrHoverValue4.push(["<b>Gender:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[0]);
							  				t4 = {
							  					name: value.value[0],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}


											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}							  			
								  		}

								  		if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}

							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,

										// xaxis: { title: 'Gender' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });


								  	// 		var content = {

								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];


								  	// 		var img_ctr = 0;

								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;
								  	// 				}
								  	// 			}
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Gender';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 		// $('.prev-img-info').tooltip(options);

								  	// 	});

							  });
						break;
					case 'activities':

						$scope.reportType = reportType;					
						$scope.title = "Activities";
						document.querySelector(".tag-val .option-selected").innerHTML = "Activities";
						
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1=[],t2=[],t3=[],t4=[],t5=[],t6=[],t7=[],t8=[],t9=[],t10=[],
										t11=[],t12=[],t13=[],t14=[],t15=[],t16=[],t17=[],t18=[],t19=[],t20=[],
										t21=[],t22=[],t23=[],t24=[],t25=[],t26=[],t27=[],t28=[],t29=[],t30=[],
										t31=[],t32=[];

							  		var arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],arrCtr5=[],
							  			arrCtr6=[],arrCtr7=[],arrCtr8=[],arrCtr9=[],arrCtr10=[],
										arrCtr11=[],arrCtr12=[],arrCtr13=[],arrCtr14=[],arrCtr15=[],
										arrCtr16=[],arrCtr17=[],arrCtr18=[],arrCtr19=[],arrCtr20=[],
										arrCtr21=[],arrCtr22=[],arrCtr23=[],arrCtr24=[],arrCtr25=[],
										arrCtr26=[],arrCtr27=[],arrCtr28=[],arrCtr29=[],arrCtr30=[],
										arrCtr31=[],arrCtr32=[];

							  		var arrAds=[],arrAds2=[],arrAds3=[],arrAds4=[],arrAds5=[],
							  			arrAds6=[],arrAds7=[],arrAds8=[],arrAds9=[],arrAds10=[],
								  		arrAds11=[],arrAds12=[],arrAds13=[],arrAds14=[],arrAds15=[],
								  		arrAds16=[],arrAds17=[],arrAds18=[],arrAds19=[],arrAds20=[],
								  		arrAds21=[],arrAds22=[],arrAds23=[],arrAds24=[],arrAds25=[],
								  		arrAds26=[],arrAds27=[],arrAds28=[],arrAds29=[],arrAds30=[],
								  		arrAds31=[],arrAds32=[];

									var arrHoverValue=[],arrHoverValue2=[],arrHoverValue3=[],arrHoverValue4=[],arrHoverValue5=[],
										arrHoverValue6=[],arrHoverValue7=[],arrHoverValue8=[],arrHoverValue9=[],arrHoverValue10=[],
										arrHoverValue11=[],arrHoverValue12=[],arrHoverValue13=[],arrHoverValue14=[],arrHoverValue15=[],
										arrHoverValue16=[],arrHoverValue17=[],arrHoverValue18=[],arrHoverValue19=[],arrHoverValue20=[],
										arrHoverValue21=[],arrHoverValue22=[],arrHoverValue23=[],arrHoverValue24=[],arrHoverValue25=[],
										arrHoverValue26=[],arrHoverValue27=[],arrHoverValue28=[],arrHoverValue29=[],arrHoverValue30=[],
										arrHoverValue31=[],arrHoverValue32=[];
									
									var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[],overalldata15=[]
							  			,overalldata16=[],overalldata17=[],overalldata18=[],overalldata19=[],overalldata20=[]
							  			,overalldata21=[],overalldata22=[],overalldata23=[],overalldata24=[],overalldata25=[]
							  			,overalldata26=[],overalldata27=[],overalldata28=[],overalldata29=[],overalldata30=[]
							  			,overalldata31=[],overalldata32=[];

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[],tags15=[]
							  			,tags16=[],tags17=[],tags18=[],tags19=[],tags20=[]
							  			,tags21=[],tags22=[],tags23=[],tags24=[],tags25=[]
							  			,tags26=[],tags27=[],tags28=[],tags29=[],tags30=[]
							  			,tags31=[],tags32=[];	


									emotion_list = [];
									var avg = [0,0,0,0,0,0,0,0,0,0,
												0,0,0,0,0,0,0,0,0,0,
												0,0,0,0,0,0,0,0,0,0,
												0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,
							  							0,0,0,0,0,0,0,0,0,0,
							  							0,0,0,0,0,0,0,0,0,0,
							  							0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'activities');
							  		

							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[1] == "Arranging flowers") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

											arrHoverValue.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[1]);
							  				t1 = {
							  					name: value.value[1],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Blowing coloured paper") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

											arrHoverValue2.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[1]);

							  				t2 = {
							  					name: value.value[1],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Body painting") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

											arrHoverValue3.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[1]);
							  				t3 = {
							  					name: value.value[1],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}				
							  			} else if (value.value[1] == "Cheering") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

											arrHoverValue4.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[1]);
							  				t4 = {
							  					name: value.value[1],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}




											if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}			
							  			} else if (value.value[1] == "Dancing") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);


							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

											arrHoverValue5.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[1]);
							  				t5 = {
							  					name: value.value[1],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Eating") {

							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);


							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

											arrHoverValue6.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[1]);

							  				t6 = {
							  					name: value.value[1],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Holding a sign") {

							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);


							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

											arrHoverValue8.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[1]);

							  				t8 = {
							  					name: value.value[1],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Jumping") {

							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);


							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

											arrHoverValue9.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[1]);

							  				t9 = {
							  					name: value.value[1],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Laughing") {

							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);


							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

											arrHoverValue10.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[1]);

							  				t10 = {
							  					name: value.value[1],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Making funny face") {

							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);


							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

											arrHoverValue11.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[1]);

							  				t11 = {
							  					name: value.value[1],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Meditating") {

							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);


							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

											arrHoverValue12.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[1]);

							  				t12 = {
							  					name: value.value[1],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "N/A") {

							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);


							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

											arrHoverValue13.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[1]);

							  				t13 = {
							  					name: value.value[1],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Opening a gift") {

							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);


							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

											arrHoverValue14.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[1]);

							  				t14 = {
							  					name: value.value[1],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Partying") {

							  				
							  				
											tags15.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr15.push(value.unique_ctr);
							  				arrAds15.push(value.ad_number);
							  				overalldata15.push(value);


							  				avg[14] += parseFloat(value.unique_ctr);
							  				arrCtr_count[14]++;

											arrHoverValue15.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr15.length; i++) arrX.push(value.value[1]);

							  				t15 = {
							  					name: value.value[1],
												y: arrCtr15,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue15,
												img: arrAds15,
												overalldata: overalldata15,
												value: tags15,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Playing Sports") {

							  				
							  				
											tags16.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr16.push(value.unique_ctr);
							  				arrAds16.push(value.ad_number);
							  				overalldata16.push(value);


							  				avg[15] += parseFloat(value.unique_ctr);
							  				arrCtr_count[15]++;

											arrHoverValue16.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr16.length; i++) arrX.push(value.value[1]);

							  				t16 = {
							  					name: value.value[1],
												y: arrCtr16,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue16,
												img: arrAds16,
												overalldata: overalldata16,
												value: tags16,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Playing with a dog") {

							  				
							  				
											tags17.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr17.push(value.unique_ctr);
							  				arrAds17.push(value.ad_number);
							  				overalldata17.push(value);


							  				avg[16] += parseFloat(value.unique_ctr);
							  				arrCtr_count[16]++;

											arrHoverValue17.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr17.length; i++) arrX.push(value.value[1]);

							  				t17 = {
							  					name: value.value[1],
												y: arrCtr17,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue17,
												img: arrAds17,
												overalldata: overalldata17,
												value: tags17,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Playing with cards") {

							  				
							  				
											tags18.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr18.push(value.unique_ctr);
							  				arrAds18.push(value.ad_number);
							  				overalldata18.push(value);


							  				avg[17] += parseFloat(value.unique_ctr);
							  				arrCtr_count[17]++;

											arrHoverValue18.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr18.length; i++) arrX.push(value.value[1]);

							  				t18 = {
							  					name: value.value[1],
												y: arrCtr18,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue18,
												img: arrAds18,
												overalldata: overalldata18,
												value: tags18,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Playing with tablets") {

											tags19.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr19.push(value.unique_ctr);
							  				arrAds19.push(value.ad_number);
							  				overalldata19.push(value);


							  				avg[18] += parseFloat(value.unique_ctr);
							  				arrCtr_count[18]++;

											arrHoverValue19.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr19.length; i++) arrX.push(value.value[1]);

							  				t19 = {
							  					name: value.value[1],
												y: arrCtr19,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue19,
												img: arrAds19,
												overalldata: overalldata19,
												value: tags19,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Resting") {

											tags20.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr20.push(value.unique_ctr);
							  				arrAds20.push(value.ad_number);
							  				overalldata20.push(value);


							  				avg[19] += parseFloat(value.unique_ctr);
							  				arrCtr_count[19]++;

											arrHoverValue20.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr20.length; i++) arrX.push(value.value[1]);

							  				t20 = {
							  					name: value.value[1],
												y: arrCtr20,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue20,
												img: arrAds20,
												overalldata: overalldata20,
												value: tags20,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Riding a vehicle") {

							  				
							  				
											tags21.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr21.push(value.unique_ctr);
							  				arrAds21.push(value.ad_number);
							  				overalldata21.push(value);


							  				avg[20] += parseFloat(value.unique_ctr);
							  				arrCtr_count[20]++;

											arrHoverValue21.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr21.length; i++) arrX.push(value.value[1]);

							  				t21 = {
							  					name: value.value[1],
												y: arrCtr21,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue21,
												img: arrAds21,
												overalldata: overalldata21,
												value: tags21,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Shopping") {

							  				
											tags22.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr22.push(value.unique_ctr);
							  				arrAds22.push(value.ad_number);
							  				overalldata22.push(value);


							  				avg[21] += parseFloat(value.unique_ctr);
							  				arrCtr_count[21]++;

											arrHoverValue22.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr22.length; i++) arrX.push(value.value[1]);

							  				t22 = {
							  					name: value.value[1],
												y: arrCtr22,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue22,
												img: arrAds22,
												overalldata: overalldata22,
												value: tags22,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Standing") {
			
											tags23.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr23.push(value.unique_ctr);
							  				arrAds23.push(value.ad_number);
							  				overalldata23.push(value);


							  				avg[22] += parseFloat(value.unique_ctr);
							  				arrCtr_count[22]++;

											arrHoverValue23.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr23.length; i++) arrX.push(value.value[1]);

							  				t23 = {
							  					name: value.value[1],
												y: arrCtr23,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue23,
												img: arrAds23,
												overalldata: overalldata23,
												value: tags23,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Talking") {

							  				
											tags24.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr24.push(value.unique_ctr);
							  				arrAds24.push(value.ad_number);
							  				overalldata24.push(value);


							  				avg[23] += parseFloat(value.unique_ctr);
							  				arrCtr_count[23]++;

											arrHoverValue24.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr24.length; i++) arrX.push(value.value[1]);

							  				t24 = {
							  					name: value.value[1],
												y: arrCtr24,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue24,
												img: arrAds24,
												overalldata: overalldata24,
												value: tags24,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Thinking") {

											tags25.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr25.push(value.unique_ctr);
							  				arrAds25.push(value.ad_number);
							  				overalldata25.push(value);


							  				avg[24] += parseFloat(value.unique_ctr);
							  				arrCtr_count[24]++;

											arrHoverValue25.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr25.length; i++) arrX.push(value.value[1]);

							  				t25 = {
							  					name: value.value[1],
												y: arrCtr25,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue25,
												img: arrAds25,
												overalldata: overalldata25,
												value: tags25,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Using a phone") {

											tags26.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr26.push(value.unique_ctr);
							  				arrAds26.push(value.ad_number);
							  				overalldata26.push(value);

							  				avg[25] += parseFloat(value.unique_ctr);
							  				arrCtr_count[25]++;

											arrHoverValue26.push("<b>Activities: </b>"+value.unique_ctr+"<br><b>Ads ID: </b>"+value.ads_id+"");
							  				for (var i = 0; i<arrCtr26.length; i++) arrX.push(value.value[1]);
							  				t26 = {

							  					name: value.value[1],
												y: arrCtr26,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue26,
												img: arrAds26,
												overalldata: overalldata26,
												value: tags26,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Waiting") {
							  				
											tags27.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr27.push(value.unique_ctr);
							  				arrAds27.push(value.ad_number);
							  				overalldata27.push(value);

							  				avg[26] += parseFloat(value.unique_ctr);
							  				arrCtr_count[26]++;

											arrHoverValue27.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				
							  				for (var i = 0; i<arrCtr27.length; i++) arrX.push(value.value[1]);
							  				t27 = {

							  					name: value.value[1],
												y: arrCtr27,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue27,
												img: arrAds27,
												overalldata: overalldata27,
												value: tags27,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Walking") {

											tags28.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr28.push(value.unique_ctr);
							  				arrAds28.push(value.ad_number);
							  				overalldata28.push(value);

							  				avg[27] += parseFloat(value.unique_ctr);
							  				arrCtr_count[27]++;

											arrHoverValue28.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				
							  				for (var i = 0; i<arrCtr28.length; i++) arrX.push(value.value[1]);
							  				t28 = {

							  					name: value.value[1],
												y: arrCtr28,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue28,
												img: arrAds28,
												overalldata: overalldata28,
												value: tags28,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Watching") {
			
											tags29.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr29.push(value.unique_ctr);
							  				arrAds29.push(value.ad_number);
							  				overalldata29.push(value);

							  				avg[28] += parseFloat(value.unique_ctr);
							  				arrCtr_count[28]++;

											arrHoverValue29.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				
							  				for (var i = 0; i<arrCtr29.length; i++) arrX.push(value.value[1]);
							  				t29 = {

							  					name: value.value[1],
												y: arrCtr29,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue29,
												img: arrAds29,
												overalldata: overalldata29,
												value: tags29,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Working") {

											tags30.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr30.push(value.unique_ctr);
							  				arrAds30.push(value.ad_number);
							  				overalldata30.push(value);

							  				avg[29] += parseFloat(value.unique_ctr);
							  				arrCtr_count[29]++;

											arrHoverValue30.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  			
							  				for (var i = 0; i<arrCtr30.length; i++) arrX.push(value.value[1]);
							  				t30 = {

							  					name: value.value[1],
												y: arrCtr30,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue30,
												img: arrAds30,
												overalldata: overalldata30,
												value: tags30,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Wrapping") {

											tags31.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr31.push(value.unique_ctr);
							  				arrAds31.push(value.ad_number);
							  				overalldata31.push(value);

							  				avg[30] += parseFloat(value.unique_ctr);
							  				arrCtr_count[30]++;

											arrHoverValue31.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				
							  				for (var i = 0; i<arrCtr31.length; i++) arrX.push(value.value[1]);
							  				t31 = {

							  					name: value.value[1],
												y: arrCtr31,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue31,
												img: arrAds31,
												overalldata: overalldata31,
												value: tags31,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			} else if (value.value[1] == "Writing") {

							  				
							  				
											tags32.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr32.push(value.unique_ctr);
							  				arrAds32.push(value.ad_number);
							  				overalldata32.push(value);

							  				avg[31] += parseFloat(value.unique_ctr);
							  				arrCtr_count[31]++;

											arrHoverValue32.push(["<b>Activities:</b> " + value.value[1] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				
							  				for (var i = 0; i<arrCtr32.length; i++) arrX.push(value.value[1]);
							  				t32 = {

							  					name: value.value[1],
												y: arrCtr32,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue32,
												img: arrAds32,
												overalldata: overalldata32,
												value: tags32,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[1], emotion_list ) == -1){
								  				emotion_list.push(value.value[1])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}

							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );

						  				
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [
													t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,
													t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,
													t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,
													t31,t32
												  ];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,
										// xaxis: { title: 'Activities' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [
													t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,
													t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,
													t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,
													t31,t32
												  ];

									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];
								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									
								  	$("#my-graph").css('height' , 450);
									$(".svg-container").css('height' , 500);
									$(".main-svg").css('height' , 470);
								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;
								  	// 				}
								  	// 			}
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Activities';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });					
						break;
					case 'age':

						$scope.reportType = reportType;					
						$scope.title = "Age";
						document.querySelector(".tag-val .option-selected").innerHTML = "Age";

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1=[],t2=[],t3=[],t4=[],t5=[],
										t6=[],t7=[],t8=[],t9=[],t10=[],
										t11=[],t12=[],t13=[],t14=[];

							  		var arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],arrCtr5=[],
							  			arrCtr6=[],arrCtr7=[],arrCtr8=[],arrCtr9=[],arrCtr10=[],
										arrCtr11=[],arrCtr12=[],arrCtr13=[],arrCtr14=[];

							  		var arrAds = [];
							  		var arrAds2 = [];
							  		var arrAds3 = [];
							  		var arrAds4 = [];
							  		var arrAds5 = [];
							  		var arrAds6 = [];
							  		var arrAds7 = [];
							  		var arrAds8 = [];
							  		var arrAds9 = [];
							  		var arrAds10 = [];
							  		var arrAds11 = [];
							  		var arrAds12 = [];
							  		var arrAds13 = [];
							  		var arrAds14 = [];

							  		var arrHoverValue = [];
							  		var arrHoverValue2 = [];
							  		var arrHoverValue3 = [];
							  		var arrHoverValue4 = [];
							  		var arrHoverValue5 = [];
							  		var arrHoverValue6 = [];
							  		var arrHoverValue7 = [];
							  		var arrHoverValue8 = [];
							  		var arrHoverValue9 = [];
							  		var arrHoverValue10 = [];
							  		var arrHoverValue11 = [];
							  		var arrHoverValue12 = [];
							  		var arrHoverValue13 = [];
							  		var arrHoverValue14 = [];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[];

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'age');
							  		

							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[2] == "Elderly") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				arrHoverValue.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[2]);
							  				t1 = {
							  					name: value.value[2],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "Kid") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				arrHoverValue2.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[2]);

							  				t2 = {
							  					name: value.value[2],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "Kid and Young Adult") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				arrHoverValue3.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[2]);
							  				t3 = {
							  					name: value.value[2],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}

											if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}	
							  			} else if (value.value[2] == "Mature Adult") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				arrHoverValue4.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[2]);
							  				t4 = {
							  					name: value.value[2],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}
											if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}		
							  			} else if (value.value[2] == "N/A") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);


							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

							  				arrHoverValue5.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[2]);
							  				t5 = {
							  					name: value.value[2],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "Teen and Young Adult") {

							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);


							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				arrHoverValue6.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[2]);

							  				t6 = {
							  					name: value.value[2],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}

							  			} else if (value.value[2] == "Young Adult") {

							  				
							  				
											tags7.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);
							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);


							  				avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

							  				arrHoverValue7.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[2]);

							  				t7 = {
							  					name: value.value[2],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}

							  			} else if (value.value[2] == "Young Adult, Mature Adult and Elderly") {

							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);


							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

							  				arrHoverValue8.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[2]);

							  				t8 = {
							  					name: value.value[2],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "40s") {

							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);


							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

							  				arrHoverValue9.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[2]);

							  				t9 = {
							  					name: value.value[2],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "50s") {

							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);


							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

							  				arrHoverValue10.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[2]);

							  				t10 = {
							  					name: value.value[2],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "5s-15s") {

							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);


							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

							  				arrHoverValue11.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[2]);

							  				t11 = {
							  					name: value.value[2],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "5s-35s") {

							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);


							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

							  				arrHoverValue12.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[2]);

							  				t12 = {
							  					name: value.value[2],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "60s") {

							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);


							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

							  				arrHoverValue13.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[2]);

							  				t13 = {
							  					name: value.value[2],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			} else if (value.value[2] == "NIL") {

							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);


							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

							  				arrHoverValue14.push(["<b>Age:</b> " + value.value[2] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[2]);

							  				t14 = {
							  					name: value.value[2],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[2], emotion_list ) == -1){
								  				emotion_list.push(value.value[2])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();


							  			}

							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,
										// xaxis: { title: 'Age' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									
									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8];

									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;

								  	// 				}

								  	// 			}

								  	// 			// display 6 list
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;								  			
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Age';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });
						break;
					case 'clothing':

						$scope.reportType = reportType;

						$scope.title = "Clothing";
						document.querySelector(".tag-val .option-selected").innerHTML = "Clothing";
						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1=[],t2=[],t3=[],t4=[],t5=[],t6=[];

							  		var arrCtr=[],arrCtr2=[],arrCtr3=[],arrCtr4=[],arrCtr5=[],arrCtr6=[];

							  		var arrAds = [];
							  		var arrAds2 = [];
							  		var arrAds3 = [];
							  		var arrAds4 = [];
							  		var arrAds5 = [];
							  		var arrAds6 = [];

							  		var arrHoverValue = [];
							  		var arrHoverValue2 = [];
							  		var arrHoverValue3 = [];
							  		var arrHoverValue4 = [];
							  		var arrHoverValue5 = [];
							  		var arrHoverValue6 = [];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[];

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];

							  		var total_avg = $scope.avgCalculator(res.data.result,'clothing');
							  		

							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[3] == "Casual") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

											arrHoverValue.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[3]);
							  				t1 = {
							  					name: value.value[3],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			} else if (value.value[3] == "Formal") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

											arrHoverValue2.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[3]);
							  				t2 = {
							  					name: value.value[3],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			} else if (value.value[3] == "Informal") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				arrHoverValue3.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[3]);
							  				t3 = {
							  					name: value.value[3],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}




											if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}			
							  			} else if (value.value[3] == "N/A") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				arrHoverValue4.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[3]);
							  				t4 = {
							  					name: value.value[3],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}		
							  			} else if (value.value[3] == "Smart Casual") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);


							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

							  				arrHoverValue5.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[3]);
							  				t5 = {
							  					name: value.value[3],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			} else if (value.value[3] == "Sportswear") {

							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);


							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				arrHoverValue6.push(["<b>Clothing:</b> " + value.value[3] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[3]);
							  				t6 = {
							  					name: value.value[3],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[3], emotion_list ) == -1){
								  				emotion_list.push(value.value[3])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}

							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,

										// xaxis: { title: 'Clothing' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4,t5,t6];
									// reportService.graphData( dataObj, layout );
									// layout = {};
									// dataObj = [];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  	// 				// do not include the clicked image
								  	// 				// to avoid redundancy
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  			
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;

								  	// 				}

								  	// 			}

								  	// 			// display 6 list
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Clothing';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });
						break;
					case 'hair colour':

						$scope.reportType = reportType;

						$scope.title = "Hair Colour";
						document.querySelector(".tag-val .option-selected").innerHTML = "Hair Colour";

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1=[],t2=[],t3=[],t4=[],t5=[],t6=[],t7=[],t8=[],t9=[],t10=[],
										t11=[],t12=[],t13=[],t14=[],t15=[],t16=[],t17=[],t18=[],t19=[],t20=[],
										t21=[];

							  		var arrCtr = [], arrCtr2 = [], arrCtr3 = [], arrCtr4 = [], arrCtr5 = [], 
							  			arrCtr6 = [], arrCtr7 = [], arrCtr8 = [], arrCtr9 = [], arrCtr10 = [],
							  			arrCtr11 = [], arrCtr12 = [], arrCtr13 = [], arrCtr14 = [], arrCtr15 = [], 
							  			arrCtr16 = [], arrCtr17 = [], arrCtr18 = [], arrCtr19 = [], arrCtr20 = [], 
							  			arrCtr21 = [];

							  		var arrAds = [];
							  		var arrAds2 = [];
							  		var arrAds3 = [];
							  		var arrAds4 = [];
							  		var arrAds5 = [];
							  		var arrAds6 = [];
							  		var arrAds7 = [];
							  		var arrAds8 = [];
							  		var arrAds9 = [];
							  		var arrAds10 = [];
							  		var arrAds11 = [];
							  		var arrAds12 = [];
							  		var arrAds13 = [];
							  		var arrAds14 = [];
							  		var arrAds15 = [];
							  		var arrAds16 = [];
							  		var arrAds17 = [];
							  		var arrAds18 = [];
							  		var arrAds19 = [];
							  		var arrAds20 = [];
							  		var arrAds21 = [];

							  		var arrHoverValue = [];
							  		var arrHoverValue2 = [];
							  		var arrHoverValue3 = [];
							  		var arrHoverValue4 = [];
							  		var arrHoverValue5 = [];
							  		var arrHoverValue6 = [];
							  		var arrHoverValue7 = [];
							  		var arrHoverValue8 = [];
							  		var arrHoverValue9 = [];
							  		var arrHoverValue10 = [];
							  		var arrHoverValue11 = [];
							  		var arrHoverValue12 = [];
							  		var arrHoverValue13 = [];
							  		var arrHoverValue14 = [];
							  		var arrHoverValue15 = [];
							  		var arrHoverValue16 = [];
							  		var arrHoverValue17 = [];
							  		var arrHoverValue18 = [];
							  		var arrHoverValue19 = [];
							  		var arrHoverValue20 = [];
							  		var arrHoverValue21 = [];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[],overalldata15=[]
							  			,overalldata16=[],overalldata17=[],overalldata18=[],overalldata19=[],overalldata20=[]
							  			,overalldata21=[];


							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[],tags15=[]
							  			,tags16=[],tags17=[],tags18=[],tags19=[],tags20=[],tags21=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];
							  		

							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[4] == "Black") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];

							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);
							  				arrHoverValue.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[4]);
							  				t1 = {
							  					name: value.value[4],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Blonde") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];

							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);
							  				arrHoverValue2.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[4]);
							  				t2 = {
							  					name: value.value[4],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};
							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Brown") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);
							  				arrHoverValue3.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;

							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[4]);

							  				t3 = {
							  					name: value.value[4],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}

											if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}			
							  			} else if (value.value[4] == "Black, Brown and Golden") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);
							  				arrHoverValue4.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[4]);

							  				t4 = {
							  					name: value.value[4],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}				
							  			} else if (value.value[4] == "Blonde") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);
							  				arrHoverValue5.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[4]);

							  				t5 = {
							  					name: value.value[4],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Blonde and red") {
							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);

							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				arrHoverValue6.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[4]);

							  				t6 = {
							  					name: value.value[4],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Blonde, brown") {
							  				
							  				
											tags7.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);
							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);

							  				avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

							  				arrHoverValue7.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[4]);

							  				t7 = {
							  					name: value.value[4],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Brown") {
							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);

							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

							  				arrHoverValue8.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[4]);

							  				t8 = {
							  					name: value.value[4],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Dark Brown") {
							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);

							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

							  				arrHoverValue9.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[4]);

							  				t9 = {
							  					name: value.value[4],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Gold") {
							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);

							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

							  				arrHoverValue10.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[4]);

							  				t10 = {
							  					name: value.value[4],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Golden") {
							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);

							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

							  				arrHoverValue11.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[4]);

							  				t11 = {
							  					name: value.value[4],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Golden Brown") {
							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);

							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

							  				arrHoverValue12.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[4]);

							  				t12 = {
							  					name: value.value[4],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Golden Brown and Grey") {
							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);

							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

							  				arrHoverValue13.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[4]);

							  				t13 = {
							  					name: value.value[4],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Light Brown") {
							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);

							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

							  				arrHoverValue14.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[4]);

							  				t14 = {
							  					name: value.value[4],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Mixed") {
							  				
							  				
											tags15.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr15.push(value.unique_ctr);
							  				arrAds15.push(value.ad_number);
							  				overalldata15.push(value);

							  				avg[14] += parseFloat(value.unique_ctr);
							  				arrCtr_count[14]++;

							  				arrHoverValue15.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr15.length; i++) arrX.push(value.value[4]);

							  				t15 = {
							  					name: value.value[4],
												y: arrCtr15,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue15,
												img: arrAds15,
												overalldata: overalldata15,
												value: tags15,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Nil") {
							  				
							  				
											tags16.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr16.push(value.unique_ctr);
							  				arrAds16.push(value.ad_number);
							  				overalldata16.push(value);

							  				avg[15] += parseFloat(value.unique_ctr);
							  				arrCtr_count[15]++;

							  				arrHoverValue16.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr16.length; i++) arrX.push(value.value[4]);

							  				t16 = {
							  					name: value.value[4],
												y: arrCtr16,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue16,
												img: arrAds16,
												overalldata: overalldata16,
												value: tags16,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Orange") {
							  				
							  				
											tags17.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr17.push(value.unique_ctr);
							  				arrAds17.push(value.ad_number);
							  				overalldata17.push(value);

							  				avg[16] += parseFloat(value.unique_ctr);
							  				arrCtr_count[16]++;

							  				arrHoverValue17.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr17.length; i++) arrX.push(value.value[4]);

							  				t17 = {
							  					name: value.value[4],
												y: arrCtr17,
												x: arrX,
												mode: 'markers',
												type: 'scatter',

												text: arrHoverValue17,
												img: arrAds17,
												overalldata: overalldata17,
												value: tags17,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Reddish Brown") {
							  				
											tags18.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr18.push(value.unique_ctr);
							  				arrAds18.push(value.ad_number);
							  				overalldata18.push(value);

							  				avg[17] += parseFloat(value.unique_ctr);
							  				arrCtr_count[17]++;

							  				arrHoverValue18.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr18.length; i++) arrX.push(value.value[4]);

							  				t18 = {
							  					name: value.value[4],
												y: arrCtr18,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue18,
												img: arrAds18,
												overalldata: overalldata18,
												value: tags18,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "N/A") {
							  				
											tags19.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr19.push(value.unique_ctr);
							  				arrAds19.push(value.ad_number);
							  				overalldata19.push(value);

							  				avg[18] += parseFloat(value.unique_ctr);
							  				arrCtr_count[18]++;

							  				arrHoverValue19.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr19.length; i++) arrX.push(value.value[4]);

							  				t19 = {
							  					name: value.value[4],
												y: arrCtr19,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue19,
												img: arrAds19,
												overalldata: overalldata19,
												value: tags19,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "White") {
							  				
											tags20.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr20.push(value.unique_ctr);
							  				arrAds20.push(value.ad_number);
							  				overalldata20.push(value);

							  				avg[19] += parseFloat(value.unique_ctr);
							  				arrCtr_count[19]++;

							  				arrHoverValue20.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr20.length; i++) arrX.push(value.value[4]);

							  				t20 = {
							  					name: value.value[4],
												y: arrCtr20,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue20,
												img: arrAds20,
												overalldata: overalldata20,
												value: tags20,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			} else if (value.value[4] == "Grey") {
							  				
											tags21.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr21.push(value.unique_ctr);
							  				arrAds21.push(value.ad_number);
							  				overalldata21.push(value);

							  				avg[20] += parseFloat(value.unique_ctr);
							  				arrCtr_count[20]++;

							  				arrHoverValue21.push(["<b>Hair Colour:</b> " + value.value[4] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr21.length; i++) arrX.push(value.value[4]);

							  				t21 = {
							  					name: value.value[4],
												y: arrCtr21,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue21,
												img: arrAds21,
												overalldata: overalldata21,
												value: tags21,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[4], emotion_list ) == -1){
								  				emotion_list.push(value.value[4])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();
							  			}


							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	
									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}									
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}									
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  			
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;

								  	// 				}

								  	// 			}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Hair Colour';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 		// $('.prev-img-info').tooltip(options);

								  	// 	});

							  });
						break;
					case 'dominant colour':

						$scope.reportType = reportType;					

						$scope.title = "Dominant Colour";
						document.querySelector(".tag-val .option-selected").innerHTML = "Dominant Colour";

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1 = [], t2 = [], t3 = [], t4 = [], t5 = [],
							  			t6 = [], t7 = [], t8 = [], t9 = [], t10 = [],
							  			t11 = [], t12 = [], t13 = [], t14 = [], t15 = [];

							  		var arrCtr = [], arrCtr2 = [], arrCtr3 = [], arrCtr4 = [], arrCtr5 = [], arrCtr6 = [],
							  			arrCtr7 = [], arrCtr8 = [], arrCtr9 = [], arrCtr10 = [],
							  			arrCtr11 = [], arrCtr12 = [], arrCtr13 = [], arrCtr14 = [], arrCtr15 = [];

							  		var arrAds = [];
							  		var arrAds2 = [];
							  		var arrAds3 = [];
							  		var arrAds4 = [];
							  		var arrAds5 = [];
							  		var arrAds6 = [];
							  		var arrAds7 = [];
							  		var arrAds8 = [];
							  		var arrAds9 = [];
							  		var arrAds10 = [];
							  		var arrAds11 = [];
							  		var arrAds12 = [];
							  		var arrAds13 = [];
							  		var arrAds14 = [];
							  		var arrAds15 = [];

							  		var arrHoverValue = [];
							  		var arrHoverValue2 = [];
							  		var arrHoverValue3 = [];
							  		var arrHoverValue4 = [];
							  		var arrHoverValue5 = [];
							  		var arrHoverValue6 = [];
							  		var arrHoverValue7 = [];
							  		var arrHoverValue8 = [];
							  		var arrHoverValue9 = [];
							  		var arrHoverValue10 = [];
							  		var arrHoverValue11 = [];
							  		var arrHoverValue12 = [];
							  		var arrHoverValue13 = [];
							  		var arrHoverValue14 = [];
							  		var arrHoverValue15 = [];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[],overalldata15=[]
							  			,overalldata16=[];

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[],tags15=[]
							  			,tags16=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];	
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];
							  		


							  		angular.forEach(res.data.result, function(value,key) {


							  			if (value.value[0] == "Beige") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);
							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);

							  				arrHoverValue.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;



							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[0]);

							  				t1 = {
							  					name: value.value[0],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Black") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr)
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);

							  				arrHoverValue2.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;



							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[0]);

							  				t2 = {
							  					name: value.value[0],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Blue") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr);
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);

							  				arrHoverValue3.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;



							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[0]);

							  				t3 = {
							  					name: value.value[0],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}				
							  			} else if (value.value[0] == "Brown") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr)
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);

							  				arrHoverValue4.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;



							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[0]);
							  				t4 = {
							  					name: value.value[0],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				}



											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}			
							  			} else if (value.value[0] == "Green") {

							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);
							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);

							  				arrHoverValue5.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;



							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[0]);

							  				t5 = {
							  					name: value.value[0],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Grey") {
							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);
							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);


							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

							  				arrHoverValue6.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[0]);

							  				t6 = {
							  					name: value.value[0],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Light Brown") {

							  				
							  				
											tags7.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);
							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);


							  				avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

							  				arrHoverValue7.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[0]);

							  				t7 = {
							  					name: value.value[0],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Multicolour") {
							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);
							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);



							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

							  				arrHoverValue8.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[0]);

							  				t8 = {
							  					name: value.value[0],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Off-White") {

							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);
							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);


							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

							  				arrHoverValue9.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[0]);

							  				t9 = {
							  					name: value.value[0],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Orange") {
							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);
							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);


							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

							  				arrHoverValue10.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[0]);

							  				t10 = {
							  					name: value.value[0],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Pink") {
							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);
							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);


							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;


							  				arrHoverValue11.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[0]);

							  				t11 = {
							  					name: value.value[0],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Purple") {
							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);
							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);


							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;


							  				arrHoverValue12.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[0]);

							  				t12 = {
							  					name: value.value[0],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Yellow") {

							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);
							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);


							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

							  				arrHoverValue13.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[0]);

							  				t13 = {
							  					name: value.value[0],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "White") {
							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);

							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

							  				arrHoverValue14.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[0]);

							  				t14 = {
							  					name: value.value[0],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Yellow") {
							  				
							  				
											tags15.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr15.push(value.unique_ctr);
							  				arrAds15.push(value.ad_number);
							  				overalldata15.push(value);

							  				avg[14] += parseFloat(value.unique_ctr);
							  				arrCtr_count[14]++;

							  				arrHoverValue15.push(["<b>Color:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr15.length; i++) arrX.push(value.value[0]);

							  				t15 = {
							  					name: value.value[0],
												y: arrCtr15,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue15,
												img: arrAds15,
												overalldata: overalldata15,
												value: tags15,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();


							  			}
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2); 
						  				}
							  			
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});						
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,

										// xaxis: { title: 'Dominant Colour' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};

									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13];

								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);

								  	thisPlot.on('plotly_click', function(data){
								  		 var point = data.points[0],
									   		ulist = document.querySelector("#myModal ul.related-gallery");

									    $('#myModal').modal('show');

									    // when modal is closed, empty list
								  			$('#myModal').on('hidden.bs.modal', function (e) {
									  			$("#myModal").find('ul.related-gallery').empty();									  			
											});

									    $('.selected-cat').text(point.x);
									    console.log(point);
									    for(var x = 0; x < point.data.allData.length; x++){
									    	if( point.data.allData[x].name == point.x ){
									    		for(var y = 0; y < point.data.allData[x].img.length; y++){
									    			var li = document.createElement('li');
										    			li.innerHTML = '<a class="thumbnail" ><img class="img-responsive" src="telstra_images/'+point.data.allData[x].img[y]+'.jpg" alt="" /></a>'
										    						+ '<div class="bot">'
																	+		'<div><span class="ctr" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;">'+point.data.allData[x].y[y]+'%</span></div>'
																	+		'<i class="fa fa-long-arrow-down" style="left:'+( parseFloat(point.data.allData[x].y[y]).toFixed(1) * 10)+'%;"></i>'
																	+		'<i class="fa avg-arrow" style="left:4.9%;"></i>'
																	+		'<div class="ctr-bar"></div>'
																	+		'<div><span class="avg" style="left:4.9%;">0.4% (Avg)</span></div>'
																	+		'<br>'
																	+		'<span class="low" style="float:left;"><b>low</b></span>'
																	+		'<span class="high" style="float:right;"><b>high</b></span>'
																	+	'</div>';
												  		ulist.appendChild(li);
												  		
									    		}
									    	}
									    }
									});

								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];


								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  	// 				// do not include the clicked image
								  	// 				// to avoid redundancy
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {

								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);


											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;



								  	// 				}
								  	// 			}

								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Dominant Colour';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });
						break;
					case 'salient object':

						$scope.reportType = reportType;					

						$scope.title = "Salient Object";
						document.querySelector(".tag-val .option-selected").innerHTML = "Focus";

						Report.show(reportType, filter)
							  .then(function (res) {

							  // 		$('#my-graph').show();
									// $('#avePlot').hide();
									// $('#ave-btn').show();
									// $('#bk-btn').hide();

							  		var t1 = [], t2 = [], t3 = [], t4 = [], t5 = [],
							  			t6 = [], t7 = [], t8 = [], t9 = [], t10 = [],
							  			t11 = [], t12 = [], t13 = [], t14 = [], t15 = [];

							  		var arrCtr = [], arrCtr2 = [], arrCtr3 = [], arrCtr4 = [], arrCtr5 = [], 
							  			arrCtr6 = [], arrCtr7 = [], arrCtr8 = [], arrCtr9 = [], arrCtr10 = [],
							  			arrCtr11 = [], arrCtr12 = [], arrCtr13 = [], arrCtr14 = [], arrCtr15 = [];

							  		var arrAds = [];
							  		var arrAds2 = [];
							  		var arrAds3 = [];
							  		var arrAds4 = [];
							  		var arrAds5 = [];
							  		var arrAds6 = [];
							  		var arrAds7 = [];
							  		var arrAds8 = [];
							  		var arrAds9 = [];
							  		var arrAds10 = [];
							  		var arrAds11 = [];
							  		var arrAds12 = [];
							  		var arrAds13 = [];
							  		var arrAds14 = [];
							  		var arrAds15 = [];

							  		var arrHoverValue = [];
							  		var arrHoverValue2 = [];
							  		var arrHoverValue3 = [];
							  		var arrHoverValue4 = [];
							  		var arrHoverValue5 = [];
							  		var arrHoverValue6 = [];
							  		var arrHoverValue7 = [];
							  		var arrHoverValue8 = [];
							  		var arrHoverValue9 = [];
							  		var arrHoverValue10 = [];
							  		var arrHoverValue11 = [];
							  		var arrHoverValue12 = [];
							  		var arrHoverValue13 = [];
							  		var arrHoverValue14 = [];
							  		var arrHoverValue15 = [];

							  		var overalldata=[],overalldata2=[],overalldata3=[],overalldata4=[],overalldata5=[],
							  			overalldata6=[],overalldata7=[],overalldata8=[],overalldata9=[],overalldata10=[]
							  			,overalldata11=[],overalldata12=[],overalldata13=[],overalldata14=[],overalldata15=[];

							  		var tags=[],tags2=[],tags3=[],tags4=[],tags5=[],
							  			tags6=[],tags7=[],tags8=[],tags9=[],tags10=[]
							  			,tags11=[],tags12=[],tags13=[],tags14=[],tags15=[];

									emotion_list = [];
							  		var avg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var arrCtr_count = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
							  		var avg_all = 0;
							  		var avg_list = [];
							  		var emo_counter = 0;

							  		var chartData = [];
							  		var xaxis = [];
							  		var yaxis = [];
							  		var barcolor = [];
							  		


							  		angular.forEach(res.data.result, function(value,key) {

							  			if (value.value[0] == "Animated Object") {
							  				
							  				
											tags.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr.push(value.unique_ctr);

											arrHoverValue.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


											avg[0] += parseFloat(value.unique_ctr);
							  				arrCtr_count[0]++;

							  				arrAds.push(value.ad_number);
							  				overalldata.push(value);


							  				for (var i = 0; i<arrCtr.length; i++) arrX.push(value.value[0]);

							  				t1 = {
							  					name: value.value[0],
												y: arrCtr,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue,
												img: arrAds,
												overalldata: overalldata,
												value: tags,
												hoverinfo: 'text'
											
											}



											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Card") {

							  				
							  				
											tags2.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr2.push(value.unique_ctr);
							  				arrAds2.push(value.ad_number);
							  				overalldata2.push(value);
											arrHoverValue2.push(["<b>Focus:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

											avg[1] += parseFloat(value.unique_ctr);
							  				arrCtr_count[1]++;

							  				for (var i = 0; i<arrCtr2.length; i++) arrX.push(value.value[0]);

							  				t2 = {
							  					name: value.value[0],
												y: arrCtr2,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue2,
												img: arrAds2,
												overalldata: overalldata2,
												value: tags2,
												hoverinfo: 'text'
											
											}



											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Dog") {

							  				
							  				
											tags3.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr3.push(value.unique_ctr)
							  				arrAds3.push(value.ad_number);
							  				overalldata3.push(value);


							  				avg[2] += parseFloat(value.unique_ctr);
							  				arrCtr_count[2]++;


							  				arrHoverValue3.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				for (var i = 0; i<arrCtr3.length; i++) arrX.push(value.value[0]);

							  				t3 = {
							  					name: value.value[0],
												y: arrCtr3,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue3,
												img: arrAds3,
												overalldata: overalldata3,
												value: tags3,
												hoverinfo: 'text'
											
							  				}




											if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}		
							  			} else if (value.value[0] == "Face") {

							  				
							  				
											tags4.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr4.push(value.unique_ctr);
							  				arrAds4.push(value.ad_number);
							  				overalldata4.push(value);


							  				avg[3] += parseFloat(value.unique_ctr);
							  				arrCtr_count[3]++;

							  				arrHoverValue4.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				for (var i = 0; i<arrCtr4.length; i++) arrX.push(value.value[0]);

							  				t5 = {
							  					name: value.value[0],
												y: arrCtr4,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue4,
												img: arrAds4,
												overalldata: overalldata4,
												value: tags4,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Feet") {
							  				
							  				
											tags5.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr5.push(value.unique_ctr);

							  				avg[4] += parseFloat(value.unique_ctr);
							  				arrCtr_count[4]++;

											arrHoverValue5.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds5.push(value.ad_number);
							  				overalldata5.push(value);

							  				for (var i = 0; i<arrCtr5.length; i++) arrX.push(value.value[0]);

							  				t5 = {
							  					name: value.value[0],
												y: arrCtr5,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue5,
												img: arrAds5,
												overalldata: overalldata5,
												value: tags5,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Fireworks") {
							  				
							  				
											tags6.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr6.push(value.unique_ctr);

							  				avg[5] += parseFloat(value.unique_ctr);
							  				arrCtr_count[5]++;

											arrHoverValue6.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				arrAds6.push(value.ad_number);
							  				overalldata6.push(value);

							  				for (var i = 0; i<arrCtr6.length; i++) arrX.push(value.value[0]);

							  				t6 = {
							  					name: value.value[0],
												y: arrCtr6,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue6,
												img: arrAds6,
												overalldata: overalldata6,
												value: tags6,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Food") {
							  				
							  				
											tags7.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr7.push(value.unique_ctr);

							  				avg[6] += parseFloat(value.unique_ctr);
							  				arrCtr_count[6]++;

											arrHoverValue7.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);


							  				arrAds7.push(value.ad_number);
							  				overalldata7.push(value);

							  				for (var i = 0; i<arrCtr7.length; i++) arrX.push(value.value[0]);

							  				t7 = {
							  					name: value.value[0],
												y: arrCtr7,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue7,
												img: arrAds7,
												overalldata: overalldata7,
												value: tags7,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Handphone") {
							  				
							  				
											tags8.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr8.push(value.unique_ctr);

							  				avg[7] += parseFloat(value.unique_ctr);
							  				arrCtr_count[7]++;

											arrHoverValue8.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds8.push(value.ad_number);
							  				overalldata8.push(value);

							  				for (var i = 0; i<arrCtr8.length; i++) arrX.push(value.value[0]);

							  				t8 = {
							  					name: value.value[0],
												y: arrCtr8,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue8,
												img: arrAds8,
												overalldata: overalldata8,
												value: tags8,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "House") {
							  				
							  				
											tags9.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr9.push(value.unique_ctr);

							  				avg[8] += parseFloat(value.unique_ctr);
							  				arrCtr_count[8]++;

											arrHoverValue9.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds9.push(value.ad_number);
							  				overalldata9.push(value);

							  				for (var i = 0; i<arrCtr9.length; i++) arrX.push(value.value[0]);

							  				t9 = {
							  					name: value.value[0],
												y: arrCtr9,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue9,
												img: arrAds9,
												overalldata: overalldata9,
												value: tags9,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "NIL") {
							  				
							  				
											tags10.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr10.push(value.unique_ctr);

							  				avg[9] += parseFloat(value.unique_ctr);
							  				arrCtr_count[9]++;

											arrHoverValue10.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds10.push(value.ad_number);
							  				overalldata10.push(value);

							  				for (var i = 0; i<arrCtr10.length; i++) arrX.push(value.value[0]);

							  				t10 = {
							  					name: value.value[0],
												y: arrCtr10,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue10,
												img: arrAds10,
												overalldata: overalldata10,
												value: tags10,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Object") {
							  				
							  				
											tags11.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr11.push(value.unique_ctr);

							  				avg[10] += parseFloat(value.unique_ctr);
							  				arrCtr_count[10]++;

											arrHoverValue11.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds11.push(value.ad_number);
							  				overalldata11.push(value);

							  				for (var i = 0; i<arrCtr11.length; i++) arrX.push(value.value[0]);

							  				t11 = {
							  					name: value.value[0],
												y: arrCtr11,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue11,
												img: arrAds11,
												overalldata: overalldata11,
												value: tags11,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Pay TV kit") {
							  				
							  				
											tags12.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr12.push(value.unique_ctr);

							  				avg[11] += parseFloat(value.unique_ctr);
							  				arrCtr_count[11]++;

											arrHoverValue12.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds12.push(value.ad_number);
							  				overalldata12.push(value);

							  				for (var i = 0; i<arrCtr12.length; i++) arrX.push(value.value[0]);

							  				t12 = {
							  					name: value.value[0],
												y: arrCtr12,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue12,
												img: arrAds12,
												overalldata: overalldata12,
												value: tags12,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "People") {
							  				
							  				
											tags13.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr13.push(value.unique_ctr);

							  				avg[12] += parseFloat(value.unique_ctr);
							  				arrCtr_count[12]++;

											arrHoverValue13.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);

							  				arrAds13.push(value.ad_number);
							  				overalldata13.push(value);

							  				for (var i = 0; i<arrCtr13.length; i++) arrX.push(value.value[0]);

							  				t13 = {
							  					name: value.value[0],
												y: arrCtr13,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue13,
												img: arrAds13,
												overalldata: overalldata13,
												value: tags13,
												hoverinfo: 'text'
											
							  				};



							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Phone") {
							  				
							  				
											tags14.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr14.push(value.unique_ctr);

							  				avg[13] += parseFloat(value.unique_ctr);
							  				arrCtr_count[13]++;

											arrHoverValue14.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				arrAds14.push(value.ad_number);
							  				overalldata14.push(value);
							  				for (var i = 0; i<arrCtr14.length; i++) arrX.push(value.value[0]);

							  				t14 = {
							  					name: value.value[0],
												y: arrCtr14,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue14,
												img: arrAds14,
												overalldata: overalldata14,
												value: tags14,
												hoverinfo: 'text'
											
							  				};


							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			} else if (value.value[0] == "Tablet") {
							  				
							  				
											tags15.push([value.type,value.ad_id]);
							  				var arrX = [];
							  				arrCtr15.push(value.unique_ctr);

							  				avg[14] += parseFloat(value.unique_ctr);
							  				arrCtr_count[14]++;

											arrHoverValue15.push(["<b>Salient Object:</b> " + value.value[0] +"<br>" + "<b>CTR: </b>"+String(Math.round(parseFloat(value.unique_ctr)*100)/100)+"<br><b>Ads Number: </b><span class='ads_number'>"+value.ad_number+"</span>"]);
							  				arrAds15.push(value.ad_number);
							  				overalldata15.push(value);
							  				for (var i = 0; i<arrCtr15.length; i++) arrX.push(value.value[0]);

							  				t15 = {
							  					name: value.value[0],
												y: arrCtr15,
												x: arrX,
												mode: 'markers',
												type: 'scatter',
												text: arrHoverValue15,
												img: arrAds15,
												overalldata: overalldata15,
												value: tags15,
												hoverinfo: 'text'
											
							  				};

							  				if( $.inArray( value.value[0], emotion_list ) == -1){
								  				emotion_list.push(value.value[0])
								  			}
							  			}

							  			if( key == (res.data.result.length -1) ){
							  				computeAvg();


							  			}
							  		});

									function computeAvg(){
										for(var x = 0; x <= (avg.length-1); x++){
						  					avg[x] =  parseFloat(avg[x] / arrCtr_count[x]).toFixed(2);
						  				}
							  				
						  				var avg_counter = 0;
						  				for(var x=0; x <= (avg.length-1); x++ ){
						  					
						  					if( !isNaN(avg[x]) ){
						  						avg_list.push(avg[x]);
						  						avg_all += parseFloat(avg[x]);
						  						avg_counter++;
						  					}

						  					if( x == (avg.length-1) ){

						  						avg_all = 0.4;

						  						document.querySelector(".tag-val .ctr-avg").innerHTML = avg_all.toFixed(2);

						  						var ctr_high;
												if( parseFloat(avg_all).toFixed(2) == parseFloat(avg_list[0]).toFixed(2) ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "none"; 
													document.querySelector(".tag-val .equal").style.display = "inline"; 
												}else if( avg_all > avg[0] ){
													ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg_list[0]).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "lower";
												}else{
													ctr_high = parseFloat(avg_list[0]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
													document.querySelector(".tag-val .high-low").style.display = "inline"; 
													document.querySelector(".tag-val .equal").style.display = "none"; 
													document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
												}

						  						if( ctr_high < 0 ){
						  							ctr_high *= ctr_high;
						  						}
						  						document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);


						  					}
						  				}
						  				
						  			
						  				var emo_ctr = 0;
						  				emotions = [];
						  				emotion_list.sort();

						  				for(var x = 0; x <= (avg.length-1); x++ ){

						  					if( !isNaN(avg[x]) ){

						  						if( (((avg[x]-0.4)/0.4).toFixed(2)*100) > 0 ){
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#1F77B4'] );
						  						}else{
						  							chartData.push( [emotion_list[emo_ctr], ((avg[x]-0.4)/0.4).toFixed(2)*100, '#FF7F0E'] );
						  						}

												emotions.push([ emotion_list[emo_ctr], x  ]);
												emo_ctr++;
						  					}
						  					
						  				}	

						  				chartData.sort(function(a, b) {
										    return a[1] - b[1];
										});



						  				for(var x = 0; x < chartData.length; x++){
						  					xaxis.push( chartData[x][0] );
						  					yaxis.push( chartData[x][1] );
						  					barcolor.push( chartData[x][2] );
						  				}

						  				$('.selected-opt-num').text( emotions[0][1] );
						  				$('.selected-opt').text( emotions[0][0] );
						  				
						  				document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[0][0];
						  				document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[0]).toFixed(2);
									}			
	

									$(".opt-next").click(function(){
										if( emo_counter != (emotions.length-1) ){
											emo_counter++;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});		

									$(".opt-back").click(function(){
										if( emo_counter > 0 ){
											emo_counter--;

											var num = emotions[emo_counter][1];

											

											document.querySelector(".tag-val .ctr-min").innerHTML = parseFloat(avg_list[num]).toFixed(2);
											document.querySelector(".tag-val .ctr-selected").innerHTML = emotions[emo_counter][0];
											document.querySelector(".tag-val .selected-opt").innerHTML = emotions[emo_counter][0];
											

											var ctr_high;
											if( parseFloat(avg_all).toFixed(2) == parseFloat(avg[num]).toFixed(2) ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "none"; 
												document.querySelector(".tag-val .equal").style.display = "inline"; 
											}else if( avg_all > avg[num] ){
												ctr_high = parseFloat(avg_all).toFixed(2) - parseFloat(avg[num]).toFixed(2);
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "lower"; 
											}else{
												ctr_high = parseFloat(avg[num]).toFixed(2) - parseFloat(avg_all).toFixed(2); 
												document.querySelector(".tag-val .high-low").style.display = "inline"; 
												document.querySelector(".tag-val .equal").style.display = "none";
												document.querySelector(".tag-val .ctr-status").innerHTML = "higher";
											}

											if( ctr_high < 0 ){
													ctr_high *= ctr_high;
												}
											document.querySelector(".tag-val .ctr-high").innerHTML = ctr_high.toFixed(2);
										}
										
									});			
									
									// $('#bk-btn').click(function(){
									// 	$('#my-graph').show();
									// 	$('#avePlot').hide();
									// 	$('#ave-btn').show();
									// 	$('#bk-btn').hide();
									// });
									// $("#ave-btn").click(function(){

										// $('#my-graph').hide();
										// $('#avePlot').fadeIn();
										// $('#ave-btn').hide();
										// $('#bk-btn').show();

										

										
										var dataObjs = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15];
										
										var data = [
										  {
										    x: xaxis,
										    y: yaxis,
										    // y:[-20,-10,25,25,50,50,50,100],
										    allData: dataObjs,
										    type: 'bar',
										     marker: {
											    color: barcolor,
											    opacity: 0.6,
											  }
										  }
										];

										var layout = {
										  title: $scope.title,
										  xaxis: {
										    marker: {
										    	colorbar: {
										    		bgcolor: 'black'
										    	}
										    }
										  },
										  yaxis: {
										    title: 'Difference from Average Performance',
										    dtick: 20,
										    ticksuffix: '%',
										    titlefont: {
										      size: 12,
										      color: '#7f7f7f'
										    }
										  }
										};

										Plotly.newPlot('avePlot', data, layout, {displayModeBar: false} 	);
									// })


									var total_avg = $scope.avgCalculator(res.data.result,emotion_list);

									var layout = {
										title: $scope.title,

										// xaxis: { title: 'Salient Object' },
										yaxis: {
											title: 'Unique CTR ( % )',
											ticksuffix: '%',
											dtick: 0.5,
											//hoverformat: '.2f%',
											showticksuffix: 'last',
											showtickprefix: 'none'
										},
										hovermode: 'closest'
									};



									var dataObj = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15];

									
								  	dataObj = $scope.constructorAvg(dataObj, total_avg);
								  	$scope.meanDraw(total_avg, dataObj);
								  	// Plotly.newPlot('my-graph', dataObj, layout, {displayModeBar: false});
									

								  	
								  	$('.chart-btm-box').show();

								  	// myPlot.on('plotly_click',
								  	// 	function (data) {
											
											// var side_images = [];
								  	// 		var point = data.points[0],
								  	// 			ulist = document.querySelector("#myModal ul.related-gallery");

								  	// 		$('#myModal').modal();

								  	// 		// when modal is closed, empty list
								  	// 		$('#myModal').on('hidden.bs.modal', function (e) {
									  // 			$("#myModal").find('ul.related-gallery').empty();									  			
											// });

								  	// 		var content = {
								  	// 			ctr: point.data.y[point.pointNumber],
								  	// 			ads_number: point.data.img[point.pointNumber],
								  	// 			emotion: point.data.x[point.pointNumber],
								  	// 			image: point.data.img[point.pointNumber] + '.jpg'
								  	// 		}

								  	// 		var image_info = [];

								  	// 		/*note: bare me with this one XD
								  	// 		spaghetti code -_-*/
								  	// 		var img_ctr = 0;
								  	// 		if (point.data.x[point.pointNumber]) {
								  	// 			for (var i = 0; i < point.data.text.length; i++) {

								  	// 				// do not include the clicked image
								  	// 				// to avoid redundancy
								  	// 				if (point.data.text[i] != point.data.text[point.pointNumber]) {
								  	// 					var li = document.createElement('li');
											//   			li.style = 'width:50%';
											//   			li.innerHTML = '<a class="thumbnail"><i class="thumb-img-num" hidden>'+img_ctr+'</i><img class="img-responsive" src="telstra_images/'+point.data.img[i]+'.jpg" alt="" /></a><span class="ctrs">CTR: ' + parseFloat(point.data.y[i]).toFixed(2) + ' %</span>';
											//   			ulist.appendChild(li);

											//   			side_images.push([ point.data.img[i], point.data.y[i], point.data.value[i][0], point.data.value[i][1] ]);

											//   			image_info.push([point.data.overalldata[i].campaign_name, 
											//   							point.data.overalldata[i].start,point.data.overalldata[i].end,
											//   							 point.data.overalldata[i].impression, 
											//   							 point.data.overalldata[i].reach, 
											//   							 parseFloat(point.data.overalldata[i].unique_ctr).toFixed(2) + '%' ,
											//   							 point.data.overalldata[point.pointNumber].cpc, 
											//   							 point.data.overalldata[i].ad_number,point.data.overalldata[i].ad_id 
											//   							]);

											//   			img_ctr++;


								  	// 				}

								  	// 			}

								  	// 			// display 6 list
								  	// 			$("#myModal ul.related-gallery li").slice(6).hide();
								  	// 		}

								  	// 		document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + content.image;
								  	// 		document.querySelector("#myModal .ctr").innerHTML = (parseFloat(content.ctr)).toFixed(2) + ' %';
								  	// 		document.querySelector("#myModal .sel-top").innerHTML = 'Salient Object';
								  	// 		// document.querySelector("#myModal .emotion").innerHTML = content.emotion;

								  	// 		document.querySelector("#myModal .campaign").innerHTML = point.data.overalldata[point.pointNumber].campaign_name;
								  	// 		document.querySelector("#myModal .start").innerHTML = point.data.overalldata[point.pointNumber].start;
								  	// 		document.querySelector("#myModal .end").innerHTML = point.data.overalldata[point.pointNumber].end;
								  	// 		document.querySelector("#myModal .impression").innerHTML = point.data.overalldata[point.pointNumber].impression;
								  	// 		document.querySelector("#myModal .reach").innerHTML = point.data.overalldata[point.pointNumber].reach;
								  	// 		document.querySelector("#myModal .unique").innerHTML = parseFloat(point.data.overalldata[point.pointNumber].unique_ctr).toFixed(2) + '%';
								  	// 		document.querySelector("#myModal .cpc").innerHTML = point.data.overalldata[point.pointNumber].cpc;
								  	// 		document.querySelector("#myModal .ad-id").innerHTML = point.data.overalldata[point.pointNumber].ad_number;
								  	// 		document.querySelector("#myModal .fb-id").innerHTML = point.data.overalldata[point.pointNumber].ad_id;

								  	// 		$('.tags-ul').html("");


								  	// 		Report.tags(point.data.value[point.pointNumber][0],point.data.value[point.pointNumber][1])
							  		// 			  .then(function (resTags) {
							  		// 			  	console.log(resTags);

							  		// 			  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
							  					  		
							  		// 			  		if( resTags.data.result[0].value[i].length != 1 ){
							  		// 			  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
							  		// 			  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
							  		// 			  			}
							  		// 			  		}else{
							  		// 			  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
							  		// 			  		}	

										 //  			}

							  		// 			});

								  	// 		$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  	// 		$('.left-col .ctr').attr('style', 'left:' + (((( parseFloat(content.ctr)).toFixed(2))*10 )-.6) + '%');
								  			
								  	// 		$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 		$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');


								  	// 		$('.thumbnail').click(function(){
								  	// 			var num = $(this).find('i').text();
								  				
								  	// 			document.querySelector("#myModal .img-responsive").src = 'telstra_images/' + side_images[num][0] + '.jpg';
								  	// 			document.querySelector("#myModal .ctr").innerHTML = (parseFloat(side_images[num][1])).toFixed(2) + ' %';

								  	// 			document.querySelector("#myModal .campaign").innerHTML = image_info[num][0];
									  // 			document.querySelector("#myModal .start").innerHTML = image_info[num][1];
									  // 			document.querySelector("#myModal .end").innerHTML = image_info[num][2];
									  // 			document.querySelector("#myModal .impression").innerHTML = image_info[num][3];
									  // 			document.querySelector("#myModal .reach").innerHTML = image_info[num][4];
									  // 			document.querySelector("#myModal .unique").innerHTML = image_info[num][5];
									  // 			document.querySelector("#myModal .cpc").innerHTML = image_info[num][6];
									  // 			document.querySelector("#myModal .ad-id").innerHTML = image_info[num][7];
									  // 			document.querySelector("#myModal .fb-id").innerHTML = image_info[num][8];

								  	// 			$('.left-col .fa-long-arrow-down').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  	// 			$('.left-col .ctr').attr('style', 'left:' + (((side_images[num][1])*10 ).toFixed(2)-.6) + '%');
								  				
								  	// 			$('.left-col .avg-arrow').attr('style', 'left:' + ( 0.4*10 ) + '%');
								  	// 			$('.left-col .avg').attr('style', 'left:' + ( 0.4*10 ) + '%');

								  	// 			$('.tags-ul').html("");


									  // 			Report.tags(side_images[num][2],side_images[num][3])
								  	// 				  .then(function (resTags) {
								  	// 				  	console.log(resTags);

								  	// 				  	for(var i = 0; i < resTags.data.result[0].value.length; i++ ){
								  					  		
								  	// 				  		if( resTags.data.result[0].value[i].length != 1 ){
								  	// 				  			for( var j = 0; j < resTags.data.result[0].value[i].length; j++ ){
								  	// 				  				$('.tags-ul').append('<li>'+resTags.data.result[0].value[i][j]+'</li>');
								  	// 				  			}
								  	// 				  		}else{
								  	// 				  			$('.tags-ul').append('<li>'+resTags.data.result[0].value[i]+'</li>');
								  	// 				  		}	

											//   			}

								  	// 				});
								  	// 		});
								  	// 		// document.querySelector("#myModal .ads_id").innerHTML = content.ads_number;

								  			

								  	// 	});

							  });						
						break;
					default:
						// 
				}

			}
			$scope.plotReport('emotions');

		}
	])
	.controller('predictController', [
		"$http",
		"$scope",
		"$state",
		"serverUrl",
		"predictService",
		    "$mdDialog", 
		    "$mdMedia",
		function controller( $http, $scope ,$state, serverUrl, predictService, $mdDialog, $mdMedia ){
			$scope.predictObject = {};
			$scope.progressValue = 0;
			$scope.imageObjectData = {};
			$scope.clientObjectData = {};

			$scope.predictObject.product_category = null;
			$scope.predictObject.gender = null;
			$scope.predictObject.objective = null;

			console.log( $scope.predictObject )
      		var is_show = false;

			console.log('predictController---------------------');
			$('.treeview').removeClass('active');
			$('.predict-menu').addClass('active');

			$scope.close = function close (id) { // Close the alert msg box
				$( '#predict-' + id ).fadeOut(  );
			}
			$scope.chart = function  chart () {
				var data = [{
					  name: "Asia",
					  text: ["Afghanistan", "Bahrain", "Bangladesh", "Yemen, Rep."],
					  marker: {
					    sizemode: "area",
					    sizeref: 200000,
					    size : [31889923, 708573, 150448339, 22211743]
					  },
					  mode: "markers",
					  y: [974.5803384, 29796.04834, 1391.253792, 2280.769906],
					  x: [43.828, 75.635, 64.062, 62.698],
					  uid: "99da6d"
					},{
					  name:"Europe",
					  text: ["Albania", "Austria", "Belgium", "United Kingdom"],
					  marker: {
					    sizemode: "area",
					    sizeref: 200000,
					    size: [3600523, 8199783, 10392226, 60776238]
					  },
					  mode: "markers",
					  y: [5937.029526, 36126.4927, 33692.60508, 33203.26128],
					  x: [76.423, 79.829, 79.441, 79.425],
					  uid: "9d3ba4"
					},
					{
					  name: "Oceania",
					  text: ["Australia","New Zealand"],
					  marker: {
					    sizemode: "area",
					    sizeref: 200000,
					    size: [20434176, 4115771]
					  },
					  mode: "markers",
					  y: [34435.36744, 25185.00911],
					  x: [81.235, 80.204],
					  uid: "f9fb74"
					}];

					var layout = {
					  // xaxis: {
					  //   title: 'Life Expectancy'
					  // },
					  yaxis: {
					    title: 'GDP per Capita',
					    type: 'log'
					  },
					  margin: {
					    t: 20
					  },
					  hovermode: 'closest'
					};

					Plotly.plot('myChart', data, layout);
			}

			$scope.newChart = function newChart(data) {
				console.log(data);
				console.log('new chart');
				var less_value = 100 - data.engaging_score;
				console.log(less_value); 
				var data = [
			    {
			        value: data.engaging_score,
			        color:"#2EAEB3",
			        highlight: "#2EAEB3",
			    },
			    {
			        value: less_value,
			        color: "#CED2D6",
			    },
				]
				var ctx = document.getElementById("myChart").getContext("2d");
				var myDoughnutChart = new Chart(ctx).Doughnut(data);
			}
			
			$scope.doughNut = function doughNut () {
				var data = [
			    {
			        value: 300,
			        color:"#2EAEB3",
			        highlight: "#2EAEB3",
			        label: "Red"
			    },
			    {
			        value: 50,
			        color: "#CED2D6",
			        highlight: "#CED2D6",
			        label: "Green"
			    },
				]
				var ctx = document.getElementById("myChart").getContext("2d");
				var myDoughnutChart = new Chart(ctx).Doughnut(data);
			}

			$scope.is_demo_img  = true;
			$scope.statusView = true;

			$scope.sortResult = function sortResult( category ) {
				if ( category == 'latest' ) {
					$scope.statusView = true;
				}else if( category == 'oldest' ) {
					$scope.statusView = false;					
				}
			}
			
			$scope.imgObject = {};
			$scope.showPicture = function showPicture (response) {
				console.log(response);
				$scope.newChart(response.client_upload_data[0]);
				if ( $scope.is_demo_img == true ) {					
					$scope.defaultPic = response;
				}else {
					$scope.defaultPic = response.filename;	
					$scope.imgObject  = response;
					predictService.imageTag( response.img_tags );
					predictService.completeImgData( response.client_upload_data[0] );				
				}
			}
			// $scope.useraction = function useraction ( option ) { // Sets default user action for Demo or Skip
			// 	$scope.close( 'msg001' ) // Removes the alert msg box
			// 	if ( option == 'demo' ) {
			// 		$scope.is_demo_img  = true;
			// 		$( '#sm-fb' ).addClass( 'active' );
			// 		$scope.predictObject.product_category = 'Recontract'; // Will be filled up the heading textbox
			// 		$scope.predictObject.product_name = 'Demo Product'; // Selected platform  
			// 		$scope.predictObject.country = 'Philippines'; // Selected Target Gender  
			// 		$scope.predictObject.gender = 'Male'; // Selected Geography  
			// 		$scope.predictObject.objective = 'App Installs'; // Selected Geography  

			// 		$scope.predictObject.mediaplatform = 'facebook'; // Selected Target object  
			// 		$scope.predictObject.hashtags = '#family, #relationship, #indoor';  

			// 	}else if( option == 'skip' ) {
			// 		$scope.is_demo_img = false;
			// 		$scope.predictObject = {}; // Remove all defaults
			// 	}
			// }

			// Fetch Images for user skipped demo
			// $scope.clientImageObject = {};
			// $scope.fetchImages = function fetchImages( category ) {
		 //    	$http.get( serverUrl.url + 'getCategoryImages/' + category )
		 //    		.success( function success( response ) {
		 //    			$scope.clientImageObject = response;
		 //    		} );
		 //    }
		    // $scope.fetchImages( 'client_upload' );

			// $scope.is_valid = false;
			// $scope.demoImages = function demoImages () {
			// 	$scope.is_demo_img = true;
			// 	$scope.defaultPic = "samplepage_background.jpg";
			// 	$('.loading-predict-container').fadeOut(500,function(){
			// 		$('.predict-result-container').fadeIn(500);
			// 		$scope.doughNut();
			// 	});
			// }

			// $scope.socialPlatform = function socialPlatform( platform ) {
			// 	$scope.predictObject.mediaplatform = platform;
			// 	console.log(platform);
			// }

			// Submit Predict Queries
			// $scope.createPredict = function createPredict() {
			// 	var width = 0;

			// 	// console.log( $scope.predictObject );
			// 	if ( $scope.predictObject.product_category == null || 
			// 		 $scope.predictObject.product_name == null || 
			// 		 $scope.predictObject.country == null || 
			// 		 $scope.predictObject.gender == null || 
			// 		 $scope.predictObject.objective == null || 
			// 		 $scope.predictObject.hashtags == null || 
			// 		 $scope.predictObject.mediaplatform == null
			// 	) {
			// 		sweetAlert("Oops...", "Dont leave the fields empty.", "error");
			// 	}else if( $scope.predictObject.ageMin >= $scope.predictObject.ageMax ) {
			// 		sweetAlert("Oops...", "Invalid scope of age.", "error");
			// 	} else {				
			// 		if ( $scope.is_demo_img  == false ) {				
			// 	        $http.post( serverUrl.url + 'createPrediction', $scope.predictObject )
			// 				.success(function success(response ){
			// 					console.log(response);
			// 	        })	
			// 		};		
			// 		$('.content-manager').fadeOut(500, function(){
			// 			$('.loading-predict-container').fadeIn(500);
			// 			$scope.close( 'msg001' );
			// 			setTimeout(function() {
			// 				$( '#predict-progress' ).css( 'width' , '100%' );
			// 			}, 10);
			// 		})			
			// 	}
			// 	console.log( $scope.predictObject );			
			// }
			// End: submit			


			// Experiment Lists
			$scope.clientObject = {};
			$scope.client_id;	

			//--- Get Client Data 
			$scope.setGlobalUserData = function setGlobalUserData() {
				$http.get( serverUrl.url + 'getuserdata' )
					.success( function success( response ) {
						console.log( response );
						$scope.getClientData( response[0].company );
						// predictService.userData(  );

						// Store User Data
						// localStorage.setItem( "userid" , response[0]._id );
					} );
			}
			// $scope.setGlobalUserData();
			$scope.resultObject = {};
			$scope.getClientData = function getClientData( id ) {
				$http.get( serverUrl.url + 'getClientExperiments/' + id )
					.success( function success( response ) {
						// $scope.clientObject.predict = response.predict;
						$scope.clientObject.predict = response;

						console.log( response )
						$scope.client_id = response._id;
					} );
			}

			// $scope.img_tag_objects = [];
			// $scope.showresult = function showresult ( arr ) {			
				// setTimeout(function() {
				// 	$( '.content-manager' ).fadeOut();
				// 	$( '.content-manager' ).attr( 'hidden', 'hidden' );
				// 	$( '.content-manager' ).addClass( 'hide' );
				// 	$( '#predict-msg001' ).fadeOut();
				// 	$( '.predict-result-container' ).fadeIn();
				// }, 10);
				// predictService.showPredictResult( true );
				// $scope.resultObject = arr;
				// console.log( '$scope.resultObject' );
				// console.log( $scope.resultObject );
				// var newData;
				// predictService.imgData( arr );

				// $http.get( serverUrl.url + 'getSingleImage/' + arr.filename )
				// 	.success( function success( response ) {
				// 		// if ( response[0].client_upload_data != undefined || response[0].client_upload_data !== '' || response[0].client_upload_data != null   ) {
				// 		// 	predictService.completeImgData(response[0].client_upload_data[0]);						
				// 		// 	newData = response[0].client_upload_data[0];
				// 		// };
				// 		var image = response.shift( );
				// 		predictService.set( "imageData" , image );
				// 		$state.go( 'predict-result' );
				// 	} );

				// setTimeout(function() {
				// 	$scope.newChart(newData);
				// }, 10);
				
			// }
			

			$scope.addTagValue = function addTagValue () {
				var dataTags = {};
				var img_tags = $('.img_tags_value').map( function() { 		    		
		    		return { val : $(this).val() , tagName : $(this).attr( 'id' ) };	
		    	} ).get();
		    	
		    	$scope.imageObjectData.tagValue = img_tags;

		    	$http.post( serverUrl.url + 'storeImageTagValue', $scope.imageObjectData  )
		    		.success( function success(response) {
		    			console.log( response );
		    			sweetAlert("", "Successfully added image tag values", "success");
		    		} );
			}

	      	// $scope.deleteExperiment = function deleteExperiment (evt, arr) {
	      	// 	console.log( arr );
        //      var confirm = $mdDialog.confirm()
        //           .title('Are you sure you want to delete this experiment?')
        //           .textContent('This experiment will permanently delete.')
        //           .ariaLabel('Lucky day')
        //           .targetEvent(evt)
        //           .ok('Okay')
        //           .cancel('Cancel');
        //         $mdDialog.show(confirm).then(function() {
        //           $http.post( serverUrl.url + 'deleteExperiment' , arr )
        //          .success( function success (response) {
        //            $( '#' + arr.predict_id ).fadeOut();
        //            console.log( response )
        //          });
        //     }, function() {
        //       console.log('cancel');
        //     });
	      	// }

	      	$scope.hashtags = [
	      		{ hash: null  },
	      		{ hash: null  },
	      		{ hash: null  },
	      		{ hash: null  },
	      		{ hash: null  }
	      	];
	      	$scope.showPredictionForm = function showPredictionForm ( status ) {
	      		if ( status == 'create' ) {
		      		$( '#experiment-table' ).removeClass( 'col-md-12' ).addClass( 'col-md-8');
		      		setTimeout(function() {
			      		$( '#predict-form-content' ).fadeIn(500);
			      		$( '#predict-form-content' ).css({"margin-top":"0px"});
		      		}, 500);
		      		$( '.custom-btn-prediction' ).hide();
		      		$( '.custom-btn-prediction-cancel' ).show();
	      		}else if ( status == 'cancel' ) {
	      			$scope.predictObject = {};
	      			$( '#predict-form-content' ).css({"margin-top":"80px"});
	      			$( '#predict-form-content' ).fadeOut(500);
	      			setTimeout(function() {
		      			$( '#experiment-table' ).removeClass( 'col-md-8' ).addClass( 'col-md-12' );
		      			$( '.custom-btn-prediction' ).show();
			      		$( '.custom-btn-prediction-cancel' ).hide();
	      			}, 500);
	      		};	      		
	      	};

	      	$scope.predictObject = {};
	      	$scope.predictObject.mediaplatform = null;
	      	$scope.selectedMedia = function selectedMedia ( media ) {
	      		$scope.predictObject.mediaplatform = media;
	      		$( '.btn-social-exp' ).removeClass( 'active' );	
	      		$( '#' + media ).addClass( 'active' );
	      	};
	   //    	$scope.createExperiment = function createExperiment () {
	   //    		console.log( $scope.hashtags.length );
	   //    		$scope.hashList = $scope.hashtags;

	   //    		for( var x = 0; x < $scope.hashList.length; x++ ) {
	   //    			if ( $scope.hashList[x].hash == null ) {
	   //    				$scope.hashList.splice( $scope.hashList.indexOf( $scope.hashList[x], 1 ) );
	   //    				console.log( 'empty value ');
	   //    			} else {
	   //    				console.log( $scope.hashList[x].hash );
	   //    			}
	   //    		}
	   //    		$scope.predictObject.hashtags = $scope.hashList;

				// if ( $scope.predictObject.product_name == null || 
				// 	 $scope.predictObject.campaign_name == null ||
				// 	 $scope.predictObject.country == null || 
				// 	 $scope.predictObject.gender == null || 
				// 	 $scope.predictObject.objective == null || 
				// 	 $scope.predictObject.mediaplatform == null
				// ) {
				// 	sweetAlert("Oops...", "Dont leave the fields empty.", "error");
				// }else {					
			 //       $http.post( serverUrl.url + 'createPrediction', $scope.predictObject )
				// 		.success(function success(response ){
				// 			console.log(response);
				// 			$scope.setGlobalUserData();
			 //      });
				// }

	   //    	};

	   		

		}
	]);

app.controller('UploadCtrl', [
	"$scope",
	function controller( $scope ) {
		console.log( 'run UploadCtrl' );

		$('.menu-header-global').removeClass('active-menu');
	  	$('.fa-icon').removeClass('active-menu-icon');
	  	$('.upload-a').addClass('active-menu');
	  	$('.upload-a span').addClass('active-menu-icon');


		$scope.sortType = 'filename';
		$scope.sortReverse = false;

		$scope.changeView = function(view){
			if( view == 'filmstrip' ){
				$( '.list' ).hide();
				$( '.gallery' ).hide();
				$( '.filmstrip' ).fadeIn();
				$( '.view-type' ).text( " Filmstrip" );
			}else if( view == 'list' ){
				$( '.list' ).fadeIn();
				$( '.gallery' ).hide();
				$( '.filmstrip' ).hide();
				$( '.view-type' ).text( " List" );
			}else{
				$( '.list' ).hide();
				$( '.gallery' ).fadeIn();
				$( '.filmstrip' ).hide();
				$( '.view-type' ).text( " Gallery" );
			}

		}

		$scope.sortBy = function(sort,reverse){
			$scope.sortType = sort;
			$scope.sortReverse = reverse;
			
			if( sort == 'filename' && reverse == false ){
				$( '.sortType' ).text('A - Z');
			}else if( sort == 'filename' && reverse == true ){
				$( '.sortType' ).text('Z - A');
			}else if( sort == 'timeCreated' && reverse == true ){
				$( '.sortType' ).text('Newest');
			}else if( sort == 'timeCreated' && reverse == false ){
				$( '.sortType' ).text('Oldest');
			}
			
		}

		$( '.shw-select' ).click(function(){
			$('.create-grp').hide();
			$('.select-grp').fadeIn();
		});
		$( '.hide-select' ).click(function(){
			$('.create-grp').fadeIn();
			$('.select-grp').hide();
		});
	}
]);


app.controller('ReportSummaryCtrl', [
	"$http",
	"$scope",
	"Report",
	"reportService",
	"$state",
  "pageService",
	function controller( $http, $scope, Report, reportService, $state, pageService ){
		console.log( 'run ReportSummaryCtrl' );

		var emotionsData = [];
		var genderData = [];
		var hairData = [];
		var ageData = [];
		var clothingData = [];
		var activitiesData = [];
		var indoorData = [];
		var scenarioData = [];
		var weatherData = [];
		var dominantData = [];
		var salientData = [];

		var allData = [];
		var summaryData = [];

		$scope.plotReport = function (reportType, filter) {
			
			var filt;

			if(filter == '6 months'){
				filt = 6;
			}else if(filter == '1 year'){
				filt = 12;
			}else{
				filt = 99999999999999999999;
			}
      
      // pageService.pageStatus("true");
      switch(reportType){
				case 'emotions':
					Report.show(reportType)
							.then(function (res) {
						console.log(res);
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {

							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();

							if( gap <= filt ){
								if( $.inArray( value.value[0], data ) == -1 ){
									data.push( value.value[0]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[0], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										emotionsData.push( [ 'Emotion', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );

										if( a == (avg.length-1) ){
											emotionsData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push(  emotionsData[0]);
											$scope.plotReport('indoor/outdoor',filter);
			
										}
									}
								}
							}

							
						});


					});	
					break;
				case 'indoor/outdoor':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[1], data ) == -1 ){
									data.push( value.value[1]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[1], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										indoorData.push( [ 'Indoor/Outdoor', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
									
										if( a == (avg.length-1) ){
											indoorData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push(indoorData[0]);
											$scope.plotReport('scenario',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'scenario':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[0], data ) == -1 ){
									data.push( value.value[0]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[0], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										scenarioData.push( [ 'Scenario', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											scenarioData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( scenarioData[0]);
											$scope.plotReport('weather',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'weather':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[3], data ) == -1 ){
									data.push( value.value[3]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[3], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										weatherData.push( [ 'Weather', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											weatherData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( weatherData[0]);
											$scope.plotReport('gender',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'gender':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[0], data ) == -1 ){
									data.push( value.value[0]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[0], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										genderData.push( [ 'Gender', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											genderData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( genderData[0]);
											$scope.plotReport('activities',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'activities':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
								
								if( $.inArray( value.value[1], data ) == -1 ){
									data.push( value.value[1]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[1], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										activitiesData.push( [ 'Activities', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											activitiesData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( activitiesData[0]);
											$scope.plotReport('age',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'age':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[2], data ) == -1 ){
									data.push( value.value[2]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[2], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										ageData.push( [ 'Age', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											ageData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( ageData[0]);
											$scope.plotReport('clothing',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'clothing':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[3], data ) == -1 ){
									data.push( value.value[3]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[3], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										clothingData.push( [ 'Clothing', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4) ] );
										if( a == (avg.length-1) ){
											clothingData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( clothingData[0]);
											$scope.plotReport('hair colour',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'hair colour':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[4], data ) == -1 ){
									data.push( value.value[4]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[4], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										hairData.push( [ 'Hair Colour', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4) ] );
										if( a == (avg.length-1) ){
											hairData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push( hairData[0]);
											$scope.plotReport('dominant colour',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'dominant colour':
					Report.show(reportType)
							.then(function (res) {
						
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[0], data ) == -1 ){
									data.push( value.value[0]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[0], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										dominantData.push( [  'Dominant Colour', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											dominantData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push(dominantData[0]);
											$scope.plotReport('salient object',filter);
										}
									}
								}
							}
						});


					});	
					break;
				case 'salient object':	
					Report.show(reportType)
							.then(function (res) {
						var data = [];
						var avg = [];
						var ctr = [];

						// console.log(res);
						angular.forEach(res.data.result, function(value,key) {
							var start =  new Date(value.start) ;
							var end = new Date(value.end);
							var gap;

							gap = (((end.getFullYear() - start.getFullYear()) * 12) - (start.getMonth() + 1)) + end.getMonth();
							
							if( gap <= filt ){
							
								if( $.inArray( value.value[0], data ) == -1 ){
									data.push( value.value[0]);
									avg.push( 0 );
									ctr.push( 0 );
								}else{
									var x = $.inArray( value.value[0], data );
									avg[x] += parseFloat(value.unique_ctr);
									ctr[x] += 1;
								}

								if( key == (res.data.result.length-1) ){
									for( a in avg ){
										salientData.push( [ 'Salient Object', data[a], ( ( (avg[a] /= ctr[a]) - 0.4 ) / 0.4 ) ] );
										if( a == (avg.length-1) ){
											salientData.sort(function(a, b) {
											    return b[2] - a[2] ;
											});

											allData.push(salientData[0]);
											$scope.summary();
										}
									}
								}
							}
						});


					});	

					break;
				default:
			
      }
		}

		$scope.summary = function(){
			allData.sort(function(a, b) {
			    return b[2] - a[2] ;
			});
			console.log(allData);
			$('.top').show();
			$('.bottom').show();

			$( '.top-0' ).text( allData[0][0] + ': ' + (allData[0][1]).substr(0, 20) ); 
			$( '.top-0-ctr' ).text( '+' + ( (parseFloat(allData[0][2]).toFixed(1) )*10) + '%' ); 

			$( '.top-1' ).text( allData[1][0] + ': ' + (allData[1][1]).substr(0, 20) ); 
			$( '.top-1-ctr' ).text( '+' + ( (parseFloat(allData[1][2]).toFixed(1) )*10) + '%' ); 

			$( '.top-2' ).text( allData[2][0] + ': ' + (allData[2][1]).substr(0, 20) ); 
			$( '.top-2-ctr' ).text( '+' + ( (parseFloat(allData[2][2]).toFixed(1) )*10) + '%' ); 

			$( '.top-3' ).text( allData[3][0] + ': ' + (allData[3][1]).substr(0, 20) ); 
			$( '.top-3-ctr' ).text( '+' + ( (parseFloat(allData[3][2]).toFixed(1) )*10) + '%' ); 

			$( '.top-4' ).text( allData[4][0] + ': ' + (allData[4][1]).substr(0, 20) ); 
			$( '.top-4-ctr' ).text( '+' + ( (parseFloat(allData[4][2]).toFixed(1) )*10) + '%' ); 

			$( '.bottom-0' ).text( allData[0][0] + ': ' + (allData[0][1]).substr(0, 20) ); 
			$( '.bottom-1' ).text( allData[1][0] + ': ' + (allData[1][1]).substr(0, 20) ); 
			$( '.bottom-2' ).text( allData[2][0] + ': ' + (allData[2][1]).substr(0, 20) ); 
			$( '.bottom-3' ).text( allData[3][0] + ': ' + (allData[3][1]).substr(0, 20) ); 
			$( '.bottom-4' ).text( allData[4][0] + ': ' + (allData[4][1]).substr(0, 20) ); 
			
		}

		$scope.filter = function(filter){
			console.log(filter);

			emotionsData = [];
			genderData = [];
			hairData = [];
			ageData = [];
			clothingData = [];
			activitiesData = [];
			indoorData = [];
			scenarioData = [];
			weatherData = [];
			dominantData = [];
			salientData = [];

			allData = [];
			summaryData = [];

			if(filter == '6 months'){
				$('.1-yr').removeClass('actives');
				$('.lifetime').removeClass('actives');
				$('.6-mnth').addClass('actives');
			}else if(filter == '1 year'){
				$('.1-yr').addClass('actives');
				$('.lifetime').removeClass('actives');
				$('.6-mnth').removeClass('actives');
			}else if(filter == 'lifetime'){
				$('.1-yr').removeClass('actives');
				$('.lifetime').addClass('actives');
				$('.6-mnth').removeClass('actives');
			}

			$scope.plotReport('emotions',filter);
			
		}
		

		// $scope.filter('6 months');


		$scope.filterOne = function(filt){
			if(filt == 'car'){
				$('.car').show();
				$('.sports').hide();
				$('.music').hide();
				$('.filt-name1').text("Sports Car Photoshoot");
			}else if(filt == 'sport'){
				$('.car').hide();
				$('.sports').show();
				$('.music').hide();
				$('.filt-name1').text("Rugby Action Shots");
			}else if(filt == 'music'){
				$('.car').hide();
				$('.sports').hide();
				$('.music').show();
				$('.filt-name1').text("Acoustic Guitar Performance");
			}
		}

		$scope.filterTwo = function(filt){
			if(filt == 'yoga'){
				$('.yoga').show();
				$('.meals').hide();
				$('.weekend').hide();
				$('.filt-name2').text("Outdoor Yoga Poses");
			}else if(filt == 'meals'){
				$('.yoga').hide();
				$('.meals').show();
				$('.weekend').hide();
				$('.filt-name2').text("Plated Meals");
			}else if(filt == 'summer'){
				$('.yoga').hide();
				$('.meals').hide();
				$('.weekend').show();
				$('.filt-name2').text("Weekend Relaxation");
			}
		}
		
	}
]);
app.controller('popUpCtrl', [
  "$scope",
  function controller( $scope ) {

    this.topDirections = ['left', 'up'];
    this.bottomDirections = ['down', 'right'];
    this.isOpen = false;
    this.availableModes = ['md-fling', 'md-scale'];
    this.selectedMode = 'md-fling';
    this.availableDirections = ['up', 'down', 'left', 'right'];
    this.selectedDirection = 'up';
  }
]);

app.controller('predictResultController', [
  "$scope",
  function controller( $scope ) {
  	console.log( 'predictResultController running' );
  	$('.menu-header-global').removeClass('active-menu');
  	$('.fa-icon').removeClass('active-menu-icon');
  	$('.predict-a').addClass('active-menu');
  	$('.predict-a span').addClass('active-menu-icon');
  }
]);

app.controller('influencersCtrl', [
  "$scope",
  function controller( $scope ) {
  	$('.menu-header-global').removeClass('active-menu');
  	$('.fa-icon').removeClass('active-menu-icon');
  	$('.user-a').addClass('active-menu');
  	$('.user-a span').addClass('active-menu-icon');
  	$scope.defaultView = true;

  	$scope.actionView = function actionView ( option ) {
  		$( '.btn-theme' ).removeClass( 'active' );
  		if (option == 'audiences') {
  			$scope.defaultView = false;
  			$( '#' + option ).addClass( 'active' );
  			setTimeout(function() {
  				$scope.doughNutGraph();
  				$scope.dataTable();
  			}, 10);
  		} else {
  			$scope.defaultView = true;
  			$( '#' + option ).addClass( 'active' );
  		}
  	}

 	$scope.doughNutGraph = function doughNutGraph() {
 		var data = [{
			values: [45, 25, 10, 10, 10],
			labels: ['Family Vacantioners', 'Dance enthusiast', 'Foodies', 'Fashionista', 'Wonderlust'],
			marker: {
			  colors: ['#6E2E9E', '#2683C6', '#00B050', '#DE1F00', '#FFBF00']
			},
			hoverinfo: 'label+percent',
			hole: 0.8,
			type: 'pie',
			textinfo: 'none'
		}];

		var layout = {
			title: '',
			width: 420,
			height: 450,
			showticklabels: false,
			showlegend: false
		};
		var stat = Plotly.newPlot('doughnut-graph', data, layout, {displayModeBar: false});
		$scope.labels = [];
		var total = 0;
		var value = 0;
		for( var y = 0; y < data[ 0 ].values.length; y++ ) {
			total += data[ 0 ].values[ y ];
		}
		for( var x = 0; x < data[ 0 ].labels.length; x++ ) {
			value = data[ 0 ].values[ x ] / total * 100;
			$scope.labels.push( { label: data[ 0 ].labels[ x ], color: data[ 0 ].marker.colors[ x ], value: value.toFixed( 1) + "%" } );
			value = 0;
		}
 	};
 	$scope.dataTable = function dataTable () {
 		$scope.profileInfo = [
 			
 			{ 
 				img_url   : '../img/audiences/1.jpg',
 				username  : 'manissah',
 				ranking	  : 1,
 				followers : '664',
 				posts 	  : '667',
 				affinities: 'Family Vacantioners',
 				other_int : 'Foodies'
  			}, 
  			{ 
 				img_url   : '../img/audiences/2.jpg',
 				username  : 'gilmangirl',
 				ranking	  : 2,
 				followers : '607',
 				posts 	  : '1057',
 				affinities: 'Dance enthusiast',
 				other_int : 'Foodies'
  			}, 
 			{ 
 				img_url   : '../img/audiences/3.jpg',
 				username  : 'krissosmart',
 				ranking	  : 3,
 				followers : '527',
 				posts 	  : '610',
 				affinities: 'Foodies',
 				other_int : 'Wonderlust'
  			}, 
 			{ 
 				img_url   : '../img/audiences/4.jpg',
 				username  : 'june.jumadi',
 				ranking	  : 4,
 				followers : '265',
 				posts 	  : '110',
 				affinities: 'Family Vacantioners',
 				other_int : 'Foodies'
  			},
  			{ 
 				img_url   : '../img/audiences/5.jpg',
 				username  : 'laowanshi',
 				ranking	  : 5,
 				followers : '46',
 				posts 	  : '196',
 				affinities: 'Foodies',
 				other_int : 'Fashionista'
  			}, 
 		] 	
 	};

  }
]);

app.controller('locationController', [
	"$scope",
	function controller( $scope ) {
		$('.menu-header-global').removeClass('active-menu');
	  	$('.fa-icon').removeClass('active-menu-icon');
	  	$('.pin-a').addClass('active-menu');
	  	$('.pin-a span').addClass('active-menu-icon');
	  }
]);
app.controller('galleryController', [
  "$scope",
  function controller( $scope ) {
  		$('.menu-header-global').removeClass('active-menu');
	  	$('.fa-icon').removeClass('active-menu-icon');
	  	$('.gallery-a').addClass('active-menu');
	  	$('.gallery-a span').addClass('active-menu-icon');
  }
]);
