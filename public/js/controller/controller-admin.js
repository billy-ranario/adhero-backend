app
  .controller('dashboardController',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$rootScope",
    "$state",
    "createService",
    "netWork",
    function controller( $http, $scope, serverUrl, $localStorage, $rootScope, $state, createService, netWork ) {
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.dashboard-menu').addClass('active');
      netWork.loadNetwork('home');
   
    }
])
  app
  .controller('uploadCtrl',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$rootScope",
    "$state",
    "createService",
    "netWork",
    "Upload",
    function controller( $http, $scope, serverUrl, $localStorage, $rootScope, $state, createService, netWork, Upload ) {
      console.log('upload controller');
      $scope.description = {};

      $scope.submit = function() {
        // if (form.file.$valid && $scope.file) {
          console.log($scope.file);
          $scope.upload($scope.file);
        // }
      };


      $scope.upload = function (file) {
        Upload.upload({
            url: serverUrl.url + 'upload',
            data: { 
              file: file, 
              'description': $scope.description
            }
        }).then(function (resp) {
          console.log( resp );
            console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };
  }
]);