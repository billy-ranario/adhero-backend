var user = angular.module('user', []);

user.run( function( ) {
	console.log( 'running user module' );
});

user
	.factory( 'serverUrl',  [
		function( ) {
			return {
				url: window.location.origin + "/",
				header: {
					headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
				}
			}
		}
]);

user
	.directive( 'userInformation', [
		"$http",
		"serverUrl",
		function directive( $http, serverUrl ) {
			return {
				restrict: "A",
				scope: true,
				compile: function compile( element, attributeSet ) {

					return {
						pre: function pre( scope, element, attributeSet ) {
							console.log( 'running userInformation directive' );
						},
						post: function post( scope, element, attributeSet ) {

						}
					}
				}
			}
		}
]);
