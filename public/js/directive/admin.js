app
  .directive('imageShow', [
    "previewImageService",
    function directive( previewImageService ) {

      return {
        restrict: "A",
        scope: true,
        compile: function link( element, attributeSet ) {
          return {
            pre: function pre( scope, element, attributeSet )
            {

              console.log('running image preview')
              scope.$watch(function(){
                return previewImageService.imageView();
              }, function(newValue, oldValue){
                scope.image = newValue;
                console.log( scope.image );
              });
            },
            post: function post( scope, element, attributeSet ) {

            }
          }
        }
      }
    }

]);

app
  .directive('imageLoadCg', [
    "previewImageService",
    "$http",
    "$mdMedia",
    "$mdDialog",
    "nodeUrl",
    function directive( previewImageService, $http, $mdMedia, $mdDialog, nodeUrl ) {
      return {
        restrict: "A",
        scope: { image: "="},
        template: "<section ng-click='previewImage($event, img_src)'><img ng-src='{{ image.filename }}' id='image_{{ image.image_id }}' class='img-responsive' /><img src='../img/loading-img.gif' class='img-responsive load-img-state' style='height: 50px!important;' /></section>",
        link: function link( scope, element, attributeSet ) {
          console.log( scope.image );


          scope.image.filename = "http://52.77.17.243/upload_get_api/" + scope.image.filename;
          console.log(scope.filename);
          // console.log( img );
          // if( img )
          // {
            // setTimeout(function() {
              // scope.img_src = img;
              $('.load-img-state').fadeOut(500);
              $('#image_' + scope.image.image_id ).fadeIn();
            // }, 1500);
          // }

           scope.previewImage = function previewImage(ev, img){
            console.log(img);
            previewImageService.imageView(img);
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && scope.customFullscreen;
            $mdDialog.show({
              // controller: imageCtrl,
              templateUrl: 'templates/admin/dialog-preview.blade.php',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: useFullScreen
            })
            .then(function(answer) {
              // $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              // $scope.status = 'You cancelled the dialog.';
            });
            scope.$watch(function() {
              return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
              // $scope.customFullscreen = (wantsFullScreen === true);
            });
          }
        }
      }
    }
]);

app
  .directive('imageLoad', [
    "previewImageService",
    "$http",
    "$mdMedia",
    "$mdDialog",
    "nodeUrl",
    function directive( previewImageService, $http, $mdMedia, $mdDialog, nodeUrl ) {
      return {
        restrict: "A",
        scope: { image: "="},
        template: "<section ng-click='previewImage($event, img_src)'><img ng-src='{{ image.image_filename }}' id='image_{{ image.image_id }}' class='img-responsive' /><img src='../img/loading-img.gif' class='img-responsive load-img-state' style='height: 50px!important;' /></section>",
        link: function link( scope, element, attributeSet ) {
          console.log( scope.image );


          // var img = "http://52.77.17.243/upload_get_api/" + scope.image.image_filename;
          scope.image.image_filename = "http://52.77.17.243/upload_get_api/" + scope.image.image_filename;
          // console.log( img );
          // if( img )
          // {
            // setTimeout(function() {
              // scope.img_src = img;
              $('.load-img-state').fadeOut(500);
              $('#image_' + scope.image.image_id ).fadeIn();
            // }, 1500);
          // }

           scope.previewImage = function previewImage(ev, img){
            console.log(img);
            previewImageService.imageView(img);
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && scope.customFullscreen;
            $mdDialog.show({
              // controller: imageCtrl,
              templateUrl: 'templates/admin/dialog-preview.blade.php',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: useFullScreen
            })
            .then(function(answer) {
              // $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              // $scope.status = 'You cancelled the dialog.';
            });
            scope.$watch(function() {
              return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
              // $scope.customFullscreen = (wantsFullScreen === true);
            });
          }
        }
      }
    }
]);
app
  .directive('immagaData', [
    "$http",
    "serverUrl",
    "ImaggaAPI",
    "nodeUrl",
    function directive( $http, serverUrl, ImaggaAPI, nodeUrl ) {
      return {
        restrict: "A",
        scope: true,
        compile: function compile( attributeSet, element ) {
          return {
            pre: function pre( scope, element, attributeSet ) {
              console.log('imagga running');
              scope.immaga = {};
              scope.fetchImages = function fetchImages( category ) {
                $http.get( serverUrl.url + 'api/imagga/' )
                  .success( function success( response ) {
                    console.log( response );
                    scope.imagga = response.data;
                    console.log(scope.imagga);
                    // $scope.clientImageObject = response;
                    // console.log($scope.clientImageObject);
                  } );

              }

              scope.deleteImage = function deleteImage( data ) {
                console.log( data._id.$id );
                swal({   
                  title: "Are you sure?",
                  text: "You will not be able to recover this image file!",   
                  type: "warning",   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes, delete it!",   
                  closeOnConfirm: true }, 
                  function(){

                    ImaggaAPI.destroy(data._id.$id).then(function(response){
                      console.log(response);
                      $http.get( nodeUrl.url + 'delete_image_admin_imagga/' + data.filename )
                      .success(function(response) {
                        scope.imagga.splice(scope.imagga.indexOf(data), 1);
                        console.log(response);
                        swal("Deleted!", "Your image file has been deleted.", "success");
                      })
                    });
                  });
              }

               scope.fetchImages();
            }
          }
        } 
      }
    }

]);
app.
  directive('adminServerImage', [
    "$http",
    "nodeUrl",
    function directive( $http, nodeUrl ) {
      return {
        restrict: "A",
        scope: true,
        compile: function compile( attributeSet, element ) {
          return {
            pre: function pre( scope, element, attributeSet ) {
              console.log('admin server images running');
              
              scope.images = [];

              scope.getListImageAdmin = function getListImageAdmin( ) {
                $http.get( nodeUrl.url + 'list_admin_images')
                .success(function(response){
                  console.log( response.file[ 0 ].split( "/" ).reverse( )[ 0 ] );
                  
                  for(var x = 0; x < response.file.length; x++) {
                    scope.images.push({ filename: response.file[ x ].split( "/" ).reverse( )[ 0 ] });
                  }

                  console.log( scope.images );

                });
              }

              scope.remove = function remove( data, remove ) {
                console.log( data, remove );
                if( remove == "remove_single")
                {
                  swal({   
                    title: "Are you sure?",
                    text: "You will not be able to recover this image file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    closeOnConfirm: true }, 
                    function(){
                      console.log('deleted');
                      $http.get( nodeUrl.url + 'delete_image_admin/' + data.filename )
                      .success(function(response) {
                        console.log(response);
                        scope.images.splice(scope.images.indexOf(data), 1);
                      })
                    });
                  console.log('remove single');
                } else if( remove == "remove_all") {
                  console.log('remove multiple');

                }
              }

              scope.getListImageAdmin();
            }
          }
        } 
      }
    }

]);
app.
  directive('cloudinaryList', [
    "$http",
    "nodeUrl",
    function directive( $http, nodeUrl ) {
      return {
        restrict: "A",
        scope: true,
        compile: function compile( attributeSet, element ) {
          return {
            pre: function pre( scope, element, attributeSet ) {
              console.log('admin cloudinary images running');
              
              scope.images = [];
              scope.next_cursor = "";
              scope.selected = [];
              scope.selected_ids = false;
              scope.status = false;
              scope.ids;
              scope.select = function select( id ) {
                scope.selected.push(id);
                console.log( scope.selected );
                scope.selected_ids = true;
              }
              scope.selectedAll = function selectedAll( data ) {
                scope.status = true;
                console.log( data );
                for(var x = 0; x < data.length; x++ ) {
                  scope.selected.push(data[x]);
                  // scope.ids = data[ x ].public_id;
                }
                scope.selected_ids = true;
                console.log( scope.selected );
              }
              scope.exists = function (item, list) {
                return list.indexOf(item) > -1;
              };

              scope.deleteMultiple = function deleteMultiple( data ) {
                console.log('deleting multiple');
                swal({   
                    title: "Are you sure?",
                    text: "You will not be able to recover this image file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    closeOnConfirm: true }, 
                    function(){
                      console.log('deleted');
                      for(var x = 0; x < data.length; x++ ) {
                        console.log( data[ x ] );
                        $http.post( nodeUrl.url + 'cloudinary_delete_image', { name: data[ x ].public_id } )
                        .success(function(response) {
                          console.log(response);
                          scope.images.splice(scope.images.indexOf(data), 1);
                        });
                      }
                      // $('#image_' + data.public_id ).fadeOut(100);
                      // $('#image_loader_' + data.public_id).fadeIn(100);
                       // scope.images = [];
                      setTimeout(function() {
                        scope.getListImageAdmin( );
                      }, 2000);
                    });
              }

              scope.getListImageAdmin = function getListImageAdmin( ) {
                scope.status = false;
                scope.images = [];
                $('.load-images-btn').addClass('loader-state-btn');
                $('.load-images-btn').attr('disabled', true);
                $('.loading-images').fadeIn();
                $http.get( nodeUrl.url + 'cloudinary_list')
                .success(function(response){
                  console.log( response );
                  scope.next_cursor = response.next_cursor;
                  // scope.images.push(response.resources);
                  for( var x = 0; x < response.resources.length; x++ ) {
                    var date = moment(response.resources[ x ].created_at).startOf('hour').fromNow();
                    response.resources[ x ].date = date;
                    scope.images.push(response.resources[ x ]);

                  }

                  console.log(scope.images);
                  console.log(scope.next_cursor);
                  $('.load-images-btn').removeClass('loader-state-btn');
                  $('.load-images-btn').attr('disabled', false);
                  $('.loading-images').fadeOut();
                })
              }

              scope.loadMoreImages = function loadMoreImages( data ) {
                $('.load-images-btn').addClass('loader-state-btn');
                $('.load-images-btn').attr('disabled', true);
                $('.loading-images').fadeIn();
                $http.post( nodeUrl.url + 'cloudinary_list_next', { next: data } )
                .success(function(response){
                  scope.next_cursor = response.next_cursor;
                  for( var x = 0; x < response.resources.length; x++ ) {
                    var date = moment(response.resources[ x ].created_at).startOf('hour').fromNow();
                    scope.images.push(response.resources[ x ]);
                  }
                  console.log(scope.images);
                  $('.load-images-btn').removeClass('loader-state-btn');
                  $('.load-images-btn').attr('disabled', false);
                  $('.loading-images').fadeOut();
                });
                console.log( data );
              }

              scope.remove = function remove( data ) {
                console.log(data);
                swal({   
                    title: "Are you sure?",
                    text: "You will not be able to recover this image file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    closeOnConfirm: true }, 
                    function(){
                      console.log('deleted');
                      $('#image_' + data.public_id ).fadeOut(100);
                      $('#image_loader_' + data.public_id).fadeIn(100);
                      $http.post( nodeUrl.url + 'cloudinary_delete_image', { name: data.public_id } )
                      .success(function(response) {
                        console.log(response);
                        scope.images.splice(scope.images.indexOf(data), 1);
                      })
                    });
              }

              scope.getListImageAdmin();
            }
          }
        } 
      }
    }

]);
app
  .directive('tableClarfai', [
  "$http",
  "serverUrl",
  function directive( $http, serverUrl ) {
    return {
      restrict: "A",
      scope: true,
      compile: function compile( attributeSet, element ) {
        return {
          post: function post( scope, element, attributeSet ) {
            scope.clientImageObject = [];
            scope.next_page = "";
            scope.last_object = "";

            scope.listImages = function listImages( ) {
              $('.loading-images').fadeIn(100);
              $('.load-images-btn').addClass('loader-state-btn');
              $('.load-images-btn').attr('disabled', true);
              $http.get( serverUrl.url + 'api/uploads')
              .success( function success( response ) {
                console.log(response);
                scope.next_page = response.next_page_url;
                for(var x = 0; x < response.data.length; x++) {
                  scope.clientImageObject.push(response.data[x]);
                  scope.last_object = scope.clientImageObject[0].image_id;
                }
                scope.last_count = scope.clientImageObject.length;
                console.log(scope.last_object);
                $('.loading-images').fadeOut(100);
                $('.load-images-btn').removeClass('loader-state-btn');
                $('.load-images-btn').attr('disabled', false);
                // setTimeout(function() {
                //   $('#image_' + scope.last_object).animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});
                // }, 1000);
              } );
            }

            scope.next = function next(data) {
              console.log(data);
              $('.loading-images').fadeIn(100);
              $('.load-images-btn').addClass('loader-state-btn');
              $('.load-images-btn').attr('disabled', true);
              $http.get( data )
              .success(function(response){
                console.log(response);
                scope.next_page = response.next_page_url;
                 for(var x = 0; x < response.data.length; x++) {
                    scope.clientImageObject.push(response.data[x]);
                  }
                var length = scope.clientImageObject.length - scope.last_count;
                console.log( length );
                scope.last_object = scope.clientImageObject[ length ].image_id;
                 console.log(scope.clientImageObject.length);
                  $('.loading-images').fadeOut(100);
                  $('.load-images-btn').removeClass('loader-state-btn');
                  $('.load-images-btn').attr('disabled', false);
                  setTimeout(function() {
                    $('#image_' + scope.last_object).animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});
                  }, 1000);
                  if(scope.clientImageObject.length == response.total) {
                    $('.load-images-btn').fadeOut();
                  }
                   // setTimeout(function() {
                    // $('.bottom-call').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});
                  // }, 1000);
              });
            }

            scope.listImages();

          }
        }
      }
    }
  }
]);