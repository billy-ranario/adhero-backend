var graph = angular.module('graph', []);

graph.run(function(){
	console.log('graph module running');
});

graph
	.directive('graphPlotly', [
		"$http",
		"reportService",
    "Report",
		function directive( $http, reportService, Report ) {
			return {
				restrict: "A",
				scope: true,
				link: function link( scope, element, attributeSet ) {
					console.log('graph');
          scope.report_objects = [];
          scope.report_values_sub_category = [];
          scope.report_values = {};
          scope.report_obj = {};
          scope.tags = [];
          var acceptance = [];
          var anticipation = [];
          var interest = [];
          var joy = [];

				  Report
                .show( 'emotions ' )
                .then(function ( res ) {
                  console.log( res );
                  console.log( res.data.result.length );
                  scope.report_values = res;
                  for( var x = 0; x < res.data.result.length; x++ ) {
                    scope.report_values_sub_category.push( res.data.result[ x ].value[ 0 ] );
                    // if( res.data.result[ x ].value[ 0 ]  == "Acceptance" ) {
                    //   acceptance.push( res.data.result[ x ] );
                    // } else if( res.data.result[ x ].value[ 0 ]  == "Anticipation" ) {
                    //   anticipation.push( res.data.result[ x ]  );
                    // } else if( res.data.result[ x ].value[ 0 ] == "Interest") {
                    //   interest.push( res.data.result[ x ] );
                    // } else if( res.data.result[ x ].value[ 0 ] == "Joy") {
                    //   joy.push( res.data.result[ x ] );
                    // }
                  }

                  var new_value = _.uniq( scope.report_values_sub_category );
                  for( var a = 0; a < new_value.length; a++ ) {
                    for( var y = 0; y < scope.report_values.data.result.length; y++ ) {
                      if(new_value[ a ] == scope.report_values.data.result[ y ].value[ 0 ] ) {
                        var obj = {
                          value: new_value[ a ],
                          key: scope.report_values.data.result[ y ]
                        };
                        scope.report_objects.push( obj );
                      }
                      var tag = {
                        tags: scope.report_values.data.result[ y ].type,
                        ad_id: scope.report_values.data.result[ y ].ad_id
                      }
                      scope.tags.push( tag );
                    }
                  }



                  console.log( scope.report_objects );
                  console.log( scope.tags );
          });

          
				}
			}
		}
	]);
graph
  .directive('predictResultGraph', [
    "NgMap",
    "$window",
    function directive( NgMap , $window ) {
      return {
        restrict: "A",
        scope: true,
        link: function link( scope, element, attributeSet ) {
          scope.graph = {};
          scope.polar = {};

          scope.area = function area( ) {

              var data = [{
                values: [0.7, 0.3],
                labels: ['Game of Thrones actors', 'Game of Throne facts'],
                name: 'Themes',
                marker: { 
                  colors: ['#F34635', '#4BBEF7']
                },
                type: 'pie',
                // textinfo: 'none'
              }];

              var layout = {
                height: 450,
                width: 400,
                showlegend: false,
                legend: {
                  x: 0.3,
                  y: -0.3
                }
              };
              scope.data_1 = [];

              Plotly.newPlot('graph', data, layout, {displayModeBar: false});
              for( var x = 0; x < data[ 0 ].values.length; x++ ) {
                scope.data_1.push( { label: data[ 0 ].labels[ x ], value: (data[ 0 ].values[ x ] * 100).toFixed(0)+ "%", color: data[ 0 ].marker.colors[ x ] });
              }
          };

          scope.barGraph = function barGraph( ) {
            var trace1 = {
              x: [0, 1, 2, 3, 4],
              y: [10, 11, 21, 31, 4],
              name: 'Male',
              type: 'bar',
              marker: {
                color: '#F34635',
                thicknessmode: 10
              }
            };

            var trace2 = {
              x: [0, 1, 2, 3, 4],
              y: [12, 18, 29, 12, 18],
              name: 'Female',
              type: 'bar',
              marker: {
                color: '#4BBEF7',
                thicknessmode: 10
              }
            };

            var data = [trace1, trace2];
            var layout = { 
                title: 'Frequency',
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 480,
                bargap: 0.1,
                bargroupgap: 0.8,
                xaxis: { 
                  title: 'Age',
                  titlefont: {
                    size: 16,
                    color: '#A9ADB0',
                  },
                  showgrid: false,
                  showticklabels: false,
                  showline: true,
                },
                yaxis: {
                  showgrid: false,
                  showline: true,
                  showticklabels: false
                }
            };

            Plotly.newPlot('bar-graph', data, layout, {displayModeBar: false});
          };
          scope.data_pie = [];
          scope.data_pie2 = [];
          scope.pie = function pie( ) {
            var pie = new d3pie("graph", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 590,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Game of Thrones actors",
                    "value": 0.7,
                    "color": "#F34635"
                  },
                  {
                    "label": "Game of Throne facts",
                    "value": 0.3,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular"
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie.options.data.content );
            for( var x = 0; x < pie.options.data.content.length; x++ ) {
                scope.data_pie.push( { label: pie.options.data.content[ x ].label, value: pie.options.data.content[ x ].value, color: pie.options.data.content[ x ].color });
              }
            console.log( scope.data_pie );
          }

          scope.pie2 = function pie( ) {
            var pie2 = new d3pie("graph2", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 590,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Delicious food",
                    "value": 0.33,
                    "color": "#F34635"
                  },
                  {
                    "label": "SG50 baby",
                    "value": 0.4,
                    "color": "#4BBEF7"
                  },
                  {
                    "label": "Singapore flyer",
                    "value": 0.33,
                    "color": "#51851A"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular"
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie2.options.data.content );
            for( var x = 0; x < pie2.options.data.content.length; x++ ) {
                scope.data_pie2.push( { label: pie2.options.data.content[ x ].label, value: pie2.options.data.content[ x ].value, color: pie2.options.data.content[ x ].color });
              }
            console.log( scope.data_pie2 );
          }

          scope.pieGraph = function pieGraph( ) {
            var pie = new d3pie("pie-graph", {
    
              "size": {
                "canvasHeight": 300,
                "canvasWidth": 300,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Male",
                    "value": 65,
                    "color": "#F34635"
                  },
                  {
                    "label": "Female",
                    "value": 35,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "percentage",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });
          };


          scope.barGraph( );
          scope.pieGraph( );
          scope.pie( );
          scope.pie2( );

          
        }
      }
    }
]);
graph.directive('locationResult', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running locationResult directive' );

         scope.data_pie = [];
          scope.pie = function pie( ) {
            var pie = new d3pie("graph", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 570,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Foodie",
                    "value": 0.4,
                    "color": "#F34635"
                  },
                  {
                    "label": "Time with others",
                    "value": 0.22,
                    "color": "#4BBEF7"
                  },
                  {
                    "label": "Retail therapy",
                    "value": 0.1,
                    "color": "#51851A"
                  },
                  {
                    "label": "Travel adventure",
                    "value": 0.05,
                    "color": "#7745FF"
                  },
                  {
                    "label": "Relaxation alone",
                    "value": 0.23,
                    "color": "#C560FF"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular",
                  "fontSize": 11
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie.options.data.content );
            for( var x = 0; x < pie.options.data.content.length; x++ ) {
                scope.data_pie.push( { label: pie.options.data.content[ x ].label, value: pie.options.data.content[ x ].value, color: pie.options.data.content[ x ].color });
              }
            console.log( scope.data_pie );
          }
          scope.line = {};
          scope.lineGraph = function lineGraph( ) {
            console.log('line');
            scope.line.labels = ['', 'Engagement', 'Impressions', 'Likes'];
            scope.line.series = [];
            scope.line.data = [
              [0, 10, 5, 12, 20, 45, 30, 50, 70, 40]
            ];

            scope.line.options = {
              animation: false,
              scaleShowLabels : false,
              bezierCurve: false,
              scaleShowGridLines: false,
              scaleFontColor : "#000",
              pointDot : true,
              pointDotRadius: 0,
              datasetFill: false,
              scaleGridLineColor: 'white'
            };
          };

          scope.pieGraph = function pieGraph( ) {
            var pie = new d3pie("pie-graph", {
    
              "size": {
                "canvasHeight": 300,
                "canvasWidth": 200,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Male",
                    "value": 68,
                    "color": "#F34635"
                  },
                  {
                    "label": "Female",
                    "value": 32,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "percentage",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });
          };

          scope.barGraph = function barGraph( ) {
            var trace1 = {
              x: [0, 1, 2, 3, 4],
              y: [10, 25, 35, 25, 20],
              name: 'Male',
              type: 'bar',
              marker: {
                color: '#F34635',
                thicknessmode: 10
              }
            };

            var trace2 = {
              x: [0, 1, 2, 3, 4],
              y: [5, 15, 10, 15, 10],
              name: 'Female',
              type: 'bar',
              marker: {
                color: '#4BBEF7',
                thicknessmode: 10
              }
            };

            var data = [trace1, trace2];
            var layout = { 
                title: 'Frequency',
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 480,
                bargap: 0.1,
                bargroupgap: 0.8,
                xaxis: { 
                  title: 'Age',
                  titlefont: {
                    size: 16,
                    color: '#A9ADB0',
                  },
                  showgrid: false,
                  showticklabels: false,
                  showline: true,
                },
                yaxis: {
                  showgrid: false,
                  showline: true,
                  showticklabels: false
                }
            };

            Plotly.newPlot('bar-graph', data, layout, {displayModeBar: false});
          };

          scope.circleGraph = function circleGraph( ) {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            var val = [
            {
              title: 'Healthy Carbs',
              subtitle: '',
              data: 200
            },
            {
              title: 'Yoga Poses',
              subtitle: '',
              data: 250,
            },
            {
              title: 'Outdoor Running',
              subtitle: '12000 people',
              data: 400,
            },
            {
              title: 'Cardio Kickboxing',
              'subtitle': '',
              'data': 210,
            },
            {
              title: 'Gym Workouts',
              subtitle: '',
              'data': 190
            }
            ];

            scope.circle = [];
            for( var x = 0; x < val.length; x++ )
            {
              val[ x ].data /= Math.pow(9, 3);
              var colors = randomColor({
                 luminosity: 'random',
                 hue: 'random'
              });
              scope.circle.push({ title: val[ x ].title, subtitle: val[ x ].subtitle, data: val[ x ].data + 0.4, color: colors });
            }

            console.log( scope.circle );
          };

          scope.datePicks = {};

          scope.dates = ["01/12/16","01/23/16","03/14/16","03/26/16","04/01/16","05/14/16","06/07/16","07/23/16"," - "];

          scope.customDatePicker = function customDatePicker( num ){
            var temp;

            if( num == 0 ){

              temp = scope.dates.pop();
              scope.dates.unshift( temp );

            }else if( num == 1 ){

              temp = scope.dates.shift();
              scope.dates.push( temp );

            }

            scope.datePicks.one = scope.dates[ 0 ];
            scope.datePicks.two = scope.dates[ 1 ];
            scope.datePicks.three = scope.dates[ 2 ];
            scope.datePicks.four = scope.dates[ 3 ];
            scope.datePicks.five = scope.dates[ 4 ]; 

            console.log(scope.dates);
          }

          scope.pie( );
          scope.lineGraph( );
          scope.pieGraph( );
          scope.barGraph( );
          scope.circleGraph( );
          scope.customDatePicker( 2 );


      }
    }
  }
]);
graph
	.service('reportService', [
		function service( ) {
			var report = {};

			return {
				graphData: function graphData( obj_data, layout ) {
					if( obj_data && layout ) {
						return report = { data: obj_data, layout: layout };
					} else {
						return report;
					}
				},
				clearGraph: function graphData( ) {
					report = {};
					console.log( report );
				}
			}
		}
	]);