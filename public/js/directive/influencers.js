var influencers_info = angular.module('influencers_info', [ ]);

influencers_info.run(function( ){
	console.log( 'influencers_info module running' );
});

influencers_info
		.directive('influencers', [
			"$stateParams",
      "$http",
      "serverUrl",
      "$window",  
		function directive( $stateParams , $http, serverUrl, $window ) {
			return {
				restrict: "A",
				scpope: true,
				link: function link( scope, element, attributeSet ) {
					console.log( 'influencer directive running ');

					console.log( $stateParams.id );

          scope.pieID = null;
          scope.pieID2 = null;
					scope.line = {};
          scope.data_pie = [];
					scope.influence = {};


          scope.pieData = [
            [
              {
                "label": "Lifestyle Travelling",
                "value": 0.35,
                "color": "#3366CC"
              },
              {
                "label": "Fashion Photoshoot",
                "value": 0.30,
                "color": "#DC3912"
              },
              {
                "label": "Luxury Food Indulgence",
                "value": 0.20,
                "color": "#FF9900"
              },
              {
                "label": "Fashion Haul",
                "value": 0.15,
                "color": "#109618"
              },
            ],
            [
              {
                "label": "Fashion Photoshoot",
                "value": 0.40,
                "color": "#3366CC"
              },
              {
                "label": "Lifestyle Travelling",
                "value": 0.45,
                "color": "#DC3912"
              },
              {
                "label": "Fashion Haul",
                "value": 0.05,
                "color": "#FF9900"
              },
              {
                "label": "Food Indulgence",
                "value": 0.10,
                "color": "#109618"
              },
            ],
            [
              {
                "label": "Living the Yoga Lifestyle",
                "value": 0.10,
                "color": "#3366CC"
              },
              {
                "label": "Fashion Haul",
                "value": 0.10,
                "color": "#DC3912"
              },
              {
                "label": "Healthy Self Cooked Meal",
                "value": 0.45,
                "color": "#FF9900"
              },
              {
                "label": "Animal Lover",
                "value": 0.15,
                "color": "#109618"
              },
              {
                "label": "Fashion Photoshoot",
                "value": 0.20,
                "color": "#990099"
              },
            ],
            [
              {
                "label": "Fashion Photoshoot",
                "value": 0.60,
                "color": "#3366CC"
              },
              {
                "label": "Fashion Haul",
                "value": 0.30,
                "color": "#DC3912"
              },
              {
                "label": "Luxury Food Indulgence",
                "value": 0.05,
                "color": "#FF9900"
              },
              {
                "label": "Lifestyle Travelling",
                "value": 0.05,
                "color": "#109618"
              },
            ],
            [
              {
                "label": "Lifestyle Travelling",
                "value": 0.40,
                "color": "#3366CC"
              },
              {
                "label": "Fashion Photoshoot",
                "value": 0.50,
                "color": "#DC3912"
              },
              {
                "label": "Luxury Food Indulgence",
                "value": 0.05,
                "color": "#FF9900"
              },
              {
                "label": "fashion Haul",
                "value": 0.05,
                "color": "#109618"
              },
            ],
          ];

          scope.pieGraphData = [
            [
              {
                "label": "Female",
                "value": 0.8,
                "color": "#F34635"
              },
              {
                "label": "Male",
                "value": 0.2,
                "color": "#4BBEF7"
              },
            ],
            [
              {
                "label": "Female",
                "value": 0.8,
                "color": "#F34635"
              },
              {
                "label": "Male",
                "value": 0.2,
                "color": "#4BBEF7"
              },
            ],
            [
              {
                "label": "Female",
                "value": 0.8,
                "color": "#F34635"
              },
              {
                "label": "Male",
                "value": 0.2,
                "color": "#4BBEF7"
              },
            ],
            [
              {
                "label": "Female",
                "value": 0.8,
                "color": "#F34635"
              },
              {
                "label": "Male",
                "value": 0.2,
                "color": "#4BBEF7"
              },
            ],
            [
              {
                "label": "Female",
                "value": 0.8,
                "color": "#F34635"
              },
              {
                "label": "Male",
                "value": 0.2,
                "color": "#4BBEF7"
              },
            ],
          ];

          scope.barGraphData = [
            [
              [10, 11, 21, 31, 4],
              [24, 18, 29, 12, 18]
            ],
            [
              [15, 22, 42, 62, 8],
              [48, 18, 29, 24, 36]
            ],
            [
              [20, 11, 21, 31, 4],
              [16, 18, 29, 52, 18]
            ],
            [
              [25, 11, 21, 31, 4],
              [12, 18, 29, 22, 78]
            ],
            [
              [30, 11, 21, 31, 4],
              [14, 18, 29, 32, 18]
            ],
          ];


          scope.influence_user = {};
					scope.gallery = {};

          scope.gallery_engagers = [
            [
              
            ],
            [
              [10,10,17,25,8,6,6,30,21,11]
            ],
            [
              [6,7,5,10,2,4,8,7,3,4]
            ],
            [
              [6,8,5,9,3,5,1,2,11,11]
            ],
            [
              [1,2,1,1,1,1,2,5,5,1]
            ],
          ];

					for( var x = 0; x < scope.influence.length; x++ ) {
						if( $stateParams.id == scope.influence[ x ].id )
						{
							setTimeout(function() {
								$('.influence_user_' + $stateParams.id ).addClass('high-light-influencer');
							}, 500);
              scope.influence_user.username = scope.influence[ x ].username;
              scope.influence_user.image = scope.influence[ x ].image;
              scope.influence_user.posts = scope.influence[ x ].posts;
              scope.influence_user.followers = scope.influence[ x ].followers;
              scope.influence_user.following = scope.influence[ x ].following;
							scope.influence_user.id = x + 1;
							console.log( scope.influence[ x ] );
						}
					}

          scope.getInfluence = function getInfluence( ) {
            $http.get( serverUrl.url + "json/influence.js" )
            .success(function( response ){
              console.log( response );
              scope.influence = response;
    					for( var x = 0; x < scope.influence.length; x++ ) {
    						if( $stateParams.id == scope.influence[ x ].id )
    						{
    							setTimeout(function() {
    								$('.influence_user_' + $stateParams.id ).addClass('high-light-influencer');
    							}, 500);
                  scope.influence_user.image = scope.influence[ x ].image;
                  scope.influence_user.posts = scope.influence[ x ].posts;
                  scope.influence_user.followers = scope.influence[ x ].followers;
                  scope.influence_user.following = scope.influence[ x ].following;
    							scope.influence_user.id = x + 1;
    							console.log( scope.influence[ x ] );
    						}
    					}
            });
          }

					scope.selectInfluencer = function selectInfluencer( response ) {
						console.log( response );
						$('.global_influence').removeClass('high-light-influencer');
						$('.influence_user_' + response.id).addClass('high-light-influencer');

						scope.influence_user = response;


            scope.pieID.destroy();
            scope.pie( (response.id - 1) );

            scope.pieID2.destroy();
            scope.pieGraph( (response.id - 1) );

            scope.barGraph( (response.id - 1) );

					}

          scope.selectGallery = function selectGallery( id , index ) {

            console.log( id + " " + index);

            scope.gallery.index = index + 1;
            
            scope.gallery.likes = scope.influence[ id-1 ].likes[ index ];
            scope.gallery.comments = scope.influence[ id-1 ].comments[ index ];
            scope.gallery.engagements = scope.influence[ id-1 ].engagements[ index ];
            scope.gallery.images = scope.influence[ id-1 ].gallery[ index ];

            console.log( scope.gallery.images );
          }

          
          scope.pie = function pie( index ) {
            scope.pieID = new d3pie("graph", {
    
              "size": {
                "canvasHeight": 300,
                "canvasWidth": 490,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": scope.pieData[index]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "mainLabel": {
                  "font": "Montserrat-Regular",
                  "size": 15
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            

          };
          
          scope.pieGraph = function pieGraph( index ) {
            scope.pieID2 = new d3pie("pie-graph", {
    
              "size": {
                "canvasHeight": 350,
                "canvasWidth": 350,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": scope.pieGraphData[index]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "mainLabel": {
                  "font": "Montserrat-Regular",
                  "size": 15
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });
          };

          

          scope.barGraph = function barGraph( index ) {
            var trace1 = {
              x: [0, 1, 2, 3, 4],
              y: scope.barGraphData[index][0],
              name: 'Male',
              type: 'bar',
              marker: {
                color: '#F34635',
                thicknessmode: 10
              }
            };

            var trace2 = {
              x: [0, 1, 2, 3, 4],
              y: scope.barGraphData[index][1],
              name: 'Female',
              type: 'bar',
              marker: {
                color: '#4BBEF7',
                thicknessmode: 10
              }
            };

            var data = [trace1, trace2];
            var layout = { 
                title: 'Frequency',
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 480,
                bargap: 0.1,
                bargroupgap: 0.8,
                xaxis: { 
                  title: 'Age',
                  titlefont: {
                    size: 16,
                    color: '#A9ADB0',
                  },
                  showgrid: false,
                  showticklabels: false,
                  showline: true,
                },
                yaxis: {
                  showgrid: false,
                  showline: true,
                  showticklabels: false
                }
            };

            Plotly.newPlot('bar-graph', data, layout, {displayModeBar: false});
          };

          

          console.log( scope.dates );

          scope.datePicks = {};

          scope.dates = ["01/12/16","01/23/16","03/14/16","03/26/16","04/01/16","05/14/16","06/07/16","07/23/16"," - "];

          scope.customDatePicker = function customDatePicker( num ){
            var temp;

            if( num == 0 ){

              temp = scope.dates.pop();
              scope.dates.unshift( temp );

            }else if( num == 1 ){

              temp = scope.dates.shift();
              scope.dates.push( temp );

            }

            scope.datePicks.one = scope.dates[ 0 ];
            scope.datePicks.two = scope.dates[ 1 ];
            scope.datePicks.three = scope.dates[ 2 ];
            scope.datePicks.four = scope.dates[ 3 ];
            scope.datePicks.five = scope.dates[ 4 ]; 

            console.log(scope.dates);
          }

          scope.pie( ($stateParams.id-1) );
          scope.pieGraph( ($stateParams.id-1) );
          scope.barGraph( ($stateParams.id-1) );
          scope.customDatePicker( 2 );
          scope.getInfluence();
				}
			}
		}
]);