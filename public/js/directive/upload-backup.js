var upload = angular.module('upload', ['ngMaterial']);

upload.run(function() {
  console.log('upload running');
})
upload
  .factory('nodeUrl',[
        function factory(){
          return {
            // url: 'http://52.77.17.243/',
            url: 'http://localhost:8000/',
            header: {
              headers: {
                'Content-Type' : 'application/x-www-form-urlencoded',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
              }
            }
          }
        }
    ]);
upload
  .directive('uploadClient',[
  "$http",
  "nodeUrl",
  "serverUrl",
  "$mdDialog", 
  "$mdMedia",
  function directive( $http, nodeUrl, serverUrl, $mdDialog, $mdMedia ) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log('client upload');

        var imgData = {};
        scope.userImageUploads = {};
        scope.preview = 'telstra_images/10.jpg';
        scope.has_img = false;
        scope.selectedImg = "";
        scope.is_preview = false;
        scope.IMGfilename = '';

        scope.getUserInfo = function getUserInfo () {
          $http.get( serverUrl.url + 'getuserdata' )
            .success( function success(response) {
              console.log(response);
              imgData._id = response[0]._id; 
              if ( response[0].client_images ) {
                scope.userImageUploads = response[0].client_images;
                scope.preview = 'http://52.77.17.243/get_client_personal_upload/' + response[0].client_images[0].filename;
                scope.has_img = true;
                if (response[0].client_images[0]) {

                } else {

                }

              } else {
                console.log('no image');
              }
              console.log( scope.has_img );
              // scope.fetchPersonalImage( response[0]._id );
            } );
        }

        scope.downloadImg = function downloadImg() {
          var file = {};
          file.filename = scope.IMGfilename;
          file.filepath = scope.preview;
          console.log(file);
          $http.post( serverUrl.url + 'downloadImage' , file )
            .success( function( response ) {
              console.log( response );
            } );
        }
        scope.getUserInfo( );

        scope.testApi = function testApi( ) {
          console.log('test')
          $http.get(nodeUrl.url)
          .success(function( response ){
            console.log('res', response );
          })
          .error(function( error ){
            console.log( error );
          });
        }

        scope.showUploadInput = function showUploadInput () {
          $( '#imgupload-wrapper' ).fadeIn();
        }
        scope.previewImg = function previewImg (filename) {
          scope.preview = 'http://52.77.17.243/get_client_personal_upload/' + filename;
          scope.IMGfilename = filename;
        }
        scope.previewImgDemo = function previewImgDemo (filename) {
          scope.preview = 'telstra_images/' + filename;
        }
        scope.images = false;

        
        scope.uploadsImg = [];

        scope.uploadFile = function uploadFile() {
          console.log('initialize upload');
          $("#input-id").fileinput({  
              uploadUrl: nodeUrl.url + 'client_upload',// server upload action
                uploadAsync: true,
                uploadExtraData: function(previewId,index) {  // callback example
                    var out = {'image_question' : $('.kv-input:eq('+index+')').val() };
                    return out;
                }
            }).on('filepreupload', function(event, data, previewId, index) {
              $('#kv-success-1').html('<h4>Upload Status</h4><ul></ul>').hide();
            }).on('fileuploaded', function(event, data, id, index) {
              // clarifai api
              imgData.filename = data.response.image;
              scope.uploadsImg.push( data.response.image );
              console.log( scope.uploadsImg );
              $http.post( serverUrl.url + 'userImageUploads' , imgData )
                .success( function success ( response ) {
                  console.log( response );
                  $( '#imgupload-wrapper' ).fadeOut();
                  scope.images = true;
                  scope.has_img = false;
                  scope.getUserInfo();
                } );

            }).on("filepredelete", function(jqXHR) {
                var abort = true;
                if (confirm("Are you sure you want to delete this image?")) {
                    abort = false;
                }
                return abort; // you can also send any data/object that you can receive on `filecustomerror` event
            });

        }
        scope.deleteImg = function deleteImg ( ev, filename ) {
          var postData = {
            '_id'     : imgData._id,
            'filename'  : filename.filename
          };
           var confirm = $mdDialog.confirm()
                  .title('Would you like to delete this image?')
                  .textContent('This image will not be recovered.')
                  .ariaLabel('Lucky day')
                  .targetEvent(ev)
                  .ok('Okay')
                  .cancel('Cancel');
              $mdDialog.show(confirm).then(function() {
                  scope.userImageUploads.splice(scope.userImageUploads.indexOf(filename), 1);           
              $http.post( serverUrl.url + 'deleteUserImage' , postData )
              .success( function success ( response ) {
                console.log( response );
                console.log('deleted');
              } );
            }, function() {
              console.log('cancel');
            });

        }
        scope.uploadFile();
        scope.testApi();
      }
    }
  }
]);