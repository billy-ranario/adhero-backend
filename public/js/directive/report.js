var report = angular.module('report', []);

report.run( function( ) {
	console.log( 'running report module' );
});

report
	.factory( 'serverUrl',  [
		function( ) {
			return {
				url: window.location.origin + "/",
				header: {
					headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
				}
			}
		}
]);

report
	.directive( 'reportInformation', [
		"$http",
		"serverUrl",
		function directive( $http, serverUrl ) {
			return {
				restrict: "A",
				scope: true,
				compile: function compile( element, attributeSet ) {

					return {
						pre: function pre( scope, element, attributeSet ) {
							console.log( 'running report Information directive' );
						},
						post: function post( scope, element, attributeSet ) {

						}
					}
				}
			}
		}
]);
