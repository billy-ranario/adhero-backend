var brief = angular.module('brief', []);

brief.run( function( ) {
	console.log( 'running brief module' );
});

brief
	.factory( 'serverUrl',  [
		function( ) {
			return {
				url: window.location.origin + "/",
				header: {
					headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
				}
			}
		}
]);

brief
	.directive( 'briefInformation', [
		"$http",
		"serverUrl",
		function directive( $http, serverUrl ) {
			return {
				restrict: "A",
				scope: true,
				compile: function compile( element, attributeSet ) {

					return {
						pre: function pre( scope, element, attributeSet ) {
							console.log( 'running briefInformation directive' );
						},
						post: function post( scope, element, attributeSet ) {

						}
					}
				}
			}
		}
]);
