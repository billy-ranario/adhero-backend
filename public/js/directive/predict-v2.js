var predict = angular.module('predict', []);

predict.run(function(){
	console.log('predict module running...');
});
predict.factory('serverUrl',[
    function factory(){
      return {
        url: 'http://52.74.62.3/',
        // url: 'http://localhost:8000/',
        header: {
          headers: {
            'Content-Type' : 'application/x-www-form-urlencoded'
            // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
        }
      }
    }
]);
predict.directive('predictView', [
	"$http",
	"$state",
	"serverUrl",
	"predictService",
	function directive( $http, $state, serverUrl, predictService ) {

		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('predict view directive');
				$('.treeview').removeClass('active');
				$('.predict-menu').addClass('active');
				// $scope.defaultPic = "";

				scope.predictObject = {};

				// Set Default to Select Tags
				scope.predictObject.product_category = null;
				scope.predictObject.gender = null;
				scope.predictObject.objective = null;

				scope.defaultPic = "samplepage_background.jpg";
				$( '.predict-submenu-list' ).slideDown();
				// scope.predictObject = {};
				scope.is_demo_img = false;

				scope.close = function close (id) { // Close the alert msg box
					$( '#predict-' + id ).fadeOut(  );
				}

				// var gender_list = [];
				// scope.toggleBoxGender = function toggleBox ( data ) {
				// 	var idx = gender_list.indexOf( data );
				// 	if (idx > -1) gender_list.splice(idx, 1);
    //     			else gender_list.push(data);
				// 	scope.predictObject.targetgender = gender_list;
				// };
				// var library_list = [];
				// scope.toggleBoxLib = function toggleBoxLib ( data ) {
				// 	var idx = library_list.indexOf( data );
				// 	if (idx > -1) library_list.splice(idx, 1);
    //     			else library_list.push(data);
				// 	scope.predictObject.recommendedlibraries = library_list;
				// };
				// scope.showInput = false;
				// scope.selectEvent = function selectEvent() {
				// 	if ( scope.predictObject.event == 'Others' ) {
				// 		scope.showInput = true;						
				// 	}else {
				// 		scope.showInput = false;	
				// 	}
				// }
				// Submit Predict Queries
				scope.createPredict = function createPredict() {
					var width = 0;
					if ( scope.showInput ) {
						scope.predictObject.event = scope.predictObject.otherEvent;
						$( 'option[value="Others"]' ).attr( 'selected' , true );
					};

					if ( scope.predictObject.product_category == null || 
						scope.predictObject.product_name == null || 
						scope.predictObject.country == null || 
						scope.predictObject.gender == null || 
						scope.predictObject.objective == null || 
						scope.predictObject.hashtags == null || 
						scope.predictObject.mediaplatform == null
					) {
						sweetAlert("Oops...", "Dont leave the fields empty.", "error");
					}else {

						if ( scope.is_demo_img  == false ) {	
					       $http.post( serverUrl.url + 'createPrediction', scope.predictObject )
								.success(function success(response ){
									console.log(response);
					      })	
						};		
						$('.content-manager').fadeOut(500, function(){
							$('.loading-predict-container').fadeIn(500);
							scope.close( 'msg001' );
							setTimeout(function() {
								$( '#predict-progress' ).css( 'width' , '100%' );
							}, 10);
						})			
					}
					console.log( scope.predictObject );			
				}

				scope.socialPlatform = function socialPlatform( platform ) {
					scope.predictObject.mediaplatform = platform;
					console.log(platform);
					$( '.icon-media' ).removeClass( 'active-img-predict' );
					if( platform == 'facebook' )
					{
						$( '#sm-fb img' ).addClass( 'active-img-predict' );
					} else if( platform == 'instagram' ){
						$( '#sm-insta img' ).addClass( 'active-img-predict' );
					} else if( platform == 'linkedin' ) {
						$( '#sm-in img' ).addClass( 'active-img-predict' );
					}
				}


				scope.useraction = function useraction ( option ) { // Sets default user action for Demo or Skip
					scope.close( 'msg001' ) // Removes the alert msg box
					if ( option == 'demo' ) {
						predictService.predictStatus("true");
						$( '#sm-fb' ).addClass( 'active' );
						scope.predictObject.product_category = 'Recontract'; // Will be filled up the heading textbox
						scope.predictObject.product_name = 'Demo Product'; // Selected platform  
						scope.predictObject.country = 'Philippines'; // Selected Target Gender  
						scope.predictObject.gender = 'Male'; // Selected Geography  
						scope.predictObject.objective = 'App Installs'; // Selected Geography  

						scope.predictObject.mediaplatform = 'facebook'; // Selected Target object  
						scope.predictObject.hashtags = '#family, #relationship, #indoor';  

						scope.predictObject.demo = true;
						scope.is_demo_img = true;
						console.log( scope.predictObject );

					}else if( option == 'skip' ) {
						scope.is_demo_img = false;
						scope.predictObject.demo = false;
						scope.predictObject = {}; // Remove all defaults
					}
				}

				scope.demoImages = function demoImages () {

					predictService.set( "imageData" , null );					
					$state.go('predict-result');
					// scope.is_demo_img = true;
					// scope.defaultPic = "samplepage_background.jpg";
					// $('.loading-predict-container').fadeOut(500,function(){
					// 	$('.predict-result-container').fadeIn(500);
					// 	scope.doughNut();
					// });
				}
			}
		}
	}
]);

predict.directive('predictResult', [
	"$http",
	"$state",
	"serverUrl",
	"predictService",
	function directive( $http, $state, serverUrl, predictService ) {

		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				scope.predictResult = { };
				scope.defaultImage = "photo1.png";
				scope.clientImageObject = {};



				/*scope.newChart = function newChart( data ) {
					console.log(data);
					console.log('new chart');
					var less_value = 100 - data;
					console.log(less_value); 
					var data = [
				    {
				        value: data.engaging_score,
				        color:"#2EAEB3",
				        highlight: "#2EAEB3",
				    },
				    {
				        value: less_value,
				        color: "#CED2D6",
				    },
					]
					var ctx = document.getElementById("myChart").getContext("2d");
					var myDoughnutChart = new Chart(ctx).Doughnut(data);
				}*/		


				// Default Gallery Image Data ( Image 1 )
				scope.imgPreview = '../img/predict-gallery-v2/1.png';
				scope.galleryImageData = {
					predicted_performance	: 45,
					content_type		  	: 'User-generated image',
					emotion					: 'Happiness',
					emotion_val				: '0.4',
					people					: [ 'Female' , 'Young adult' ],
					people_val				: '0.6',
					theme					: [ 'In a room' , 'Indoor' ],
					theme_val				: '0.9',
					colours					: 'Off-white',
					colours_val				: '0.1',
					aesthetic				: '0.60'
				};
				
				scope.previewGalleryImg = function previewGalleryImg( filepath , data ) {
					scope.imgPreview = filepath;
					
					if ( data == 1 ) {
						scope.galleryImageData = {
							predicted_performance	: 45,
							content_type		  	: 'User-generated image',
							emotion					: 'Happiness',
							emotion_val				: '0.4',
							people					: [ 'Female' , 'Young adult' ],
							people_val				: '0.6',
							theme					: [ 'In a room' , 'Indoor' ],
							theme_val				: '0.9',
							colours					: 'Off-white',
							colours_val				: '0.1',
							aesthetic				: '0.60'
						}
					}else if( data == 2 ) {
						scope.galleryImageData = {
							predicted_performance	: 55,
							content_type		  	: 'Stock Image',
							emotion					: 'Trust',
							emotion_val				: '0.4',
							people					: [ 'Male' , 'Young adult' ],
							people_val				: '0.6',
							theme					: [ 'In a room' , 'Indoor' ],
							theme_val				: '0.6',
							colours					: 'Black ',
							colours_val				: '0.2',
							aesthetic				: '0.45'
						}
						
					}else if( data == 3 ) {
						scope.galleryImageData = {
							predicted_performance	: 30,
							content_type		  	: 'Stock Image',
							emotion					: 'N/A',
							emotion_val				: 'N/A',
							people					: [ 'N/A' ],
							people_val				: 'N/A',
							theme					: [ 'Nature' , 'Outdoor' ],
							theme_val				: '0.6',
							colours					: 'Blue',
							colours_val				: '0.8',
							aesthetic				: '0.55'
						}
						
					}else if( data == 4 ) {
						scope.galleryImageData = {
							predicted_performance	: 45,
							content_type		  	: 'User-generated image',
							emotion					: 'Happiness',
							emotion_val				: '0.4',
							people					: [ 'Female' , 'Kid' ],
							people_val				: '0.6',
							theme					: [ 'In a car' , 'Indoor' ],
							theme_val				: '0.9',
							colours					: 'Off-white',
							colours_val				: '0.1',
							aesthetic				: '0.60'
						}
						
					}else if( data == 5 ) {
						scope.galleryImageData = {
							predicted_performance	: 80,
							content_type		  	: 'User-generated image',
							emotion					: 'Joy',
							emotion_val				: '0.6',
							people					: [ 'Female' , 'Young Adult' ],
							people_val				: '0.9',
							theme					: [ 'Outdoor' ],
							theme_val				: '0.2',
							colours					: 'Grey',
							colours_val				: '0.4',
							aesthetic				: '0.65'
						}
						
					}else if( data == 6 ) {
						scope.galleryImageData = {
							predicted_performance	: 75,
							content_type		  	: 'Stock Image',
							emotion					: 'Joy',
							emotion_val				: '0.65',
							people					: [ 'Male' , 'Young adult' ],
							people_val				: '0.55',
							theme					: [ 'Urban' , 'Outdoor' ],
							theme_val				: '0.7',
							colours					: 'Off-white',
							colours_val				: '0.3',
							aesthetic				: '0.85'
						}
						
					}
				};
				scope.newChart = function newChart (data) {
					var less_value = 100 - parseFloat( data.engaging_score );
					console.log( less_value );
					var data = [
				    {
				        value: data.engaging_score,
				        color:"#2EAEB3",
				        highlight: "#2EAEB3",
				    },
				    {
				        value: less_value,
				        color: "#CED2D6",
				    },
					]
					var ctx = document.getElementById("myChart").getContext("2d");
					var myDoughnutChart = new Chart(ctx).Doughnut(data);
				};

				scope.getData = function getData ( ) {
					scope.predictResult = predictService.imageData( );	
					console.log( scope.predictResult );
					// scope.newChart( 
					// 	( (scope.predictResult) ? scope.predictResult.client_upload_data[0] : {
					// 		"engaging_score": 75
					// 	} )
					// );					
				};

				scope.getData( );

				scope.imgObject = {};
				scope.showPicture = function showPicture ( image ) {					
					if ( !scope.predictResult ) {					
						scope.defaultImage = image;						
					}else {
						// scope.newChart(image.client_upload_data[0]);
						scope.defaultPic = image.filename;	
						scope.imgObject  = image;
						console.log( image );
						
						scope.predictResult = image;
					}
				}
						
				scope.fetchImages = function fetchImages( category ) {
		    	$http.get( serverUrl.url + 'getCategoryImages/' + category )
		    		.success( function success( response ) {
		    			scope.clientImageObject = response;
		    		} );
		    }

				scope.fetchImages( 'client_upload' );
			}
		}
	}
]);