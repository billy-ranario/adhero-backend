var image = angular.module('image', []);

image.run( function( ) {
	console.log( 'running image module' );
});

image
	.factory( 'serverUrl',  [
		function( ) {
			return {
				url: window.location.origin + "/",
				header: {
					headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
				}
			}
		}
]);

image
	.directive( 'imageRequest', [
		"$http",
		"serverUrl",
		function directive( $http, serverUrl ) {
			return {
				restrict: "A",
				scope: true,
				compile: function compile( element, attributeSet ) {

					return {
						pre: function pre( scope, element, attributeSet ) {
							console.log( 'running imageRequest directive' );
						},
						post: function post( scope, element, attributeSet ) {

						}
					}
				}
			}
		}
]);
