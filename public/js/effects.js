$(function(){

// $("html").niceScroll();

 var shrinkHeader = 50;
 var measuredHeader = 400;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
        console.log('shrink');
        $('.custom-navbar-default').addClass('custom-navbar-default-transform');
        $('.custom-navbar-brand img').addClass('transform-logo');
        $('.menus-li a').addClass('menus-li-a-transform');
        $('.bottom-content').fadeIn(500);
        $('.bottom-top-call').addClass('bottom-top-call-animate');
      } else {
        console.log('not shrink');
        $('.custom-navbar-default').removeClass('custom-navbar-default-transform');
        $('.custom-navbar-brand img').removeClass('transform-logo');
        $('.menus-li a').removeClass('menus-li-a-transform');
        $('.bottom-content').fadeOut(1000);
        $('.bottom-top-call').removeClass('bottom-top-call-animate');
      }
  });
  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  };

   $(".scroll").click(function(event){
       event.preventDefault();
       //calculate destination place
       var dest=0;
       if($(this.hash).offset().top > $(document).height()-$(window).height()){
            dest= 29 - $(document).height()-$(window).height();
            console.log( dest );
       }else{
            dest= $(this.hash).offset().top - 80;
            console.log( dest );
       } 
       //go to destination
       $('html,body').animate({scrollTop:dest}, 1000,'swing');
   });
   
});