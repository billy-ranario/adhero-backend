<?php

namespace App;

// use DB;
// use Illuminate\Database\Eloquent\Model;

// use Jenssegers\Eloquent\Model as Eloquent;

use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Eloquent implements AuthenticatableContract,
                                       AuthorizableContract,
                                       CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $collection = 'users';

    protected $guarded = [];    // for mass assignment

    protected $hidden = ['password'];


    // Note to jhon
    // - create mutator
    // - add role field for client and admin then guard mass assignment


    /*public function getAccountStatus( $id )
    {
    	$query = DB::table('users')->where('uid' , $id)->pluck('account_status');
    	return $query;
    }
    public function storeUserInfo( $data )
    {
        $table = '';
        
        if ( $data['usertype'] == 'customer' )
            $table = 'user_info';
        else
            $table = 'creative_info';

    	$updateStatus = DB::table( 'users' )
    					->where('uid', $data['uid'])
    					->update(['account_status' => true]);
    	
        $query = DB::table( $table )
    			 ->insert(
			    	[ 
                        'uid'       => $data['uid'], 
                        'fullname'  => $data['fullname'] , 
                        'region'    => $data['region'] 
                    ]
				 );

        return 'true';
    }*/
}