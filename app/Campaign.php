<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public function saveBriefCampaign( $data )
    {
    	$response = 'success';
		try {
			DB::table('campaign')->insert( $data );
		}catch(\Exception $e){
			$response = 'error';
		}
		return $response;
    }
    public function getUserCampaign( $id )
    {
    	$response = 'success';    	
    	try {	
			$query = DB::table( 'campaign' )->where( 'uniqid' , '=' , $id )->get();
		}catch(\Exception $e){
			$query = 'Error on fetching data from table';
			$response = 'error';
		}
		return array( 'status' => $response , 'data' => $query );
    }
    public function retrieveUserCampaignByStatus( $status )
    {
        $response = 'success';      
        try {   
            $query = DB::table( 'campaign' )
                ->where( 'uniqid' , '=' , session('uniqid') )
                ->where( 'status' , '=' , $status )
                ->get();
        }catch(\Exception $e){
            $query = 'Error on fetching data from table';
            $response = 'error';
        }
        return array( 'status' => $response , 'data' => $query );
    }
    public function updateCampaign( $data ) 
    {		
    	$response = 'success';    	
    	try {	
			$query = DB::table('campaign')->where('id', $data['id'])->update( $data );
		}catch(\Exception $e){
			$response = 'error';
		}
		return $response;		
    }
    public function deleteCampaign( $id )
    {
    	$response = 'success';    	
    	try {	
			$query = DB::table('campaign')->where( 'id', '=' , $id )->delete();
		}catch(\Exception $e){
			$response = 'error';
		}
		return $response;
    }
    public function checkForCampaign( $id ) //Checks if user has created campaigns before
    {
    	$response = DB::table('campaign')->where( 'uniqid' , '=' , $id )->count();
    	return $response;
    }
}
