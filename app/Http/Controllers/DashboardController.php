<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Fileentry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Contracts\Filesystem\Filesystem;

use Auth;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        // $user = User::find(Auth::id());

        return view('dashboard.client');
    }


    public function loginAdmin()
    {
        return view('login-admin');
    }
    public function authAdmin(Request $request)
    {
        $this->validate($request, [
                'email'     => 'required|email',
                'password'  => 'required'
            ]);

        $credentials = [
            'email'     => $request->get('email'),
            'password'  => $request->get('password'),
            'has_role'  => 1 // admin
        ];

        if (Auth::attempt($credentials)) {
            return redirect()->intended('admin');
        }

        return redirect('login-admin')->withMsg('<div class="alert alert-danger">Invalid Email/Password</div>');
    }
    public function logoutAdmin()
    {
        Auth::logout();

        return redirect('login-admin')
                ->withMsg('<div class="alert alert-success">Successfully logged out!</div>');
    }

    public function admin()
    {
        return view(' dashboard.admin');
    }

    public function upload(Request $request)
    {
        $image = $request->file('image');
        $imageFileName = time() . '.' . $image;
        $s3 = \Storage::disk('s3');
        $filePath = $imageFileName;

        return $s3->put($filePath, file_get_contents($image), 'public');
    }
}
