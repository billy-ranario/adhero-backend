<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Upload;

use App;

use Excel;

class DashboardController extends Controller
{
    public function index()
    {
    	return view('dashboard.admin');
    }
    public function getClarifaiData()
    {
        // echo "<pre>";
    	// echo var_dump(Upload::where( 'category' , 'api_upload' )->get());
        // echo "</pre>";
        $data_array = array();

        // $data = Upload::where( 'category' , 'api_upload' )->select( 'filename' , 'clarifai' )->get();
        // foreach ($data as $key => $img) {
        //     // echo "Filename:" .$img['filename']. "<br>";
        //     array_push( $data_array , $img['filename']);
        //     foreach ($img['clarifai'] as $key2 => $clfy) {
        //         array_push( $data_array , $clfy);
        //     }
        // }
        // $xls_arr = array();
        // $xls_arr['filename'] = $data_array[0];
        // $xls_arr['clarifai'] = $data_array[1];
        echo "<pre>";
        var_dump( $data );
        echo "</pre>";
    }
    public function exportCSV()
    {
        $data = Upload::where( 'category' , 'api_upload' )->select( 'filename' , 'clarifai' )->get();

        $data_array = array();

        foreach ($data as $key => $value) {
            foreach ($value['clarifai'] as $key => $clfy) {
                $data_array[ 'filename' ]   = $value['filename'];
                // $data_array[ 'tags' ]       = $value2;
                // echo "<pre>";
                // var_dump($clfy);
                // echo "</pre>";
                array_push( $data_array , $clfy);
                
            }
        }


        $xls = array();  

        // $xls = array(
        //     'dasdasdadadasdd',
        //     array(  )
        // );


        $tag = array();

        foreach ($data_array[0] as $key => $value) {
            // $tag[0] = $value['conceptId'];
            // $tag[1] = $value['class'];
            array_push( $tag , array( 
                    'Filename' => $data_array[ 'filename' ], 
                    'class' => $value['class'] ,
                    'probability' => $value['probability'] 
                ) 
            );
        }

        // echo "<pre>";      
        // var_dump($tag);
        // echo "</pre>";



        Excel::create('clarfiy', function( $excel ) use( $tag ) {
            $excel->sheet('Clarifai', function($sheet) use ( $tag ) {
                $sheet->fromArray($tag);
            });
        })->export('xls');
       
    }
}
