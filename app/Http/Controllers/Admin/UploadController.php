<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Upload;
use App\User;
use DB;
use Response;

use Jleagle\Imagga\Imagga;

class UploadController extends Controller
{

	public function index()
	{
		return DB::collection('uploads')->paginate(5);	
	}


	public function storeDataImage( Request $request )
	{
		// Imagga
    	/*$url = 'http://52.77.17.243/upload_get_api/' . $request->get('image');

		$apiKey 	= "acc_a86099052dc6b27";
		$apiSecret	= "2bee0283f94898ff0539e70cbc563fa0";

		$imagga = new Imagga($apiKey, $apiSecret);

		$tags = $imagga->tags($url);

		$colors = $imagga->colors($url);*/

		$data = array(
			'image_id'		=> time().rand( 10,99 ),
			'category'		=> $request->get( 'category' ),
			'filename' 		=> $request->get( 'image' ),
			// 'client_upload_data' => $request->get( 'data' )		
		);
		if ( $request->get( 'category' ) == 'api_upload' ) {
			$data['clarifai'] 		= $request->get( 'clarifai' );
			$data['graymatic']		= $request->get( 'graymatic' );
			/*$data['imagga_tags']	= $tags;
			$data['imagga_colors']	= $colors;*/
		}
		$response = DB::table('uploads')->insert( $data );
		echo json_encode( $response );
	}
	public function getCategoryImages( $category )
	{
		echo json_encode( Upload::where( 'category' , $category )->get() );
	}
	public function getSingleImage( $id )
	{
		echo json_encode( Upload::find( $id ) );
	}
	public function getSingleImagebyFilename( $filename )
	{
		echo json_encode( Upload::where( 'filename' , $filename )->get() );
	}
	public function addClientImageAttributes( Request $request )
	{
    	Upload::where( 'image_id' , $request->get( 'img_id' )  )->push( 'client_upload_data' , [ 
    		'emotional_response' => $request->get( 'emotional_response' ),
    		'aesthetic_score'	 => $request->get( 'aesthetic_score' ),
    		'engaging_score'	 => $request->get( 'engaging_score' ),
    		'ctr'	 			 => $request->get( 'ctr' )
    	] );
    	Upload::where( 'image_id' , $request->get( 'img_id' )  )->push( 'img_tags' , $request->get( 'img_tags' ) );

    	echo json_encode($request->get( 'img_tags' ));
	}
	public function updateClientImageAttributes( Request $request )
	{
		
    	Upload::where( 'image_id', $request->get( 'img_id' ) )->update( ['client_upload_data' => [] ]);
    	Upload::where( 'image_id', $request->get( 'img_id' ) )->unset( 'img_tags' );

    	Upload::where( 'image_id' , $request->get( 'img_id' )  )->push( 'client_upload_data' , [ 
    		'emotional_response' => $request->get( 'emotional_response' ),
    		'aesthetic_score'	 => $request->get( 'aesthetic_score' ),
    		'engaging_score'	 => $request->get( 'engaging_score' ),
    		'ctr'	 			 => $request->get( 'ctr' )
    	] );
    	Upload::where( 'image_id' , $request->get( 'img_id' )  )->push( 'img_tags' , $request->get( 'img_tags' ) );
	}
	public function storeImageTagValue( Request $request )
	{
    	$a = User::where( 'company' , 'Telstra' )->first();
    	$b = DB::collection('users')->where( 'company', 'Telstra' )->pull('predict', [ 'predict_id' => $request->get( 'predict_id' ) ] );		    	
   
    	User::where( '_id' , $a->_id  )->push( 'predict' , [ 
            'predict_id'            => $request->get( 'predict_id' ),    
			'image_lib'				=> $request->get( 'image_lib' ),
            'tags'					=> $request->get( 'tagValue' ),
            'date_created'          => $request->get( 'date_created' ),  
            'headingtext'           => $request->get( 'headingtext' ),          
            'targetgender'          => $request->get( 'targetgender' ),
            'mediaplatform'         => $request->get( 'mediaplatform' ),
            'campaigntype'          => $request->get( 'campaigntype' ), 
            'product'               => $request->get( 'product' ), 
            'recommendedlibraries'  => $request->get( 'recommendedlibraries' ), 
            'targetAge'             => $request->get( 'targetAge' ),   
            'targetcustomer'        => $request->get( 'targetcustomer' ),   
            'event'                 => $request->get( 'event' ),   
            'filename'              => $request->get( 'filename' ),
            'status'        		=> 'Completed'

    	] );
		echo json_encode( $b );
	}
	public function removeImageUpload( Request $request )
	{
		// Delete Image in uploads table
		Upload::destroy( $request->get( '_id' ) ); 
		
		// Delete Image filename in users table and update the experiment status
		$predictArr = array();
		$newArr = array();
		$data = User::where( 'company' , 'Telstra' )->get();
		$is_filename = false;
		foreach ($data as $key) {			
			if ($key['predict']) {				
				foreach ($key['predict'] as $predict) {
					$predict['user_id'] = $key['_id'];
					$predictArr[] = $predict;
					if ( isset($predict['filename']) ) {
						DB::collection('users')->where( 'company', 'Telstra' )->pull('predict', [ 'filename' => $predict['filename'] ] );
						User::where( '_id' , $predict['user_id']  )->push( 'predict' , [ 
				            'predict_id'            => $predict['predict_id'],    
				            'date_created'          => $predict['date_created'],  
				            'headingtext'           => $predict['headingtext'],          
				            'targetgender'          => $predict['targetgender'],
				            'mediaplatform'         => $predict['mediaplatform'],
				            'campaigntype'          => $predict['campaigntype'], 
				            'product'               => $predict['product'], 
				            'recommendedlibraries'  => $predict['recommendedlibraries'], 
				            'targetAge'             => $predict['targetAge'],   
				            'targetcustomer'        => $predict['targetcustomer'],   
				            'event'                 => $predict['event'],   
				            'filename'              => $predict['filename'],
				            'image_lib'             => $predict['image_lib'],
				            'status'        		=> 'Completed'
				    	] );					
					}
				}
			}
		}
		echo  json_encode( $predictArr );
	}
	
	public function userImageUploads( Request $request )
	{
		$response = User::where( '_id' , $request->get( '_id' ) )->push( 'client_images' , 
			[ 'category' => $request->get( 'category' ),
			  'filename' => $request->get( 'filename' ),
			  'timeCreated' => $request->get( 'timeCreated' ) ]
		 ); 
		echo json_encode( $response );
	}

	public function deleteUserGroup( Request $request )
	{
		$response = User::where( '_id' , $request->get( '_id' ) )->pull( 'client_images' , 
			[ 'category' => $request->get( 'category' ) ]
		 ); 
		echo json_encode( $response );
	}

	public function deleteUserImage( Request $request )
	{
		$response = User::where( '_id' , $request->get( '_id' ) )->pull( 'client_images' , 
			[ 'filename' => $request->get( 'filename' ) ]
		 ); 
		echo json_encode( $response );
	}
	public function removeImageAPI( $id )
	{
		return Upload::destroy( $id ); 
	}
}
