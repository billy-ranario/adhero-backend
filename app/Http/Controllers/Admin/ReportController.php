<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Excel;

class ReportController extends Controller
{
    
	public function getClarifai()
	{
		$criteria = [
			['$project' => [
					'_id'		=> 0,
					'image' 	=> '$clarifai.image',
					'tags'		=> '$clarifai.data.class',
					'scores'	=> '$clarifai.data.probability'
				]
			],
			// ['$unwind'	=> '$tags'],
		];

		$criteria2 = [
			['$project' => [
					'_id'		=> 0,
					'image' 	=> '$clarifai.image',
					'scores'	=> '$clarifai.data.probability'
				]
			],
			['$unwind'	=> '$scores'],
		];

		$data = DB::collection('uploads')->raw()->aggregate($criteria);
		$data2 = DB::collection('uploads')->raw()->aggregate($criteria2);

		// return $data;

		// $result = $data["result"];
		// $result2 = $data2["result"];

		Excel::create('Clarifai', function ($excel) use ($result,$result2) {

			$excel->sheet('Tags', function ($sheet) use ($result) {

				$sheet->fromArray($result);

			});

			$excel->sheet('Scores', function ($sheet) use ($result2) {

				$sheet->fromArray($result2);

			});

		})->export('xlsx');

	}

}
