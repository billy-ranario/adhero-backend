<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PredictAPI;
use Jleagle\Imagga\Imagga;

use DB;

class PredictAPIController extends Controller
{

    public function getGraymatic(Request $request)
    {

		// $url = 'http://52.77.17.243/upload_get_api/' . $request->get('image');
		$url = $request->get('image');

		$api_key = '8204f65235dacdfe68d2bedd443b1b7e';
		
		$api_url = 'http://api.graymatics.com/grayit/process/image/ondemand';
		
		$post_args = array(	'API_KEY' 	=> 	$api_key,
							'URL' 		=> 	base64_encode($url),
						);

		$curl = curl_init($api_url); 

		curl_setopt($curl, CURLOPT_POST, true); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_args); 

		// this is needed
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec($curl), true);

		curl_close($curl);

		return PredictAPI::create($result);

    }

    public function removeGraymaticAndClarifai($id)
    {

    	// $url = 'http://52.77.17.243/upload_get_api/' . $id;
    	// $url = $id;

    	DB::collection('image_score_graymatic')->where('_id', $id)->delete();

    	DB::collection('uploads')->where('_id', $id)->delete();

    	return 'true';
    		
    }

}
