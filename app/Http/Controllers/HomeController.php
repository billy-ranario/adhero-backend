<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

  public function getLogin()
  {
    return view('login');
  }



    public function home()
    {
    	return view( 'home.main' );
    }
    public function pricing()
    {
      return view( 'home.price' );
    }
    public function testimonials()
    {
      return view( 'home.testimonials' );
    }
    public function sample_ad()
    {
      return view( 'home.sample-ad' );
    }
    public function login_customer()
    {
      return view( 'home.login-customer' );
    }
    public function creative()
    {
      return view( 'home.creative' );
    }
    public function creative_login()
    {
      return view( 'home.login-creative' );
    }
    public function privacy()
    {
      return view( 'home.privacy' );
    }
    public function terms()
    {
      return view( 'home.terms' );
    }
    public function creative_terms()
    {
      return view( 'home.creative-term' );
    }
    public function about()
    {
      return view( 'home.about' );
    }
    public function platform()
    {
      return view( 'home.platform' );
    }
    public function login()
    {
      return view('login');
    }
}
