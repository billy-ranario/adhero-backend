<?php

namespace App\Http\Controllers;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\GraphUser;

use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;

use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Fields\InsightsFields;

use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;

use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Fields\TargetingSpecsFields;

use DateTime;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Values\OptimizationGoals;

use FacebookAds\Object\AdAccountGroup;
use FacebookAds\Object\Fields\AdAccountGroupFields;
use FacebookAds\Object\Values\ArchivableCrudObjectEffectiveStatuses;

use FacebookAds\Object\Values\InsightsPresets;

// Ad Image
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;

// Ad Fields
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\AdGroup;
use FacebookAds\Object\Fields\AdGroupFields;

// Ad Creative
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\ObjectStory\LinkData;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\ObjectStorySpec;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Values\CallToActionTypes;


class MarketingAPIController extends Controller
{
	/**
	* Connects to the Facebook Marketing API
	*
	* @return instance
	*/
	public function apiConnect()
	{
		$app_id = '500764730093160';
		$app_secret = 'eafab7c00444ae38c9c06575ee134c08';
		$access_token = session('fb_user_access_token');
		Api::init( $app_id, $app_secret, $access_token );
		return Api::instance();
	}

	/**
	* Get currently logged in user's facebook info
	*
	* @return account_id
	*/	
	public function getAdUser()
	{
		$this->apiConnect();
		$me = new AdUser('me');
		$user_account = $me->getAdAccounts()->current();
		return $user_account->getData()['id'];
		// $this->vardump( $user_account->getData );
	}

	/**
	* Creates an Ad Account 
	*
	* @return adAccount array 
	*/	
	public function createAdAccount()
	{
		$account_id = $this->getAdUser();
		$fields = array(
			AdAccountFields::ID,
			AdAccountFields::NAME,
		);
		$account = new AdAccount($account_id);
		return $account;
	}

	/**
	* Creating Campaign
	*
	* @return campaign array
	*/	
	public function createCampaign() // Error
	{
		$account_id = $this->getAdUser();
		$campaign = new Campaign( null, $account_id );
		$campaign->setData(array(
			CampaignFields::NAME => 'HEY LALA for March 2, 2016',
			CampaignFields::OBJECTIVE => AdObjectives::LINK_CLICKS,
		));
		$campaign->create(array(
			Campaign::STATUS_PARAM_NAME => Campaign::STATUS_PAUSED,
		));
		return $campaign;	
	}

	/**
	* Fetch User Campaign
	*
	* @return campaign array
	*/	
	public function fetchCampaign()
	{
		/* Fetch Campaigns */
		$account = new AdAccount( $this->getAdUser() );
		$campaigns = $account->getCampaigns(
			array(
				CampaignFields::NAME,
				CampaignFields::OBJECTIVE,
				CampaignFields::ID,
			), array()
		);		
		foreach ($campaigns as $camp) {
			echo $camp->name . "<br>";
		}
	}

	/**
	* Fetch User Ad Accounts
	*
	* @return accounts array
	*/	
	public function fetchAdAccounts()
	{	
		$this->apiConnect();
		$me = new AdUser('me');
		$accounts = $me->getAdAccounts();
		$arr = array();
		/* Fetch Ad Accounts */
		$account = new AdAccount( $accounts );
		foreach ($accounts as $account) {
			$arr[] = $account->id;
		}

		$this->vardump( $arr );
	}

	/**
	* Setting the target audience for the ads
	*
	* @return targeting array
	*/	
	public function targetAudience(){
		$this->apiConnect();
		$result = TargetingSearch::search(
			TargetingSearchTypes::INTEREST,
			null,
			'baseball'
		);
		$target = (count($result)) ? $result->current() : null;

		$targeting = new TargetingSpecs();
		$targeting->{TargetingSpecsFields::GEO_LOCATIONS} =
			array(
			'countries' => array('US')
		);
		$targeting->{TargetingSpecsFields::INTERESTS} = array(
  		 array(
        	'id' => $target->id,
        	'name' => $target->name,
    		),
		);	
		return $targeting;
	}

	/**
	* Creating Adset
	*
	* @return adset array
	*/	
	public function createAdSet()
	{
		$account_id = $this->getAdUser();
		$targeting = $this->targetAudience();
		$campaign = $this->createCampaign( $account_id );

		$start_time = (new \DateTime("+1 week"))->format(DateTime::ISO8601);
		$end_time = (new \DateTime("+2 week"))->format(DateTime::ISO8601);

		$adset = new AdSet( null, $account_id );
		$adset->setData(array(
			AdSetFields::NAME => 'Adset ni Billy',
			AdSetFields::OPTIMIZATION_GOAL => OptimizationGoals::REACH,
			AdSetFields::BILLING_EVENT => BillingEvents::IMPRESSIONS,
			AdSetFields::BID_AMOUNT => 1,
			AdSetFields::DAILY_BUDGET => 20000,
			AdSetFields::CAMPAIGN_ID => $campaign->id,
			AdSetFields::TARGETING => $targeting,
			AdSetFields::START_TIME => $start_time,
			AdSetFields::END_TIME => $end_time,
		));
		$adset->create(array(
			AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED,
		));
		// $this->vardump( $adset );
		return $adset;
	}

	/**
	* Uploading Ad Image
	*
	* @return image array
	*/	
	public function createAdImage() 
	{
		$image_path = 'img/ads1.jpg';
		$image = new AdImage(null, $this->getAdUser());
		$image->{ AdImageFields::FILENAME } = $image_path;
		$image->create();
		return $image->{AdImageFields::HASH}.PHP_EOL;
		// $this->vardump( $image->{AdImageFields::HASH}.PHP_EOL );
	}

	/*
	*  Get Ads Images
	*/
    public function getAdsImage()
    {
    	$account_id = $this->getAdUser();
		$account = new AdAccount( $account_id );
		$images = $account->getAdImages();
		foreach ($images as $key) {			
			$this->vardump($key);
		}
		// $images = $account->getAdImages(
		// 	array(),
		// 	array(
		// 		'hashes' => array(
		// 			'ed95802a52d76991ed478dee16bcea1d',
		// 			'c233a4b06fed417c5adb37ca917decf8',
		// 		),
		// 	));
		// $this->vardump($images);
    }
	/**
	* Creating an Ad Creative
	*
	* @return creative array
	*/	
	public function createAdCreative()
	{
		$link_data = new LinkData();
		$link_data->setData(array(
			LinkDataFields::MESSAGE => 'try it out',
			LinkDataFields::LINK => 'http://ohsomedevs.com',
			LinkDataFields::CAPTION => 'Caption nako ni',
			LinkDataFields::CALL_TO_ACTION => array(
				'type' => CallToActionTypes::SIGN_UP,
				'value' => array(
				'link' => 'http://ohsomedevs.com',
				'link_caption' => 'Sign up!',
				),
			),
		));

		$object_story_spec = new ObjectStorySpec();
		$object_story_spec->setData(array(
			ObjectStorySpecFields::PAGE_ID => 188125257902113,
			ObjectStorySpecFields::LINK_DATA => $link_data,
		));

		$creative = new AdCreative( null, $this->getAdUser() );

		$creative->setData(array(
			AdCreativeFields::NAME => 'Hoy! Sample Creative',
			AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
		));

		$creative->create();
		return $creative;
		// $this->vardump( $creative );
	}

	/**
	* Creating an Ad
	*
	* @return void
	*/	
	function createAd(){
		// $data = array(
		//   	AdFields::NAME => 'THIS IS MY AAAAAD!!!!',
		//   	AdFields::ADSET_ID => $this->createAdSet()->id,
		//   	AdFields::CREATIVE => array(
	 //    			'creative_id' => $this->createAdCreative()->id,
	 //  		),
		// );
		// $ad = new Ad(null, $this->getAdUser() );
		// $ad->setData($data);
		// $ad->create(array(
		// 	Ad::STATUS_PARAM_NAME => Ad::STATUS_PAUSED,
		// ));

		$ad = new Ad( null , $this->getAdUser() );
		$ad->setData(array(
			AdFields::NAME => 'THIS IS MY AAAAAD!!!!',
			AdFields::ADSET_ID => $this->createAdSet()->id,
			AdFields::CREATIVE => array(
	    		'creative_id' => $this->createAdCreative()->id,
	  		),
		));
		$ad->create(array(
			Ad::STATUS_PARAM_NAME => Ad::STATUS_PAUSED
		));

		$this->vardump( $ad );
	}
	// Custom Functions
	function vardump( $arr ) 
	{
		echo "<pre>";
		var_dump($arr);
		echo "</pre>";
	}
	/*
	*  Get Graph Posts
	*/
    public function getGraph()
    {
		$fb = new Facebook([
			'app_id' 				=> '500764730093160',
			'app_secret' 			=> 'eafab7c00444ae38c9c06575ee134c08',
			'default_graph_version' => 'v2.5',
			'enable_beta_mode'		=> false
		]);

		$fb->setDefaultAccessToken( session('fb_user_access_token') );
		$response = $fb->get('/9322764314/?fields=posts');
		$arr = array();
		$data = $response->getGraphObject();

		// foreach ($data as $value) {
		// 	echo $value;
		// }

		$this->vardump( $data );
    }

    public function getSession()
    {
    	$this->vardump( session::all() );
    }
	/*
	*  Create Ads Images
	*/

	public function testGraphAPI()
	{
		$fb = new Facebook ( [
			'app_id' => '543676355786357',
			'app_secret' => '311b7fd7836d37464e5af99a566d6351',
			'default_graph_version' => 'v2.5',
		] );
		
		$fb->setDefaultAccessToken('app-access-token');
		$response = $fb->get('/9322764314/?fields=posts');
		$data = $response->getGraphObject();
		print_r($data);
	}

	// $account_id = getAdUser();
	// $campaign = createCampaign($account_id);

	// $targeting = targetAudience();
	// createAdSet($account_id, $targeting, $campaign);

	// $hash = createAdImage('FILE_PATH');
	// $creative_id = createAdCreative($hash)->id;

	// $ad_set_id = createAdSet()->id;
	// createAd($creative_id, $ad_set_id, $account_id);
}
