<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Response;

class PredictController extends Controller
{
    public function createPredict( Request $request )
    {
        $predict_id = time().rand( 10,99 );
        $request->merge( array( 'predict_id' => $predict_id ) );
        
    	User::where( '_id' , Auth::user()->id )->push( 'predict' , [ 
			'predict_id'	        => $predict_id,
            'date_created'          => date('m-d-Y h:i A'),             
            'product_name'          => $request->get( 'product_name' ),         
			'campaign_name'	        => $request->get( 'campaign_name' ),			
            'product_category'      => $request->get( 'product_category' ), 
            'gender'                => $request->get( 'gender' ),
            'mediaplatform'         => $request->get( 'mediaplatform' ),
            'objective'             => $request->get( 'objective' ), 
            'hashtags'              => $request->get( 'hashtags' ), 
            'country'               => $request->get( 'country' ),   
            'status'                => 'In Progress'
    	] );
        echo json_encode( $request->all() );
    }
    public function adminDashbordPredict()
    {
    	echo json_encode( User::all() );
    }
    public function getClientExperiments( $id )
    {
        // LIST DOWN ALL EXPERIMENTS FOR ALL USERS - *done
        // PUT IT ON THE SAME ARRAY - *done

        $data = User::where( 'company' , $id )->get();
        $dataArr = array();
        foreach ($data as $key) {
            if ( $key['predict'] ) {
                foreach ( $key['predict'] as $predictkey) {
                    $predictkey['client_id'] = $key['_id'];
                    $dataArr[] = $predictkey;
                }
            }
        }
        echo json_encode( $dataArr );
    }
    public function addImageToExperiment( Request $request )
    {
        $result = DB::collection('users')->where( '_id', $request->get( 'client_id' ) )->pull('predict', [ 'predict_id' => $request->get( 'predict_id' ) ] );
        User::where( '_id' , $request->get( 'client_id' ) )->push( 'predict' , [ 
            'predict_id'            => $request->get( 'predict_id' ),    
            'date_created'          => $request->get( 'date_created' ),  
            'product_name'          => $request->get( 'product_name' ),         
            'product_category'      => $request->get( 'product_category' ), 
            'gender'                => $request->get( 'gender' ),
            'mediaplatform'         => $request->get( 'mediaplatform' ),
            'objective'             => $request->get( 'objective' ), 
            'hashtags'              => $request->get( 'hashtags' ), 
            'country'               => $request->get( 'country' ),
            'filename'              => $request->get( 'filename' ),
            'image_lib'             => $request->get( 'image_lib' ),
            'status'                => 'Completed'
        ] ); 
        echo json_encode( $request->all() );
    }
    public function removeExperiment( Request $request )
    {
        $response = DB::collection('users')->where( '_id', $request->get( 'client_id' ) )->pull('predict', [ 'predict_id' => $request->get( 'predict_id' ) ] );
        echo json_encode( $response );
    }
    public function downloadBrief($num)
    {
        if( $num == 0 ){
            $download_path = ( public_path() . '/file_downloads/brief_report.pdf' );   
            return( Response::download( $download_path ) ); 
        }else if( $num == 1 ){
            $download_path = ( public_path() . '/file_downloads/brief_report2.pdf' );
            return( Response::download( $download_path ) );
        }else if( $num == 2 ){
            $download_path = ( public_path() . '/file_downloads/brief_report3.pdf' );
            return( Response::download( $download_path ) );
        }
        // return response()->download( asset( '/file_downloads/brief_report.pptx' ) );
    }
    public function updateValue()
    {
        $arr_data = array(
            'client_id'     => '56c30501317c3d76bf8b4569',
            'predict_id'    => '145871526987',
            'date_created'  => '03-20-2016 02:39 PM' //data you wanted to insert
        );

        DB::collection('users')->where( '_id', $arr_data['client_id'] )->pull('predict', [ 'predict_id' => $arr_data['predict_id'] ] );
        User::where( '_id' , $arr_data['client_id'] )->push( 'predict' , [ 
            'predict_id'            => $arr_data['predict_id'],    
            'date_created'          => $arr_data['date_created'],  
            'product_name'          => 'Mobile Plan',         
            'product_category'      => null, 
            'gender'                => 'Both',
            'mediaplatform'         => 'facebook',
            'objective'             => null, 
            'hashtags'              => '#Sports Car Photoshot, #Sports, #Entertainment', 
            'country'               => 'Australia',
            'status'                => 'In Progress'
        ] ); 


        // Second Batch
        $arr_data2 = array(
            'client_id'     => '56c30501317c3d76bf8b4569',
            'predict_id'    => '145871626469',
            'date_created'  => '12-03-2016 04:12 PM' //data you wanted to insert
        );

        DB::collection('users')->where( '_id', $arr_data2['client_id'] )->pull('predict', [ 'predict_id' => $arr_data2['predict_id'] ] );
        User::where( '_id' , $arr_data2['client_id'] )->push( 'predict' , [ 
            'predict_id'            => $arr_data2['predict_id'],    
            'date_created'          => $arr_data2['date_created'],  
            'product_name'          => 'Yogurt drinks',         
            'product_category'      => null, 
            'gender'                => 'Female',
            'mediaplatform'         => 'facebook',
            'objective'             => null, 
            'hashtags'              => '#Health, #Foods, #Summer', 
            'country'               => 'Australia',
            'status'                => 'In Progress'
        ] ); 

    }
}
