<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ReportController extends Controller
{

    public function index(Request $request)
    {
    	$criteria = [
    		['$unwind' => '$tags'],
    		['$project' => [
    				'field'	=> '$tags.details.field',
    				'value'	=> '$tags.details.value',
    				'type'	=> '$tags.type',
    				'unique_ctr' => 1,
    				'ad_id'	=> 1,
    				'ad_number'	=> 1,
    				'target_country' => 1,
    				'target_gender'	 => 1,
    				'campaign_name'	 => 1,
    				'start'			 => 1,
    				'end'			 => 1,
    				'impression'	 => 1,
    				'reach'			 => 1,
    				'cpc'			 => 1,
    				'product_filter' => 1
    			]
    		]
    	];

    	if ($request->get('filter') != 'all') {
    		array_push($criteria, ['$match'  => [
   		    		'field' 			=> $request->get('type'),
   		    		'product_filter' 	=> $request->get('filter')
    		    ]
    		]);
    	} else {
    		array_push($criteria, ['$match'  => [
   		    		'field' 	=> $request->get('type'),
    		    ]
    		]);
    	}

		return \DB::collection('dummy_reports')->raw()->aggregate($criteria);

    }

    public function tags(Request $request)
    {
    	$criteria = [
    		['$project' => [
    				'value'	=> '$tags.details.value',
    				'type'	=> '$tags.type',
    				'ad_id'	=> 1
    			]
    		],
    		['$unwind' => '$type'],
    		['$match'  => [
    		    			'type' 	=> $request->get('type'),
    		    			'ad_id'	=> $request->get('ad_id')
    		    ]
    		],
    	];

		return \DB::collection('dummy_reports')->raw()->aggregate($criteria);    	
    }


    public function generateExcel()
    {
        $criteria = [
            ['$project' => [
                    '_id'       => 0,
                    'image'     => '$clarifai.image',
                    'tags'      => '$clarifai.data.class',
                    'scores'    => '$clarifai.data.probability'
                ]
            ]
        ];

        return \DB::collection('uploads')->raw()->aggregate($criteria);
    }

}
