<?php


Route::get( 'token' , function() {
	return csrf_token();
} );




/**
 * From Jhon
 * import from csv to mongo
 */
Route::get('import/csv', function () {

	$csv = public_path() . '/csv/report.csv';

	$arr = array_map('str_getcsv', file($csv));

	for ($i=0; $i < count($arr); $i++) {

		$d = [
			'unique_ctr'		=> $arr[$i][0],
			'ad_id'				=> $arr[$i][1],
			'ad_number'			=> $arr[$i][2],
			'target_country'	=> $arr[$i][3],
			'target_gender' 	=> $arr[$i][4],
			'campaign_name'		=> $arr[$i][19],
			'start'				=> $arr[$i][20],
			'end'				=> $arr[$i][21],
			'impression'		=> $arr[$i][22],
			'reach'				=> $arr[$i][23],
			'cpc'				=> $arr[$i][24]
		];

		// insert initial data
		\DB::collection('dummy_reports')->insert($d);

		$type 	= explode(",", $arr[$i][5]);
		$field 	= explode(",", $arr[$i][6]);

		foreach ($type as $key => $value_type) {

			$d['tags'][] = [
				'type' => $value_type,
			];

			for ($j=0; $j < count($field); $j++) { 

				if ($value_type=="emotions" && $field[$j]=="emotions") {
					$d['tags'][$key]['details'][] = [
						'field' => $field[$j],
						'value'	=> $arr[$i][7]
					];
				} elseif ($value_type=="theme") {
					if ($field[$j]=="scenario") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][8]
						];
					} elseif ($field[$j]=="indoor/outdoor") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][9]
						];
					} elseif ($field[$j]=="layout") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][10]
						];
					} elseif ($field[$j]=="weather") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][11]
						];
					}
				} elseif ($value_type=="people") {
					if ($field[$j]=="gender") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][12]
						];
					} elseif ($field[$j]=="activities") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][13]
						];
					} elseif ($field[$j]=="age") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][14]
						];
					} elseif ($field[$j]=="clothing") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][15]
						];
					} elseif ($field[$j]=="hair colour") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][16]
						];
					}
				} elseif ($value_type=="focus") {
					if ($field[$j]=="salient object") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][17]
						];
					}
				} elseif ($value_type=="dominant colours") {
					if ($field[$j]=="dominant colour") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][18]
						];
					}
				}

			}

		}

		\DB::collection('dummy_reports')->raw()->update(["ad_id"=>$arr[$i][1]],$d);

	}

});

Route::get('import/csv/filter', function () {

	$csv = public_path() . '/csv/filter.csv';

	$arr = array_map('str_getcsv', file($csv));

	for ($i=0; $i < count($arr); $i++) { 

		$query = [

			'ad_id' 	=> $arr[$i][0],
			'ad_number'	=> $arr[$i][1]
		];

		$data = [
			'$push' => [
				'product_filter' => $arr[$i][2]
			]
		];

		\DB::collection('dummy_reports')->raw()->update($query,$data);
	}

});

Route::get( 'test-lang' , function ()
{
	echo public_path().'/ddd';
} );

Route::get( '/view/clarifai/test' , 'Admin\DashboardController@getClarifaiData' );

// Route::get( '/pdf/download' , 'Admin\DashboardController@exportPDF' );

Route::get( '/export/csv/clarify' , 'Admin\DashboardController@exportCSV' );
// {
//     $html = view( 'tpl.tpl-pdf-exports' )->render();
//     // return PDF::load($html)->download();
// });

/**
 * Authentication
 *
 */
Route::group(['middleware'=>'guest'], function() {
	Route::get('login', 	'AuthController@getLogin');
	Route::post('login',	'AuthController@postLogin');
});

Route::get('logout',	'AuthController@getLogout');

Route::get('register',	'AuthController@getRegister');
Route::post('register',	'AuthController@postRegister');

Route::group(['middleware'=>'auth'], function() {

	// Client
	Route::get('dashboard',	'Client\DashboardController@index');

	Route::group(['prefix'=>'api'], function() {
		Route::resource('user/client', 'Client\UserController',['only'=>['index']]);
	});

	// Admin
	// Route::get('admin',	'Admin\DashboardController@index');

});

// Note: jhon
// isulod dayon ni sa auth nga middleware after, for testing purpose
Route::group(['prefix'=>'api'], function() {

	Route::get('graymatic', 					'PredictAPIController@getGraymatic');					// Graymatic API
	Route::delete('graymatic-clarifai/{image}',	'PredictAPIController@removeGraymaticAndClarifai');

	Route::get('uploads',		'Admin\UploadController@index');
	Route::resource('imagga', 	'ImaggaController', ['only'=>['index','store','destroy']]);		// Imagga API

	// Dummy Report
	Route::get('report',		'ReportController@index');
	// Route::get('report/excel', 	'ReportController@generateExcel');
	// this should be consolidated to the upper part, use aggregation for this
	Route::get('report-tags', 'ReportController@tags');

	Route::group(['prefix'=>'admin'], function() {
		Route::get('report/clarifai', 'Admin\ReportController@getClarifai');
	});

});

Route::post('send-email-contact', 'PageController@sendEmailContact');


/**
 * Test IG API
 *
 * test IG api like websta
 * pull out themes and trends
 */
Route::get('ig/auth', 'InstagramAPIController@auth');




Route::get( 'getuserdata' , 'AuthController@getUserData' );




/* Page Routings */ 
Route::get( '/' , 'HomeController@home' );

Route::get( '/home' , 'HomeController@home' );


Route::get( '/pricing' , 'HomeController@pricing' );

Route::get( '/testimonials' , 'HomeController@testimonials' );

Route::get( '/sample-ad' , 'HomeController@sample_ad' );

Route::get( '/login-customer' , 'HomeController@login_customer' );

Route::get( '/creative' , 'HomeController@creative' );

Route::get( '/login-creative' , 'HomeController@creative_login' );

Route::get( '/privacy' , 'HomeController@privacy' );

Route::get( '/terms' , 'HomeController@terms' );

Route::get( '/creative-terms' , 'HomeController@creative_terms' );

Route::get( '/about' , 'HomeController@about' );

Route::get( '/platform' , 'HomeController@platform' );

// Route::get( '/dashboard' , ['middleware'=>'auth','uses'=>'DashboardController@index'] );

// Route::get( '/admin' , ['middleware' => 'auth', 'uses' => 'DashboardController@admin'] );


Route::group(['middleware' => 'guest'], function() {
	Route::get('login-admin', 'DashboardController@loginAdmin');
	Route::post('auth-admin', 'DashboardController@authAdmin');
});

Route::get('logout-admin', 'DashboardController@logoutAdmin');


Route::get( '/admin' , ['middleware' => 'auth', 'uses' => 'DashboardController@admin']);



/* Predic Routes */ 
Route::post( 'createPrediction' , 'PredictController@createPredict' );
Route::get( 'getClientLists' , 'PredictController@adminDashbordPredict' );
Route::get( 'getClientExperiments/{id}' , 'PredictController@getClientExperiments' );
Route::get( 'getSingleExperiment/{id}' , 'PredictController@getSingleExperiments' );

// UPLOAD FILE IMAGES
Route::post( 'upload-experiments' , 'Admin\UploadController@storeDataImage' );
Route::get( 'getCategoryImages/{category}' , 'Admin\UploadController@getCategoryImages' );
Route::get( 'getSingleImage/{filename}' , 'Admin\UploadController@getSingleImagebyFilename' );
Route::post( 'downloadImage' , 'Admin\UploadController@downloadImage' );

// Client Image Experiment
Route::post( 'addimageexperimentattribute' , 'Admin\UploadController@addClientImageAttributes' );
Route::post( 'updateimageexperimentattribute' , 'Admin\UploadController@updateClientImageAttributes' );
Route::post( 'addclientexperimentimage' , 'PredictController@addImageToExperiment' );
Route::post( 'storeImageTagValue' , 'Admin\UploadController@storeImageTagValue' );
Route::post( 'deleteExperiment' , 'PredictController@removeExperiment' );
Route::post( 'deleteImageUpload' , 'Admin\UploadController@removeImageUpload' );
Route::get( 'deleteAPIImage/{id}' , 'Admin\UploadController@removeImageAPI' );
Route::post( 'userImageUploads' ,  'Admin\UploadController@userImageUploads' );
Route::post( 'deleteUserImage' ,  'Admin\UploadController@deleteUserImage' );
Route::post( 'deleteUserGroup' ,  'Admin\UploadController@deleteUserGroup' );

/* Facebook Login */
Route::get('/auth/fbloginurl/{usertype}', 'AuthenticationController@FBlogin' ); // Generate a login URL

Route::get('/facebook/callback', 'AuthenticationController@FBcallback' );
// End Facebook Login


/* For AngularJS RESTFul API */
/*Route::post( 'accountStatus' , 'UserController@accountStatus' );

Route::post( 'createUserInfo' , 'UserController@createUserInfo' );*/

Route::post( 'createBriefCampaign' , 'CampaignController@createBriefCampaign' );

Route::get( 'campaigns/deleteCampaign/{id}', 'CampaignController@deleteCampaign' );

Route::get( 'campaigns/count/{uniqid}' , 'CampaignController@countCampaigns' );

Route::get( 'campaigns/bystatus' , 'CampaignController@fetchCampaignBStatus' ); // Fetch Campaign separted by status

Route::resource( 'campaigns', 'CampaignController');

Route::post( 'campaignsUpdate', 'CampaignController@updateCampaign' );

// Route::get( 'sessionUserInfo', 'UserController@getFBUserInfo' ); // Fetch Facebook UID

// Route::get( 'logout' , 'UserController@sessionDestroy' );
// Route::get( 'access_token/{token_id}', 'MarketingAPIController@test' );

Route::post( 'upload', 'DashboardController@upload' );

Route::get( 'downloadBrief/{num}', 'PredictController@downloadBrief' );

// Email API
Route::get( 'sendLaunchCampaignEmail' , 'CampaignController@sendLaunchCampaignEmail' );

// Tests
Route::get( 'api-test' , function () {
	return view( 'welcome' );
} );

Route::get( 'showuser' , 'MarketingAPIController@getAdUser' );

Route::get( 'checkifloggedin' , 'AuthenticationController@login_test' );

Route::get( 'createaduser' , 'MarketingAPIController@getAdsImage' );

Route::get( 'session' , 'MarketingAPIController@getSession' );

Route::get( 'test-rani' , 'MarketingAPIController@testGraphAPI' );

Route::get( 'updateValue' , 'PredictController@updateValue' );

Route::get( 'test' , function() {
	return DB::table('users')->get();
} );


/* For AngularJS Path */ 
Route::any('{path?}', function()
{	
    return view("dashboard.client");
})->where("path", ".+");
