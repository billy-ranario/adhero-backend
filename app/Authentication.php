<?php

namespace App;

use DB;
use Hash;
use Illuminate\Database\Eloquent\Model;

class Authentication extends Model
{	
    public function checkFbUID( $uid )
    {	
    	return DB::table('users')->where( 'uid' , '=' , $uid )->count();
    }
    public function createFBuserData( $userdata ) 
    {	
		try {
			DB::table('users')->insert(
			    [ 
			    	'uid' 				=> $userdata['id'] , 			//Facebook user ID
			    	'email' 			=> $userdata['email'] ,
			    	'password'			=> Hash::make('adheroclient'), 	//default password: adheroclient
			    	'usertype'			=> $userdata['usertype'],
			    	'account_status' 	=> false, 						//default value
			    	'uniqid'			=> $this->generateID() 
			    ]
			);
		}catch(\Exception $e){
		}

		return 'uid and email inserted. redirect to admin page';			
    }
    public static function generateID( $length = 16 )
	{
	    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
	}
}
