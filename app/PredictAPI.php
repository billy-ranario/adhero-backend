<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class PredictAPI extends Eloquent
{
    protected $collection = 'image_score_graymatic';

    protected $guarded = ['_id'];
}
