<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function ($collection) {
            $collection->increments('id');
            // $collection->string('uid')->unique()->nullable();
            $collection->string('name');
            // $collection->string('email')->unique()->nullable();
            $collection->unique('email');
            // $collection->string('uniqid');
            // $collection->string('usertype');

            $collection->string('password', 60);
            $collection->boolean('account_status');
            $collection->timestamps();

            // $collection->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            // $collection->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
