<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreativeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creative_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid')->unique();
            $table->string('uniqid');
            $table->string('fullname');
            $table->string('address');
            $table->string('region');
            $table->timestamp('date_joined')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('creative_info');
    }
}
